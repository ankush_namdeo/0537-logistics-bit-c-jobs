package com.postnord.isp.utils.remote.lookup;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

public class RemoteLookupServiceBeanTest {

    @Test
    public void shouldHandleOneHost() throws Exception {
        RemoteLookupServiceBean bean = new RemoteLookupServiceBean();
        for (int i = 0; i < 100; i++) {
            String result = bean.sortJndiBindAdrAndPorts("jnp://as2bs.idp.posten.se:1099");
            assertEquals("jnp://as2bs.idp.posten.se:1099", result);
        }
    }

    @Test
    public void shouldSortLocalhostFirst() throws Exception {
        RemoteLookupServiceBean bean = new RemoteLookupServiceBean();
        for (int i = 0; i < 100; i++) {
            String result = bean.sortJndiBindAdrAndPorts("jnp://as2bs.idp.posten.se:1099,localhost:1099");
            assertEquals("jnp://localhost:1099,as2bs.idp.posten.se:1099", result);
        }
    }

    @Test
    public void shouldSortWithoutLocalhost() throws Exception {
        RemoteLookupServiceBean bean = new RemoteLookupServiceBean();
        // random order
        Set<String> result = new HashSet<String>();
        for (int i = 0; i < 100000; i++) {
            result.add(bean.sortJndiBindAdrAndPorts("jnp://as2as.idp.posten.se:1099,as2bs.idp.posten.se:1099"));
        }
        assertEquals(2, result.size());
        assertTrue(result.contains("jnp://as2as.idp.posten.se:1099,as2bs.idp.posten.se:1099"));
        assertTrue(result.contains("jnp://as2bs.idp.posten.se:1099,as2as.idp.posten.se:1099"));
    }

    @Test
    public void shouldSortManyWithoutLocalhost() throws Exception {
        RemoteLookupServiceBean bean = new RemoteLookupServiceBean();
        // random order
        Set<String> result = new HashSet<String>();
        for (int i = 0; i < 100000; i++) {
            result.add(bean.sortJndiBindAdrAndPorts("jnp://a:1099,b:1099,localhost:1099,c:1099"));
        }
        assertEquals(6, result.size());

        assertTrue(result.contains("jnp://localhost:1099,a:1099,b:1099,c:1099"));
        assertTrue(result.contains("jnp://localhost:1099,a:1099,c:1099,b:1099"));
        assertTrue(result.contains("jnp://localhost:1099,b:1099,a:1099,c:1099"));
        assertTrue(result.contains("jnp://localhost:1099,b:1099,c:1099,a:1099"));
        assertTrue(result.contains("jnp://localhost:1099,c:1099,a:1099,b:1099"));
        assertTrue(result.contains("jnp://localhost:1099,c:1099,b:1099,a:1099"));
    }
    
    @Test
    public void shouldParfileOK() throws Exception {

        RemoteLookupServiceBean bean = new RemoteLookupServiceBean();
        bean.initialize();
        
        Map<String, JndiBindParameters> bind = bean.getBindRegistry();
        
        assertEquals(3, bind.size());
    }

}
