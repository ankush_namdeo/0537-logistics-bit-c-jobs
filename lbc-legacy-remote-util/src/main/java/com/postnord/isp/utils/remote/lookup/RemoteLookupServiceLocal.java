package com.postnord.isp.utils.remote.lookup;

import javax.ejb.Local;

@Local
public interface RemoteLookupServiceLocal extends RemoteLookupService {

}
