package com.postnord.isp.utils.remote.lookup;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.DefaultConfigurationBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * <p>
 * Utility for lookup of remote service. Use this lookup instead of ordinary @EJB
 * inject of remote. It facilitates late binding and possibility to configure
 * hosts in external, reloadable, properties file.
 * <p>
 * Looks for a property file located in the class path with location
 * "/appdata/lispServiceBindRegistry.properties".
 * <p>
 * Each binding in the properties file has the following format:
 * 
 * <pre>
 * cem.shipmentService=shipmentService/remote jnp://host1:1099,host2:1099 partition
 * </pre>
 * <p>
 * Properties are reloaded each 5 minutes. More often in case of failures.
 * <p>
 * It will sort the host name with current "localhost" first, i.e. prefer local
 * calls over remote. "localhost" is determined from System property
 * "jboss.bind.address".
 * <p>
 * It is important that you use lookup for each remote call. You should not
 * cache the proxy instance, since it might be stale and the server of the proxy
 * might have crashed.
 * <p>
 * Since version 1.0.12 of lisp-common, the algorithm for lookup has been
 * slightly modified with a retry scheme to better handle restart of remote
 * servers. Further investigation should be done to make this even more robust.
 * Areas to look into are socket timeouts, circuit-breaker and jboss clustering
 * support. /Stefan Carlsson
 * 
 * @author nigr707
 * 
 */
@Stateless(name = "remoteLookupService")
@Local(RemoteLookupServiceLocal.class)
@Remote(RemoteLookupServiceRemote.class)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class RemoteLookupServiceBean implements RemoteLookupServiceLocal,
		RemoteLookupServiceRemote {

	private static final String SERVICE_BIND_REGISTRY_PROPERTIES_FILE = "/appdata/lispServiceBindRegistry.properties";
	private static final String JNP = "jnp://";
	private static final Log log = LogFactory
			.getLog(RemoteLookupServiceBean.class);
	private long configurationExpireTime;
	private static final int CONFIGURATION_TTL = 5 * 60 * 1000;
	private static final int CONFIGURATION_FAILURE_TTL = 10 * 1000;
	private Random random = new Random();
	private String prefix = null;

	private Configuration config;

	private Map<String, JndiBindParameters> bindRegistry = new HashMap<String, JndiBindParameters>();

	/**
	 * Lazy initialization of configuration properties and jndi.
	 */
	protected void initialize() {
		long now = System.currentTimeMillis();
        if (now > configurationExpireTime) {
            config = null;
            configurationExpireTime = now + CONFIGURATION_TTL;
            log.info("Reload LispServiceBindRegistry file");
        }
		initConfiguration();
		setupBindRegistry();
	}

	protected void initConfiguration() {
		if (config == null) {
			try {
				DefaultConfigurationBuilder builder = new DefaultConfigurationBuilder();
				builder.setFile(new File(System.getProperty("jboss.server.base.dir") + "/configuration/appdata/"+prefix+"-remote_config.xml"));
				builder.clearErrorListeners(); // Ignore any configuration
				builder.setListDelimiter(' ');
				DefaultConfigurationBuilder.setDefaultListDelimiter(' ');
				config = builder.getConfiguration(true);
			} catch (ConfigurationException e) {
				throw new IllegalStateException(
						"Failed to initialize configuration settings. EJB lookup will not work properly.",
						e);
			}
		}
		if (config == null) {
			String errMessage = "Missing resouce: "
					+ SERVICE_BIND_REGISTRY_PROPERTIES_FILE;
			log.error(errMessage);
			throw new RuntimeException(errMessage);
		}
	}

	@SuppressWarnings("unchecked")
	protected void setupBindRegistry() {
		Map<String, JndiBindParameters> newBindRegistry = new HashMap<String, JndiBindParameters>();
		Iterator<String> keyIter = config.getKeys();
		while (keyIter.hasNext()) {
			final String key = keyIter.next().trim();
			final String[] values = config.getStringArray(key);
			if (key.length() > 0 && values.length > 2
					&& values[0].trim().length() > 0
					&& values[1].trim().length() > 0
					&& values[2].trim().length() > 0) {

				JndiBindParameters params = new JndiBindParameters(key,
						values[0].trim(), values[1].trim(), values[2].trim());
				newBindRegistry.put(params.logicalName.toLowerCase(), params);
				if(log.isDebugEnabled()) {
					log.debug("Registered LispServiceBindRegistry logical service: "
							+ key + " BindAdrAndPorts: "
							+ params.possibleJndiBindAdrAndPorts + " JndiName: "
							+ params.mappedJndiName);
				}
			} else {
				if(log.isDebugEnabled()) {
					log.debug("LispServiceBindRegistry configuration key \"" + key
						+ "\" was ignored due to illegal configuration.");
				}
			}
		}

		bindRegistry = newBindRegistry;
	}

	protected Map<String, JndiBindParameters> getBindRegistry() {
		return bindRegistry;
	}

	@SuppressWarnings("unchecked")
	public <T> T lookup(String logicalServiceName, String prefix) throws NamingException {
		this.prefix = prefix;
		initialize();

		JndiBindParameters bindParams = bindRegistry.get(logicalServiceName
				.toLowerCase());
		if (bindParams == null) {
			throw new IllegalStateException(
					"No jndi mapping configuration found for logicalServiceName \""
							+ logicalServiceName + "\"");
		}

		T result = null;
		NamingException firstException = null;
		String jndiBindAdrAndPorts = sortJndiBindAdrAndPorts(bindParams.possibleJndiBindAdrAndPorts);

		// Loop until break or exception.
		while (true) {
			Context ctx = createInitialContext(bindParams, jndiBindAdrAndPorts);
			try {
				result = (T) ctx.lookup(bindParams.mappedJndiName);
				break;
			} catch (NamingException e) {
				if (log.isDebugEnabled()) {
					log.debug("Got NamingException for jndBindAdrAndPorts "
							+ jndiBindAdrAndPorts + ". Message: "
							+ e.getMessage());
				}
				setConfigurationExpireTimeAfterFailure();

				if (firstException == null) {
					firstException = e;
				}

				// During restart of a remote server we may get an exception
				// when doing a lookup.
				// Typically, we get a CommunicationException or a
				// NameNotFoundException (if JNDI service is up, but not
				// the service we ask for.).
				// The NamingContext does not automatically try the other hosts
				// in our list of hosts for all these
				// cases, so we have to do this by ourselves.

				int index = jndiBindAdrAndPorts.indexOf(',');
				if (index >= 0) {
					// Try again, but skip first host.
					jndiBindAdrAndPorts = JNP
							+ jndiBindAdrAndPorts.substring(index + 1);
					continue;
				} else {
					// There is nothing more we can do.
					// Re-throw the first exception that was thrown when we
					// tried the full list of hosts.
					throw firstException;
				}
			}
		}
		return result;
	}

	/**
	 * Do the remote lookup
	 * 
	 * @param context
	 *            The initialcontext object
	 * @param appName
	 *            The app name is the application name of the deployed EJBs.
	 *            This is typically the ear name without the .ear suffix.
	 *            However, the application name could be overridden in the
	 *            application.xml of the EJB deployment on the server. If not
	 *            deployed as a .ear, the app name for will be an empty string.
	 * @param moduleName
	 *            The modulename name
	 * @param distinctName
	 *            AS7 allows each deployment to have an (optional) distinct
	 *            name. We
	 * @param beanClass
	 *            The EJB name which by default is the simple class name of the
	 *            bean implementation class
	 * @param interfaceClass
	 *            The remote view fully qualified class name
	 * 
	 * @return
	 * @throws NamingException
	 */
	@SuppressWarnings("unchecked")
	private <T> T doLookup(Context context, String appName, String moduleName,
			String distinctName, Class<?> beanClass, Class<?> interfaceClass)
			throws NamingException {
		// The EJB name which by default is the simple class name of the bean
		// implementation class
		final String beanName = beanClass.getSimpleName();

		// the remote view fully qualified class name
		final String interfaceClassName = interfaceClass.getName();

		// let's do the lookup
		return (T) context.lookup("ejb:" + appName + "/" + moduleName + "/"
				+ distinctName + "/" + beanName + "!" + interfaceClassName);
	}

	// csp.service=10.127.83.19:25099,10.127.83.20:25099

	private void setConfigurationExpireTimeAfterFailure() {
		// reload configuration more often in case of failure
		long now = System.currentTimeMillis();
		if ((configurationExpireTime - now) > CONFIGURATION_FAILURE_TTL) {
			configurationExpireTime = now + CONFIGURATION_FAILURE_TTL;
		}
	}

	protected Context createInitialContext(JndiBindParameters bindParams,
			String jndiBindAdrAndPort) throws NamingException {
		Properties p = new Properties();
		p.put(Context.INITIAL_CONTEXT_FACTORY,
				"org.jnp.interfaces.NamingContextFactory");
		p.put(Context.URL_PKG_PREFIXES, "jboss.naming:org.jnp.interfaces");
		p.put(Context.PROVIDER_URL, jndiBindAdrAndPort);
		// JBoss specific cluster partition parameter
		p.put("jnp.partitionName", bindParams.partitionName);
		return new InitialContext(p);

		// final Hashtable<String, String> jndiProperties = new
		// Hashtable<String, String>();
		// jndiProperties.put(Context.URL_PKG_PREFIXES,
		// "org.jboss.ejb.client.naming");
		// jndiProperties.put(Context.PROVIDER_URL, jndiBindAdrAndPort);
		// return new InitialContext(jndiProperties);

	}

	String sortJndiBindAdrAndPorts(String possibleJndiBindAdrAndPorts) {
		String str;
		if (possibleJndiBindAdrAndPorts.startsWith(JNP)) {
			str = possibleJndiBindAdrAndPorts.substring(JNP.length());
		} else {
			// strange url
			return possibleJndiBindAdrAndPorts;
		}
		String[] split = str.split(",");
		if (split.length <= 1) {
			return possibleJndiBindAdrAndPorts;
		}
		String bindAddress = System.getProperty("jboss.bind.address",
				"localhost");
		String primary = null;
		List<String> rest = new ArrayList<String>();
		for (String each : split) {
			if (primary == null && each.startsWith(bindAddress + ":")) {
				primary = each;
			} else {
				rest.add(each);
			}
		}
		StringBuilder result = new StringBuilder();
		result.append(JNP);
		if (primary != null) {
			result.append(primary);
			if (!rest.isEmpty()) {
				result.append(",");
			}
		}
		if (rest.size() > 1) {
			Collections.shuffle(rest, random);
		}
		if (rest.size() == 1) {
			result.append(rest.get(0));
		} else {
			for (Iterator<String> iter = rest.iterator(); iter.hasNext();) {
				String each = iter.next();
				result.append(each);
				if (iter.hasNext()) {
					result.append(",");
				}
			}
		}

		return result.toString();
	}
}
