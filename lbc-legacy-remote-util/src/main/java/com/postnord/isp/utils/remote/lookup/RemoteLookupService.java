package com.postnord.isp.utils.remote.lookup;

import javax.naming.NamingException;

public interface RemoteLookupService {

    <T> T lookup(String logicalMappedName, String prefix) throws NamingException;
}
