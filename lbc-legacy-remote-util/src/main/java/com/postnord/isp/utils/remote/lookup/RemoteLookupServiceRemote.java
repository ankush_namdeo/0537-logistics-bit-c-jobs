package com.postnord.isp.utils.remote.lookup;

import javax.ejb.Remote;

@Remote
public interface RemoteLookupServiceRemote extends RemoteLookupService {

}
