package com.postnord.isp.utils.remote.lookup;

public class JndiBindParameters {
	public final String logicalName;
	public final String mappedJndiName;
	public final String possibleJndiBindAdrAndPorts;
	public final String partitionName;
	
	public JndiBindParameters(String logicalName, String mappedJndiName, String possibleJndiBindAdrAndPorts, String partitionName) {
		this.logicalName = logicalName;
		this.mappedJndiName = mappedJndiName;
		this.possibleJndiBindAdrAndPorts = possibleJndiBindAdrAndPorts;
		this.partitionName = partitionName;
	}
}
