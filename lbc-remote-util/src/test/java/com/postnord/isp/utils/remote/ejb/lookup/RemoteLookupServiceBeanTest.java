package com.postnord.isp.utils.remote.ejb.lookup;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import com.postnord.isp.utils.remote.ejb.lookup.JndiBindParameters;
import com.postnord.isp.utils.remote.ejb.lookup.JndiBindParameters.BindAdrAndPort;
import com.postnord.isp.utils.remote.ejb.lookup.RemoteLookupServiceBean;

public class RemoteLookupServiceBeanTest {
//
//    @Test
//    public void shouldHandleOneHost() throws Exception {
//        RemoteLookupServiceBean bean = new RemoteLookupServiceBean();
//        for (int i = 0; i < 100; i++) {
//            String result = bean.sortJndiBindAdrAndPorts("http-remoting://as2bs.idp.posten.se:1099");
//            assertEquals("http-remoting://as2bs.idp.posten.se:1099", result);
//        }
//    }
//
//    @Test
//    public void shouldSortLocalhostFirst() throws Exception {
//        RemoteLookupServiceBean bean = new RemoteLookupServiceBean();
//        for (int i = 0; i < 100; i++) {
//            String result = bean.sortJndiBindAdrAndPorts("http-remoting://as2bs.idp.posten.se:1099,localhost:1099");
//            assertEquals("http-remoting://localhost:1099,as2bs.idp.posten.se:1099", result);
//        }
//    }
//
//    @Test
//    public void shouldSortWithoutLocalhost() throws Exception {
//        RemoteLookupServiceBean bean = new RemoteLookupServiceBean();
//        // random order
//        Set<String> result = new HashSet<String>();
//        for (int i = 0; i < 100000; i++) {
//            result.add(bean.sortJndiBindAdrAndPorts("http-remoting://as2as.idp.posten.se:1099,as2bs.idp.posten.se:1099"));
//        }
//        assertEquals(2, result.size());
//        assertTrue(result.contains("http-remoting://as2as.idp.posten.se:1099,as2bs.idp.posten.se:1099"));
//        assertTrue(result.contains("http-remoting://as2bs.idp.posten.se:1099,as2as.idp.posten.se:1099"));
//    }
//
//    @Test
//    public void shouldSortManyWithoutLocalhost() throws Exception {
//        RemoteLookupServiceBean bean = new RemoteLookupServiceBean();
//        // random order
//        Set<String> result = new HashSet<String>();
//        for (int i = 0; i < 100000; i++) {
//            result.add(bean.sortJndiBindAdrAndPorts("http-remoting://a:1099,b:1099,localhost:1099,c:1099"));
//        }
//        assertEquals(6, result.size());
//
//        assertTrue(result.contains("http-remoting://localhost:1099,a:1099,b:1099,c:1099"));
//        assertTrue(result.contains("http-remoting://localhost:1099,a:1099,c:1099,b:1099"));
//        assertTrue(result.contains("http-remoting://localhost:1099,b:1099,a:1099,c:1099"));
//        assertTrue(result.contains("http-remoting://localhost:1099,b:1099,c:1099,a:1099"));
//        assertTrue(result.contains("http-remoting://localhost:1099,c:1099,a:1099,b:1099"));
//        assertTrue(result.contains("http-remoting://localhost:1099,c:1099,b:1099,a:1099"));
//    }
    
    @Test
    public void shouldParfileOK() throws Exception {

        RemoteLookupServiceBean bean = new RemoteLookupServiceBean();
        bean.initialize();
        
        Map<String, JndiBindParameters> bind = bean.getBindRegistry();
        
        assertEquals(3, bind.size());
    }
    
    @Test
    public void jndiBindParametersContentOK() throws Exception {

        RemoteLookupServiceBean bean = new RemoteLookupServiceBean();
        bean.initialize();
        
        Map<String, JndiBindParameters> bind = bean.getBindRegistry();
        
        JndiBindParameters jndiBindParameters = bind.get("ldp.awpexporterservice");
        
        assertEquals(2, jndiBindParameters.bindAdrAndPorts.size());
        
        assertEquals("ISP", jndiBindParameters.username);
        assertEquals("ISP", jndiBindParameters.password);
        
        BindAdrAndPort content = jndiBindParameters.bindAdrAndPorts.get(0);
        assertEquals("10.127.83.19", content.ipAddress);
        assertEquals("44099", content.port);
    }
    
    @Test
    public void jndiBindParametersContentOneUrlOK() throws Exception {

        RemoteLookupServiceBean bean = new RemoteLookupServiceBean();
        bean.initialize();
        
        Map<String, JndiBindParameters> bind = bean.getBindRegistry();
        
        JndiBindParameters jndiBindParameters = bind.get("isp.planPartitionService".toLowerCase());
        
        assertEquals(1, jndiBindParameters.bindAdrAndPorts.size());
        
        assertEquals("ISP", jndiBindParameters.username);
        assertEquals("ISP", jndiBindParameters.password);
        
        BindAdrAndPort content = jndiBindParameters.bindAdrAndPorts.get(0);
        assertEquals("10.127.83.19", content.ipAddress);
        assertEquals("44099", content.port);
    }

    @Test
    public void jndiBindParametersContentThreeUrlOK() throws Exception {

        RemoteLookupServiceBean bean = new RemoteLookupServiceBean();
        bean.initialize();
        
        Map<String, JndiBindParameters> bind = bean.getBindRegistry();
        
        JndiBindParameters jndiBindParameters = bind.get("awpadapter.awpService".toLowerCase());
        
        assertEquals(3, jndiBindParameters.bindAdrAndPorts.size());
        
        assertEquals("ISP", jndiBindParameters.username);
        assertEquals("ISP", jndiBindParameters.password);
        
        BindAdrAndPort content = jndiBindParameters.bindAdrAndPorts.get(0);
        assertEquals("10.127.83.19", content.ipAddress);
        assertEquals("44099", content.port);
    }
    
    @Test
    public void jndiBindParametersContentNoUsernamePasswordOK() throws Exception {

        RemoteLookupServiceBean bean = new RemoteLookupServiceBean();
        bean.setPrefix("nopass");
        bean.initialize();
        
        Map<String, JndiBindParameters> bind = bean.getBindRegistry();
        
        JndiBindParameters jndiBindParameters = bind.get("isp.planPartitionService".toLowerCase());
        
        assertEquals(1, jndiBindParameters.bindAdrAndPorts.size());
        
        assertEquals(null, jndiBindParameters.username);
        assertEquals(null, jndiBindParameters.password);
        
        BindAdrAndPort content = jndiBindParameters.bindAdrAndPorts.get(0);
        assertEquals("10.127.83.19", content.ipAddress);
        assertEquals("44099", content.port);
    }


}
