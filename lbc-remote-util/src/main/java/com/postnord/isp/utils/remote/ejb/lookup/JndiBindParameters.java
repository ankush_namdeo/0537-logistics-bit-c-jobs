package com.postnord.isp.utils.remote.ejb.lookup;

import java.util.ArrayList;
import java.util.List;

public class JndiBindParameters {
    public final String logicalName;
    public final String mappedJndiName;
    public String username;
    public String password;
    public List<JndiBindParameters.BindAdrAndPort> bindAdrAndPorts = new ArrayList<JndiBindParameters.BindAdrAndPort>();

    public JndiBindParameters(String logicalName, String mappedJndiName, String possibleJndiBindAdrAndPorts) {
        this.logicalName = logicalName;
        this.mappedJndiName = mappedJndiName;
        initbindAdrAndPorts(possibleJndiBindAdrAndPorts);
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private void initbindAdrAndPorts(String possibleJndiBindAdrAndPorts) {

        String[] addresses = possibleJndiBindAdrAndPorts.split(",");
        for (String address : addresses) {
            String[] ipPort = address.split(":");
            if (ipPort.length <= 1) {
                continue;
            }
            bindAdrAndPorts.add(new BindAdrAndPort(ipPort[0], ipPort[1]));
        }

        if (bindAdrAndPorts.size() == 0) {
            throw new IllegalStateException("No jndi mapping configuration found for possibleJndiBindAdrAndPorts \""
                    + possibleJndiBindAdrAndPorts + "\"");
        }
    }

    class BindAdrAndPort {

        String ipAddress;
        String port;

        BindAdrAndPort(String ipAddress, String port) {
            this.ipAddress = ipAddress;
            this.port = port;
        }

        public String getIpAddress() {
            return ipAddress;
        }

        public String getPort() {
            return port;
        }

        @Override
        public String toString() {
            return "BindAdrAndPort [ipAddress=" + ipAddress + ", port=" + port + "]";
        }
    }
}
