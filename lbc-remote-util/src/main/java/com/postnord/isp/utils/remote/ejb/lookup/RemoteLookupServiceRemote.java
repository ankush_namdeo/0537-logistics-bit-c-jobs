package com.postnord.isp.utils.remote.ejb.lookup;

import javax.ejb.Remote;

@Remote
public interface RemoteLookupServiceRemote extends RemoteLookupService {

}
