package com.postnord.isp.utils.remote.ejb.lookup;

import javax.ejb.Local;

@Local
public interface RemoteLookupServiceLocal extends RemoteLookupService {

}
