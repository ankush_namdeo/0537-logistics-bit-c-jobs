package com.postnord.isp.utils.remote.ejb.lookup;

import javax.naming.NamingException;

public interface RemoteLookupService {

    <T> T lookup(String logicalMappedName, String prefix, String remoteInterfaceName) throws NamingException;
}
