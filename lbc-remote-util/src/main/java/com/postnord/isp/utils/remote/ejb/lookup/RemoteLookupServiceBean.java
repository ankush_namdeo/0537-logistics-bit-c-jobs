package com.postnord.isp.utils.remote.ejb.lookup;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameNotFoundException;
import javax.naming.NamingException;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.DefaultConfigurationBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.postnord.isp.utils.remote.ejb.lookup.JndiBindParameters.BindAdrAndPort;

/**
 * 
 * @author ANNI300
 *
 */
@Stateless(name = "remoteLookupService")
@Local(RemoteLookupServiceLocal.class)
@Remote(RemoteLookupServiceRemote.class)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class RemoteLookupServiceBean implements RemoteLookupServiceLocal, RemoteLookupServiceRemote {

    private static final Log LOGGER = LogFactory.getLog(RemoteLookupServiceBean.class);
    private long configurationExpireTime;
    private static final int CONFIGURATION_TTL = 5 * 60 * 1000;
    private static final int CONFIGURATION_FAILURE_TTL = 10 * 1000;
    private Random random = new Random();
    private String prefix = null;

    private Configuration config;

    private Map<String, JndiBindParameters> bindRegistry = new HashMap<String, JndiBindParameters>();

    /**
     * Lazy initialization of configuration properties and jndi.
     */
    protected void initialize() {
        long now = System.currentTimeMillis();
        if (now > configurationExpireTime) {
            config = null;
            configurationExpireTime = now + CONFIGURATION_TTL;
            LOGGER.info("Reload LbcServiceBindRegistry file");
        }
        initConfiguration();
        setupBindRegistry();
    }

    protected void initConfiguration() {
        if (config == null) {
            try {
                DefaultConfigurationBuilder builder = new DefaultConfigurationBuilder();
                builder.setFile(new File(System.getProperty("jboss.server.base.dir") + "/configuration/appdata/"
                        + prefix + "-remote_config.xml"));
                //builder.clearErrorListeners(); // Ignore any configuration
                builder.setListDelimiter(' ');
                DefaultConfigurationBuilder.setDefaultListDelimiter(' ');
                config = builder.getConfiguration(true);
            } catch (ConfigurationException e) {
                throw new IllegalStateException(
                        "Failed to initialize configuration settings. EJB lookup will not work properly.", e);
            }
        }
        if (config == null) {
            String errMessage = "Missing resouce: " + System.getProperty("jboss.server.base.dir")
                    + "/configuration/appdata/" + prefix + "-remote_config.xml";
            LOGGER.error(errMessage);
            throw new RuntimeException(errMessage);
        }
    }

    @SuppressWarnings("unchecked")
    protected void setupBindRegistry() {
        Map<String, JndiBindParameters> newBindRegistry = new HashMap<String, JndiBindParameters>();
        Iterator<String> keyIter = config.getKeys();
        while (keyIter.hasNext()) {
            final String key = keyIter.next().trim();
            final String[] values = config.getStringArray(key);
            if (key.length() > 0 && values.length >= 2 && values[0].trim().length() > 0 && values[1].trim().length() > 0) {

                JndiBindParameters params = new JndiBindParameters(key, values[0].trim(), values[1].trim());
                // Both username and password must be set
                if (values.length > 3) {
                    params.setUsername(values[2].trim());
                    params.setPassword(values[3].trim());
                }
                newBindRegistry.put(params.logicalName.toLowerCase(), params);
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Registered LbcServiceBindRegistry logical service: " + key + " BindAdrAndPorts: "
                            + params.bindAdrAndPorts + " JndiName: " + params.mappedJndiName);
                }
            } else {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("LbcServiceBindRegistry configuration key \"" + key
                            + "\" was ignored due to illegal configuration.");
                }
            }
        }

        bindRegistry = newBindRegistry;
    }

    protected Map<String, JndiBindParameters> getBindRegistry() {
        return bindRegistry;
    }
    
    protected void setPrefix(String testPrefix) {
        this.prefix = testPrefix;
    }

    @SuppressWarnings("unchecked")
    public <T> T lookup(String logicalServiceName, String prefix, String remoteInterfaceName) throws NamingException {
        this.prefix = prefix;
        initialize();

        JndiBindParameters bindParams = bindRegistry.get(logicalServiceName.toLowerCase());
        if (bindParams == null) {
            throw new IllegalStateException("No jndi mapping configuration found for logicalServiceName \""
                    + logicalServiceName + "\"");
        }

        NamingException firstException = null;

        Collections.shuffle(bindParams.bindAdrAndPorts, random);

        // Loop until break or exception.
        for (BindAdrAndPort bindAdrAndPort : bindParams.bindAdrAndPorts) {
            Context ctx = createInitialContext(bindAdrAndPort, bindParams.username, bindParams.password);
            try {
                return (T) ctx.lookup("java:global/" + bindParams.mappedJndiName + "!" + remoteInterfaceName);
            } catch (NamingException e) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Got NamingException for jndBindAdrAndPorts " + bindAdrAndPort + ". Message: "
                            + e.getMessage());
                }
                setConfigurationExpireTimeAfterFailure();

                if (firstException == null) {
                    firstException = e;
                }
            }
        }

        if (firstException != null) {
            throw firstException;
        }

        throw new NameNotFoundException("Filed to find " + logicalServiceName + " for " + prefix + " RemoteInterface: "
                + remoteInterfaceName);
    }

    private void setConfigurationExpireTimeAfterFailure() {
        // reload configuration more often in case of failure
        long now = System.currentTimeMillis();
        if ((configurationExpireTime - now) > CONFIGURATION_FAILURE_TTL) {
            configurationExpireTime = now + CONFIGURATION_FAILURE_TTL;
        }
    }

    protected Context createInitialContext(BindAdrAndPort bindAdrAndPort, String username, String password)
            throws NamingException {
        Properties prop = new Properties();

        // JBoss EAP 6.x
        prop.put("remote.connections", "lbc");
        prop.put("remote.connection.lbc.host", bindAdrAndPort.getIpAddress());
        prop.put("remote.connection.lbc.port", bindAdrAndPort.getPort());
        if (username != null && password != null) {
            prop.put("remote.connection.lbc.username", username);
            prop.put("remote.connection.lbc.password", password);
        }
        prop.put("org.jboss.ejb.client.scoped.context", true);
        prop.put("remote.connection.lbc.connect.options.org.xnio.Options.SSL_ENABLED", "false");
        prop.put("remote.connection.lbc.connect.options.org.xnio.Options.SASL_POLICY_NOANONYMOUS", "false");

        return new InitialContext(prop);
    }
}
