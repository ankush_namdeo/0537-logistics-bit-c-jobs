package com.postnord.isp.utils.sculptor.jpa.accessimpl;

import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.sculptor.framework.accessimpl.ChunkFetcherBase;
import org.sculptor.framework.accessimpl.jpa.JpaHelper;

/**
 * This class can be used when fetching objects with a Query IN expression
 * when there are many values in the 'in' criteria. It is not "possible" to use
 * huge number of parameters in a IN criterion and therefore we
 * chunk the query into pieces.
 *
 * @author Patrik Nordwall
 *
 */
@SuppressWarnings("unchecked")
public abstract class JpaChunkFetcher<T, KEY> extends ChunkFetcherBase<T, KEY> {

	private final EntityManager entityManager;

    /**
     * @param restrictionPropertyName
     *            the name of the property to use for the 'in' criteria
     */
    public JpaChunkFetcher(EntityManager entityManager, String restrictionPropertyName) {
    	super(restrictionPropertyName);
        this.entityManager = entityManager;
    }


    /**
     *
     * @return This is our base query which we will add the desired
     *         restrictions to
     */
    protected abstract String createBaseQuery();

    protected List<T> getChunk(Collection<KEY> keys) {
        String queryStr = createBaseQuery();

        Collection<KEY> restrictionPropertyValues = restrictionPropertyValues(keys);
        // get all with matching property
        if (queryStr.toLowerCase().contains(" where ")) {
        	queryStr += " and ";
        } else {
        	queryStr += " where ";
        }

        queryStr += getRestrictionPropertyName() + " IN (:restrictionPropertyValues)";
        Query query = entityManager.createQuery(queryStr);
        query.setParameter("restrictionPropertyValues", restrictionPropertyValues);
  	
        return query.getResultList();
    }

}