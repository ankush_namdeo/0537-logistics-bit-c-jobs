package com.postnord.isp.utils.sculptor.date;

import java.sql.Date;

import org.hibernate.SessionFactory;
import org.hibernate.usertype.ParameterizedType;
import org.jadira.usertype.dateandtime.joda.columnmapper.DateColumnLocalDateMapper;
import org.jadira.usertype.dateandtime.joda.util.ZoneHelper;
import org.jadira.usertype.spi.shared.AbstractParameterizedUserType;
import org.jadira.usertype.spi.shared.IntegratorConfiguredType;
import org.joda.time.LocalDate;

public class PersistentLocalDate extends AbstractParameterizedUserType<LocalDate, Date, DateColumnLocalDateMapper>
implements ParameterizedType, IntegratorConfiguredType {

	@Override
	public void applyConfiguration(SessionFactory sessionFactory) {

		super.applyConfiguration(sessionFactory);

		DateColumnLocalDateMapper columnMapper = (DateColumnLocalDateMapper) getColumnMapper();
		columnMapper.setDatabaseZone(ZoneHelper.getDefault());
	}
}
