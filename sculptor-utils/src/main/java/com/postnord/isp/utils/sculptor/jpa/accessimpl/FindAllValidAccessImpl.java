package com.postnord.isp.utils.sculptor.jpa.accessimpl;

import java.util.List;

import javax.persistence.Query;

import org.sculptor.framework.accessimpl.jpa.JpaAccessBase;

import com.postnord.isp.utils.sculptor.jpa.accessapi.FindAllValidAccess;


/**
 * Generic access object for finding all valid entities. Usage in repository:
 * findAllValid;
 * <p>
 * You have to add the following properties in sculptor-generator.properties:
 * 
 * <pre>
 * framework.accessapi.FindAllValidAccess=com.postnord.isp.utils.sculptor.accessapi.FindAllValidAccess
 * framework.accessimpl.jpahibernate.JpaHibFindAllValidAccessImpl=com.postnord.isp.utils.sculptor.accessimpl.FindAllValidAccessImpl
 * genericAccessObjectStrategy.findAllValid=com.postnord.isp.utils.sculptor.accessimpl.FindAllValidAccessObjectStrategy
 * </pre>
 * 
 */
public class FindAllValidAccessImpl<T> extends JpaAccessBase<T> implements FindAllValidAccess<T> {

    private List<T> result;
    
    public FindAllValidAccessImpl(Class<? extends T> persistentClass) {
        setPersistentClass(persistentClass);
    }

	/**
     * The result of the command.
     */
    public List<T> getResult() {
        return this.result;
    }

    protected void setResult(List<T> result) {
        this.result = result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void performExecute() {

        StringBuilder queryStr = new StringBuilder();
        String className = getPersistentClass().getSimpleName();

        queryStr.append("select e from ").append(className).append(" e ").append(
                "where CURRENT_TIMESTAMP between e.validFrom and e.validTo");

        Query query = getEntityManager().createQuery(queryStr.toString());
        List<T> resultList = query.getResultList();
        setResult(resultList);
    }
}
