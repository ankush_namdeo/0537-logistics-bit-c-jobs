package com.postnord.isp.utils.sculptor.errorhandling;

import java.io.Serializable;

import org.sculptor.framework.errorhandling.ApplicationException;
import org.sculptor.framework.errorhandling.ExceptionHelper;

import com.postnord.isp.utils.sculptor.util.LogCodes;

public class JMSReceiveFailedException extends ApplicationException {

    private static final long serialVersionUID = 2131503124711821160L;

    public JMSReceiveFailedException(String message) {
        super(LogCodes.LIN_203.name(), LogCodes.LIN_203.getMsg());
        setMessageParameters(new Serializable[] { ExceptionHelper.isJmsRedelivered(), message });
    }

}
