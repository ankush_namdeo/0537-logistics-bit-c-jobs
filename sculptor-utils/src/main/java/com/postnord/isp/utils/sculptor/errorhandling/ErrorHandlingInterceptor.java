package com.postnord.isp.utils.sculptor.errorhandling;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sculptor.framework.errorhandling.DatabaseAccessException;
import org.sculptor.framework.errorhandling.ExceptionHelper;
import org.sculptor.framework.errorhandling.InvalidMessageException;
import org.sculptor.framework.errorhandling.LogMessage;
import org.sculptor.framework.errorhandling.MessageException;
import org.sculptor.framework.errorhandling.OptimisticLockingException;
import org.sculptor.framework.errorhandling.UnexpectedRuntimeException;
import org.sculptor.framework.errorhandling.ValidationException;

/**
 * This advice logs exceptions. RuntimeExceptions are caught and new
 * SystemException (subclasses) are thrown.
 * {@link org.fornax.cartridges.sculptor.framework.errorhandling.SystemException}
 * and RuntimeException are logged at error or fatal level.
 * {@link org.fornax.cartridges.sculptor.framework.errorhandling.ApplicationException}
 * is logged at debug level.
 * 
 * @author Patrik Nordwall
 * 
 */
public class ErrorHandlingInterceptor extends
		org.sculptor.framework.errorhandling.ErrorHandlingInterceptor {

	private static Map<String, String> defaultLogCodeMapping = new ConcurrentHashMap<String, String>();
	static {
		defaultLogCodeMapping.put(ValidationException.ERROR_CODE,
				CommonLogCodes.COM_001.toString());
		defaultLogCodeMapping.put(UnexpectedRuntimeException.ERROR_CODE,
				CommonLogCodes.COM_002.toString());
		defaultLogCodeMapping.put(DatabaseAccessException.ERROR_CODE,
				CommonLogCodes.COM_003.toString());
		defaultLogCodeMapping.put(OptimisticLockingException.ERROR_CODE,
				CommonLogCodes.COM_005.toString());
		defaultLogCodeMapping.put(OutOfMemoryError.class.getName(),
				CommonLogCodes.COM_006.toString());
		defaultLogCodeMapping.put(InvalidMessageException.ERROR_CODE,
				CommonLogCodes.COM_008.toString());
		defaultLogCodeMapping.put(MessageException.ERROR_CODE,
				CommonLogCodes.COM_009.toString());
	}

	public ErrorHandlingInterceptor() {
	}

	@Override
	protected String mapLogCode(String logCode) {

		if (ValidationException.ERROR_CODE.equals(logCode)
				&& ExceptionHelper.isJmsContext()) {
			return CommonLogCodes.COM_010.toString();
		}
		if (DatabaseAccessException.ERROR_CODE.equals(logCode)
				&& ExceptionHelper.isJmsContext()
				&& !ExceptionHelper.isJmsRedelivered()) {
			return CommonLogCodes.COM_004.toString();
		}
		if (OptimisticLockingException.ERROR_CODE.equals(logCode)
				&& ExceptionHelper.isJmsContext()
				&& ExceptionHelper.isJmsRedelivered()) {
			return CommonLogCodes.COM_011.toString();
		}

		String result = defaultLogCodeMapping.get(logCode);
		if (result == null) {
			return logCode;
		} else {
			return result;
		}
	}

	@Override
	protected void handleError(Object target, Error e) {
		Log log = LogFactory.getLog(target.getClass());
		String errorCode = e.getClass().getName();
		String mappedErrorCode = mapLogCode(errorCode);
		if (errorCode.equals(mappedErrorCode)) {
			mappedErrorCode = CommonLogCodes.COM_007.toString();
		}
		LogMessage message = new LogMessage(mappedErrorCode,
				ExceptionHelper.excMessage(e));
		log.fatal(message, e);
	}

}
