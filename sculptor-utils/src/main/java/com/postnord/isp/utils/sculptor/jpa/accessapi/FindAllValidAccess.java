package com.postnord.isp.utils.sculptor.jpa.accessapi;

import java.util.List;

/**
 * Generic access object for finding all valid entities.
 * 
 */
public interface FindAllValidAccess<T> {
    void execute();

    /**
     * The result of the command.
     */
    List<T> getResult();
}
