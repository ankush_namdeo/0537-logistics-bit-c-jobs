package com.postnord.isp.utils.sculptor.date;

import java.sql.Timestamp;

import org.hibernate.SessionFactory;
import org.hibernate.usertype.ParameterizedType;
import org.jadira.usertype.dateandtime.joda.columnmapper.TimestampColumnDateTimeMapper;
import org.jadira.usertype.dateandtime.joda.util.ZoneHelper;
import org.jadira.usertype.spi.shared.AbstractVersionableUserType;
import org.jadira.usertype.spi.shared.IntegratorConfiguredType;
import org.joda.time.DateTime;

public class PersistentDateTime extends AbstractVersionableUserType<DateTime, Timestamp, TimestampColumnDateTimeMapper> implements ParameterizedType, IntegratorConfiguredType {

	@Override
	public int compare(Object o1, Object o2) {
		return ((DateTime) o1).compareTo((DateTime) o2);
	}

	@Override
	public void applyConfiguration(SessionFactory sessionFactory) {

		super.applyConfiguration(sessionFactory);

		TimestampColumnDateTimeMapper columnMapper = (TimestampColumnDateTimeMapper) getColumnMapper();
		columnMapper.setDatabaseZone(ZoneHelper.getDefault());
		columnMapper.setJavaZone(null);
	}
}
