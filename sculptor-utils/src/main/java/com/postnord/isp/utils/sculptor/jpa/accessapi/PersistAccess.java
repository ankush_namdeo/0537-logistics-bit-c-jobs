package com.postnord.isp.utils.sculptor.jpa.accessapi;

/**
 * <p>
 * Save a new entity. When you know that it is a new entity it can be more
 * efficient to use this persist instead of the ordinary save.
 * </p>
 * <p>
 * Command design pattern.
 * </p>
 */
public interface PersistAccess<T> {

    void setEntity(T entity);

    void execute();
}
