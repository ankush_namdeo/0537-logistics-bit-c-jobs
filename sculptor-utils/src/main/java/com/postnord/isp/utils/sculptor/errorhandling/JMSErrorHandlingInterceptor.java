package com.postnord.isp.utils.sculptor.errorhandling;

import static org.sculptor.framework.errorhandling.ExceptionHelper.excMessage;
import static org.sculptor.framework.errorhandling.ExceptionHelper.isJmsContext;
import static org.sculptor.framework.errorhandling.ExceptionHelper.isJmsRedelivered;

import java.lang.reflect.Method;

import javax.persistence.OptimisticLockException;

import org.apache.log4j.Logger;
import org.sculptor.framework.errorhandling.DatabaseAccessException;
import org.sculptor.framework.errorhandling.LogMessage;
import org.sculptor.framework.errorhandling.OptimisticLockingException;



/**
 * The purpose of this class is to lessen and refine the amount of logged data
 * in a JMS context on database errors and optimistic logging errors.
 * <p>
 * The super ErrorHandlingInterceptor adds the full stack trace to the log each
 * time a database exception is thrown for a re-delivered message. This is not
 * really necessary, since it is still not a problem that requires that amount
 * of logging.
 * <p>
 * The super ErrorHandlingInterceptor also only adds SQLExceptions to the cause
 * message. In LINDA we are using Hibernate which throws exceptions that are not
 * a subclass of this exception and thus the specific exception is lost. This
 * class adds all cause messages to the logged data.
 * 
 * @author ansk719
 * 
 */
public class JMSErrorHandlingInterceptor extends ErrorHandlingInterceptor {

    @Override
    protected void handleDatabaseAccessException(Object target, Exception e) {
        if (!isJmsContext()) {
            super.handleDatabaseAccessException(target, e);
            return;
        }

        String message = logException(e, target, DatabaseAccessException.ERROR_CODE);

        DatabaseAccessException newException = new DatabaseAccessException(message);
        newException.setLogged(true);
        throw newException;
    }

    private String logException(Exception e, Object target, String errorCode) {
        Logger log = Logger.getLogger(target.getClass());
        StringBuilder message = new StringBuilder();
        message.append(e.getClass().getName()).append(": ");
        message.append(excMessage(e));
        Throwable cause = e.getCause();
        while (cause != null) {
            message.append(", Caused by: ");
            message.append(cause.getClass().getName()).append(": ");
            message.append(excMessage(cause));
            cause = cause.getCause();
        }

        LogMessage logMessage = new LogMessage(mapLogCode(errorCode), message.toString());
        // No stack trace here, all interesting information should be in the
        // logged message.
        if (isJmsRedelivered()) {
            log.error(logMessage);
        } else {
            log.info(logMessage);
        }
        return message.toString();
    }

    /**
     * JPA exception for Optimistic Locking.
     */
    @Override
    public void afterThrowing(Method m, Object[] args, Object target, OptimisticLockException e)
            throws OptimisticLockingException {
        handleOptimisticLockingException(target, e);
    }

    protected void handleOptimisticLockingException(Object target, Exception e) throws OptimisticLockingException {
        String message = logException(e, target, OptimisticLockingException.ERROR_CODE);
        OptimisticLockingException newException = new OptimisticLockingException(message);
        newException.setLogged(true);
        throw newException;
    }

}
