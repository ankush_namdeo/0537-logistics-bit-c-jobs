package com.postnord.isp.utils.sculptor.jpa.accessimpl;

import org.sculptor.generator.util.GenericAccessObjectManager;
import org.sculptor.generator.util.GenericAccessObjectStrategy;

import sculptormetamodel.RepositoryOperation;

/**
 * Used by sculptor code generator.
 * 
 * @see se.posten.loab.lisp.common.accessimpl.FindAllValidAccessImpl
 * 
 */
public class PersistAccessObjectStrategy extends
		GenericAccessObjectManager.SaveStrategy implements
		GenericAccessObjectStrategy {

	public PersistAccessObjectStrategy(GenericAccessObjectManager manager) {
		manager.super();
	}
	
	public void addDefaultValues(RepositoryOperation operation) {
		addDefaultAggregateRootParameter(operation, "entity");
	}
}
