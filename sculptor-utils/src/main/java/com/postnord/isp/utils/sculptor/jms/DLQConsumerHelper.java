package com.postnord.isp.utils.sculptor.jms;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils; 
import org.apache.log4j.Logger;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.postnord.isp.utils.sculptor.errorhandling.LindaIOException;

public final class DLQConsumerHelper {

    private static final Logger log = Logger.getLogger(DLQConsumerHelper.class);
    private static final DateTimeFormatter timestampFormatter = DateTimeFormat.forPattern("yyyy-MM-dd--HH.mm.ss.SSS");

    private DLQConsumerHelper() {
    }

    public static void writeMessageToDLQDir(String baseDirName, String messageName, String messageXML)
            throws LindaIOException {
        File baseDir = new File(baseDirName, messageName);
        if (!baseDir.exists() && !baseDir.mkdirs()) {
            throw LindaIOException.failedToCreateDLQMessageDir(baseDir);
        }
        String prefix = messageName + "_" + timestampFormatter.print(System.currentTimeMillis());
        String suffix = ".xml";

        try {
            File f = File.createTempFile(prefix, suffix, baseDir);
            try {
                FileUtils.writeStringToFile(f, messageXML);
            } catch (IOException e) {
                throw LindaIOException.failedToWriteDLQMessageFile(e);
            }
            log.info("Wrote dead letter queue message to file " + f.getAbsolutePath() + ". Size: " + f.length()
                    / 1024.0 + "kb");
        } catch (IOException e) {
            throw LindaIOException.failedToCreateDLQMessageFile(e);
        }
    }
}
