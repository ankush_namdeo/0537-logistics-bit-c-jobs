package com.postnord.isp.utils.sculptor.jpa.accessimpl;

import org.sculptor.generator.util.GenericAccessObjectManager;
import org.sculptor.generator.util.GenericAccessObjectStrategy;

import sculptormetamodel.RepositoryOperation;

/**
 * Used by sculptor code generator.
 * 
 * @see se.posten.loab.lisp.common.accessimpl.FindAllValidAccessImpl
 * 
 */
public class FindAllValidAccessObjectStrategy extends GenericAccessObjectManager.FindAllStrategy implements GenericAccessObjectStrategy {

	public FindAllValidAccessObjectStrategy(GenericAccessObjectManager manager) {
		manager.super();
	}

    public String getGenericType(RepositoryOperation operation) {
        return "<" + aggregateRootClassName(operation) + ">";
    }

    public boolean isPersistentClassConstructor() {
        return true;
    }
}

