package com.postnord.isp.utils.sculptor.jpa.accessimpl;

import javax.persistence.PersistenceException;

import org.sculptor.framework.accessimpl.jpa.JpaAccessBase;

import com.postnord.isp.utils.sculptor.jpa.accessapi.PersistAccess;

public class PersistAccessImpl<T> extends JpaAccessBase<T> implements PersistAccess<T> {

    private T entity;

    public PersistAccessImpl(Class<? extends T> persistentClass) {
        setPersistentClass(persistentClass);
    }

	public void setEntity(T entity) {
		this.entity = entity;
	}
	
	public T getEntity() {
		return entity;
	}

    @Override
    public void performExecute() throws PersistenceException {
    	if (entity != null) {
    		getEntityManager().persist(entity);
		} 
    }
}
