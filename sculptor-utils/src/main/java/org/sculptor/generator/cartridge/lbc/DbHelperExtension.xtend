package org.sculptor.generator.cartridge.lbc

import javax.inject.Inject
import org.sculptor.generator.chain.ChainOverride
import org.sculptor.generator.ext.DbHelper
import org.sculptor.generator.util.DbHelperBase
import org.sculptor.generator.util.PropertiesBase
import sculptormetamodel.Reference
import sculptormetamodel.DomainObject

@ChainOverride
class DbHelperExtension extends DbHelper {

	@Inject extension PropertiesBase propertiesBase
	@Inject extension DbHelperBase dbHelperBase
	
	override String truncateLongDatabaseName(String part1, String part2) {
		truncateLongDatabaseName(part1 + "_" + part2, 27)
	}

	override String getDefaultForeignKeyName(Reference ref) {
		val defaultName = next.getDefaultForeignKeyName(ref)
		defaultName.removeTablePrefix().removeDuplicateStart()
	}

	/*
	around extensions::dbhelper::getDefaultOppositeForeignKeyName(Reference ref) :
	let defaultName = (String) ctx.proceed() :
	defaultName.removeTablePrefix().removeDuplicateStart();
	*/

	override String getOppositeForeignKeyName(Reference ref) {
		val defaultName = next.getOppositeForeignKeyName(ref)
		defaultName.removeTablePrefix().removeDuplicateStart()
	}

	override String getExtendsForeignKeyName(DomainObject domainObject) {
		val defaultName = next.getExtendsForeignKeyName(domainObject)
		val newName = defaultName.removeTablePrefix()
		val tableNameWithoutPrefix = domainObject.databaseTable.removeTablePrefix()
		if (newName.startsWith(tableNameWithoutPrefix + "_" + tableNameWithoutPrefix + "_"))
			newName.replaceFirst(tableNameWithoutPrefix + "_", "")
		else 
			newName
	}

	
	/*
	around extensions::dbhelper::getManyToManyJoinTableName(Reference ref) :
	let defaultName = (String) ctx.proceed() :
	getProperty("db.tablePrefix") + "_" + defaultName.removeAllTablePrefixes().removeAllGID();

	override def resolveManyToManyRelations(Application application, boolean ascending) {
		val domainObjects = next.resolveManyToManyRelations(application,ascending)
		domainObjects.modifyDatabaseTablePrefixForManyToManyRelations() ->
		domainObjects;
	}

	override def resolveManyToManyRelations(Application application, boolean ascending) {
		next.resolveManyToManyRelations(application,ascending)
		application.modules.domainObjects.forEach[modifyDatabaseTablePrefixForManyToManyRelations()]
	}
	
	
	def modifyDatabaseTablePrefixForManyToManyRelations(DomainObject domainObject) {
		domainObject.setDatabaseTable(getProperty("db.tablePrefix") + "_" +
		domainObject.databaseTable.removeAllTablePrefixes().removeAllGID()) ->
		domainObject.references.modifyDatabaseTablePrefixForManyToManyRelation();
	}
	*/

	def modifyDatabaseTablePrefixForManyToManyRelation(Reference ref) {
		val newName = ref.databaseColumn.removeTablePrefix()
		val tableNameWithoutPrefix = ref.to.databaseTable.removeTablePrefix()
		if (newName.startsWith(tableNameWithoutPrefix + "_" + tableNameWithoutPrefix + "_"))
			ref.setDatabaseColumn(newName.replaceFirst(tableNameWithoutPrefix + "_", ""))
		else 
			ref.setDatabaseColumn(newName)
	}
	
	def String removeDuplicateStart(String name) {
		val newName = removeTablePrefix(name) 
		val split = newName.split("_") 
		val start = if (split.size > 1) { split.get(0) } else {""} 
		val start2 = if (split.size >= 4) { split.get(0) + "_" + split.get(1) } else {""} 
		val start3 = if (split.size >= 6) { split.get(0) + "_" + split.get(1) + "_" + split.get(2) } else {""} 
		if (start != "" && newName.startsWith(start + "_" + start)) 
			newName.replaceFirst(start + "_", "")
		else if (start2 != "" && newName.startsWith(start2 + "_" + start2))  
			newName.replaceFirst(start2 + "_", "")
		else if (start3 != "" && newName.startsWith(start3 + "_" + start3))  
			newName.replaceFirst(start3 + "_", "")
		else
			newName
	}

	def String removeTablePrefix(String name) {
		if (name.startsWith(getProperty("db.tablePrefix") + "_"))
			name.replaceFirst(getProperty("db.tablePrefix") + "_", "")
		else
			name
	}
		
	def String removeAllTablePrefixes(String name) {
		val newName = name.removeTablePrefix()
		newName.replaceAll("_" + getProperty("db.tablePrefix") + "_", "_")
	}
	
	def String removeAllGID(String name) {
		val newName = name.replaceAll("_GID_", "_")
		if (newName.endsWith("_GID")) 
			newName.replaceAll("_GID", "")
		else
			newName
	}
}
