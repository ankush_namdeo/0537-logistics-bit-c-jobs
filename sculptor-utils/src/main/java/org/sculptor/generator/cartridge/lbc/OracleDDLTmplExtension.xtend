package org.sculptor.generator.cartridge.lbc

import javax.inject.Inject
import org.sculptor.generator.util.PropertiesBase
import org.sculptor.generator.chain.ChainOverride
import org.sculptor.generator.template.db.OracleDDLTmpl
import sculptormetamodel.Application

@ChainOverride
class OracleDDLTmplExtension extends OracleDDLTmpl {
	
	@Inject extension PropertiesBase propertiesBase

	override String dropSequence(Application it) {
		'''
		DROP SEQUENCE «getProperty("db.tablePrefix")»_GID_SEQ;
		'''
	}
	
	override String createSequence(Application it) {
		'''
		CREATE SEQUENCE «getProperty("db.tablePrefix")»_GID_SEQ;
		'''
	}
}