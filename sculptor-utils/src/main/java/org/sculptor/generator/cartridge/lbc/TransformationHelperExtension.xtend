
package org.sculptor.generator.cartridge.lbc

import javax.inject.Inject
import org.sculptor.generator.ext.Helper
import sculptormetamodel.Attribute
import sculptormetamodel.DomainObject
import sculptormetamodel.ValueObject
import org.sculptor.generator.transform.TransformationHelper
import org.sculptor.generator.chain.ChainOverride
import org.sculptor.generator.util.PropertiesBase

@ChainOverride
class TransformationHelperExtension extends TransformationHelper {
	@Inject extension Helper helper
	@Inject extension PropertiesBase propertiesBase

	override dispatch void modifyDatabaseColumn(Attribute attribute) {
		if (attribute.name == "id")
			attribute.setDatabaseColumn(attribute.getDomainObject().databaseTable + "_GID")
    	else
	    	next.modifyDatabaseColumn(attribute)
	}

	override dispatch void modifyDatabaseNames(DomainObject domainObject) {
		next.modifyDatabaseNames(domainObject)
		if (domainObject.hasOwnDatabaseRepresentation())
			domainObject.setDatabaseTable(getProperty("db.tablePrefix") + "_" + domainObject.databaseTable)
	}

	override dispatch void modifyDatabaseNames(ValueObject valueObject) {
		next.modifyDatabaseNames(valueObject)
		if (valueObject.persistent)
			valueObject.setDatabaseTable(getProperty("db.tablePrefix") + "_" + valueObject.databaseTable)	
	}
}