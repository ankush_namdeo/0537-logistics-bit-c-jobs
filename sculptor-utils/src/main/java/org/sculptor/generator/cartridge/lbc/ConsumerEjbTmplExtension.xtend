package org.sculptor.generator.cartridge.lbc

import javax.inject.Inject
import org.sculptor.generator.ext.Properties
import org.sculptor.generator.util.PropertiesBase
import sculptormetamodel.Consumer
import org.sculptor.generator.chain.ChainOverride
import org.sculptor.generator.template.consumer.ConsumerEjbTmpl

@ChainOverride
class ConsumerEjbTmplExtension extends ConsumerEjbTmpl {

	@Inject extension Properties properties
	@Inject extension PropertiesBase propertiesBase
	
	override String jmsConnection(Consumer it) {
		'''
		«IF applicationServer() == "jboss"»
			@javax.annotation.Resource(mappedName = "java:jboss/activemq/ConnectionFactory")
		«ELSE»
			@javax.annotation.Resource(mappedName = "java:/jms/QueueFactory")
		«ENDIF»
		private javax.jms.ConnectionFactory connectionFactory;
		private javax.jms.Connection connection;

			@Override
			protected javax.jms.Connection getJmsConnection() {
				try {
					if (connection == null) {
					    connection = connectionFactory.createConnection();
					    connection.start();
					}
					return connection;
				} catch (Exception e) {
					getLog().error("Can't create JmsConnection: " + e.getMessage(), e);
					return null;
				}
			}
			
			@javax.annotation.PreDestroy
			public void ejbRemove() {
				closeConnection();
			}

		@Override
			protected void closeConnection() {
				try {
					if (connection != null) {
					    connection.close();
					}
				} catch (Exception ignore) {
				} finally {
					connection = null;
				}
			}
		'''
	}

	override String invalidMessageQueue(Consumer it) {
		'''
		«IF applicationServer() == "jboss"»
			«IF hasProperty("invalidMessageQueue")»
				@javax.annotation.Resource(mappedName = "java:/jboss/activemq/queue/queue/«getProperty('invalidMessageQueue')»")
	      	«ELSE»
	        	@javax.annotation.Resource(mappedName = "java:/jboss/activemq/queue/queue/jms.invalidMessageQueue")
	      	«ENDIF»
			
		«ELSE»
			@javax.annotation.Resource(mappedName = "java:/jms/invalidMessageQueue")
		«ENDIF»
		private javax.jms.Queue invalidMessageQueue;

		protected javax.jms.Destination getInvalidMessageQueue() {
			return invalidMessageQueue;
		}
		'''
	}
		
	
}