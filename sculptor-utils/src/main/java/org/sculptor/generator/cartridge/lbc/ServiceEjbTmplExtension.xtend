package org.sculptor.generator.cartridge.lbc

import javax.inject.Inject
import org.sculptor.generator.ext.Helper
import org.sculptor.generator.ext.Properties
import org.sculptor.generator.template.common.PubSubTmpl
import org.sculptor.generator.template.service.ServiceTmpl
import org.sculptor.generator.util.HelperBase
import org.sculptor.generator.util.OutputSlot
import sculptormetamodel.Service
import org.sculptor.generator.template.service.ServiceEjbTmpl
import org.sculptor.generator.chain.ChainOverride

@ChainOverride
class ServiceEjbTmplExtension extends ServiceEjbTmpl {

	@Inject private var PubSubTmpl pubSubTmpl
	@Inject private var ServiceTmpl serviceTmpl
	@Inject extension HelperBase helperBase
	@Inject extension Helper helper
	@Inject extension Properties properties



override String ejbBeanImplSubclass(Service it) {
	fileOutput(javaFileName(it.getServiceimplPackage() + "." + name + getSuffix("Impl")), OutputSlot::TO_SRC, '''
	«javaHeader()»
	package «it.getServiceimplPackage()»;

/// Sculptor code formatter imports ///

	
	«IF remoteInterface»
		import «it.getServiceapiPackage()».«name»Remote;
	«ENDIF»
	«IF localInterface»
		import «it.getServiceapiPackage()».«name»Local;
	«ENDIF»
	
	/**
	 * Implementation of «name».
	 */
	@javax.ejb.Stateless(name="«name.toFirstLower()»")
	«IF remoteInterface»
		@javax.ejb.Remote(«name»Remote.class)
	«ENDIF»
	«IF localInterface»
		@javax.ejb.Local(«name»Local.class)
	«ENDIF»
	«IF webService»
		«webServiceAnnotations(it)»
	«ENDIF»
	«ejbInterceptors(it)»
	«IF subscribe != null»«pubSubTmpl.subscribeAnnotation(it.subscribe)»«ENDIF»
	public class «name + getSuffix("Impl")» «IF gapClass»extends «name + getSuffix("Impl")»Base«ENDIF» {
		«serviceTmpl.serialVersionUID(it)»
		public «name + getSuffix("Impl")»() {
		}

	«serviceTmpl.otherDependencies(it)»

		«it.operations.filter(op | op.isImplementedInGapClass()) .map[serviceTmpl.implMethod(it)].join()»

	«serviceTmpl.serviceHook(it)»
	}
	'''
	)
}
}
