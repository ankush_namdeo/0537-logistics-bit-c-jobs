package org.sculptor.generator.cartridge.lbc

import javax.inject.Inject
import org.sculptor.generator.template.service.ServiceTmpl
import org.sculptor.generator.chain.ChainOverride
import org.sculptor.generator.ext.Helper
import org.sculptor.generator.ext.Properties
import org.sculptor.generator.template.common.ExceptionTmpl
import org.sculptor.generator.template.common.PubSubTmpl
import org.sculptor.generator.util.HelperBase
import org.sculptor.generator.util.OutputSlot
import sculptormetamodel.Parameter
import sculptormetamodel.Service
import sculptormetamodel.ServiceOperation

@ChainOverride
class ServiceTmplExtension extends ServiceTmpl {

	@Inject private var ExceptionTmpl exceptionTmpl
	@Inject private var PubSubTmpl pubSubTmpl

	@Inject extension HelperBase helperBase
	@Inject extension Helper helper
	@Inject extension Properties properties
	
	override def String delegateRepositories(Service it) {
		'''
		«FOR delegateRepository : it.getDelegateRepositories()»
			«IF isSpringToBeGenerated()»
				@org.springframework.beans.factory.annotation.Autowired
			«ENDIF»
			«IF pureEjb3()»
				@javax.ejb.EJB
			«ENDIF»
			private «getRepositoryapiPackage(delegateRepository.aggregateRoot.module)».«delegateRepository.name» «delegateRepository.name.toFirstLower()»;
	
			protected «getRepositoryapiPackage(delegateRepository.aggregateRoot.module)».«delegateRepository.name» get«delegateRepository.name»() {
				return «delegateRepository.name.toFirstLower()»;
			}
			
			/**
			* For mock purpose
			*
			*/
			public void set«delegateRepository.name»(«getRepositoryapiPackage(delegateRepository.aggregateRoot.module)».«delegateRepository.name» «delegateRepository.name.toFirstLower()») {
				this.«delegateRepository.name.toFirstLower()» = «delegateRepository.name.toFirstLower()»;
			}
		«ENDFOR»
		'''
	}
}