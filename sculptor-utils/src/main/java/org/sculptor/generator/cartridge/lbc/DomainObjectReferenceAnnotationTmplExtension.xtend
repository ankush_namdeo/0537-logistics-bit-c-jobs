package org.sculptor.generator.cartridge.lbc

import org.sculptor.generator.ext.DbHelper
import org.sculptor.generator.ext.Helper
import org.sculptor.generator.ext.Properties
import org.sculptor.generator.util.DbHelperBase
import org.sculptor.generator.util.HelperBase
import sculptormetamodel.NamedElement
import sculptormetamodel.Reference
import sculptormetamodel.Attribute
import org.sculptor.generator.chain.ChainOverride
import org.sculptor.generator.template.domain.DomainObjectReferenceAnnotationTmpl
import javax.inject.Inject

@ChainOverride
class DomainObjectReferenceAnnotationTmplExtension extends DomainObjectReferenceAnnotationTmpl {

	@Inject extension DbHelperBase dbHelperBase
	@Inject extension DbHelper dbHelper
	@Inject extension HelperBase helperBase
	@Inject extension Helper helper
	@Inject extension Properties properties
	
	override def String oneReferenceOnDeleteJpaAnnotation(Reference it) {
	'''
		«/* use orphanRemoval in JPA2 */»
		@org.hibernate.annotations.OnDelete(action = org.hibernate.annotations.OnDeleteAction.CASCADE)
	'''
	}
	
	

}