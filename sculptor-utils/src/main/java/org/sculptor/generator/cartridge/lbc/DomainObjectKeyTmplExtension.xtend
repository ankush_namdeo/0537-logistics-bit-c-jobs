package org.sculptor.generator.cartridge.lbc

import org.sculptor.generator.template.domain.DomainObjectKeyTmpl
import org.sculptor.generator.template.domain.DomainObjectAttributeTmpl
import javax.inject.Inject
import org.sculptor.generator.ext.Helper
import org.sculptor.generator.ext.Properties
import org.sculptor.generator.util.HelperBase
import sculptormetamodel.DomainObject
import org.sculptor.generator.chain.ChainOverride


@ChainOverride
class DomainObjectKeyTmplExtension extends DomainObjectKeyTmpl {

	@Inject extension HelperBase helperBase
	@Inject extension Helper helper
	@Inject extension Properties properties
	
	override def String compositeKeyGetter(DomainObject it) {
		'''
			/**
			 * This method is used by equals and hashCode.
			 * @return {@link #get«name»Key}
			 */
			«IF isJpaAnnotationToBeGenerated() && !isJpaAnnotationOnFieldToBeGenerated()»
				@javax.persistence.Transient
			«ENDIF»
			public Object getKey() {
				return get«name»Key();
			}
			
			«IF isJpaAnnotationToBeGenerated() && isJpaAnnotationOnFieldToBeGenerated()»
				@javax.persistence.Transient
			«ENDIF»
			private transient «name»Key cached«name»Key;
			
			/**
			 * The natural key for the domain object is
			 * a composite key consisting of several attributes.
			 */
			«IF isJpaAnnotationToBeGenerated() && !isJpaAnnotationOnFieldToBeGenerated()»
				@javax.persistence.Transient
			«ENDIF»
			public «name»Key get«name»Key() {
				if (cached«name»Key == null) {
				// Andreas 20100930:
				// This implementation is a bug fix for sculptor 1.9.0 in combination
				// with a buggy hibernate. Sometimes when hibernate loads database
				// entities, it puts them in a HashSet before all attributes are set.
				// This invokes the equals() method which will create the cachedItemKey
				// but with null values for the key attributes. Since the key is cached, 
				// the entity is unusable later on because the key is broken...
				// This is fixed in a later hibernate release
				// Nissemark 20150604:
				// Same issue with 3.1.0 and hibernate 4.2
					«IF !it.getAllNaturalKeyAttributes().isEmpty »
						if («FOR a : it.getAllNaturalKeyAttributes() SEPARATOR " && "»
							«IF a.getTypeName() == "boolean"»!«a.getGetAccessor()»()
							«ELSEIF a.getTypeName() == "int"»«a.getGetAccessor()»() == 0
	        				«ELSEIF a.getTypeName() == "long"»«a.getGetAccessor()»() == 0L
	        				«ELSE»«a.getGetAccessor()»() == null
	        				«ENDIF»
	        				«ENDFOR»
	        				«IF !it.getAllNaturalKeyReferences().isEmpty »
	        				&& «FOR b : it.getAllNaturalKeyReferences() SEPARATOR " && "»
	        				«b.getGetAccessor()»() == null
	        				«ENDFOR»
	        				«ENDIF»){
	        				return new «name»Key(«FOR aa : it.getAllNaturalKeys() SEPARATOR ","»«aa.getGetAccessor()»()«ENDFOR»);
	        				}
	        		«ELSE»
	        			if («FOR a : it.getAllNaturalKeys() SEPARATOR " && "»
							«a.getGetAccessor()»() == null
	        				«ENDFOR»){
	        				return new «name»Key(«FOR aa : it.getAllNaturalKeys() SEPARATOR ","»«aa.getGetAccessor()»()«ENDFOR»);
	        			}
	        		«ENDIF»
	        		
	        		cached«name»Key = new «name»Key(«FOR a : it.getAllNaturalKeys() SEPARATOR ","»«a.getGetAccessor()»()«ENDFOR»);
	        	}
				return cached«name»Key;
			}
			«compositeKey(it) »
		'''
	}

}