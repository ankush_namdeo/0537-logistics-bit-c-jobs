package org.sculptor.generator.cartridge.lbc

import javax.inject.Inject
import org.sculptor.generator.ext.Helper
import org.sculptor.generator.ext.Properties
import sculptormetamodel.Application
import org.sculptor.generator.chain.ChainOverride
import org.sculptor.generator.template.jpa.JPATmpl
import org.sculptor.generator.util.PropertiesBase

@ChainOverride
class JPATmplExtension extends JPATmpl {

	@Inject extension Helper helper
	@Inject extension Properties properties
	@Inject extension PropertiesBase propBase
	
	override String persistenceUnitDataSource(Application it, String unitName) {
		/* TODO: add additional support for jta */
		/* Invoke old dataSourceName() for backwards compatibility reasons */
		val dataSourceName = if (it.isDefaultPersistenceUnitName(unitName)) it.dataSourceName() else it.dataSourceName(unitName)
		'''
		«IF isEar()»
			«IF applicationServer() == "jboss"»
				<jta-data-source>java:jboss/datasources/«dataSourceName»</jta-data-source>
			«ELSE»
				«IF !isSpringDataSourceSupportToBeGenerated()»
					<jta-data-source>java:comp/env/jdbc/«dataSourceName»</jta-data-source>
				«ENDIF»
			«ENDIF»
		«ELSEIF isWar()»
			«IF applicationServer() == "appengine"»
			«ELSEIF applicationServer() == "jboss"»
				<non-jta-data-source>java:jboss/datasources/«dataSourceName»</non-jta-data-source>
			«ELSE»
				«IF !isSpringDataSourceSupportToBeGenerated()»
					<non-jta-data-source>java:comp/env/jdbc/«dataSourceName»</non-jta-data-source>
				«ENDIF»
			«ENDIF»
		«ENDIF»
		'''
	}
	
	override String persistenceUnitPropertiesHibernate(Application it, String unitName) {
		'''
		<property name="hibernate.dialect" value="«propBase.hibernateDialect»" />
		<property name="hibernate.id.new_generator_mappings" value="false" />
		<property name="query.substitutions" value="true 1, false 0" />
		«/* for testing purposes only */»
		«IF propBase.dbProduct == "hsqldb-inmemory"»
			<property name="hibernate.show_sql" value="true" />
			<property name="hibernate.hbm2ddl.auto" value="create-drop" />
		«ENDIF»
		«persistenceUnitCacheProperties(it, unitName)»
		«IF isEar()»
			«persistenceUnitTransactionProperties(it, unitName)»
			«IF !isSpringDataSourceSupportToBeGenerated() || applicationServer() == "jboss"»
				<!-- Bind entity manager factory to JNDI -->
				<property name="jboss.entity.manager.factory.jndi.name" value="java:jboss/«unitName»"/>
			«ENDIF»
		«ENDIF»
		'''
	}
}