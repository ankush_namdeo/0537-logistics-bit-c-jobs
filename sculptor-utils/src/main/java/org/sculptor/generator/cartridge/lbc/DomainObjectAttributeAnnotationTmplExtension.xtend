package org.sculptor.generator.cartridge.lbc

import javax.inject.Inject
import org.sculptor.generator.ext.DbHelper
import org.sculptor.generator.util.PropertiesBase
import sculptormetamodel.Attribute
import org.sculptor.generator.chain.ChainOverride
import org.sculptor.generator.template.domain.DomainObjectAttributeAnnotationTmpl

@ChainOverride
class DomainObjectAttributeAnnotationTmplExtension extends DomainObjectAttributeAnnotationTmpl {
	
	@Inject extension DbHelper dbHelper
	@Inject extension PropertiesBase propertiesBase
	
	override String idAnnotations(Attribute it) {
		'''
			@javax.persistence.Id
			@javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.SEQUENCE, generator = "idSequence")
			@javax.persistence.SequenceGenerator(name = "idSequence", sequenceName="«getProperty("db.tablePrefix")»_GID_SEQ")
			@javax.persistence.Column(name="«it.getDatabaseName()»")
		'''
	}

}