package org.sculptor.generator.cartridge.lbc

import javax.inject.Inject
import org.sculptor.generator.ext.Helper
import org.sculptor.generator.ext.Properties
import org.sculptor.generator.template.common.ExceptionTmpl
import org.sculptor.generator.util.HelperBase
import sculptormetamodel.RepositoryOperation
import sculptormetamodel.Repository
import org.sculptor.generator.template.repository.RepositoryTmpl
import org.sculptor.generator.chain.ChainOverride

@ChainOverride
class RepositoryTmplExtension extends RepositoryTmpl {

	@Inject private var ExceptionTmpl exceptionTmpl

	@Inject extension HelperBase helperBase
	@Inject extension Helper helper
	@Inject extension Properties properties
	
	override def String genericBaseRepositoryMethod(RepositoryOperation it) {
	'''
		/**
		 * Delegates to {@link «genericAccessObjectInterface(name)»}
		 */
		«repositoryMethodAnnotation(it)»
		«IF it.useGenericAccessStrategy()»
			«it.getVisibilityLitteral()»«it.getTypeName()» «name»(«it.parameters.map[paramTypeAndName(it)].join(",")») «exceptionTmpl.throwsDecl(it)» {
				return «name»(«FOR param : parameters SEPARATOR ","»«param.name»«ENDFOR»«IF it.hasParameters()»,«ENDIF»getPersistentClass());
			}

			«it.getVisibilityLitteral()» <R> «it.getGenericResultTypeName()» «name»(«it.parameters.map[paramTypeAndName(it)].join(",")»«IF it.hasParameters()»,«ENDIF» Class<R> resultType) «exceptionTmpl.throwsDecl(it)» {
		«ELSE»
			«it.getVisibilityLitteral()»«it.getTypeName()» «name»(«it.parameters.map[paramTypeAndName(it)].join(",")») «exceptionTmpl.throwsDecl(it)» {
		«ENDIF»

			«/* TODO:implement a better solution */»
			«IF it.useGenericAccessStrategy()»
				«IF name == "findByKeys"»
					com.postnord.isp.utils.sculptor.jpa.accessimpl.FindByKeysAccessImpl<R> ao = new com.postnord.isp.utils.sculptor.jpa.accessimpl.FindByKeysAccessImpl<R>(resultType);
					ao.setEntityManager(getEntityManager());
				«ELSEIF name != "findByExample"»
					«genericAccessObjectInterface(name)»2<R> ao = create«getAccessNormalizedName()»(resultType);
				«ELSE»
					«genericAccessObjectInterface(name)»2<«it.getAggregateRootTypeName()»,R> ao = create«getAccessNormalizedName()»(«it.getAggregateRootTypeName()».class, resultType);
				«ENDIF»
			«ELSE»
				«genericAccessObjectInterface(name)»«it.getGenericType()» ao = create«getAccessNormalizedName()»();
			«ENDIF»
			«setCache(it)»
			«setEagerColumns(it)»
			«setOrdered(it)»
			«IF it.hasHint("useSingleResult")»
				ao.setUseSingleResult(true);
			«ENDIF»
			«IF name != "findByKey"»
				«/* TODO: why do you need to remove persistentClass from parameter list? */»
				«FOR parameter : parameters.filter[e | !(jpa() && e.name == "persistentClass")]»
					ao.set«parameter.name.toFirstUpper()»(«parameter.name»);
				«ENDFOR»
			«ENDIF»
			«findByKeysSpecialCase(it)»
			«findByKeySpecialCase(it)»
			ao.execute();
			«IF it.getTypeName() != "void" »
				«nullThrowsNotFoundExcpetion(it)»
				«IF (parameters.exists(e|e.name == "useSingleResult") || it.hasHint("useSingleResult")) && it.collectionType == null»
					return «IF !jpa() && it.getTypeName() != "Object"»(«it.getTypeName().getObjectTypeName()») «ENDIF»ao.getSingleResult();
				«ELSE»
					return ao.getResult();
				«ENDIF»
			«ENDIF»
		}
		«findByNaturalKeys(it) »
	'''
	}
	
	override def String repositoryDependencies(Repository it) {
		'''
		«FOR dependency : repositoryDependencies»
			«IF isSpringToBeGenerated()»
				@org.springframework.beans.factory.annotation.Autowired
			«ENDIF»
			«IF pureEjb3()»
				@javax.ejb.EJB
			«ENDIF»
			private «dependency.aggregateRoot.module.getRepositoryapiPackage()».«dependency.name» «dependency.name.toFirstLower()»;
	
			protected «dependency.aggregateRoot.module.getRepositoryapiPackage()».«dependency.name» get«dependency.name»() {
				return «dependency.name.toFirstLower()»;
			}
			
			/**
			* For mock purpose
			*
			*/
			public void set«dependency.name»(«dependency.aggregateRoot.module.getRepositoryapiPackage()».«dependency.name» «dependency.name.toFirstLower()») {
				this.«dependency.name.toFirstLower()» = «dependency.name.toFirstLower()»;
			}
		«ENDFOR»
		'''
	}
	
}