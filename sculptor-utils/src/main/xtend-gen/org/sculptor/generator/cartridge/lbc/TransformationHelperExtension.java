package org.sculptor.generator.cartridge.lbc;

import com.google.common.base.Objects;
import java.util.Arrays;
import javax.inject.Inject;
import org.eclipse.xtext.xbase.lib.Extension;
import org.sculptor.generator.chain.ChainOverride;
import org.sculptor.generator.ext.Helper;
import org.sculptor.generator.transform.TransformationHelper;
import org.sculptor.generator.util.PropertiesBase;
import sculptormetamodel.Attribute;
import sculptormetamodel.BasicType;
import sculptormetamodel.DomainObject;
import sculptormetamodel.NamedElement;
import sculptormetamodel.Reference;
import sculptormetamodel.ValueObject;

@ChainOverride
@SuppressWarnings("all")
public class TransformationHelperExtension extends TransformationHelper {
  @Inject
  @Extension
  private Helper helper;
  
  @Inject
  @Extension
  private PropertiesBase propertiesBase;
  
  @Override
  public void _modifyDatabaseColumn(final Attribute attribute) {
    TransformationHelper headObj = getMethodsDispatchHead()[org.sculptor.generator.transform.TransformationHelperMethodIndexes._MODIFYDATABASECOLUMN_ATTRIBUTE];
    headObj._chained__modifyDatabaseColumn(attribute);
  }
  
  @Override
  public void _modifyDatabaseNames(final DomainObject domainObject) {
    TransformationHelper headObj = getMethodsDispatchHead()[org.sculptor.generator.transform.TransformationHelperMethodIndexes._MODIFYDATABASENAMES_DOMAINOBJECT];
    headObj._chained__modifyDatabaseNames(domainObject);
  }
  
  @Override
  public void _modifyDatabaseNames(final ValueObject valueObject) {
    TransformationHelper headObj = getMethodsDispatchHead()[org.sculptor.generator.transform.TransformationHelperMethodIndexes._MODIFYDATABASENAMES_VALUEOBJECT];
    headObj._chained__modifyDatabaseNames(valueObject);
  }
  
  public void modifyDatabaseColumn(final NamedElement attribute) {
    if (attribute instanceof Attribute) {
      _modifyDatabaseColumn((Attribute)attribute);
      return;
    } else if (attribute instanceof Reference) {
      _modifyDatabaseColumn((Reference)attribute);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(attribute).toString());
    }
  }
  
  public void modifyDatabaseNames(final DomainObject valueObject) {
    if (valueObject instanceof BasicType) {
      _modifyDatabaseNames((BasicType)valueObject);
      return;
    } else if (valueObject instanceof ValueObject) {
      _modifyDatabaseNames((ValueObject)valueObject);
      return;
    } else if (valueObject != null) {
      _modifyDatabaseNames(valueObject);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(valueObject).toString());
    }
  }
  
  public TransformationHelperExtension(final TransformationHelper next, final TransformationHelper[] methodsDispatchNext) {
    super(next);
  }
  
  public void _chained__modifyDatabaseColumn(final Attribute attribute) {
    String _name = attribute.getName();
    boolean _equals = Objects.equal(_name, "id");
    if (_equals) {
      DomainObject _domainObject = this.helper.getDomainObject(attribute);
      String _databaseTable = _domainObject.getDatabaseTable();
      String _plus = (_databaseTable + "_GID");
      attribute.setDatabaseColumn(_plus);
    } else {
      TransformationHelper _next = this.getNext();
      _next.modifyDatabaseColumn(attribute);
    }
  }
  
  public void _chained__modifyDatabaseNames(final DomainObject domainObject) {
    TransformationHelper _next = this.getNext();
    _next.modifyDatabaseNames(domainObject);
    boolean _hasOwnDatabaseRepresentation = this.helper.hasOwnDatabaseRepresentation(domainObject);
    if (_hasOwnDatabaseRepresentation) {
      String _property = this.propertiesBase.getProperty("db.tablePrefix");
      String _plus = (_property + "_");
      String _databaseTable = domainObject.getDatabaseTable();
      String _plus_1 = (_plus + _databaseTable);
      domainObject.setDatabaseTable(_plus_1);
    }
  }
  
  public void _chained__modifyDatabaseNames(final ValueObject valueObject) {
    TransformationHelper _next = this.getNext();
    _next.modifyDatabaseNames(valueObject);
    boolean _isPersistent = valueObject.isPersistent();
    if (_isPersistent) {
      String _property = this.propertiesBase.getProperty("db.tablePrefix");
      String _plus = (_property + "_");
      String _databaseTable = valueObject.getDatabaseTable();
      String _plus_1 = (_plus + _databaseTable);
      valueObject.setDatabaseTable(_plus_1);
    }
  }
  
  public TransformationHelper[] _getOverridesDispatchArray() {
    org.sculptor.generator.transform.TransformationHelper[] result = new org.sculptor.generator.transform.TransformationHelper[org.sculptor.generator.transform.TransformationHelperMethodIndexes.NUM_METHODS];
    result[org.sculptor.generator.transform.TransformationHelperMethodIndexes._MODIFYDATABASECOLUMN_ATTRIBUTE] = this; 
    result[org.sculptor.generator.transform.TransformationHelperMethodIndexes._MODIFYDATABASENAMES_DOMAINOBJECT] = this; 
    result[org.sculptor.generator.transform.TransformationHelperMethodIndexes._MODIFYDATABASENAMES_VALUEOBJECT] = this; 
    return result;
  }
}
