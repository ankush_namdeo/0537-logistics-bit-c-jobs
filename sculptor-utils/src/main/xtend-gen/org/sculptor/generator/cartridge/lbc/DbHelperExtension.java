package org.sculptor.generator.cartridge.lbc;

import com.google.common.base.Objects;
import java.util.List;
import javax.inject.Inject;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.sculptor.generator.chain.ChainOverride;
import org.sculptor.generator.ext.DbHelper;
import org.sculptor.generator.util.DbHelperBase;
import org.sculptor.generator.util.PropertiesBase;
import sculptormetamodel.DomainObject;
import sculptormetamodel.Reference;

@ChainOverride
@SuppressWarnings("all")
public class DbHelperExtension extends DbHelper {
  @Inject
  @Extension
  private PropertiesBase propertiesBase;
  
  @Inject
  @Extension
  private DbHelperBase dbHelperBase;
  
  @Override
  public String truncateLongDatabaseName(final String part1, final String part2) {
    DbHelper headObj = getMethodsDispatchHead()[org.sculptor.generator.ext.DbHelperMethodIndexes.TRUNCATELONGDATABASENAME_STRING_STRING];
    return headObj._chained_truncateLongDatabaseName(part1, part2);
  }
  
  @Override
  public String getDefaultForeignKeyName(final Reference ref) {
    DbHelper headObj = getMethodsDispatchHead()[org.sculptor.generator.ext.DbHelperMethodIndexes.GETDEFAULTFOREIGNKEYNAME_REFERENCE];
    return headObj._chained_getDefaultForeignKeyName(ref);
  }
  
  /**
   * around extensions::dbhelper::getDefaultOppositeForeignKeyName(Reference ref) :
   * let defaultName = (String) ctx.proceed() :
   * defaultName.removeTablePrefix().removeDuplicateStart();
   */
  @Override
  public String getOppositeForeignKeyName(final Reference ref) {
    DbHelper headObj = getMethodsDispatchHead()[org.sculptor.generator.ext.DbHelperMethodIndexes.GETOPPOSITEFOREIGNKEYNAME_REFERENCE];
    return headObj._chained_getOppositeForeignKeyName(ref);
  }
  
  @Override
  public String getExtendsForeignKeyName(final DomainObject domainObject) {
    DbHelper headObj = getMethodsDispatchHead()[org.sculptor.generator.ext.DbHelperMethodIndexes.GETEXTENDSFOREIGNKEYNAME_DOMAINOBJECT];
    return headObj._chained_getExtendsForeignKeyName(domainObject);
  }
  
  /**
   * around extensions::dbhelper::getManyToManyJoinTableName(Reference ref) :
   * let defaultName = (String) ctx.proceed() :
   * getProperty("db.tablePrefix") + "_" + defaultName.removeAllTablePrefixes().removeAllGID();
   * 
   * override def resolveManyToManyRelations(Application application, boolean ascending) {
   * val domainObjects = next.resolveManyToManyRelations(application,ascending)
   * domainObjects.modifyDatabaseTablePrefixForManyToManyRelations() ->
   * domainObjects;
   * }
   * 
   * override def resolveManyToManyRelations(Application application, boolean ascending) {
   * next.resolveManyToManyRelations(application,ascending)
   * application.modules.domainObjects.forEach[modifyDatabaseTablePrefixForManyToManyRelations()]
   * }
   * 
   * 
   * def modifyDatabaseTablePrefixForManyToManyRelations(DomainObject domainObject) {
   * domainObject.setDatabaseTable(getProperty("db.tablePrefix") + "_" +
   * domainObject.databaseTable.removeAllTablePrefixes().removeAllGID()) ->
   * domainObject.references.modifyDatabaseTablePrefixForManyToManyRelation();
   * }
   */
  public void modifyDatabaseTablePrefixForManyToManyRelation(final Reference ref) {
    String _databaseColumn = ref.getDatabaseColumn();
    final String newName = this.removeTablePrefix(_databaseColumn);
    DomainObject _to = ref.getTo();
    String _databaseTable = _to.getDatabaseTable();
    final String tableNameWithoutPrefix = this.removeTablePrefix(_databaseTable);
    boolean _startsWith = newName.startsWith((((tableNameWithoutPrefix + "_") + tableNameWithoutPrefix) + "_"));
    if (_startsWith) {
      String _replaceFirst = newName.replaceFirst((tableNameWithoutPrefix + "_"), "");
      ref.setDatabaseColumn(_replaceFirst);
    } else {
      ref.setDatabaseColumn(newName);
    }
  }
  
  public String removeDuplicateStart(final String name) {
    String _xblockexpression = null;
    {
      final String newName = this.removeTablePrefix(name);
      final String[] split = newName.split("_");
      String _xifexpression = null;
      int _size = ((List<String>)Conversions.doWrapArray(split)).size();
      boolean _greaterThan = (_size > 1);
      if (_greaterThan) {
        _xifexpression = split[0];
      } else {
        _xifexpression = "";
      }
      final String start = _xifexpression;
      String _xifexpression_1 = null;
      int _size_1 = ((List<String>)Conversions.doWrapArray(split)).size();
      boolean _greaterEqualsThan = (_size_1 >= 4);
      if (_greaterEqualsThan) {
        String _get = split[0];
        String _plus = (_get + "_");
        String _get_1 = split[1];
        _xifexpression_1 = (_plus + _get_1);
      } else {
        _xifexpression_1 = "";
      }
      final String start2 = _xifexpression_1;
      String _xifexpression_2 = null;
      int _size_2 = ((List<String>)Conversions.doWrapArray(split)).size();
      boolean _greaterEqualsThan_1 = (_size_2 >= 6);
      if (_greaterEqualsThan_1) {
        String _get_2 = split[0];
        String _plus_1 = (_get_2 + "_");
        String _get_3 = split[1];
        String _plus_2 = (_plus_1 + _get_3);
        String _plus_3 = (_plus_2 + "_");
        String _get_4 = split[2];
        _xifexpression_2 = (_plus_3 + _get_4);
      } else {
        _xifexpression_2 = "";
      }
      final String start3 = _xifexpression_2;
      String _xifexpression_3 = null;
      boolean _and = false;
      boolean _notEquals = (!Objects.equal(start, ""));
      if (!_notEquals) {
        _and = false;
      } else {
        boolean _startsWith = newName.startsWith(((start + "_") + start));
        _and = _startsWith;
      }
      if (_and) {
        _xifexpression_3 = newName.replaceFirst((start + "_"), "");
      } else {
        String _xifexpression_4 = null;
        boolean _and_1 = false;
        boolean _notEquals_1 = (!Objects.equal(start2, ""));
        if (!_notEquals_1) {
          _and_1 = false;
        } else {
          boolean _startsWith_1 = newName.startsWith(((start2 + "_") + start2));
          _and_1 = _startsWith_1;
        }
        if (_and_1) {
          _xifexpression_4 = newName.replaceFirst((start2 + "_"), "");
        } else {
          String _xifexpression_5 = null;
          boolean _and_2 = false;
          boolean _notEquals_2 = (!Objects.equal(start3, ""));
          if (!_notEquals_2) {
            _and_2 = false;
          } else {
            boolean _startsWith_2 = newName.startsWith(((start3 + "_") + start3));
            _and_2 = _startsWith_2;
          }
          if (_and_2) {
            _xifexpression_5 = newName.replaceFirst((start3 + "_"), "");
          } else {
            _xifexpression_5 = newName;
          }
          _xifexpression_4 = _xifexpression_5;
        }
        _xifexpression_3 = _xifexpression_4;
      }
      _xblockexpression = _xifexpression_3;
    }
    return _xblockexpression;
  }
  
  public String removeTablePrefix(final String name) {
    String _xifexpression = null;
    String _property = this.propertiesBase.getProperty("db.tablePrefix");
    String _plus = (_property + "_");
    boolean _startsWith = name.startsWith(_plus);
    if (_startsWith) {
      String _property_1 = this.propertiesBase.getProperty("db.tablePrefix");
      String _plus_1 = (_property_1 + "_");
      _xifexpression = name.replaceFirst(_plus_1, "");
    } else {
      _xifexpression = name;
    }
    return _xifexpression;
  }
  
  public String removeAllTablePrefixes(final String name) {
    String _xblockexpression = null;
    {
      final String newName = this.removeTablePrefix(name);
      String _property = this.propertiesBase.getProperty("db.tablePrefix");
      String _plus = ("_" + _property);
      String _plus_1 = (_plus + "_");
      _xblockexpression = newName.replaceAll(_plus_1, "_");
    }
    return _xblockexpression;
  }
  
  public String removeAllGID(final String name) {
    String _xblockexpression = null;
    {
      final String newName = name.replaceAll("_GID_", "_");
      String _xifexpression = null;
      boolean _endsWith = newName.endsWith("_GID");
      if (_endsWith) {
        _xifexpression = newName.replaceAll("_GID", "");
      } else {
        _xifexpression = newName;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public DbHelperExtension(final DbHelper next, final DbHelper[] methodsDispatchNext) {
    super(next);
  }
  
  public String _chained_truncateLongDatabaseName(final String part1, final String part2) {
    return this.dbHelperBase.truncateLongDatabaseName(((part1 + "_") + part2), Integer.valueOf(27));
  }
  
  public String _chained_getDefaultForeignKeyName(final Reference ref) {
    String _xblockexpression = null;
    {
      DbHelper _next = this.getNext();
      final String defaultName = _next.getDefaultForeignKeyName(ref);
      String _removeTablePrefix = this.removeTablePrefix(defaultName);
      _xblockexpression = this.removeDuplicateStart(_removeTablePrefix);
    }
    return _xblockexpression;
  }
  
  /**
   * around extensions::dbhelper::getDefaultOppositeForeignKeyName(Reference ref) :
   * let defaultName = (String) ctx.proceed() :
   * defaultName.removeTablePrefix().removeDuplicateStart();
   */
  public String _chained_getOppositeForeignKeyName(final Reference ref) {
    String _xblockexpression = null;
    {
      DbHelper _next = this.getNext();
      final String defaultName = _next.getOppositeForeignKeyName(ref);
      String _removeTablePrefix = this.removeTablePrefix(defaultName);
      _xblockexpression = this.removeDuplicateStart(_removeTablePrefix);
    }
    return _xblockexpression;
  }
  
  public String _chained_getExtendsForeignKeyName(final DomainObject domainObject) {
    String _xblockexpression = null;
    {
      DbHelper _next = this.getNext();
      final String defaultName = _next.getExtendsForeignKeyName(domainObject);
      final String newName = this.removeTablePrefix(defaultName);
      String _databaseTable = domainObject.getDatabaseTable();
      final String tableNameWithoutPrefix = this.removeTablePrefix(_databaseTable);
      String _xifexpression = null;
      boolean _startsWith = newName.startsWith((((tableNameWithoutPrefix + "_") + tableNameWithoutPrefix) + "_"));
      if (_startsWith) {
        _xifexpression = newName.replaceFirst((tableNameWithoutPrefix + "_"), "");
      } else {
        _xifexpression = newName;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  public DbHelper[] _getOverridesDispatchArray() {
    org.sculptor.generator.ext.DbHelper[] result = new org.sculptor.generator.ext.DbHelper[org.sculptor.generator.ext.DbHelperMethodIndexes.NUM_METHODS];
    result[org.sculptor.generator.ext.DbHelperMethodIndexes.TRUNCATELONGDATABASENAME_STRING_STRING] = this; 
    result[org.sculptor.generator.ext.DbHelperMethodIndexes.GETDEFAULTFOREIGNKEYNAME_REFERENCE] = this; 
    result[org.sculptor.generator.ext.DbHelperMethodIndexes.GETOPPOSITEFOREIGNKEYNAME_REFERENCE] = this; 
    result[org.sculptor.generator.ext.DbHelperMethodIndexes.GETEXTENDSFOREIGNKEYNAME_DOMAINOBJECT] = this; 
    return result;
  }
}
