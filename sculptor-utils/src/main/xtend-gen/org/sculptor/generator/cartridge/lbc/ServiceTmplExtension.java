package org.sculptor.generator.cartridge.lbc;

import java.util.Collection;
import javax.inject.Inject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;
import org.sculptor.generator.chain.ChainOverride;
import org.sculptor.generator.ext.Helper;
import org.sculptor.generator.ext.Properties;
import org.sculptor.generator.template.common.ExceptionTmpl;
import org.sculptor.generator.template.common.PubSubTmpl;
import org.sculptor.generator.template.service.ServiceTmpl;
import org.sculptor.generator.util.HelperBase;
import sculptormetamodel.DomainObject;
import sculptormetamodel.Module;
import sculptormetamodel.Repository;
import sculptormetamodel.Service;

@ChainOverride
@SuppressWarnings("all")
public class ServiceTmplExtension extends ServiceTmpl {
  @Inject
  private ExceptionTmpl exceptionTmpl;
  
  @Inject
  private PubSubTmpl pubSubTmpl;
  
  @Inject
  @Extension
  private HelperBase helperBase;
  
  @Inject
  @Extension
  private Helper helper;
  
  @Inject
  @Extension
  private Properties properties;
  
  @Override
  public String delegateRepositories(final Service it) {
    ServiceTmpl headObj = getMethodsDispatchHead()[org.sculptor.generator.template.service.ServiceTmplMethodIndexes.DELEGATEREPOSITORIES_SERVICE];
    return headObj._chained_delegateRepositories(it);
  }
  
  public ServiceTmplExtension(final ServiceTmpl next, final ServiceTmpl[] methodsDispatchNext) {
    super(next);
  }
  
  public String _chained_delegateRepositories(final Service it) {
    StringConcatenation _builder = new StringConcatenation();
    {
      Collection<Repository> _delegateRepositories = this.helper.getDelegateRepositories(it);
      for(final Repository delegateRepository : _delegateRepositories) {
        {
          boolean _isSpringToBeGenerated = this.properties.isSpringToBeGenerated();
          if (_isSpringToBeGenerated) {
            _builder.append("@org.springframework.beans.factory.annotation.Autowired");
            _builder.newLine();
          }
        }
        {
          boolean _pureEjb3 = this.properties.pureEjb3();
          if (_pureEjb3) {
            _builder.append("@javax.ejb.EJB");
            _builder.newLine();
          }
        }
        _builder.append("private ");
        DomainObject _aggregateRoot = delegateRepository.getAggregateRoot();
        Module _module = _aggregateRoot.getModule();
        String _repositoryapiPackage = this.helperBase.getRepositoryapiPackage(_module);
        _builder.append(_repositoryapiPackage, "");
        _builder.append(".");
        String _name = delegateRepository.getName();
        _builder.append(_name, "");
        _builder.append(" ");
        String _name_1 = delegateRepository.getName();
        String _firstLower = this.helperBase.toFirstLower(_name_1);
        _builder.append(_firstLower, "");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.newLine();
        _builder.append("protected ");
        DomainObject _aggregateRoot_1 = delegateRepository.getAggregateRoot();
        Module _module_1 = _aggregateRoot_1.getModule();
        String _repositoryapiPackage_1 = this.helperBase.getRepositoryapiPackage(_module_1);
        _builder.append(_repositoryapiPackage_1, "");
        _builder.append(".");
        String _name_2 = delegateRepository.getName();
        _builder.append(_name_2, "");
        _builder.append(" get");
        String _name_3 = delegateRepository.getName();
        _builder.append(_name_3, "");
        _builder.append("() {");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("return ");
        String _name_4 = delegateRepository.getName();
        String _firstLower_1 = this.helperBase.toFirstLower(_name_4);
        _builder.append(_firstLower_1, "\t");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
        _builder.append("}");
        _builder.newLine();
        _builder.newLine();
        _builder.append("/**");
        _builder.newLine();
        _builder.append("* For mock purpose");
        _builder.newLine();
        _builder.append("*");
        _builder.newLine();
        _builder.append("*/");
        _builder.newLine();
        _builder.append("public void set");
        String _name_5 = delegateRepository.getName();
        _builder.append(_name_5, "");
        _builder.append("(");
        DomainObject _aggregateRoot_2 = delegateRepository.getAggregateRoot();
        Module _module_2 = _aggregateRoot_2.getModule();
        String _repositoryapiPackage_2 = this.helperBase.getRepositoryapiPackage(_module_2);
        _builder.append(_repositoryapiPackage_2, "");
        _builder.append(".");
        String _name_6 = delegateRepository.getName();
        _builder.append(_name_6, "");
        _builder.append(" ");
        String _name_7 = delegateRepository.getName();
        String _firstLower_2 = this.helperBase.toFirstLower(_name_7);
        _builder.append(_firstLower_2, "");
        _builder.append(") {");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("this.");
        String _name_8 = delegateRepository.getName();
        String _firstLower_3 = this.helperBase.toFirstLower(_name_8);
        _builder.append(_firstLower_3, "\t");
        _builder.append(" = ");
        String _name_9 = delegateRepository.getName();
        String _firstLower_4 = this.helperBase.toFirstLower(_name_9);
        _builder.append(_firstLower_4, "\t");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
        _builder.append("}");
        _builder.newLine();
      }
    }
    return _builder.toString();
  }
  
  public ServiceTmpl[] _getOverridesDispatchArray() {
    org.sculptor.generator.template.service.ServiceTmpl[] result = new org.sculptor.generator.template.service.ServiceTmpl[org.sculptor.generator.template.service.ServiceTmplMethodIndexes.NUM_METHODS];
    result[org.sculptor.generator.template.service.ServiceTmplMethodIndexes.DELEGATEREPOSITORIES_SERVICE] = this; 
    return result;
  }
}
