package org.sculptor.generator.cartridge.lbc;

import javax.inject.Inject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;
import org.sculptor.generator.chain.ChainOverride;
import org.sculptor.generator.template.db.OracleDDLTmpl;
import org.sculptor.generator.util.PropertiesBase;
import sculptormetamodel.Application;

@ChainOverride
@SuppressWarnings("all")
public class OracleDDLTmplExtension extends OracleDDLTmpl {
  @Inject
  @Extension
  private PropertiesBase propertiesBase;
  
  @Override
  public String dropSequence(final Application it) {
    OracleDDLTmpl headObj = getMethodsDispatchHead()[org.sculptor.generator.template.db.OracleDDLTmplMethodIndexes.DROPSEQUENCE_APPLICATION];
    return headObj._chained_dropSequence(it);
  }
  
  @Override
  public String createSequence(final Application it) {
    OracleDDLTmpl headObj = getMethodsDispatchHead()[org.sculptor.generator.template.db.OracleDDLTmplMethodIndexes.CREATESEQUENCE_APPLICATION];
    return headObj._chained_createSequence(it);
  }
  
  public OracleDDLTmplExtension(final OracleDDLTmpl next, final OracleDDLTmpl[] methodsDispatchNext) {
    super(next);
  }
  
  public String _chained_dropSequence(final Application it) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("DROP SEQUENCE ");
    String _property = this.propertiesBase.getProperty("db.tablePrefix");
    _builder.append(_property, "");
    _builder.append("_GID_SEQ;");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }
  
  public String _chained_createSequence(final Application it) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("CREATE SEQUENCE ");
    String _property = this.propertiesBase.getProperty("db.tablePrefix");
    _builder.append(_property, "");
    _builder.append("_GID_SEQ;");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }
  
  public OracleDDLTmpl[] _getOverridesDispatchArray() {
    org.sculptor.generator.template.db.OracleDDLTmpl[] result = new org.sculptor.generator.template.db.OracleDDLTmpl[org.sculptor.generator.template.db.OracleDDLTmplMethodIndexes.NUM_METHODS];
    result[org.sculptor.generator.template.db.OracleDDLTmplMethodIndexes.DROPSEQUENCE_APPLICATION] = this; 
    result[org.sculptor.generator.template.db.OracleDDLTmplMethodIndexes.CREATESEQUENCE_APPLICATION] = this; 
    return result;
  }
}
