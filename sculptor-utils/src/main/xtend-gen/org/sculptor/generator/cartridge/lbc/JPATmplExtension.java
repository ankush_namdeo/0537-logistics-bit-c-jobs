package org.sculptor.generator.cartridge.lbc;

import com.google.common.base.Objects;
import javax.inject.Inject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;
import org.sculptor.generator.chain.ChainOverride;
import org.sculptor.generator.ext.Helper;
import org.sculptor.generator.ext.Properties;
import org.sculptor.generator.template.jpa.JPATmpl;
import org.sculptor.generator.util.PropertiesBase;
import sculptormetamodel.Application;

@ChainOverride
@SuppressWarnings("all")
public class JPATmplExtension extends JPATmpl {
  @Inject
  @Extension
  private Helper helper;
  
  @Inject
  @Extension
  private Properties properties;
  
  @Inject
  @Extension
  private PropertiesBase propBase;
  
  @Override
  public String persistenceUnitDataSource(final Application it, final String unitName) {
    JPATmpl headObj = getMethodsDispatchHead()[org.sculptor.generator.template.jpa.JPATmplMethodIndexes.PERSISTENCEUNITDATASOURCE_APPLICATION_STRING];
    return headObj._chained_persistenceUnitDataSource(it, unitName);
  }
  
  @Override
  public String persistenceUnitPropertiesHibernate(final Application it, final String unitName) {
    JPATmpl headObj = getMethodsDispatchHead()[org.sculptor.generator.template.jpa.JPATmplMethodIndexes.PERSISTENCEUNITPROPERTIESHIBERNATE_APPLICATION_STRING];
    return headObj._chained_persistenceUnitPropertiesHibernate(it, unitName);
  }
  
  public JPATmplExtension(final JPATmpl next, final JPATmpl[] methodsDispatchNext) {
    super(next);
  }
  
  public String _chained_persistenceUnitDataSource(final Application it, final String unitName) {
    String _xblockexpression = null;
    {
      String _xifexpression = null;
      boolean _isDefaultPersistenceUnitName = this.helper.isDefaultPersistenceUnitName(it, unitName);
      if (_isDefaultPersistenceUnitName) {
        _xifexpression = this.helper.dataSourceName(it);
      } else {
        _xifexpression = this.helper.dataSourceName(it, unitName);
      }
      final String dataSourceName = _xifexpression;
      StringConcatenation _builder = new StringConcatenation();
      {
        boolean _isEar = this.properties.isEar();
        if (_isEar) {
          {
            String _applicationServer = this.properties.applicationServer();
            boolean _equals = Objects.equal(_applicationServer, "jboss");
            if (_equals) {
              _builder.append("<jta-data-source>java:jboss/datasources/");
              _builder.append(dataSourceName, "");
              _builder.append("</jta-data-source>");
              _builder.newLineIfNotEmpty();
            } else {
              {
                boolean _isSpringDataSourceSupportToBeGenerated = this.properties.isSpringDataSourceSupportToBeGenerated();
                boolean _not = (!_isSpringDataSourceSupportToBeGenerated);
                if (_not) {
                  _builder.append("<jta-data-source>java:comp/env/jdbc/");
                  _builder.append(dataSourceName, "");
                  _builder.append("</jta-data-source>");
                  _builder.newLineIfNotEmpty();
                }
              }
            }
          }
        } else {
          boolean _isWar = this.properties.isWar();
          if (_isWar) {
            {
              String _applicationServer_1 = this.properties.applicationServer();
              boolean _equals_1 = Objects.equal(_applicationServer_1, "appengine");
              if (_equals_1) {
              } else {
                String _applicationServer_2 = this.properties.applicationServer();
                boolean _equals_2 = Objects.equal(_applicationServer_2, "jboss");
                if (_equals_2) {
                  _builder.append("<non-jta-data-source>java:jboss/datasources/");
                  _builder.append(dataSourceName, "");
                  _builder.append("</non-jta-data-source>");
                  _builder.newLineIfNotEmpty();
                } else {
                  {
                    boolean _isSpringDataSourceSupportToBeGenerated_1 = this.properties.isSpringDataSourceSupportToBeGenerated();
                    boolean _not_1 = (!_isSpringDataSourceSupportToBeGenerated_1);
                    if (_not_1) {
                      _builder.append("<non-jta-data-source>java:comp/env/jdbc/");
                      _builder.append(dataSourceName, "");
                      _builder.append("</non-jta-data-source>");
                      _builder.newLineIfNotEmpty();
                    }
                  }
                }
              }
            }
          }
        }
      }
      _xblockexpression = _builder.toString();
    }
    return _xblockexpression;
  }
  
  public String _chained_persistenceUnitPropertiesHibernate(final Application it, final String unitName) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<property name=\"hibernate.dialect\" value=\"");
    String _hibernateDialect = this.propBase.getHibernateDialect();
    _builder.append(_hibernateDialect, "");
    _builder.append("\" />");
    _builder.newLineIfNotEmpty();
    _builder.append("<property name=\"hibernate.id.new_generator_mappings\" value=\"false\" />");
    _builder.newLine();
    _builder.append("<property name=\"query.substitutions\" value=\"true 1, false 0\" />");
    _builder.newLine();
    _builder.newLine();
    {
      String _dbProduct = this.propBase.getDbProduct();
      boolean _equals = Objects.equal(_dbProduct, "hsqldb-inmemory");
      if (_equals) {
        _builder.append("<property name=\"hibernate.show_sql\" value=\"true\" />");
        _builder.newLine();
        _builder.append("<property name=\"hibernate.hbm2ddl.auto\" value=\"create-drop\" />");
        _builder.newLine();
      }
    }
    String _persistenceUnitCacheProperties = this.persistenceUnitCacheProperties(it, unitName);
    _builder.append(_persistenceUnitCacheProperties, "");
    _builder.newLineIfNotEmpty();
    {
      boolean _isEar = this.properties.isEar();
      if (_isEar) {
        String _persistenceUnitTransactionProperties = this.persistenceUnitTransactionProperties(it, unitName);
        _builder.append(_persistenceUnitTransactionProperties, "");
        _builder.newLineIfNotEmpty();
        {
          boolean _or = false;
          boolean _isSpringDataSourceSupportToBeGenerated = this.properties.isSpringDataSourceSupportToBeGenerated();
          boolean _not = (!_isSpringDataSourceSupportToBeGenerated);
          if (_not) {
            _or = true;
          } else {
            String _applicationServer = this.properties.applicationServer();
            boolean _equals_1 = Objects.equal(_applicationServer, "jboss");
            _or = _equals_1;
          }
          if (_or) {
            _builder.append("<!-- Bind entity manager factory to JNDI -->");
            _builder.newLine();
            _builder.append("<property name=\"jboss.entity.manager.factory.jndi.name\" value=\"java:jboss/");
            _builder.append(unitName, "");
            _builder.append("\"/>");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    return _builder.toString();
  }
  
  public JPATmpl[] _getOverridesDispatchArray() {
    org.sculptor.generator.template.jpa.JPATmpl[] result = new org.sculptor.generator.template.jpa.JPATmpl[org.sculptor.generator.template.jpa.JPATmplMethodIndexes.NUM_METHODS];
    result[org.sculptor.generator.template.jpa.JPATmplMethodIndexes.PERSISTENCEUNITDATASOURCE_APPLICATION_STRING] = this; 
    result[org.sculptor.generator.template.jpa.JPATmplMethodIndexes.PERSISTENCEUNITPROPERTIESHIBERNATE_APPLICATION_STRING] = this; 
    return result;
  }
}
