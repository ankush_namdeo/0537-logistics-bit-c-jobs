package org.sculptor.generator.cartridge.lbc;

import com.google.common.base.Objects;
import java.util.List;
import javax.inject.Inject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;
import org.sculptor.generator.chain.ChainOverride;
import org.sculptor.generator.ext.Helper;
import org.sculptor.generator.ext.Properties;
import org.sculptor.generator.template.domain.DomainObjectKeyTmpl;
import org.sculptor.generator.util.HelperBase;
import sculptormetamodel.Attribute;
import sculptormetamodel.DomainObject;
import sculptormetamodel.NamedElement;
import sculptormetamodel.Reference;

@ChainOverride
@SuppressWarnings("all")
public class DomainObjectKeyTmplExtension extends DomainObjectKeyTmpl {
  @Inject
  @Extension
  private HelperBase helperBase;
  
  @Inject
  @Extension
  private Helper helper;
  
  @Inject
  @Extension
  private Properties properties;
  
  @Override
  public String compositeKeyGetter(final DomainObject it) {
    DomainObjectKeyTmpl headObj = getMethodsDispatchHead()[org.sculptor.generator.template.domain.DomainObjectKeyTmplMethodIndexes.COMPOSITEKEYGETTER_DOMAINOBJECT];
    return headObj._chained_compositeKeyGetter(it);
  }
  
  public DomainObjectKeyTmplExtension(final DomainObjectKeyTmpl next, final DomainObjectKeyTmpl[] methodsDispatchNext) {
    super(next);
  }
  
  public String _chained_compositeKeyGetter(final DomainObject it) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/**");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("* This method is used by equals and hashCode.");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("* @return {@link #get");
    String _name = it.getName();
    _builder.append(_name, " ");
    _builder.append("Key}");
    _builder.newLineIfNotEmpty();
    _builder.append(" ");
    _builder.append("*/");
    _builder.newLine();
    {
      boolean _and = false;
      boolean _isJpaAnnotationToBeGenerated = this.properties.isJpaAnnotationToBeGenerated();
      if (!_isJpaAnnotationToBeGenerated) {
        _and = false;
      } else {
        boolean _isJpaAnnotationOnFieldToBeGenerated = this.properties.isJpaAnnotationOnFieldToBeGenerated();
        boolean _not = (!_isJpaAnnotationOnFieldToBeGenerated);
        _and = _not;
      }
      if (_and) {
        _builder.append("@javax.persistence.Transient");
        _builder.newLine();
      }
    }
    _builder.append("public Object getKey() {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return get");
    String _name_1 = it.getName();
    _builder.append(_name_1, "\t");
    _builder.append("Key();");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    {
      boolean _and_1 = false;
      boolean _isJpaAnnotationToBeGenerated_1 = this.properties.isJpaAnnotationToBeGenerated();
      if (!_isJpaAnnotationToBeGenerated_1) {
        _and_1 = false;
      } else {
        boolean _isJpaAnnotationOnFieldToBeGenerated_1 = this.properties.isJpaAnnotationOnFieldToBeGenerated();
        _and_1 = _isJpaAnnotationOnFieldToBeGenerated_1;
      }
      if (_and_1) {
        _builder.append("@javax.persistence.Transient");
        _builder.newLine();
      }
    }
    _builder.append("private transient ");
    String _name_2 = it.getName();
    _builder.append(_name_2, "");
    _builder.append("Key cached");
    String _name_3 = it.getName();
    _builder.append(_name_3, "");
    _builder.append("Key;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("/**");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("* The natural key for the domain object is");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("* a composite key consisting of several attributes.");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*/");
    _builder.newLine();
    {
      boolean _and_2 = false;
      boolean _isJpaAnnotationToBeGenerated_2 = this.properties.isJpaAnnotationToBeGenerated();
      if (!_isJpaAnnotationToBeGenerated_2) {
        _and_2 = false;
      } else {
        boolean _isJpaAnnotationOnFieldToBeGenerated_2 = this.properties.isJpaAnnotationOnFieldToBeGenerated();
        boolean _not_1 = (!_isJpaAnnotationOnFieldToBeGenerated_2);
        _and_2 = _not_1;
      }
      if (_and_2) {
        _builder.append("@javax.persistence.Transient");
        _builder.newLine();
      }
    }
    _builder.append("public ");
    String _name_4 = it.getName();
    _builder.append(_name_4, "");
    _builder.append("Key get");
    String _name_5 = it.getName();
    _builder.append(_name_5, "");
    _builder.append("Key() {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("if (cached");
    String _name_6 = it.getName();
    _builder.append(_name_6, "\t");
    _builder.append("Key == null) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("// Andreas 20100930:");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// This implementation is a bug fix for sculptor 1.9.0 in combination");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// with a buggy hibernate. Sometimes when hibernate loads database");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// entities, it puts them in a HashSet before all attributes are set.");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// This invokes the equals() method which will create the cachedItemKey");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// but with null values for the key attributes. Since the key is cached, ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// the entity is unusable later on because the key is broken...");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// This is fixed in a later hibernate release");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// Nissemark 20150604:");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("// Same issue with 3.1.0 and hibernate 4.2");
    _builder.newLine();
    {
      List<Attribute> _allNaturalKeyAttributes = this.helper.getAllNaturalKeyAttributes(it);
      boolean _isEmpty = _allNaturalKeyAttributes.isEmpty();
      boolean _not_2 = (!_isEmpty);
      if (_not_2) {
        _builder.append("\t\t");
        _builder.append("if (");
        {
          List<Attribute> _allNaturalKeyAttributes_1 = this.helper.getAllNaturalKeyAttributes(it);
          boolean _hasElements = false;
          for(final Attribute a : _allNaturalKeyAttributes_1) {
            if (!_hasElements) {
              _hasElements = true;
            } else {
              _builder.appendImmediate(" && ", "\t\t");
            }
            _builder.newLineIfNotEmpty();
            _builder.append("\t\t");
            {
              String _typeName = this.helperBase.getTypeName(a);
              boolean _equals = Objects.equal(_typeName, "boolean");
              if (_equals) {
                _builder.append("!");
                String _getAccessor = this.helper.getGetAccessor(a);
                _builder.append(_getAccessor, "\t\t");
                _builder.append("()");
                _builder.newLineIfNotEmpty();
                _builder.append("\t\t");
              } else {
                String _typeName_1 = this.helperBase.getTypeName(a);
                boolean _equals_1 = Objects.equal(_typeName_1, "int");
                if (_equals_1) {
                  String _getAccessor_1 = this.helper.getGetAccessor(a);
                  _builder.append(_getAccessor_1, "\t\t");
                  _builder.append("() == 0");
                  _builder.newLineIfNotEmpty();
                  _builder.append("\t        \t\t\t\t");
                } else {
                  String _typeName_2 = this.helperBase.getTypeName(a);
                  boolean _equals_2 = Objects.equal(_typeName_2, "long");
                  if (_equals_2) {
                    String _getAccessor_2 = this.helper.getGetAccessor(a);
                    _builder.append(_getAccessor_2, "\t\t");
                    _builder.append("() == 0L");
                    _builder.newLineIfNotEmpty();
                    _builder.append("\t        \t\t\t\t");
                  } else {
                    String _getAccessor_3 = this.helper.getGetAccessor(a);
                    _builder.append(_getAccessor_3, "\t\t");
                    _builder.append("() == null");
                    _builder.newLineIfNotEmpty();
                  }
                }
              }
            }
          }
        }
        {
          List<Reference> _allNaturalKeyReferences = this.helper.getAllNaturalKeyReferences(it);
          boolean _isEmpty_1 = _allNaturalKeyReferences.isEmpty();
          boolean _not_3 = (!_isEmpty_1);
          if (_not_3) {
            _builder.append("\t        \t\t\t\t");
            _builder.append("&& ");
            {
              List<Reference> _allNaturalKeyReferences_1 = this.helper.getAllNaturalKeyReferences(it);
              boolean _hasElements_1 = false;
              for(final Reference b : _allNaturalKeyReferences_1) {
                if (!_hasElements_1) {
                  _hasElements_1 = true;
                } else {
                  _builder.appendImmediate(" && ", "\t        \t\t\t\t");
                }
                _builder.newLineIfNotEmpty();
                _builder.append("\t        \t\t\t\t");
                String _getAccessor_4 = this.helper.getGetAccessor(b);
                _builder.append(_getAccessor_4, "\t        \t\t\t\t");
                _builder.append("() == null");
                _builder.newLineIfNotEmpty();
              }
            }
            _builder.append("\t        \t\t\t\t");
          }
        }
        _builder.append("){");
        _builder.newLineIfNotEmpty();
        _builder.append("\t        \t\t\t\t");
        _builder.append("return new ");
        String _name_7 = it.getName();
        _builder.append(_name_7, "\t        \t\t\t\t");
        _builder.append("Key(");
        {
          List<NamedElement> _allNaturalKeys = this.helper.getAllNaturalKeys(it);
          boolean _hasElements_2 = false;
          for(final NamedElement aa : _allNaturalKeys) {
            if (!_hasElements_2) {
              _hasElements_2 = true;
            } else {
              _builder.appendImmediate(",", "\t        \t\t\t\t");
            }
            String _getAccessor_5 = this.helper.getGetAccessor(aa);
            _builder.append(_getAccessor_5, "\t        \t\t\t\t");
            _builder.append("()");
          }
        }
        _builder.append(");");
        _builder.newLineIfNotEmpty();
        _builder.append("\t        \t\t\t\t");
        _builder.append("}");
        _builder.newLine();
      } else {
        _builder.append("if (");
        {
          List<NamedElement> _allNaturalKeys_1 = this.helper.getAllNaturalKeys(it);
          boolean _hasElements_3 = false;
          for(final NamedElement a_1 : _allNaturalKeys_1) {
            if (!_hasElements_3) {
              _hasElements_3 = true;
            } else {
              _builder.appendImmediate(" && ", "");
            }
            _builder.newLineIfNotEmpty();
            String _getAccessor_6 = this.helper.getGetAccessor(a_1);
            _builder.append(_getAccessor_6, "");
            _builder.append("() == null");
            _builder.newLineIfNotEmpty();
            _builder.append("\t        \t\t\t\t");
          }
        }
        _builder.append("){");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("return new ");
        String _name_8 = it.getName();
        _builder.append(_name_8, "\t");
        _builder.append("Key(");
        {
          List<NamedElement> _allNaturalKeys_2 = this.helper.getAllNaturalKeys(it);
          boolean _hasElements_4 = false;
          for(final NamedElement aa_1 : _allNaturalKeys_2) {
            if (!_hasElements_4) {
              _hasElements_4 = true;
            } else {
              _builder.appendImmediate(",", "\t");
            }
            String _getAccessor_7 = this.helper.getGetAccessor(aa_1);
            _builder.append(_getAccessor_7, "\t");
            _builder.append("()");
          }
        }
        _builder.append(");");
        _builder.newLineIfNotEmpty();
        _builder.append("}");
        _builder.newLine();
      }
    }
    _builder.append("\t        \t\t");
    _builder.newLine();
    _builder.append("\t        \t\t");
    _builder.append("cached");
    String _name_9 = it.getName();
    _builder.append(_name_9, "\t        \t\t");
    _builder.append("Key = new ");
    String _name_10 = it.getName();
    _builder.append(_name_10, "\t        \t\t");
    _builder.append("Key(");
    {
      List<NamedElement> _allNaturalKeys_3 = this.helper.getAllNaturalKeys(it);
      boolean _hasElements_5 = false;
      for(final NamedElement a_2 : _allNaturalKeys_3) {
        if (!_hasElements_5) {
          _hasElements_5 = true;
        } else {
          _builder.appendImmediate(",", "\t        \t\t");
        }
        String _getAccessor_8 = this.helper.getGetAccessor(a_2);
        _builder.append(_getAccessor_8, "\t        \t\t");
        _builder.append("()");
      }
    }
    _builder.append(");");
    _builder.newLineIfNotEmpty();
    _builder.append("\t        \t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return cached");
    String _name_11 = it.getName();
    _builder.append(_name_11, "\t");
    _builder.append("Key;");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    String _compositeKey = this.compositeKey(it);
    _builder.append(_compositeKey, "");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }
  
  public DomainObjectKeyTmpl[] _getOverridesDispatchArray() {
    org.sculptor.generator.template.domain.DomainObjectKeyTmpl[] result = new org.sculptor.generator.template.domain.DomainObjectKeyTmpl[org.sculptor.generator.template.domain.DomainObjectKeyTmplMethodIndexes.NUM_METHODS];
    result[org.sculptor.generator.template.domain.DomainObjectKeyTmplMethodIndexes.COMPOSITEKEYGETTER_DOMAINOBJECT] = this; 
    return result;
  }
}
