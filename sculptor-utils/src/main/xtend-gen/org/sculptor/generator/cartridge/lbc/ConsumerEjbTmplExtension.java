package org.sculptor.generator.cartridge.lbc;

import com.google.common.base.Objects;
import javax.inject.Inject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;
import org.sculptor.generator.chain.ChainOverride;
import org.sculptor.generator.ext.Properties;
import org.sculptor.generator.template.consumer.ConsumerEjbTmpl;
import org.sculptor.generator.util.PropertiesBase;
import sculptormetamodel.Consumer;

@ChainOverride
@SuppressWarnings("all")
public class ConsumerEjbTmplExtension extends ConsumerEjbTmpl {
  @Inject
  @Extension
  private Properties properties;
  
  @Inject
  @Extension
  private PropertiesBase propertiesBase;
  
  @Override
  public String jmsConnection(final Consumer it) {
    ConsumerEjbTmpl headObj = getMethodsDispatchHead()[org.sculptor.generator.template.consumer.ConsumerEjbTmplMethodIndexes.JMSCONNECTION_CONSUMER];
    return headObj._chained_jmsConnection(it);
  }
  
  @Override
  public String invalidMessageQueue(final Consumer it) {
    ConsumerEjbTmpl headObj = getMethodsDispatchHead()[org.sculptor.generator.template.consumer.ConsumerEjbTmplMethodIndexes.INVALIDMESSAGEQUEUE_CONSUMER];
    return headObj._chained_invalidMessageQueue(it);
  }
  
  public ConsumerEjbTmplExtension(final ConsumerEjbTmpl next, final ConsumerEjbTmpl[] methodsDispatchNext) {
    super(next);
  }
  
  public String _chained_jmsConnection(final Consumer it) {
    StringConcatenation _builder = new StringConcatenation();
    {
      String _applicationServer = this.properties.applicationServer();
      boolean _equals = Objects.equal(_applicationServer, "jboss");
      if (_equals) {
        _builder.append("@javax.annotation.Resource(mappedName = \"java:jboss/activemq/ConnectionFactory\")");
        _builder.newLine();
      } else {
        _builder.append("@javax.annotation.Resource(mappedName = \"java:/jms/QueueFactory\")");
        _builder.newLine();
      }
    }
    _builder.append("private javax.jms.ConnectionFactory connectionFactory;");
    _builder.newLine();
    _builder.append("private javax.jms.Connection connection;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@Override");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("protected javax.jms.Connection getJmsConnection() {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("try {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("if (connection == null) {");
    _builder.newLine();
    _builder.append("\t\t\t    ");
    _builder.append("connection = connectionFactory.createConnection();");
    _builder.newLine();
    _builder.append("\t\t\t    ");
    _builder.append("connection.start();");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("return connection;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("} catch (Exception e) {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("getLog().error(\"Can\'t create JmsConnection: \" + e.getMessage(), e);");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("return null;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@javax.annotation.PreDestroy");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public void ejbRemove() {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("closeConnection();");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    _builder.append("@Override");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("protected void closeConnection() {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("try {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("if (connection != null) {");
    _builder.newLine();
    _builder.append("\t\t\t    ");
    _builder.append("connection.close();");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("} catch (Exception ignore) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("} finally {");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("connection = null;");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }
  
  public String _chained_invalidMessageQueue(final Consumer it) {
    StringConcatenation _builder = new StringConcatenation();
    {
      String _applicationServer = this.properties.applicationServer();
      boolean _equals = Objects.equal(_applicationServer, "jboss");
      if (_equals) {
        {
          boolean _hasProperty = this.propertiesBase.hasProperty("invalidMessageQueue");
          if (_hasProperty) {
            _builder.append("@javax.annotation.Resource(mappedName = \"java:/jboss/activemq/queue/queue/");
            String _property = this.propertiesBase.getProperty("invalidMessageQueue");
            _builder.append(_property, "");
            _builder.append("\")");
            _builder.newLineIfNotEmpty();
          } else {
            _builder.append("@javax.annotation.Resource(mappedName = \"java:/jboss/activemq/queue/queue/jms.invalidMessageQueue\")");
            _builder.newLine();
          }
        }
        _builder.newLine();
      } else {
        _builder.append("@javax.annotation.Resource(mappedName = \"java:/jms/invalidMessageQueue\")");
        _builder.newLine();
      }
    }
    _builder.append("private javax.jms.Queue invalidMessageQueue;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("protected javax.jms.Destination getInvalidMessageQueue() {");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("return invalidMessageQueue;");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }
  
  public ConsumerEjbTmpl[] _getOverridesDispatchArray() {
    org.sculptor.generator.template.consumer.ConsumerEjbTmpl[] result = new org.sculptor.generator.template.consumer.ConsumerEjbTmpl[org.sculptor.generator.template.consumer.ConsumerEjbTmplMethodIndexes.NUM_METHODS];
    result[org.sculptor.generator.template.consumer.ConsumerEjbTmplMethodIndexes.JMSCONNECTION_CONSUMER] = this; 
    result[org.sculptor.generator.template.consumer.ConsumerEjbTmplMethodIndexes.INVALIDMESSAGEQUEUE_CONSUMER] = this; 
    return result;
  }
}
