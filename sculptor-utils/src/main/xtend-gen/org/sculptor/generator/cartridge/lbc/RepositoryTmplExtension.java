package org.sculptor.generator.cartridge.lbc;

import com.google.common.base.Objects;
import java.util.List;
import javax.inject.Inject;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.sculptor.generator.chain.ChainOverride;
import org.sculptor.generator.ext.Helper;
import org.sculptor.generator.ext.Properties;
import org.sculptor.generator.template.common.ExceptionTmpl;
import org.sculptor.generator.template.repository.RepositoryTmpl;
import org.sculptor.generator.util.HelperBase;
import sculptormetamodel.DomainObject;
import sculptormetamodel.Module;
import sculptormetamodel.Parameter;
import sculptormetamodel.Repository;
import sculptormetamodel.RepositoryOperation;

@ChainOverride
@SuppressWarnings("all")
public class RepositoryTmplExtension extends RepositoryTmpl {
  @Inject
  private ExceptionTmpl exceptionTmpl;
  
  @Inject
  @Extension
  private HelperBase helperBase;
  
  @Inject
  @Extension
  private Helper helper;
  
  @Inject
  @Extension
  private Properties properties;
  
  @Override
  public String genericBaseRepositoryMethod(final RepositoryOperation it) {
    RepositoryTmpl headObj = getMethodsDispatchHead()[org.sculptor.generator.template.repository.RepositoryTmplMethodIndexes.GENERICBASEREPOSITORYMETHOD_REPOSITORYOPERATION];
    return headObj._chained_genericBaseRepositoryMethod(it);
  }
  
  @Override
  public String repositoryDependencies(final Repository it) {
    RepositoryTmpl headObj = getMethodsDispatchHead()[org.sculptor.generator.template.repository.RepositoryTmplMethodIndexes.REPOSITORYDEPENDENCIES_REPOSITORY];
    return headObj._chained_repositoryDependencies(it);
  }
  
  public RepositoryTmplExtension(final RepositoryTmpl next, final RepositoryTmpl[] methodsDispatchNext) {
    super(next);
  }
  
  public String _chained_genericBaseRepositoryMethod(final RepositoryOperation it) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("/**");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("* Delegates to {@link ");
    String _name = it.getName();
    String _genericAccessObjectInterface = this.properties.genericAccessObjectInterface(_name);
    _builder.append(_genericAccessObjectInterface, " ");
    _builder.append("}");
    _builder.newLineIfNotEmpty();
    _builder.append(" ");
    _builder.append("*/");
    _builder.newLine();
    String _repositoryMethodAnnotation = this.repositoryMethodAnnotation(it);
    _builder.append(_repositoryMethodAnnotation, "");
    _builder.newLineIfNotEmpty();
    {
      boolean _useGenericAccessStrategy = this.helper.useGenericAccessStrategy(it);
      if (_useGenericAccessStrategy) {
        String _visibilityLitteral = this.helper.getVisibilityLitteral(it);
        _builder.append(_visibilityLitteral, "");
        String _typeName = this.helperBase.getTypeName(it);
        _builder.append(_typeName, "");
        _builder.append(" ");
        String _name_1 = it.getName();
        _builder.append(_name_1, "");
        _builder.append("(");
        EList<Parameter> _parameters = it.getParameters();
        final Function1<Parameter, String> _function = new Function1<Parameter, String>() {
          @Override
          public String apply(final Parameter it) {
            return RepositoryTmplExtension.this.paramTypeAndName(it);
          }
        };
        List<String> _map = ListExtensions.<Parameter, String>map(_parameters, _function);
        String _join = IterableExtensions.join(_map, ",");
        _builder.append(_join, "");
        _builder.append(") ");
        String _throwsDecl = this.exceptionTmpl.throwsDecl(it);
        _builder.append(_throwsDecl, "");
        _builder.append(" {");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("return ");
        String _name_2 = it.getName();
        _builder.append(_name_2, "\t");
        _builder.append("(");
        {
          EList<Parameter> _parameters_1 = it.getParameters();
          boolean _hasElements = false;
          for(final Parameter param : _parameters_1) {
            if (!_hasElements) {
              _hasElements = true;
            } else {
              _builder.appendImmediate(",", "\t");
            }
            String _name_3 = param.getName();
            _builder.append(_name_3, "\t");
          }
        }
        {
          boolean _hasParameters = this.helper.hasParameters(it);
          if (_hasParameters) {
            _builder.append(",");
          }
        }
        _builder.append("getPersistentClass());");
        _builder.newLineIfNotEmpty();
        _builder.append("}");
        _builder.newLine();
        _builder.newLine();
        String _visibilityLitteral_1 = this.helper.getVisibilityLitteral(it);
        _builder.append(_visibilityLitteral_1, "");
        _builder.append(" <R> ");
        String _genericResultTypeName = this.helper.getGenericResultTypeName(it);
        _builder.append(_genericResultTypeName, "");
        _builder.append(" ");
        String _name_4 = it.getName();
        _builder.append(_name_4, "");
        _builder.append("(");
        EList<Parameter> _parameters_2 = it.getParameters();
        final Function1<Parameter, String> _function_1 = new Function1<Parameter, String>() {
          @Override
          public String apply(final Parameter it) {
            return RepositoryTmplExtension.this.paramTypeAndName(it);
          }
        };
        List<String> _map_1 = ListExtensions.<Parameter, String>map(_parameters_2, _function_1);
        String _join_1 = IterableExtensions.join(_map_1, ",");
        _builder.append(_join_1, "");
        {
          boolean _hasParameters_1 = this.helper.hasParameters(it);
          if (_hasParameters_1) {
            _builder.append(",");
          }
        }
        _builder.append(" Class<R> resultType) ");
        String _throwsDecl_1 = this.exceptionTmpl.throwsDecl(it);
        _builder.append(_throwsDecl_1, "");
        _builder.append(" {");
        _builder.newLineIfNotEmpty();
      } else {
        String _visibilityLitteral_2 = this.helper.getVisibilityLitteral(it);
        _builder.append(_visibilityLitteral_2, "");
        String _typeName_1 = this.helperBase.getTypeName(it);
        _builder.append(_typeName_1, "");
        _builder.append(" ");
        String _name_5 = it.getName();
        _builder.append(_name_5, "");
        _builder.append("(");
        EList<Parameter> _parameters_3 = it.getParameters();
        final Function1<Parameter, String> _function_2 = new Function1<Parameter, String>() {
          @Override
          public String apply(final Parameter it) {
            return RepositoryTmplExtension.this.paramTypeAndName(it);
          }
        };
        List<String> _map_2 = ListExtensions.<Parameter, String>map(_parameters_3, _function_2);
        String _join_2 = IterableExtensions.join(_map_2, ",");
        _builder.append(_join_2, "");
        _builder.append(") ");
        String _throwsDecl_2 = this.exceptionTmpl.throwsDecl(it);
        _builder.append(_throwsDecl_2, "");
        _builder.append(" {");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    {
      boolean _useGenericAccessStrategy_1 = this.helper.useGenericAccessStrategy(it);
      if (_useGenericAccessStrategy_1) {
        {
          String _name_6 = it.getName();
          boolean _equals = Objects.equal(_name_6, "findByKeys");
          if (_equals) {
            _builder.append("\t");
            _builder.append("com.postnord.isp.utils.sculptor.jpa.accessimpl.FindByKeysAccessImpl<R> ao = new com.postnord.isp.utils.sculptor.jpa.accessimpl.FindByKeysAccessImpl<R>(resultType);");
            _builder.newLine();
            _builder.append("\t");
            _builder.append("ao.setEntityManager(getEntityManager());");
            _builder.newLine();
          } else {
            String _name_7 = it.getName();
            boolean _notEquals = (!Objects.equal(_name_7, "findByExample"));
            if (_notEquals) {
              _builder.append("\t");
              String _name_8 = it.getName();
              String _genericAccessObjectInterface_1 = this.properties.genericAccessObjectInterface(_name_8);
              _builder.append(_genericAccessObjectInterface_1, "\t");
              _builder.append("2<R> ao = create");
              String _accessNormalizedName = this.helper.getAccessNormalizedName(it);
              _builder.append(_accessNormalizedName, "\t");
              _builder.append("(resultType);");
              _builder.newLineIfNotEmpty();
            } else {
              _builder.append("\t");
              String _name_9 = it.getName();
              String _genericAccessObjectInterface_2 = this.properties.genericAccessObjectInterface(_name_9);
              _builder.append(_genericAccessObjectInterface_2, "\t");
              _builder.append("2<");
              String _aggregateRootTypeName = this.helper.getAggregateRootTypeName(it);
              _builder.append(_aggregateRootTypeName, "\t");
              _builder.append(",R> ao = create");
              String _accessNormalizedName_1 = this.helper.getAccessNormalizedName(it);
              _builder.append(_accessNormalizedName_1, "\t");
              _builder.append("(");
              String _aggregateRootTypeName_1 = this.helper.getAggregateRootTypeName(it);
              _builder.append(_aggregateRootTypeName_1, "\t");
              _builder.append(".class, resultType);");
              _builder.newLineIfNotEmpty();
            }
          }
        }
      } else {
        _builder.append("\t");
        String _name_10 = it.getName();
        String _genericAccessObjectInterface_3 = this.properties.genericAccessObjectInterface(_name_10);
        _builder.append(_genericAccessObjectInterface_3, "\t");
        String _genericType = this.helper.getGenericType(it);
        _builder.append(_genericType, "\t");
        _builder.append(" ao = create");
        String _accessNormalizedName_2 = this.helper.getAccessNormalizedName(it);
        _builder.append(_accessNormalizedName_2, "\t");
        _builder.append("();");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.append("\t");
    String _setCache = this.setCache(it);
    _builder.append(_setCache, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    String _setEagerColumns = this.setEagerColumns(it);
    _builder.append(_setEagerColumns, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    String _setOrdered = this.setOrdered(it);
    _builder.append(_setOrdered, "\t");
    _builder.newLineIfNotEmpty();
    {
      boolean _hasHint = this.helper.hasHint(it, "useSingleResult");
      if (_hasHint) {
        _builder.append("\t");
        _builder.append("ao.setUseSingleResult(true);");
        _builder.newLine();
      }
    }
    {
      String _name_11 = it.getName();
      boolean _notEquals_1 = (!Objects.equal(_name_11, "findByKey"));
      if (_notEquals_1) {
        _builder.append("\t");
        _builder.newLine();
        {
          EList<Parameter> _parameters_4 = it.getParameters();
          final Function1<Parameter, Boolean> _function_3 = new Function1<Parameter, Boolean>() {
            @Override
            public Boolean apply(final Parameter e) {
              boolean _and = false;
              boolean _jpa = RepositoryTmplExtension.this.properties.jpa();
              if (!_jpa) {
                _and = false;
              } else {
                String _name = e.getName();
                boolean _equals = Objects.equal(_name, "persistentClass");
                _and = _equals;
              }
              return Boolean.valueOf((!_and));
            }
          };
          Iterable<Parameter> _filter = IterableExtensions.<Parameter>filter(_parameters_4, _function_3);
          for(final Parameter parameter : _filter) {
            _builder.append("\t");
            _builder.append("ao.set");
            String _name_12 = parameter.getName();
            String _firstUpper = this.helperBase.toFirstUpper(_name_12);
            _builder.append(_firstUpper, "\t");
            _builder.append("(");
            String _name_13 = parameter.getName();
            _builder.append(_name_13, "\t");
            _builder.append(");");
            _builder.newLineIfNotEmpty();
          }
        }
      }
    }
    _builder.append("\t");
    String _findByKeysSpecialCase = this.findByKeysSpecialCase(it);
    _builder.append(_findByKeysSpecialCase, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    String _findByKeySpecialCase = this.findByKeySpecialCase(it);
    _builder.append(_findByKeySpecialCase, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("ao.execute();");
    _builder.newLine();
    {
      String _typeName_2 = this.helperBase.getTypeName(it);
      boolean _notEquals_2 = (!Objects.equal(_typeName_2, "void"));
      if (_notEquals_2) {
        _builder.append("\t");
        String _nullThrowsNotFoundExcpetion = this.nullThrowsNotFoundExcpetion(it);
        _builder.append(_nullThrowsNotFoundExcpetion, "\t");
        _builder.newLineIfNotEmpty();
        {
          boolean _and = false;
          boolean _or = false;
          EList<Parameter> _parameters_5 = it.getParameters();
          final Function1<Parameter, Boolean> _function_4 = new Function1<Parameter, Boolean>() {
            @Override
            public Boolean apply(final Parameter e) {
              String _name = e.getName();
              return Boolean.valueOf(Objects.equal(_name, "useSingleResult"));
            }
          };
          boolean _exists = IterableExtensions.<Parameter>exists(_parameters_5, _function_4);
          if (_exists) {
            _or = true;
          } else {
            boolean _hasHint_1 = this.helper.hasHint(it, "useSingleResult");
            _or = _hasHint_1;
          }
          if (!_or) {
            _and = false;
          } else {
            String _collectionType = it.getCollectionType();
            boolean _equals_1 = Objects.equal(_collectionType, null);
            _and = _equals_1;
          }
          if (_and) {
            _builder.append("\t");
            _builder.append("return ");
            {
              boolean _and_1 = false;
              boolean _jpa = this.properties.jpa();
              boolean _not = (!_jpa);
              if (!_not) {
                _and_1 = false;
              } else {
                String _typeName_3 = this.helperBase.getTypeName(it);
                boolean _notEquals_3 = (!Objects.equal(_typeName_3, "Object"));
                _and_1 = _notEquals_3;
              }
              if (_and_1) {
                _builder.append("(");
                String _typeName_4 = this.helperBase.getTypeName(it);
                String _objectTypeName = this.helperBase.getObjectTypeName(_typeName_4);
                _builder.append(_objectTypeName, "\t");
                _builder.append(") ");
              }
            }
            _builder.append("ao.getSingleResult();");
            _builder.newLineIfNotEmpty();
          } else {
            _builder.append("\t");
            _builder.append("return ao.getResult();");
            _builder.newLine();
          }
        }
      }
    }
    _builder.append("}");
    _builder.newLine();
    String _findByNaturalKeys = this.findByNaturalKeys(it);
    _builder.append(_findByNaturalKeys, "");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }
  
  public String _chained_repositoryDependencies(final Repository it) {
    StringConcatenation _builder = new StringConcatenation();
    {
      EList<Repository> _repositoryDependencies = it.getRepositoryDependencies();
      for(final Repository dependency : _repositoryDependencies) {
        {
          boolean _isSpringToBeGenerated = this.properties.isSpringToBeGenerated();
          if (_isSpringToBeGenerated) {
            _builder.append("@org.springframework.beans.factory.annotation.Autowired");
            _builder.newLine();
          }
        }
        {
          boolean _pureEjb3 = this.properties.pureEjb3();
          if (_pureEjb3) {
            _builder.append("@javax.ejb.EJB");
            _builder.newLine();
          }
        }
        _builder.append("private ");
        DomainObject _aggregateRoot = dependency.getAggregateRoot();
        Module _module = _aggregateRoot.getModule();
        String _repositoryapiPackage = this.helperBase.getRepositoryapiPackage(_module);
        _builder.append(_repositoryapiPackage, "");
        _builder.append(".");
        String _name = dependency.getName();
        _builder.append(_name, "");
        _builder.append(" ");
        String _name_1 = dependency.getName();
        String _firstLower = this.helperBase.toFirstLower(_name_1);
        _builder.append(_firstLower, "");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.newLine();
        _builder.append("protected ");
        DomainObject _aggregateRoot_1 = dependency.getAggregateRoot();
        Module _module_1 = _aggregateRoot_1.getModule();
        String _repositoryapiPackage_1 = this.helperBase.getRepositoryapiPackage(_module_1);
        _builder.append(_repositoryapiPackage_1, "");
        _builder.append(".");
        String _name_2 = dependency.getName();
        _builder.append(_name_2, "");
        _builder.append(" get");
        String _name_3 = dependency.getName();
        _builder.append(_name_3, "");
        _builder.append("() {");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("return ");
        String _name_4 = dependency.getName();
        String _firstLower_1 = this.helperBase.toFirstLower(_name_4);
        _builder.append(_firstLower_1, "\t");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
        _builder.append("}");
        _builder.newLine();
        _builder.newLine();
        _builder.append("/**");
        _builder.newLine();
        _builder.append("* For mock purpose");
        _builder.newLine();
        _builder.append("*");
        _builder.newLine();
        _builder.append("*/");
        _builder.newLine();
        _builder.append("public void set");
        String _name_5 = dependency.getName();
        _builder.append(_name_5, "");
        _builder.append("(");
        DomainObject _aggregateRoot_2 = dependency.getAggregateRoot();
        Module _module_2 = _aggregateRoot_2.getModule();
        String _repositoryapiPackage_2 = this.helperBase.getRepositoryapiPackage(_module_2);
        _builder.append(_repositoryapiPackage_2, "");
        _builder.append(".");
        String _name_6 = dependency.getName();
        _builder.append(_name_6, "");
        _builder.append(" ");
        String _name_7 = dependency.getName();
        String _firstLower_2 = this.helperBase.toFirstLower(_name_7);
        _builder.append(_firstLower_2, "");
        _builder.append(") {");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("this.");
        String _name_8 = dependency.getName();
        String _firstLower_3 = this.helperBase.toFirstLower(_name_8);
        _builder.append(_firstLower_3, "\t");
        _builder.append(" = ");
        String _name_9 = dependency.getName();
        String _firstLower_4 = this.helperBase.toFirstLower(_name_9);
        _builder.append(_firstLower_4, "\t");
        _builder.append(";");
        _builder.newLineIfNotEmpty();
        _builder.append("}");
        _builder.newLine();
      }
    }
    return _builder.toString();
  }
  
  public RepositoryTmpl[] _getOverridesDispatchArray() {
    org.sculptor.generator.template.repository.RepositoryTmpl[] result = new org.sculptor.generator.template.repository.RepositoryTmpl[org.sculptor.generator.template.repository.RepositoryTmplMethodIndexes.NUM_METHODS];
    result[org.sculptor.generator.template.repository.RepositoryTmplMethodIndexes.GENERICBASEREPOSITORYMETHOD_REPOSITORYOPERATION] = this; 
    result[org.sculptor.generator.template.repository.RepositoryTmplMethodIndexes.REPOSITORYDEPENDENCIES_REPOSITORY] = this; 
    return result;
  }
}
