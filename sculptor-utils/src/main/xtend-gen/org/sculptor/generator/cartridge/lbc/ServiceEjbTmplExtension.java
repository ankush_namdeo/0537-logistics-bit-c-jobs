package org.sculptor.generator.cartridge.lbc;

import com.google.common.base.Objects;
import javax.inject.Inject;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.sculptor.generator.chain.ChainOverride;
import org.sculptor.generator.ext.Helper;
import org.sculptor.generator.ext.Properties;
import org.sculptor.generator.template.common.PubSubTmpl;
import org.sculptor.generator.template.service.ServiceEjbTmpl;
import org.sculptor.generator.template.service.ServiceTmpl;
import org.sculptor.generator.util.HelperBase;
import org.sculptor.generator.util.OutputSlot;
import sculptormetamodel.Service;
import sculptormetamodel.ServiceOperation;
import sculptormetamodel.Subscribe;

@ChainOverride
@SuppressWarnings("all")
public class ServiceEjbTmplExtension extends ServiceEjbTmpl {
  @Inject
  private PubSubTmpl pubSubTmpl;
  
  @Inject
  private ServiceTmpl serviceTmpl;
  
  @Inject
  @Extension
  private HelperBase helperBase;
  
  @Inject
  @Extension
  private Helper helper;
  
  @Inject
  @Extension
  private Properties properties;
  
  @Override
  public String ejbBeanImplSubclass(final Service it) {
    ServiceEjbTmpl headObj = getMethodsDispatchHead()[org.sculptor.generator.template.service.ServiceEjbTmplMethodIndexes.EJBBEANIMPLSUBCLASS_SERVICE];
    return headObj._chained_ejbBeanImplSubclass(it);
  }
  
  public ServiceEjbTmplExtension(final ServiceEjbTmpl next, final ServiceEjbTmpl[] methodsDispatchNext) {
    super(next);
  }
  
  public String _chained_ejbBeanImplSubclass(final Service it) {
    String _serviceimplPackage = this.helper.getServiceimplPackage(it);
    String _plus = (_serviceimplPackage + ".");
    String _name = it.getName();
    String _plus_1 = (_plus + _name);
    String _suffix = this.properties.getSuffix("Impl");
    String _plus_2 = (_plus_1 + _suffix);
    String _javaFileName = this.helper.javaFileName(_plus_2);
    StringConcatenation _builder = new StringConcatenation();
    String _javaHeader = this.properties.javaHeader();
    _builder.append(_javaHeader, "");
    _builder.newLineIfNotEmpty();
    _builder.append("package ");
    String _serviceimplPackage_1 = this.helper.getServiceimplPackage(it);
    _builder.append(_serviceimplPackage_1, "");
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("/// Sculptor code formatter imports ///");
    _builder.newLine();
    _builder.newLine();
    _builder.newLine();
    {
      boolean _isRemoteInterface = it.isRemoteInterface();
      if (_isRemoteInterface) {
        _builder.append("import ");
        String _serviceapiPackage = this.helper.getServiceapiPackage(it);
        _builder.append(_serviceapiPackage, "");
        _builder.append(".");
        String _name_1 = it.getName();
        _builder.append(_name_1, "");
        _builder.append("Remote;");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      boolean _isLocalInterface = it.isLocalInterface();
      if (_isLocalInterface) {
        _builder.append("import ");
        String _serviceapiPackage_1 = this.helper.getServiceapiPackage(it);
        _builder.append(_serviceapiPackage_1, "");
        _builder.append(".");
        String _name_2 = it.getName();
        _builder.append(_name_2, "");
        _builder.append("Local;");
        _builder.newLineIfNotEmpty();
      }
    }
    _builder.newLine();
    _builder.append("/**");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("* Implementation of ");
    String _name_3 = it.getName();
    _builder.append(_name_3, " ");
    _builder.append(".");
    _builder.newLineIfNotEmpty();
    _builder.append(" ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("@javax.ejb.Stateless(name=\"");
    String _name_4 = it.getName();
    String _firstLower = this.helperBase.toFirstLower(_name_4);
    _builder.append(_firstLower, "");
    _builder.append("\")");
    _builder.newLineIfNotEmpty();
    {
      boolean _isRemoteInterface_1 = it.isRemoteInterface();
      if (_isRemoteInterface_1) {
        _builder.append("@javax.ejb.Remote(");
        String _name_5 = it.getName();
        _builder.append(_name_5, "");
        _builder.append("Remote.class)");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      boolean _isLocalInterface_1 = it.isLocalInterface();
      if (_isLocalInterface_1) {
        _builder.append("@javax.ejb.Local(");
        String _name_6 = it.getName();
        _builder.append(_name_6, "");
        _builder.append("Local.class)");
        _builder.newLineIfNotEmpty();
      }
    }
    {
      boolean _isWebService = it.isWebService();
      if (_isWebService) {
        String _webServiceAnnotations = this.webServiceAnnotations(it);
        _builder.append(_webServiceAnnotations, "");
        _builder.newLineIfNotEmpty();
      }
    }
    String _ejbInterceptors = this.ejbInterceptors(it);
    _builder.append(_ejbInterceptors, "");
    _builder.newLineIfNotEmpty();
    {
      Subscribe _subscribe = it.getSubscribe();
      boolean _notEquals = (!Objects.equal(_subscribe, null));
      if (_notEquals) {
        Subscribe _subscribe_1 = it.getSubscribe();
        String _subscribeAnnotation = this.pubSubTmpl.subscribeAnnotation(_subscribe_1);
        _builder.append(_subscribeAnnotation, "");
      }
    }
    _builder.newLineIfNotEmpty();
    _builder.append("public class ");
    String _name_7 = it.getName();
    String _suffix_1 = this.properties.getSuffix("Impl");
    String _plus_3 = (_name_7 + _suffix_1);
    _builder.append(_plus_3, "");
    _builder.append(" ");
    {
      boolean _isGapClass = it.isGapClass();
      if (_isGapClass) {
        _builder.append("extends ");
        String _name_8 = it.getName();
        String _suffix_2 = this.properties.getSuffix("Impl");
        String _plus_4 = (_name_8 + _suffix_2);
        _builder.append(_plus_4, "");
        _builder.append("Base");
      }
    }
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    String _serialVersionUID = this.serviceTmpl.serialVersionUID(it);
    _builder.append(_serialVersionUID, "\t");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("public ");
    String _name_9 = it.getName();
    String _suffix_3 = this.properties.getSuffix("Impl");
    String _plus_5 = (_name_9 + _suffix_3);
    _builder.append(_plus_5, "\t");
    _builder.append("() {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    String _otherDependencies = this.serviceTmpl.otherDependencies(it);
    _builder.append(_otherDependencies, "");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    EList<ServiceOperation> _operations = it.getOperations();
    final Function1<ServiceOperation, Boolean> _function = new Function1<ServiceOperation, Boolean>() {
      @Override
      public Boolean apply(final ServiceOperation op) {
        return Boolean.valueOf(ServiceEjbTmplExtension.this.helper.isImplementedInGapClass(op));
      }
    };
    Iterable<ServiceOperation> _filter = IterableExtensions.<ServiceOperation>filter(_operations, _function);
    final Function1<ServiceOperation, String> _function_1 = new Function1<ServiceOperation, String>() {
      @Override
      public String apply(final ServiceOperation it) {
        return ServiceEjbTmplExtension.this.serviceTmpl.implMethod(it);
      }
    };
    Iterable<String> _map = IterableExtensions.<ServiceOperation, String>map(_filter, _function_1);
    String _join = IterableExtensions.join(_map);
    _builder.append(_join, "\t");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    String _serviceHook = this.serviceTmpl.serviceHook(it);
    _builder.append(_serviceHook, "");
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return this.helper.fileOutput(_javaFileName, OutputSlot.TO_SRC, _builder.toString());
  }
  
  public ServiceEjbTmpl[] _getOverridesDispatchArray() {
    org.sculptor.generator.template.service.ServiceEjbTmpl[] result = new org.sculptor.generator.template.service.ServiceEjbTmpl[org.sculptor.generator.template.service.ServiceEjbTmplMethodIndexes.NUM_METHODS];
    result[org.sculptor.generator.template.service.ServiceEjbTmplMethodIndexes.EJBBEANIMPLSUBCLASS_SERVICE] = this; 
    return result;
  }
}
