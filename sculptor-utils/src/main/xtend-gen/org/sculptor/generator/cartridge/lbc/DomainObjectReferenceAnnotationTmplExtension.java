package org.sculptor.generator.cartridge.lbc;

import javax.inject.Inject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;
import org.sculptor.generator.chain.ChainOverride;
import org.sculptor.generator.ext.DbHelper;
import org.sculptor.generator.ext.Helper;
import org.sculptor.generator.ext.Properties;
import org.sculptor.generator.template.domain.DomainObjectReferenceAnnotationTmpl;
import org.sculptor.generator.util.DbHelperBase;
import org.sculptor.generator.util.HelperBase;
import sculptormetamodel.Reference;

@ChainOverride
@SuppressWarnings("all")
public class DomainObjectReferenceAnnotationTmplExtension extends DomainObjectReferenceAnnotationTmpl {
  @Inject
  @Extension
  private DbHelperBase dbHelperBase;
  
  @Inject
  @Extension
  private DbHelper dbHelper;
  
  @Inject
  @Extension
  private HelperBase helperBase;
  
  @Inject
  @Extension
  private Helper helper;
  
  @Inject
  @Extension
  private Properties properties;
  
  @Override
  public String oneReferenceOnDeleteJpaAnnotation(final Reference it) {
    DomainObjectReferenceAnnotationTmpl headObj = getMethodsDispatchHead()[org.sculptor.generator.template.domain.DomainObjectReferenceAnnotationTmplMethodIndexes.ONEREFERENCEONDELETEJPAANNOTATION_REFERENCE];
    return headObj._chained_oneReferenceOnDeleteJpaAnnotation(it);
  }
  
  public DomainObjectReferenceAnnotationTmplExtension(final DomainObjectReferenceAnnotationTmpl next, final DomainObjectReferenceAnnotationTmpl[] methodsDispatchNext) {
    super(next);
  }
  
  public String _chained_oneReferenceOnDeleteJpaAnnotation(final Reference it) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.newLine();
    _builder.append("@org.hibernate.annotations.OnDelete(action = org.hibernate.annotations.OnDeleteAction.CASCADE)");
    _builder.newLine();
    return _builder.toString();
  }
  
  public DomainObjectReferenceAnnotationTmpl[] _getOverridesDispatchArray() {
    org.sculptor.generator.template.domain.DomainObjectReferenceAnnotationTmpl[] result = new org.sculptor.generator.template.domain.DomainObjectReferenceAnnotationTmpl[org.sculptor.generator.template.domain.DomainObjectReferenceAnnotationTmplMethodIndexes.NUM_METHODS];
    result[org.sculptor.generator.template.domain.DomainObjectReferenceAnnotationTmplMethodIndexes.ONEREFERENCEONDELETEJPAANNOTATION_REFERENCE] = this; 
    return result;
  }
}
