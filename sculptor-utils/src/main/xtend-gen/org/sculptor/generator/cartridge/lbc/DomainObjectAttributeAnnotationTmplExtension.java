package org.sculptor.generator.cartridge.lbc;

import javax.inject.Inject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Extension;
import org.sculptor.generator.chain.ChainOverride;
import org.sculptor.generator.ext.DbHelper;
import org.sculptor.generator.template.domain.DomainObjectAttributeAnnotationTmpl;
import org.sculptor.generator.util.PropertiesBase;
import sculptormetamodel.Attribute;

@ChainOverride
@SuppressWarnings("all")
public class DomainObjectAttributeAnnotationTmplExtension extends DomainObjectAttributeAnnotationTmpl {
  @Inject
  @Extension
  private DbHelper dbHelper;
  
  @Inject
  @Extension
  private PropertiesBase propertiesBase;
  
  @Override
  public String idAnnotations(final Attribute it) {
    DomainObjectAttributeAnnotationTmpl headObj = getMethodsDispatchHead()[org.sculptor.generator.template.domain.DomainObjectAttributeAnnotationTmplMethodIndexes.IDANNOTATIONS_ATTRIBUTE];
    return headObj._chained_idAnnotations(it);
  }
  
  public DomainObjectAttributeAnnotationTmplExtension(final DomainObjectAttributeAnnotationTmpl next, final DomainObjectAttributeAnnotationTmpl[] methodsDispatchNext) {
    super(next);
  }
  
  public String _chained_idAnnotations(final Attribute it) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("@javax.persistence.Id");
    _builder.newLine();
    _builder.append("@javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.SEQUENCE, generator = \"idSequence\")");
    _builder.newLine();
    _builder.append("@javax.persistence.SequenceGenerator(name = \"idSequence\", sequenceName=\"");
    String _property = this.propertiesBase.getProperty("db.tablePrefix");
    _builder.append(_property, "");
    _builder.append("_GID_SEQ\")");
    _builder.newLineIfNotEmpty();
    _builder.append("@javax.persistence.Column(name=\"");
    String _databaseName = this.dbHelper.getDatabaseName(it);
    _builder.append(_databaseName, "");
    _builder.append("\")");
    _builder.newLineIfNotEmpty();
    return _builder.toString();
  }
  
  public DomainObjectAttributeAnnotationTmpl[] _getOverridesDispatchArray() {
    org.sculptor.generator.template.domain.DomainObjectAttributeAnnotationTmpl[] result = new org.sculptor.generator.template.domain.DomainObjectAttributeAnnotationTmpl[org.sculptor.generator.template.domain.DomainObjectAttributeAnnotationTmplMethodIndexes.NUM_METHODS];
    result[org.sculptor.generator.template.domain.DomainObjectAttributeAnnotationTmplMethodIndexes.IDANNOTATIONS_ATTRIBUTE] = this; 
    return result;
  }
}
