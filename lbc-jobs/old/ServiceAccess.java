package com.postnord.isp.lbc.jobs.service;

import javax.ejb.Remote;

/**
 * Business interface to access the SingletonService via this EJB
 * 
 */
@Remote
public interface ServiceAccess {
    public abstract String getNodeNameOfService();
} 