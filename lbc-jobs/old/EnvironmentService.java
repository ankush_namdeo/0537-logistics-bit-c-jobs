package com.postnord.isp.lbc.jobs.service;

import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.jboss.as.server.ServerEnvironment;
import org.jboss.msc.inject.Injector;
import org.jboss.msc.service.Service;
import org.jboss.msc.service.ServiceName;
import org.jboss.msc.service.StartContext;
import org.jboss.msc.service.StartException;
import org.jboss.msc.service.StopContext;
import org.jboss.msc.value.InjectedValue;

public class EnvironmentService implements Service<String> {
	private static final Logger LOGGER = Logger
			.getLogger(EnvironmentService.class.getCanonicalName());
	public static final ServiceName SINGLETON_SERVICE_NAME = ServiceName.JBOSS
			.append("lbc-jobs", "ha", "singleton");
	/**
	 * A flag whether the service is started.
	 */
	private final AtomicBoolean started = new AtomicBoolean(false);

	private String nodeName;

	private final InjectedValue<ServerEnvironment> env = new InjectedValue<ServerEnvironment>();

	public Injector<ServerEnvironment> getEnvInjector() {
		return this.env;
	}

	/**
	 * @return the name of the server node
	 */
	public String getValue() throws IllegalStateException,
			IllegalArgumentException {
		if (!started.get()) {
			throw new IllegalStateException("The service '"
					+ this.getClass().getName() + "' is not ready!");
		}
		return this.nodeName;
	}

	public void start(StartContext context) throws StartException {
		if (!started.compareAndSet(false, true)) {
			throw new StartException("The service is still started!");
		}
		LOGGER.info("Start service '" + this.getClass().getName() + "'");
		this.nodeName = this.env.getValue().getNodeName();

		try {
			InitialContext ic = new InitialContext();
			((QuartzService) ic
					.lookup("global/lbc-jobs/QuartzServiceBean!com.postnord.isp.lbc.jobs.service.QuartzService"))
					.start("HASingleton timer @"
							+ this.env.getValue().getNodeName() + " started "
							+ new Date(), this.env.getValue().getNodeName());
		} catch (NamingException e) {
			throw new StartException("Could not initialize timer", e);
		}
	}

	public void stop(StopContext context) {
		if (!started.compareAndSet(true, false)) {
			LOGGER.warning("The service '" + this.getClass().getName()
					+ "' is not active!");
		} else {
			LOGGER.info("Stop service '" + this.getClass().getName() + "'");
			try {
				InitialContext ic = new InitialContext();
				((QuartzService) ic
						.lookup("global/lbc-jobs/QuartzServiceBean!com.postnord.isp.lbc.jobs.service.QuartzService"))
						.stop("HASingleton timer @"
								+ this.env.getValue().getNodeName()
								+ " stopped " + new Date());
			} catch (NamingException e) {
				LOGGER.log(org.jboss.logmanager.Level.ERROR,
						"Could not stop timer", e);
			}
		}
	}
}