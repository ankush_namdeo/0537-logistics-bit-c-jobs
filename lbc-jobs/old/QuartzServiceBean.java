package com.postnord.isp.lbc.jobs.service;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.util.Date;
import java.util.Properties;

import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.inject.Inject;

import org.jboss.logging.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

import com.postnord.isp.lbc.jobs.config.JobConfiguration;
import com.postnord.isp.lbc.jobs.config.JobConfigurationBean.JobProps;
import com.postnord.isp.lbc.jobs.job.HttpInvokerJob;
import com.postnord.isp.lbc.jobs.job.TestJob;
import com.postnord.isp.lbc.jobs.util.StringUtils;
import com.postnord.isp.lbc.jobs.service.factory.CDIJobFactory;

/**
 * 
 * 
 */
@Singleton
public class QuartzServiceBean implements QuartzService {
	private static Logger LOGGER = Logger.getLogger(QuartzServiceBean.class);
	
	@Inject
	private CDIJobFactory cdiJobFactory;
	
	@Inject
	private JobConfiguration jobConfiguration;

	private Scheduler scheduler;

	@Override
	public void start(String info, String name) {
		LOGGER.info(info);
		try {
			Properties props = new Properties();
			props.setProperty("org.quartz.scheduler.instanceName", name);
			props.setProperty("org.quartz.threadPool.threadCount", "10");
			scheduler = new StdSchedulerFactory(props).getScheduler();
			if(cdiJobFactory != null) {
				scheduler.setJobFactory(cdiJobFactory);
			}
		} catch (SchedulerException e) {
			LOGGER.error("Failed to init scheduler", e);
		}

		try {
			if (scheduler != null) {
				LOGGER.info("Start Quartz scheduler");
				scheduler.start();
				LOGGER.info("Schedule Quartz Jobs");
				scheduleJobs();
			}
		} catch (SchedulerException e) {
			LOGGER.error("Failed to start scheduler", e);
		}
	}

	@Override
	public void stop(String info) {
		LOGGER.info(info);
		try {
			if (scheduler != null) {
				LOGGER.info("Shutdown Quartz scheduler");
				scheduler.shutdown();
			}
		} catch (SchedulerException e) {
			LOGGER.error("Failed to stop scheduler", e);
		}
	}

	private void scheduleJobs() {
		try {
		    for(String prefix : jobConfiguration.getJobPrefixList()) {
		        int jobsInPrefix = jobConfiguration.getNumberOfJobs(prefix);
    			for (int i = 1; i <=jobsInPrefix ; i++) {
    				String jobPrefix = jobConfiguration.getJobPrefix(prefix, i); 
    				
    				String name = jobConfiguration.getString(jobPrefix, JobProps.NAME);
    				if (StringUtils.isEmpty(name)) {
    					break;
    				}
    				
    				String jobtype = jobConfiguration.getString(jobPrefix, JobProps.JOBTYPE);
    				
    				JobBuilder builder = null;
    				if(jobtype == null) {
    					builder = newJob(HttpInvokerJob.class);
    				} else if(HttpInvokerJob.class.getName().equals(jobtype)) {
    					builder = newJob(HttpInvokerJob.class);
    				} else if(TestJob.class.getName().equals(jobtype)) {
    					builder = newJob(TestJob.class);
    				} else {
    					builder = newJob(HttpInvokerJob.class);
    				}
    	
    				JobDetail jobDetail = builder.withIdentity(
    						name + "Job", name + "JobGroup")
    						.usingJobData(JobProps.ENABLED.getValue(), jobConfiguration.getString(jobPrefix, JobProps.ENABLED))
    						.usingJobData(JobProps.BASE64AUTH.getValue(), jobConfiguration.getString(jobPrefix, JobProps.BASE64AUTH))
    						.usingJobData(JobProps.HTTPCONTENT.getValue(), jobConfiguration.getString(jobPrefix, JobProps.HTTPCONTENT))
    						.usingJobData(JobProps.HTTPMETHOD.getValue(), jobConfiguration.getString(jobPrefix, JobProps.HTTPMETHOD))
    						.usingJobData(JobProps.HTTPURL.getValue(), jobConfiguration.getHttpUrlString(jobPrefix, true))
    						.usingJobData(JobProps.TIMEOUT.getValue(), jobConfiguration.getString(jobPrefix, JobProps.TIMEOUT))
    						.usingJobData(JobProps.CRON.getValue(), jobConfiguration.getString(jobPrefix, JobProps.CRON))
    						.usingJobData(JobProps.ROWPREFIX.getValue(), jobPrefix)
    						.build();
    				
    				String cronExpression = jobConfiguration.getString(jobPrefix, JobProps.CRON);
    				CronTrigger cronTrigger = newTrigger()
    						.withIdentity(name + "Trigger", name + "TriggerGroup")
    						.withSchedule(cronSchedule(cronExpression))
    						.forJob(jobDetail).build();
    
    				Date date = scheduler.scheduleJob(jobDetail, cronTrigger);
    				LOGGER.debug("****** Scheduled job (date = '" + date + "').");
    			}
    			LOGGER.info("Scheduled " + jobsInPrefix + " jobs for application " + prefix.toUpperCase());
		    }
		} catch (SchedulerException e) {
			LOGGER.error("Failed to schedule Jobs", e);
		}
	}

	public Scheduler getScheduler() {
		return scheduler;
	}

	/**
	 * Only for test
	 * 
	 * @param jobConfiguration
	 */
	public void setJobConfiguration(JobConfiguration jobConfiguration) {
		this.jobConfiguration = jobConfiguration;
	}
}
