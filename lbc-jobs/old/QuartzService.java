package com.postnord.isp.lbc.jobs.service;

public interface QuartzService {

    void start(String info, String name);

    void stop(String info);

}