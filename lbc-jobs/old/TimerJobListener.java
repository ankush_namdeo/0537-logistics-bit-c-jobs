package com.postnord.isp.lbc.jobs.listener;

import java.text.SimpleDateFormat;

import org.jboss.logging.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;

public class TimerJobListener implements JobListener {

    private static final Logger LOGGER = Logger.getLogger(TimerJobListener.class);
    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    @Override
    public String getName() {
        return getClass().getSimpleName();
    }

    @Override
    public void jobToBeExecuted(JobExecutionContext context) {
        String printableName = printableName(context.getJobDetail().getKey().getName());
        LOGGER.info("Job " + printableName + " started");
    }

    @Override
    public void jobExecutionVetoed(JobExecutionContext context) {

    }

    @Override
    public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
        String printableName = printableName(context.getJobDetail().getKey().getName());
        if (jobException != null) {
            LOGGER.info("Job " + printableName + " was executed in " + context.getJobRunTime() + "ms, with exception "
                    + jobException.getMessage());
        } else {
            LOGGER.info("Job " + printableName + " was executed in " + context.getJobRunTime() + "ms. "
                    + "Next time to run " + FORMAT.format(context.getNextFireTime()));
        }
    }

    private String printableName(String jobname) {
        String[] names = jobname.split(".");
        if (names.length > 1) {
            return names[0] + "." + names[1];
        }

        return jobname;
    }
}
