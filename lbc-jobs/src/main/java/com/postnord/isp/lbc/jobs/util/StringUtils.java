package com.postnord.isp.lbc.jobs.util;

public class StringUtils {

	public static boolean isEmpty(String input) {
		return !(input != null && input.length() > 0);
	}
}
