package com.postnord.isp.lbc.jobs.config;

import java.net.URL;
import java.security.ProviderException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.DefaultConfigurationBuilder;
import org.jboss.logging.Logger;

import com.postnord.isp.lbc.jobs.util.StringUtils;

@Stateless
public class JobConfigurationBean implements JobConfiguration {

	private static Logger LOGGER = Logger.getLogger(JobConfigurationBean.class);
	private static final int MAX_NUMBER_OF_INSTANCES = 3;
	private Properties properties = null;
	
	public enum JobProps {
		NAME("name"),
		JOBTYPE("jobtype"),
		ENABLED("enabled"), 
		HTTPURL("httpUrl"), 
		HTTPMETHOD("httpMethod"), 
		HTTPCONTENT("httpContent"), 
		BASE64AUTH("base64Auth"), 
		TIMEOUT("timeout"), 
		CRON("cron.expression"), 
		LOADBALANCE("loadBalance"), 
		ROWPREFIX("rowprefix");

		private String value;

		private JobProps(String value) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}
	};
	
	@PostConstruct
	public void init() {
		reloadProperties();
	}
	
	public void reloadProperties() {
		properties = getProperties();
	}
	
	@SuppressWarnings("unchecked")
	private Properties getProperties() {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Reading properties file: " + propertiesFile());
		}

		Configuration config = loadConfiguration(propertiesFile());

		Properties properties = new Properties();
		for (Iterator<String> iter = config.getKeys(); iter.hasNext();) {
			String key = iter.next();
			String value = config.getString(key);
			properties.setProperty(key, value);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("key = " + key + " value = " + value);
			}
		}

		return properties;
	}

	private Configuration loadConfiguration(String resource) {
		try {
			URL configURL = getClass().getResource(resource);
			DefaultConfigurationBuilder builder = new DefaultConfigurationBuilder(
					configURL);
			// Ignore any configuration errors
			builder.clearErrorListeners();
			return builder.getConfiguration(true);
		} catch (ConfigurationException e) {
			throw new ProviderException(e);
		}
	}

	private String propertiesFile() {
		return "/cronJobConfig.xml";
	}
	
	public List<String> getHttpUrls(String jobPrefix) {
        List<String> result = new ArrayList<String>();
        String mainUrl = properties.getProperty(jobPrefix + JobProps.HTTPURL.getValue());
        if(mainUrl != null) {
        	result.add(mainUrl);
        }
        for (int i = 1; i <= MAX_NUMBER_OF_INSTANCES; i++) {
            String url = properties.getProperty(jobPrefix +JobProps.HTTPURL.getValue()+"." + i);
            if(url != null && url.length() > 0) {
                result.add(url);
            }
        }
        
        return result;
    }
	
	public String getHttpUrlString(String jobPrefix) {
		String urlString = "";
		for(String url : getHttpUrls(jobPrefix)) {
			urlString += url;
			urlString += ",";
		}
		if(urlString.endsWith(",")) {
			urlString = urlString.substring(0, urlString.length()-1);
		}
		
		return urlString;
	}
	
	public String getString(String jobPrefix, JobProps jobProps) {
		return properties.getProperty(jobPrefix + jobProps.getValue());
	}
	
	public boolean getBoolean(String jobPrefix, JobProps jobProps) {
		String value = properties.getProperty(jobPrefix + jobProps.getValue());
		if(StringUtils.isEmpty(value)) {
			return false;
		}
		return Boolean.valueOf(value);
	}
	
	public int getNumberOfJobs(String prefix) {
	    int numberOfJobs = 0;
        int index = 1;
        while(true) {
			String number = properties.getProperty(prefix + "." + getPrefix(index) + JobProps.NAME.getValue());
			if(number != null) {
			    index++;
			    numberOfJobs++;
			} else {
			    break;
			}
        }
		
		return numberOfJobs;
	}
	
	public Set<String> getJobPrefixList() {
	    Set<String> result = new HashSet<String>();
	    for(Object key : properties.keySet()) {
	        String keyString = (String)key;
	        if(!keyString.contains(".01")) {
	            continue;
	        }
	        int indexOf = keyString.indexOf(".");
	        if(indexOf > 0) {
    	        result.add(keyString.substring(0, keyString.indexOf(".")));
	        }
	    }
        
        return result;
    }
	
	public String getJobPrefix(String prefix, int i) {
		String jobPrefix = prefix;
		jobPrefix += ".";
		if (i < 10) {
			jobPrefix += "0";
		}
		jobPrefix += i;
		jobPrefix += ".";
		
		return jobPrefix;
	}
	
	public String getPrefix(int i) {
        String jobPrefix = "";
        if (i < 10) {
            jobPrefix += "0";
        }
        jobPrefix += i;
        jobPrefix += ".";
        
        return jobPrefix;
    }
}
