package com.postnord.isp.lbc.jobs.job;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.jboss.logging.Logger;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerKey;
import org.quartz.impl.matchers.GroupMatcher;

import com.postnord.isp.lbc.jobs.config.JobConfiguration;
import com.postnord.isp.lbc.jobs.config.JobConfigurationBean.JobProps;

public abstract class JobBase {

	private static Logger LOGGER = Logger.getLogger(JobBase.class);
	protected static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Inject
	private JobConfiguration jobConfiguration;
	
	protected Integer getTimeOut(JobExecutionContext context) {
	    String timeout = getProperty(context.getJobDetail().getJobDataMap(), JobProps.TIMEOUT, true);   
		if (timeout == null) {
			return null;
		}
		return Integer.parseInt(timeout);
	}

	protected Boolean getBooleanProperty(JobDataMap jobDataMap, JobProps props, boolean fromPropertiesfile) {
	    String value = getProperty(jobDataMap, props, fromPropertiesfile);
	    if(value != null) {
	        if("true".equalsIgnoreCase(value)) {
	            return true;
	        } else {
	            return false;
	        }
	    }
	    
	    return null;
    }
	
	protected String printableName(String jobname) {
        String[] names = jobname.split("\\.");
        if(names.length > 1) {
            return names[0] + "." + names[1];
        }
        
        return jobname;
    }
	
	protected String getProperty(JobDataMap jobDataMap, JobProps props, boolean fromPropertiesfile) {
	    if(fromPropertiesfile) {
	        return getStringFromProperties(jobDataMap.getString(JobProps.ROWPREFIX.getValue()), props);
	    }
		return (String) jobDataMap.get(props.getValue());
	}

	protected List<String> getPropertyList(JobDataMap jobDataMap, JobProps props) {
	    String httpUrls = getProperty(jobDataMap, props, false);   
		List<String> result = Arrays.asList(httpUrls.split(","));
		
		Boolean loadBalance = getBooleanProperty(jobDataMap, JobProps.LOADBALANCE, true);
        if(loadBalance == null) {
            loadBalance = true;
        }

		if(loadBalance && result.size() > 1) {
		    Collections.shuffle(result);
		}
		
		return result;
	}
	
	protected boolean reschedule(JobExecutionContext jobExecutionContext,
			String cronExpression) {
		org.quartz.Scheduler scheduler = jobExecutionContext.getScheduler();
		org.quartz.JobDetail jobDetail = jobExecutionContext.getJobDetail();
		org.quartz.Trigger trigger = jobExecutionContext.getTrigger();
		org.quartz.CronTrigger cronTrigger = (org.quartz.CronTrigger) trigger;

		if (!cronExpression.equals(cronTrigger.getCronExpression())) {
			LOGGER.info("****** Will change cron expression for "
					+ jobDetail.getKey().getName() + " : '" + cronExpression);

			LOGGER.debug("Before change");
			printJobsAndTriggers(scheduler);

			try {
				// Create new trigger
				LOGGER.info("****** Create new cron trigger with expression: '"
						+ cronExpression + ".");
				cronTrigger = newTrigger()
						.withIdentity(cronTrigger.getKey().getName(),
								cronTrigger.getKey().getGroup())
						.withSchedule(cronSchedule(cronExpression))
						.forJob(jobDetail).build();
				
				Date date = scheduler.rescheduleJob(cronTrigger.getKey(), cronTrigger);
				LOGGER.info("****** Scheduled job (date = '" + date + "').");

				LOGGER.info("After change");
				printJobsAndTriggers(scheduler);
			} catch (org.quartz.SchedulerException e) {
				LOGGER.warn("Couldn't schedule jobs with cron expression: '"
						+ cronExpression + "'.");
				return false;
			}

		}
		return true;
	}
	
	protected void printJobsAndTriggers(Scheduler scheduler) {
		try {
	    	LOGGER.debug("Quartz Scheduler: " + scheduler.getSchedulerName());
	        for (String group : scheduler.getJobGroupNames()) {
	            for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.<JobKey>groupEquals(group))) {
	            	LOGGER.debug("Found job identified by " + jobKey);
	            }
	        }
	        for (String group : scheduler.getTriggerGroupNames()) {
	            for (TriggerKey triggerKey : scheduler.getTriggerKeys(GroupMatcher.<TriggerKey>groupEquals(group))) {
	            	LOGGER.debug("Found trigger identified by " + triggerKey);
	            }
	        }
		} catch(SchedulerException e) {
			// Do Noting
		}
    }

	private String getStringFromProperties(String rowPrefix, JobProps jobProps) {
		jobConfiguration.reloadProperties();
		return jobConfiguration.getString(rowPrefix, jobProps);
	}
	
	public void setJobConfiguration(JobConfiguration jobConfiguration) {
        this.jobConfiguration = jobConfiguration;
    }
}
