package com.postnord.isp.lbc.jobs.job;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.HttpContext;
import org.jboss.logging.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.postnord.isp.lbc.jobs.config.JobConfigurationBean.JobProps;
import com.postnord.isp.lbc.jobs.util.StringUtils;

public class HttpInvokerJob extends JobBase implements Job {

	private static Logger LOGGER = Logger.getLogger(HttpInvokerJob.class);
	private static final int RESPONSE_ERROR_LIMIT = 300;
	private HttpClientBuilder httpClientBuilder = null;
	
	public HttpInvokerJob() {
	    super();
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		String jobName = context.getJobDetail().getKey().toString();
        String printableName = printableName(jobName);
        
		try {	
		    long startTime = System.currentTimeMillis();
	        
			String enabled = getProperty(jobDataMap, JobProps.ENABLED, true);	
			if(enabled == null || "false".equals(enabled)) {
				LOGGER.debug(printableName + " is disabled");
				return;
			} 
			
			Integer timeout = getTimeOut(context);
            if(timeout != null) {
                LOGGER.info(printableName + " started with timeout of " + timeout + "ms");
            } else {
                LOGGER.info(printableName + " started");
            }
			
			List<String> httpUrls = getPropertyList(jobDataMap, JobProps.HTTPURL);
			String httpMethod = getProperty(jobDataMap, JobProps.HTTPMETHOD, false);
			String httpContent = getProperty(jobDataMap, JobProps.HTTPCONTENT, false);
			String base64Auth = getProperty(jobDataMap, JobProps.BASE64AUTH, false);
			
			Exception exception = null;
			int count = 0;
			for (String eachUrl : httpUrls) {
			    count++;
				if (eachUrl == null || eachUrl.length() == 0) {
					continue;
				}

				int httpResponseCode = 200;
				try {
					httpResponseCode = invokeHttpService(eachUrl, httpMethod,
							httpContent, base64Auth, context);
					if (httpResponseCode < RESPONSE_ERROR_LIMIT) {
						// Successful
					    exception = null;
						break;
					} else {
					    if(count < httpUrls.size()) {
					        LOGGER.warn(printableName + " HTTP response code: "+httpResponseCode+" >= " + RESPONSE_ERROR_LIMIT + ", retry with nex url"); 
					    } else {
					        LOGGER.warn(printableName + " HTTP response code: "+httpResponseCode+" >= " + RESPONSE_ERROR_LIMIT + ", no more url to retry"); 
					    }    
					}
				} catch (Exception e) {
				    if(e instanceof java.net.SocketTimeoutException) {
	                    if(timeout != null) {
	                        long time = (System.currentTimeMillis() - startTime);
	                        if(time >= timeout.intValue()) {
	                            LOGGER.info(printableName + " SocketTimeoutException: " + e.getMessage() + " is treated as ok");  
	                            // Successful
	                            exception = null;
	                            break;
	                        }
	                    }
				    }
				   
				    if(count < httpUrls.size()) {
				        LOGGER.warn(printableName + " got an exception, retry with nex url. Exception: " + e.getMessage());
				    }
				    exception = e;
				}
			}
			
			LOGGER.info(printableName + " was executed in " + (System.currentTimeMillis() - startTime) + "ms. "
                    + "Next time to run " + FORMAT.format(context.getNextFireTime()));
			
			if (exception != null) {
				throw exception;
			}
			
		} catch (Exception e) {
		    LOGGER.error(printableName + " aborted with Exception: " + e.getMessage());
		    throw new JobExecutionException(e);
        } finally {
			// Read properties from file
            String cronExpression = getProperty(jobDataMap, JobProps.CRON, true);
			if (!StringUtils.isEmpty(cronExpression)) {
				reschedule(context, cronExpression);
			}
		}
	}

	private int invokeHttpService(String urlStr, String method,
			String content, String base64Auth, JobExecutionContext context) throws ClientProtocolException, IOException {

	    HttpRequestBase request = null;
		
		if (method.equals("POST")) {
		    request = new HttpPost(urlStr);
			((HttpPost)request).setEntity(new StringEntity(content, ContentType.TEXT_XML));
		} else if (method.equals("GET")) {
		    request = new HttpGet(urlStr);
		} else {
			throw new IllegalArgumentException("Unknown HTTP method; "
					+ method);
		}

		HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();

		// Set connection and socket timeout
		Integer timeout = getTimeOut(context);
		if (timeout != null) {
			RequestConfig config = RequestConfig.custom()
					.setSocketTimeout(timeout)
					.setConnectionRequestTimeout(timeout)
					.setConnectTimeout(timeout).build();
			httpClientBuilder = httpClientBuilder
					.setDefaultRequestConfig(config);
		}

		// Add basic authentication if needed
		if (base64Auth != null && base64Auth.length() > 0) {
		    request.addHeader("Authorization", "Basic " + base64Auth);
		}
		
		// Never retry request
        DefaultHttpRequestRetryHandler retryHandler = new DefaultHttpRequestRetryHandler(0, false) {

            @Override
            public boolean retryRequest(final IOException exception, final int executionCount,
                    final HttpContext context) {
                LOGGER.warn("DefaultHttpRequestRetryHandler wants to retry because of " + exception.getMessage() + ", ignoring that"); 
                return false;
            }
        };
        
		CloseableHttpClient httpClient = buildHttpClient(httpClientBuilder.setRetryHandler(retryHandler));
		try {
		    return httpClient.execute(request).getStatusLine().getStatusCode();
		} finally {
		    if(httpClient != null) {
		        httpClient.close(); 
		    }    
		}
	}
	
	protected CloseableHttpClient buildHttpClient(HttpClientBuilder httpClientBuilder) {
	    if(this.httpClientBuilder != null) {
	        return this.httpClientBuilder.build();
	    }
	    return httpClientBuilder.build();
	}
	
	protected void setHttpClientBuilder(HttpClientBuilder httpClientBuilder) {
	    this.httpClientBuilder = httpClientBuilder;
	}
	
	
	
}
