package com.postnord.isp.lbc.jobs.job;

import org.jboss.logging.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.postnord.isp.lbc.jobs.config.JobConfigurationBean.JobProps;
import com.postnord.isp.lbc.jobs.util.StringUtils;

public class MonitoringJob extends JobBase implements Job {

    private static Logger LOGGER = Logger.getLogger(MonitoringJob.class);

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();

        String jobName = context.getJobDetail().getKey().toString();
        String printableName = printableName(jobName);
        
        try {
            String enabled = getProperty(jobDataMap, JobProps.ENABLED, true);
            if (enabled == null || "false".equals(enabled)) {
                LOGGER.debug(printableName + " is disabled");
                return;
            }

            LOGGER.info(printableName + " is a MonitoringJob. Next time to run " + FORMAT.format(context.getNextFireTime()));
            
        } catch (Exception e) {
            LOGGER.error(printableName + " aborted with Exception: " + e.getMessage());
            throw new JobExecutionException(e);
        } finally {
            // Read properties from file
            String cronExpression = getProperty(jobDataMap, JobProps.CRON, true);
            if (!StringUtils.isEmpty(cronExpression)) {
                reschedule(context, cronExpression);
            }
        }
    }
}
