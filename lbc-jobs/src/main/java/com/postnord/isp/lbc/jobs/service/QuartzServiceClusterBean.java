package com.postnord.isp.lbc.jobs.service;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.jboss.logging.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;

import com.postnord.isp.lbc.jobs.config.JobConfiguration;
import com.postnord.isp.lbc.jobs.config.JobConfigurationBean.JobProps;
import com.postnord.isp.lbc.jobs.job.HttpInvokerJob;
import com.postnord.isp.lbc.jobs.job.MonitoringJob;
import com.postnord.isp.lbc.jobs.job.TestJob;
import com.postnord.isp.lbc.jobs.service.factory.CDIJobFactory;
import com.postnord.isp.lbc.jobs.util.StringUtils;

/**
 * Bean that is clusterable via Database. Users external file load its jobs and
 * triggers. Scans the file every X seconds to see if it needs to reload the jobs
 * 
 * Should use DbStore and use a table prefix of the affected RTPI. E.g prefix=P1CSP
 * One DB user should be used and scheduler should use its own table prefix.
 * 
 */
@Singleton
@Startup
public class QuartzServiceClusterBean {
    private static Logger LOGGER = Logger.getLogger(QuartzServiceClusterBean.class);
    private Scheduler scheduler;

    @Inject
    private CDIJobFactory cdiJobFactory;
    
    @Inject
    private JobConfiguration jobConfiguration;
    
    @PostConstruct
    public void start() {
        LOGGER.info("Starting Quartz scheduler");
        try {
            String propertiesFile = System.getProperty("jboss.server.base.dir") + "/configuration/appdata/quartz.properties";
            Properties properties = new Properties();
            properties.load(new FileInputStream(propertiesFile));
            scheduler = new StdSchedulerFactory(properties).getScheduler();
            if(cdiJobFactory != null) {
                scheduler.setJobFactory(cdiJobFactory);
            }
        } catch (SchedulerException e) {
            LOGGER.error("Error while creating Quartz scheduler", e);
        } catch (FileNotFoundException e) {
            LOGGER.error("Error while creating Quartz scheduler", e);
        } catch (IOException e) {
            LOGGER.error("Error while creating Quartz scheduler", e);
        }
        
        try {
            if (scheduler != null) {
                LOGGER.info("Start Quartz scheduler");
                scheduler.start();
                LOGGER.info("Schedule Quartz Jobs");
                scheduleJobs();
            }
        } catch (SchedulerException e) {
            LOGGER.error("Failed to start scheduler", e);
        }
    }

    @PreDestroy
    public void shutdown() {
        LOGGER.info("Stopping Quartz scheduler");
        try {
            if (scheduler != null) {
                scheduler.shutdown();
            }
        } catch (SchedulerException e) {
            LOGGER.error("Error while closing Quartz scheduler", e);
        }
    }
    
    private void scheduleJobs() {
        try {
            Set<JobKey> persistedJobKeys = new HashSet<JobKey>();
            for (String group : scheduler.getJobGroupNames()) {
                for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.<JobKey>groupEquals(group))) {
                    persistedJobKeys.add(jobKey);
                }
            }
            
            Set<TriggerKey> persistedTriggerKeys = new HashSet<TriggerKey>();
            for (String group : scheduler.getTriggerGroupNames()) {
                for (TriggerKey triggerKey : scheduler.getTriggerKeys(GroupMatcher.<TriggerKey>groupEquals(group))) {
                    persistedTriggerKeys.add(triggerKey);
                }
            }
            
            for(String prefix : jobConfiguration.getJobPrefixList()) {
                int jobsInPrefix = jobConfiguration.getNumberOfJobs(prefix);
                for (int i = 1; i <=jobsInPrefix ; i++) {
                    String jobPrefix = jobConfiguration.getJobPrefix(prefix, i); 
                    
                    String name = jobConfiguration.getString(jobPrefix, JobProps.NAME);
                    if (StringUtils.isEmpty(name)) {
                        break;
                    }
                    
                    String jobtype = jobConfiguration.getString(jobPrefix, JobProps.JOBTYPE);
                    
                    JobBuilder builder = null;
                    if(jobtype == null) {
                        builder = newJob(HttpInvokerJob.class);
                    } else if(HttpInvokerJob.class.getName().equals(jobtype)) {
                        builder = newJob(HttpInvokerJob.class);
                    } else if(TestJob.class.getName().equals(jobtype)) {
                        builder = newJob(TestJob.class);
                    } else if(MonitoringJob.class.getName().equals(jobtype)) {
                        builder = newJob(MonitoringJob.class);
                    } else {
                        builder = newJob(HttpInvokerJob.class);
                    }
        
                    JobDetail jobDetail = builder.withIdentity(
                            name + ".J", name + ".JG")
                            .usingJobData(JobProps.ENABLED.getValue(), jobConfiguration.getString(jobPrefix, JobProps.ENABLED))
                            .usingJobData(JobProps.BASE64AUTH.getValue(), jobConfiguration.getString(jobPrefix, JobProps.BASE64AUTH))
                            .usingJobData(JobProps.HTTPCONTENT.getValue(), jobConfiguration.getString(jobPrefix, JobProps.HTTPCONTENT))
                            .usingJobData(JobProps.HTTPMETHOD.getValue(), jobConfiguration.getString(jobPrefix, JobProps.HTTPMETHOD))
                            .usingJobData(JobProps.HTTPURL.getValue(), jobConfiguration.getHttpUrlString(jobPrefix))
                            .usingJobData(JobProps.TIMEOUT.getValue(), jobConfiguration.getString(jobPrefix, JobProps.TIMEOUT))
                            .usingJobData(JobProps.CRON.getValue(), jobConfiguration.getString(jobPrefix, JobProps.CRON))
                            .usingJobData(JobProps.LOADBALANCE.getValue(), jobConfiguration.getString(jobPrefix, JobProps.LOADBALANCE))
                            .usingJobData(JobProps.ROWPREFIX.getValue(), jobPrefix)
                            .build();
                    
                    
                    
                    String cronExpression = jobConfiguration.getString(jobPrefix, JobProps.CRON);
                    CronTrigger cronTrigger = newTrigger()
                            .withIdentity(name + "Trigger", name + "TriggerGroup")
                            .withSchedule(cronSchedule(cronExpression).withMisfireHandlingInstructionDoNothing())
                            .forJob(jobDetail).build();
     
                    try {
                        // Reschedule Job is exists nad crontrigger exist and cronexpression is different.
                        if(persistedJobKeys.contains(jobDetail.getKey()) && persistedTriggerKeys.contains(cronTrigger.getKey())) {
                            CronTrigger persistedCronTrigger = (CronTrigger)scheduler.getTrigger(cronTrigger.getKey());
                            if(persistedCronTrigger != null && persistedCronTrigger.getCronExpression() != null && 
                                    !cronExpression.equals(persistedCronTrigger.getCronExpression())) {
                                Date date = scheduler.rescheduleJob(cronTrigger.getKey(), cronTrigger);
                                LOGGER.info("****** Rescheduled job (date = '" + date + "').");
                            }
                        } else {
                            Date date = scheduler.scheduleJob(jobDetail, cronTrigger);
                            LOGGER.info("****** Scheduled job (date = '" + date + "').");
                        }
                        
                        // Remove job from set
                        persistedJobKeys.remove(jobDetail.getKey());
                    } catch (SchedulerException e) {
                        LOGGER.error("Error while schedule jobs", e);
                    }
                }
                LOGGER.info("Scheduled " + jobsInPrefix + " jobs for application " + prefix.toUpperCase());
            }
            
            // Remove persisted job not used any more
            if(persistedJobKeys.size() > 0) {
                for(JobKey key : persistedJobKeys) {
                    scheduler.deleteJob(key);
                    LOGGER.info("Deleted unused job " + key);
                }
            }
        } catch (SchedulerException e) {
            LOGGER.error("Failed to schedule Jobs", e);
        }
    }
    
    public Scheduler getScheduler() {
        return scheduler;
    }
    
    /**
     * Only for test
     * 
     * @param jobConfiguration
     */
    public void setJobConfiguration(JobConfiguration jobConfiguration) {
        this.jobConfiguration = jobConfiguration;
    }
}