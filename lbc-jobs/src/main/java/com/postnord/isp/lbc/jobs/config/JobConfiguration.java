package com.postnord.isp.lbc.jobs.config;

import java.util.List;
import java.util.Set;

import javax.ejb.Local;

import com.postnord.isp.lbc.jobs.config.JobConfigurationBean.JobProps;

@Local
public interface JobConfiguration {

	public void reloadProperties();
	
	public List<String> getHttpUrls(String jobPrefix);
	
	public String getHttpUrlString(String jobPrefix);
	
	public String getString(String jobPrefix, JobProps jobProps);
	
	public boolean getBoolean(String jobPrefix, JobProps jobProps);
	
	public int getNumberOfJobs(String prefix);
	
	public Set<String> getJobPrefixList();
	
	public String getJobPrefix(String prefix, int i);
}
