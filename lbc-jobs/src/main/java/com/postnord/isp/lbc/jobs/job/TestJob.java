package com.postnord.isp.lbc.jobs.job;

import java.util.List;

import org.jboss.logging.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.postnord.isp.lbc.jobs.config.JobConfigurationBean.JobProps;
import com.postnord.isp.lbc.jobs.util.StringUtils;

public class TestJob extends JobBase implements Job {

	private static Logger LOGGER = Logger.getLogger(TestJob.class);
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		try {
			String jobName = context.getJobDetail().getKey().toString();
	        String printableName = printableName(jobName);
			String enabled = getProperty(jobDataMap, JobProps.ENABLED, true);	
			if(enabled == null || "false".equals(enabled)) {
				LOGGER.info("Quartz job: " + jobName + " is disabled");
				return;
			} 
			
			int index = 1;
			List<String> httpUrls = getPropertyList(jobDataMap, JobProps.HTTPURL);
            for (String eachUrl : httpUrls) {
                LOGGER.info("Endpoint " + index++ + ": " + eachUrl);
            }

			LOGGER.info("Job " + printableName + " was executed in " + context.getJobRunTime() + "ms. "
                    + "Next time to run " + FORMAT.format(context.getNextFireTime()));
		} finally {
			// Read properties from file
			String cronExpression = getProperty(jobDataMap, JobProps.CRON, true);
			if (!StringUtils.isEmpty(cronExpression)) {
			    reschedule(context, cronExpression);
			}
		}
	}
}
