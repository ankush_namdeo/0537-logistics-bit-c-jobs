package com.postnord.isp.lbc.jobs.job;

import static org.quartz.JobBuilder.newJob;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnit44Runner;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.triggers.CronTriggerImpl;

import static org.mockito.Mockito.*;


import com.postnord.isp.lbc.jobs.config.JobConfiguration;
import com.postnord.isp.lbc.jobs.config.JobConfigurationBean;
import com.postnord.isp.lbc.jobs.config.JobConfigurationBean.JobProps;
import com.postnord.isp.lbc.jobs.util.StringUtils;

public class TestHttpInvokerJob {


    @Test
    public void testNormalJobExceution() throws JobExecutionException, ClientProtocolException, IOException, ParseException {
        
        JobConfigurationBean jobConfigurationBean = new JobConfigurationBean();
        jobConfigurationBean.init();
        
        HttpInvokerJob httpInvokerJob = new HttpInvokerJob();
        
        CloseableHttpClient mockClient = mockHttpClient(jobConfigurationBean, httpInvokerJob);
        CloseableHttpResponse mockResponse = mockCloseableHttpResponse(200);
        
        when(mockClient.execute(Mockito.any(HttpRequestBase.class))).thenReturn(mockResponse);
        
        httpInvokerJob.execute(createJobExecutionContext(createJobDetail(jobConfigurationBean, "csp.01.")));
        
        verify(mockClient, times(1)).execute(Mockito.any(HttpRequestBase.class));
    }
    
    @Test
    public void testHttp400() throws JobExecutionException, ClientProtocolException, IOException, ParseException {
        
        JobConfigurationBean jobConfigurationBean = new JobConfigurationBean();
        jobConfigurationBean.init();
        
        HttpInvokerJob httpInvokerJob = new HttpInvokerJob();
        
        CloseableHttpClient mockClient = mockHttpClient(jobConfigurationBean, httpInvokerJob);
        CloseableHttpResponse mockResponse = mockCloseableHttpResponse(400);
        
        when(mockClient.execute(Mockito.any(HttpRequestBase.class))).thenReturn(mockResponse);
        
        httpInvokerJob.execute(createJobExecutionContext(createJobDetail(jobConfigurationBean, "csp.01.")));
        
        verify(mockClient, times(2)).execute(Mockito.any(HttpRequestBase.class));
    }
    
    @Test
    public void testHttp400ThenOk() throws JobExecutionException, ClientProtocolException, IOException, ParseException {
        
        JobConfigurationBean jobConfigurationBean = new JobConfigurationBean();
        jobConfigurationBean.init();
        
        HttpInvokerJob httpInvokerJob = new HttpInvokerJob();
        
        CloseableHttpClient mockClient = mockHttpClient(jobConfigurationBean, httpInvokerJob);
        CloseableHttpResponse mockResponse = mockCloseableHttpResponse(400);
        CloseableHttpResponse mockResponse2 = mockCloseableHttpResponse(200);
        
        when(mockClient.execute(Mockito.any(HttpRequestBase.class))).thenReturn(mockResponse).thenReturn(mockResponse2);
        
        httpInvokerJob.execute(createJobExecutionContext(createJobDetail(jobConfigurationBean, "csp.01.")));
        
        verify(mockClient, times(2)).execute(Mockito.any(HttpRequestBase.class));
    }
    
    @SuppressWarnings("unchecked")
    @Test
    public void testFirstFailSecondSuccess() throws JobExecutionException, ClientProtocolException, IOException, ParseException {
        
        JobConfigurationBean jobConfigurationBean = new JobConfigurationBean();
        jobConfigurationBean.init();
        
        HttpInvokerJob httpInvokerJob = new HttpInvokerJob();
        
        CloseableHttpClient mockClient = mockHttpClient(jobConfigurationBean, httpInvokerJob);
        CloseableHttpResponse mockResponse = mockCloseableHttpResponse(200);
        
        when(mockClient.execute(Mockito.any(HttpRequestBase.class))).thenThrow(SocketTimeoutException.class).thenReturn(mockResponse);
        
        httpInvokerJob.execute(createJobExecutionContext(createJobDetail(jobConfigurationBean, "csp.01.")));
        
        verify(mockClient, times(2)).execute(Mockito.any(HttpRequestBase.class));
    }
    
    @SuppressWarnings("unchecked")
    @Test(expected=JobExecutionException.class)
    public void testBothFail() throws JobExecutionException, ClientProtocolException, IOException, ParseException {
        
        JobConfigurationBean jobConfigurationBean = new JobConfigurationBean();
        jobConfigurationBean.init();
        
        HttpInvokerJob httpInvokerJob = new HttpInvokerJob();
        
        CloseableHttpClient mockClient = mockHttpClient(jobConfigurationBean, httpInvokerJob);
        mockCloseableHttpResponse(200);
        
        when(mockClient.execute(Mockito.any(HttpRequestBase.class))).thenThrow(SocketTimeoutException.class).thenThrow(SocketTimeoutException.class);
        
        httpInvokerJob.execute(createJobExecutionContext(createJobDetail(jobConfigurationBean, "csp.01.")));
        
        verify(mockClient, times(2)).execute(Mockito.any(HttpRequestBase.class));
    }
    
    @Test
    public void testDisabled() throws JobExecutionException, ClientProtocolException, IOException, ParseException {
        
        JobConfigurationBean jobConfigurationBean = new JobConfigurationBean();
        jobConfigurationBean.init();
        
        HttpInvokerJob httpInvokerJob = new HttpInvokerJob();
        
        CloseableHttpClient mockClient = mockHttpClient(jobConfigurationBean, httpInvokerJob);
        
        httpInvokerJob.execute(createJobExecutionContext(createJobDetail(jobConfigurationBean, "csp.02.")));
        
        verify(mockClient, times(0)).execute(Mockito.any(HttpRequestBase.class));
    }
    
    @SuppressWarnings("unchecked")
    @Test
    public void testFirstFailTreatAsOk() throws JobExecutionException, ClientProtocolException, IOException, ParseException {
        
        JobConfigurationBean jobConfigurationBean = new JobConfigurationBean();
        jobConfigurationBean.init();
        
        HttpInvokerJob httpInvokerJob = new HttpInvokerJob();
        
        CloseableHttpClient mockClient = mockHttpClient(jobConfigurationBean, httpInvokerJob);

        when(mockClient.execute(Mockito.any(HttpRequestBase.class))).thenThrow(SocketTimeoutException.class);
        
        httpInvokerJob.execute(createJobExecutionContext(createJobDetail(jobConfigurationBean, "csp.03.")));
        
        verify(mockClient, times(1)).execute(Mockito.any(HttpRequestBase.class));
    }
    
    @SuppressWarnings("unchecked")
    @Test(expected=JobExecutionException.class)
    public void testFirstAndOnlyFail() throws JobExecutionException, ClientProtocolException, IOException, ParseException {
        
        JobConfigurationBean jobConfigurationBean = new JobConfigurationBean();
        jobConfigurationBean.init();
        
        HttpInvokerJob httpInvokerJob = new HttpInvokerJob();
        
        CloseableHttpClient mockClient = mockHttpClient(jobConfigurationBean, httpInvokerJob);

        when(mockClient.execute(Mockito.any(HttpRequestBase.class))).thenThrow(SocketTimeoutException.class);
        
        httpInvokerJob.execute(createJobExecutionContext(createJobDetail(jobConfigurationBean, "csp.04.")));
        
        verify(mockClient, times(0)).execute(Mockito.any(HttpRequestBase.class));
    }
    
    @Test
    public void testTimeout() throws JobExecutionException, ClientProtocolException, IOException, ParseException {
        
        JobConfigurationBean jobConfigurationBean = new JobConfigurationBean();
        jobConfigurationBean.init();
        
        HttpInvokerJob httpInvokerJob = new HttpInvokerJob();
        
        CloseableHttpClient mockClient = mockHttpClientTimeout(1,jobConfigurationBean, httpInvokerJob);
        
        final CloseableHttpResponse mockResponse = mockCloseableHttpResponse(200);
        
        when(mockClient.execute(Mockito.any(HttpRequestBase.class))).thenAnswer(new Answer<CloseableHttpResponse>() {
            @Override
            public CloseableHttpResponse answer(InvocationOnMock invocation){
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
    
                }
                return mockResponse;
            }
         });
        
        httpInvokerJob.execute(createJobExecutionContext(createJobDetail(jobConfigurationBean, "csp.05.")));
        
        verify(mockClient, times(1)).execute(Mockito.any(HttpRequestBase.class));
        
    }
    
    
    
    private CloseableHttpResponse mockCloseableHttpResponse(int reponseCode) {
        CloseableHttpResponse mockResponse = mock(CloseableHttpResponse.class);
        StatusLine mockStatusLine = mock(StatusLine.class);
        when(mockStatusLine.getStatusCode()).thenReturn(reponseCode);
        when(mockResponse.getStatusLine()).thenReturn(mockStatusLine);
        return mockResponse;
    }
    
    private CloseableHttpClient mockHttpClient(JobConfigurationBean jobConfigurationBean, HttpInvokerJob httpInvokerJob) throws ClientProtocolException, IOException {
        HttpClientBuilder mockHttpClientBuilder = mock(HttpClientBuilder.class);
        
        httpInvokerJob.setJobConfiguration(jobConfigurationBean);
        httpInvokerJob.setHttpClientBuilder(mockHttpClientBuilder);
       
        CloseableHttpClient mockClient = mock(CloseableHttpClient.class);
        
        
        when(mockHttpClientBuilder.build()).thenReturn(mockClient);
        when(httpInvokerJob.buildHttpClient(mockHttpClientBuilder)).thenReturn(mockClient);
        
        
       return mockClient;
    }
    
    private CloseableHttpClient mockHttpClientTimeout(int timeout, JobConfigurationBean jobConfigurationBean, HttpInvokerJob httpInvokerJob) throws ClientProtocolException, IOException {
        HttpClientBuilder mockHttpClientBuilder = mock(HttpClientBuilder.class);
        
        RequestConfig config = RequestConfig.custom()
                .setSocketTimeout(timeout * 1000)
                .setConnectionRequestTimeout(timeout * 1000)
                .setConnectTimeout(timeout * 1000).build();
        mockHttpClientBuilder = mockHttpClientBuilder
                .setDefaultRequestConfig(config);
        
        httpInvokerJob.setJobConfiguration(jobConfigurationBean);
        httpInvokerJob.setHttpClientBuilder(mockHttpClientBuilder);
       
        CloseableHttpClient mockClient = mock(CloseableHttpClient.class);
        
        
        when(mockHttpClientBuilder.build()).thenReturn(mockClient);
        when(httpInvokerJob.buildHttpClient(mockHttpClientBuilder)).thenReturn(mockClient);
        
        
       return mockClient;
    }
    
    @SuppressWarnings("deprecation")
    private JobExecutionContext createJobExecutionContext(JobDetail jobDetail) throws ParseException {
        
        JobExecutionContext jobExecutionContext = Mockito.mock(JobExecutionContext.class);
        when(jobExecutionContext.getJobDetail()).thenReturn(jobDetail);
        when(jobExecutionContext.getNextFireTime()).thenReturn(new Date());
        when(jobExecutionContext.getTrigger()).thenReturn(new CronTriggerImpl("name", "group", jobDetail.getJobDataMap().getString(JobProps.CRON.getValue())));
        
        
        return jobExecutionContext;
    }
    
    private JobDetail createJobDetail(JobConfiguration jobConfiguration, String jobPrefixHandled) {
        
        
        for(String prefix : jobConfiguration.getJobPrefixList()) {
            int jobsInPrefix = jobConfiguration.getNumberOfJobs(prefix);
            for (int i = 1; i <=jobsInPrefix ; i++) {
                String jobPrefix = jobConfiguration.getJobPrefix(prefix, i); 
                
                
                String name = jobConfiguration.getString(jobPrefix, JobProps.NAME);
                if (StringUtils.isEmpty(name)) {
                    break;
                }
                
                
                JobBuilder builder = newJob(HttpInvokerJob.class);
               
    
                JobDetail jobDetail = builder.withIdentity(
                        name + ".J", name + ".JG")
                        .usingJobData(JobProps.ENABLED.getValue(), jobConfiguration.getString(jobPrefix, JobProps.ENABLED))
                        .usingJobData(JobProps.BASE64AUTH.getValue(), jobConfiguration.getString(jobPrefix, JobProps.BASE64AUTH))
                        .usingJobData(JobProps.HTTPCONTENT.getValue(), jobConfiguration.getString(jobPrefix, JobProps.HTTPCONTENT))
                        .usingJobData(JobProps.HTTPMETHOD.getValue(), jobConfiguration.getString(jobPrefix, JobProps.HTTPMETHOD))
                        .usingJobData(JobProps.HTTPURL.getValue(), jobConfiguration.getHttpUrlString(jobPrefix))
                        .usingJobData(JobProps.TIMEOUT.getValue(), jobConfiguration.getString(jobPrefix, JobProps.TIMEOUT))
                        .usingJobData(JobProps.CRON.getValue(), jobConfiguration.getString(jobPrefix, JobProps.CRON))
                        .usingJobData(JobProps.LOADBALANCE.getValue(), jobConfiguration.getString(jobPrefix, JobProps.LOADBALANCE))
                        .usingJobData(JobProps.ROWPREFIX.getValue(), jobPrefix)
                        .build();
                       
                if(jobPrefixHandled.equals(jobPrefix)) {
                    return jobDetail;
                }
            }
        }
        
        
        return null;
    }
    
    

}
