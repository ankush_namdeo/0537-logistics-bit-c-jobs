package com.postnord.isp.lbc.jobs.service;

import java.io.File;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.quartz.SchedulerException;

import com.postnord.isp.lbc.jobs.config.JobConfiguration;
import com.postnord.isp.lbc.jobs.config.JobConfigurationBean;


public class QuartzServiceTest {

    @Before
    public void init() {
        File dir = new File(getClass().getResource("/").getFile());
        System.setProperty("jboss.server.base.dir", dir.getAbsolutePath());
    }
	
	@Test
	public void testStartSchedular() throws SchedulerException {
		JobConfiguration jobconf = new JobConfigurationBean();
		jobconf.reloadProperties();
		QuartzServiceClusterBean quartzServiceBean = new QuartzServiceClusterBean();
		quartzServiceBean.setJobConfiguration(jobconf);
		quartzServiceBean.start();
		
		Assert.assertTrue(quartzServiceBean.getScheduler().isStarted());
	}
	
	@Test
	public void testStopSchedular() throws SchedulerException {
		JobConfiguration jobconf = new JobConfigurationBean();
		jobconf.reloadProperties();
		QuartzServiceClusterBean quartzServiceBean = new QuartzServiceClusterBean();
		quartzServiceBean.setJobConfiguration(jobconf);
		quartzServiceBean.start();
		quartzServiceBean.shutdown();
		
		Assert.assertTrue(quartzServiceBean.getScheduler().isShutdown());
	}
}
