package se.posten.loab.lisp.common.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import se.posten.loab.lisp.common.validation.impl.TrackingInformationValidatorImpl;

public class TrackingInformationValidatorTest {

    private TrackingInformationValidator validator = new TrackingInformationValidatorImpl();

    @Test
    public void testValidateValidItemId() {
        String validDPD15ItemId = "03015016342972M";
        assertTrue(validator.validateItemId(validDPD15ItemId));

        String validDPD14ItemId = "03015016342972";
        assertTrue(validator.validateItemId(validDPD14ItemId));

    }

    @Test
    public void testNotValidateInvalidItemId() {
        String invalidDPDItemId = "03015016342972N";
        assertFalse(validator.validateItemId(invalidDPDItemId));
    }

    @Test
    public void testValidateItemId() throws Exception {
        assertTrue(validator.validateItemId("84273112223SE"));
        assertFalse(validator.validateItemId("12345678901SE"));
    }

    @Test
    public void testValidateShipmentId() throws Exception {
        assertTrue(validator.validateShipmentId("84273112223SE"));
        assertFalse(validator.validateShipmentId("274rt2878yt27yt892yh437t923tyhtfgh892yh437t923tyhtfgh"));
        assertFalse(validator.validateShipmentId("274rt"));
        assertFalse(validator.validateShipmentId("84273åäö23SE"));
    }

    @Test
    public void testValidateCustomerNumber() throws Exception {
        assertTrue(validator.validateCustomerNumber("1234567"));
        assertFalse(validator.validateCustomerNumber("1234567890123"));
        assertFalse(validator.validateCustomerNumber("Abcdef"));
    }

    @Test
    public void testValidateReference() throws Exception {
        assertTrue(validator.validateReference("213, dsf, gg: erqwerRRäö."));
        assertFalse(validator.validateReference("1234567890123456789012345678901234567890"));
        assertTrue(validator.validateReference("213, dsf, gg: ÆøerqwerRRæØ."));
        assertTrue(validator.validateReference("213, ,_-/.:*"));
    }

    @Test
    public void testValidatePhoneNumber() throws Exception {
        assertTrue(validator.validatePhoneNumber("08-12345678"));
        assertTrue(validator.validatePhoneNumber("08-123 456 78"));
        assertTrue(validator.validatePhoneNumber("08/12345678"));
        assertTrue(validator.validatePhoneNumber("+46812345678"));
        assertFalse(validator.validatePhoneNumber("08+12345678"));
        assertFalse(validator.validatePhoneNumber("1234567"));
        assertFalse(validator.validatePhoneNumber("12345678901231234567890123"));
    }

    @Test
    public void testNotificationCode() throws Exception {
        assertTrue(validator.validateNotificationCode("1234"));
        assertFalse(validator.validateNotificationCode("12345"));
        assertFalse(validator.validateNotificationCode("123"));
        assertFalse(validator.validateNotificationCode("123x"));
    }

}
