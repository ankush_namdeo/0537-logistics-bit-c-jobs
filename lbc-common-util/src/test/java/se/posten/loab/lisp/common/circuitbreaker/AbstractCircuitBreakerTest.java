package se.posten.loab.lisp.common.circuitbreaker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import se.posten.loab.lisp.common.circuitbreaker.support.CircuitBreakerAspectSupport;
import se.posten.loab.lisp.common.circuitbreaker.test.IStockService;
import se.posten.loab.lisp.common.circuitbreaker.util.Duration;

public abstract class AbstractCircuitBreakerTest {

    protected IStockService stocks;
    protected CircuitBreakerAspectSupport stocksBreaker;

    @Before
    public void resetCircuit() {
        // this is needed because in Spring the service and the aspect are
        // singletons.
        CircuitInfo circuit = stocksBreaker.getCircuitInfo();
        circuit.close();
        circuit.resetToDefaultConfig();
        circuit.resetStatistics();
    }

    @Test
    public void normal_operation_while_closed() {
        assertEquals(5, stocks.getQuote("JAVA"));
    }

    @Test(expected = OpenCircuitException.class)
    public void after_a_number_of_faults_the_circuit_opens() {
        generateFaultsToOpen();
        stocks.getQuote("JAVA");
    }

    @Test
    public void the_circuit_can_be_opened_after_being_closed() {
        generateFaultsToOpen();
        try {
            stocks.getQuote("JAVA");
        } catch (OpenCircuitException e) {
            CircuitInfo c = e.getCircuit();
            assertCircuitOpen(c);
            c.close();
            assertCircuitClosed(c);
            assertEquals(5, stocks.getQuote("JAVA"));
            return;
        }
        fail("Should have raised an OpenCircuitException");
    }

    @Test
    public void the_circuit_is_half_open_after_the_timeout() throws Exception {
        CircuitInfo circuit = stocksBreaker.getCircuitInfo();
        circuit.setTimeout(Duration.millis(10));
        generateFaultsToOpen();
        Thread.sleep(100);
        assertCircuitHalfOpen(circuit);
        assertEquals(5, stocks.getQuote("JAVA"));
        assertEquals(0, circuit.getCurrentFailures());
    }

    @Test
    public void the_circuit_moves_from_half_open_to_open_on_first_failure() throws Exception {
        CircuitInfo circuit = stocksBreaker.getCircuitInfo();
        circuit.setTimeout(Duration.millis(10));
        generateFaultsToOpen();
        Thread.sleep(100);
        assertCircuitHalfOpen(circuit);
        try {
            stocks.faultyGetQuote("JAVA");
        } catch (RuntimeException expected) {
        }
        assertCircuitOpen(circuit);
    }

    @Test
    public void the_failure_count_gets_reset_after_an_amount_of_time() throws Exception {
        CircuitInfo circuit = stocksBreaker.getCircuitInfo();
        circuit.setCurrentFailuresDuration(Duration.millis(10));

        generateFaults(CircuitInfo.DEFAULT_MAX_FAILURES - 1);
        assertCircuitClosed(circuit);
        Thread.sleep(100);

        generateFaults(1);
        assertCircuitClosed(circuit);
        assertEquals(5, stocks.getQuote("JAVA"));
    }

    @Test
    public void almost_instant_failure_count_reset() {
        CircuitInfo circuit = stocksBreaker.getCircuitInfo();
        circuit.setCurrentFailuresDuration(Duration.nanos(1));
        generateFaultsToOpen();
        assertEquals(5, stocks.getQuote("JAVA"));
        assertCircuitClosed(stocksBreaker.getCircuitInfo());
    }

    @Test
    public void the_circuit_moves_from_half_open_to_open_on_first_failure_even_if_failure_count_is_reset()
        throws Exception {
        CircuitInfo circuit = stocksBreaker.getCircuitInfo();
        circuit.setTimeout(Duration.millis(10));
        generateFaultsToOpen();
        Thread.sleep(100);
        assertCircuitHalfOpen(circuit);
        circuit.setCurrentFailuresDuration(Duration.millis(100));
        try {
            stocks.faultyGetQuote("JAVA");
        } catch (RuntimeException expected) {
        }
        assertCircuitOpen(circuit);
    }

    @Test
    public void ignored_exceptions_do_not_open_a_circuit() {
        stocksBreaker.ignoreException(ArithmeticException.class);
        generateFaultsToOpen();
        assertEquals(5, stocks.getQuote("JAVA"));
        assertCircuitClosed(stocksBreaker.getCircuitInfo());
        stocksBreaker.removeIgnoredExcpetion(ArithmeticException.class);
    }

    @Test
    public void ignored_exceptions_capture_subclasses() {
        stocksBreaker.ignoreException(RuntimeException.class);
        generateFaultsToOpen();
        assertEquals(5, stocks.getQuote("JAVA"));
        assertCircuitClosed(stocksBreaker.getCircuitInfo());
        stocksBreaker.removeIgnoredExcpetion(RuntimeException.class);
    }

    @Test
    public void slow_metnod_executions_count_as_failures() {
        stocksBreaker.setMaxMethodDuration(Duration.nanos(1));
        for (int i = 0; i < CircuitInfo.DEFAULT_MAX_FAILURES; i++)
            stocks.getQuote("JAVA");
        assertCircuitOpen(stocksBreaker.getCircuitInfo());
        stocksBreaker.setMaxMethodDuration(null);
    }

    // ------------------------------------------------------------------------

    protected void generateFaultsToOpen() {
        generateFaults(CircuitInfo.DEFAULT_MAX_FAILURES);
    }

    protected void generateFaults(int numOfFaults) {
        for (int i = 0; i < numOfFaults; i++) {
            try {
                stocks.faultyGetQuote("JAVA");
            } catch (ArithmeticException expected) {
            }
        }
    }

    protected void assertCircuitOpen(CircuitInfo circuitInfo) {
        assertTrue(circuitInfo.isOpen());
        assertFalse(circuitInfo.isClosed());
        assertFalse(circuitInfo.isHalfOpen());
        assertNotNull(circuitInfo.getOpenTimestamp());
    }

    protected void assertCircuitClosed(CircuitInfo circuitInfo) {
        assertTrue(circuitInfo.isClosed());
        assertFalse(circuitInfo.isOpen());
        assertFalse(circuitInfo.isHalfOpen());
        assertNull(circuitInfo.getOpenTimestamp());
    }

    protected void assertCircuitHalfOpen(CircuitInfo circuitInfo) {
        assertTrue(circuitInfo.isHalfOpen());
        assertTrue(circuitInfo.isClosed());
        assertFalse(circuitInfo.isOpen());
    }
}
