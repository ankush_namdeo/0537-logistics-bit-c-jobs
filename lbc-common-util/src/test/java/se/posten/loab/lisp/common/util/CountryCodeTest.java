package se.posten.loab.lisp.common.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

/** Junit for testing country codes
 *
 * @author Christer Wikman, DevCode
 *
 */
public class CountryCodeTest {

    @Test
    public void testGetTwoAlphaCode() {
        CountryCode DNK = CountryCode.getInstanceFromIsoTwoAlpha("DK");
        assertEquals("DK", DNK.getTwoAlphaCode());
    }

    @Test
    public void testGetThreeAlphaCode() {
        CountryCode DNK = CountryCode.getInstanceFromIsoThreeAlpha("DNK");
        assertEquals("DNK", DNK.getThreeAlphaCode());
    }

    @Test
    public void testGetThreeDigitCode() {
        CountryCode DNK = CountryCode.getInstanceFromIsoThreeDigit("208");
        assertEquals("208", DNK.getThreeDigitCode());
    }

    @Test
    public void testGetName() {
        CountryCode DNK = CountryCode.getInstanceFromIsoThreeAlpha("DNK");
        assertEquals("DENMARK", DNK.getName());
    }

    @Test
    public void testToString() {
        CountryCode DNK = CountryCode.getInstanceFromIsoThreeAlpha("DNK");
        assertNotNull(DNK.toString());
    }

    @Test
    public void testGetInstanceFromIsoTwoAlpha() {
        CountryCode SWE = CountryCode.getInstanceFromIsoTwoAlpha("SE");
        assertNotNull(SWE);
    }

    @Test
    public void testGetInstanceFromIsoThreeDigit() {
        CountryCode SWE = CountryCode.getInstanceFromIsoThreeDigit("752");
        assertNotNull(SWE);
    }

    @Test
    public void testGetInstanceFromIsoThreeAlpha() {
        CountryCode SWE = CountryCode.getInstanceFromIsoThreeAlpha("SWE");
        assertNotNull(SWE);
    }

    @Test
    public void testSWE() {
        assertEquals("SWE",CountryCode.SWE.getThreeAlphaCode());
    }

    @Test
    public void testFIN() {
        assertEquals("FIN",CountryCode.FIN.getThreeAlphaCode());
    }

    @Test
    public void testDNK() {
        assertEquals("DNK",CountryCode.DNK.getThreeAlphaCode());
    }

    @Test
    public void testNOR() {
        assertEquals("NOR",CountryCode.NOR.getThreeAlphaCode());
    }

}
