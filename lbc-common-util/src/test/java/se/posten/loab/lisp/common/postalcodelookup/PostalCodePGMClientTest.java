package se.posten.loab.lisp.common.postalcodelookup;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import se.posten.loab.lisp.common.util.CountryCode;

/** Junit test of PostalCodePGMClient
 * 
 * @author Christer Wikman, DevCode
 *
 */
public class PostalCodePGMClientTest {

	@Test
	public void testGetPostalCode() throws Exception {
		PostalCodePGMClient client = new PostalCodePGMClient();
		PostalCode pc = client.getPostalCode(CountryCode.SWE, "11245");
		assertNotNull(pc);
		assertEquals(CountryCode.SWE, pc.getCountryCode());
		assertEquals("11245", pc.getCode());
		assertEquals("STOCKHOLM", pc.getCity());
	}

}
