package se.posten.loab.lisp.common.validation.ella;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class EllaValidatorTest {

    @Test
    public void testRemoveDpdCheck() throws Exception {
        assertEquals(14, EllaValidator.removeDpdChecksum("123456789012345").length());
        assertEquals(14, EllaValidator.removeDpdChecksum("12345678901234").length());
    }
}
