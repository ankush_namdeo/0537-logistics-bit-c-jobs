package se.posten.loab.lisp.common.circuitbreaker.proxy;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import se.posten.loab.lisp.common.circuitbreaker.AbstractCircuitBreakerTest;
import se.posten.loab.lisp.common.circuitbreaker.jmx.CircuitBreakerMBean;
import se.posten.loab.lisp.common.circuitbreaker.jmx.JmxUtils;
import se.posten.loab.lisp.common.circuitbreaker.test.IStockService;
import se.posten.loab.lisp.common.circuitbreaker.test.StockService;

public class CircuitBreakerTest extends AbstractCircuitBreakerTest {

    public CircuitBreakerTest() {
        IStockService delegate = new StockService();
        CircuitBreaker breaker = new CircuitBreaker();
        stocks = breaker.createDynamicProxy(delegate);
        stocksBreaker = breaker;
    }

    @Test
    public void enable_and_use_jmx() throws Exception {
        stocksBreaker.setEnableJmx(true);
        stocks.getQuote("JAVA");

        CircuitBreakerMBean mbean = JmxUtils.circuitBreakersForType(StockService.class).iterator().next();
        assertTrue(mbean.getCalls() >= 1);

        mbean.open();
        assertTrue(stocksBreaker.getCircuitInfo().isOpen());

        stocksBreaker.setEnableJmx(false);
    }
}
