package se.posten.loab.lisp.common.validation.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class ValidationUtilsTest {

	@Test
	public void shouldRecognizeValidDPDItemId() {
		assertTrue(ValidationUtils.isDPDItem("03015016342972M"));
	}
	
	@Test
	public void shouldRecognizeInvalidDPDItemId() {
		assertFalse(ValidationUtils.isDPDItem("03015016342972N"));
	}
}
