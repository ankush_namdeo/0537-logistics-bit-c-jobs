package se.posten.loab.lisp.common.circuitbreaker.test;

import java.util.Date;

public class TimeService implements ITimeService {

	public Date networkTime() {
		return EXPECTED;
	}
	
	public Date faultyNetworkTime() {
		throw new IllegalStateException();
	}
}
