package se.posten.loab.lisp.common.circuitbreaker;

import static org.junit.Assert.*;

import javax.management.JMException;

import se.posten.loab.lisp.common.circuitbreaker.jmx.CircuitBreakerMBean;
import se.posten.loab.lisp.common.circuitbreaker.jmx.JmxUtils;
import se.posten.loab.lisp.common.circuitbreaker.test.ITimeService;


public abstract class AbstractJmxTest {
	
	protected ITimeService time;
	
	protected abstract void disableJmx();
	
	protected CircuitBreakerMBean mbean() throws JMException {
		return JmxUtils.getCircuitBreaker(time);
	}
	
	protected void readStatsAndOpenCircuitViaJmx() throws Exception {
		time.networkTime();
		
		CircuitBreakerMBean mbean = mbean();
		assertTrue(mbean.getCalls() >= 1);
		assertEquals(0, mbean.getCurrentFailures());
		assertEquals(0, mbean.getFailures());
		assertEquals(0, mbean.getTimesOpened());
		assertTrue(mbean.isClosed());
		assertFalse(mbean.isHalfOpen());
		assertFalse(mbean.isOpen());
		
		mbean.open();
		assertTrue(mbean.isOpen());
		time.networkTime();
	}
}
