package se.posten.loab.lisp.common.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PostAddressUtilTest {

    @Test
    public void shouldSplitSimpleAddress() {
        String[] result = PostAddressUtil.splitStreetAddressToStreetAndNumber("Gatan 1");
        assertEquals(result[0], "Gatan");
        assertEquals(result[1], "1");
    }

    @Test
    public void shouldSplitAddressWithSpaces() {
        String[] result = PostAddressUtil.splitStreetAddressToStreetAndNumber("  Tredje  Gatan  1  ");
        assertEquals(result[0], "Tredje  Gatan");
        assertEquals(result[1], "1");
    }

    @Test
    public void shouldSplitAddressWithLeadingNumber() {
        String[] result = PostAddressUtil.splitStreetAddressToStreetAndNumber("3:e Gatan 1");
        assertEquals(result[0], "3:e Gatan");
        assertEquals(result[1], "1");
    }

    @Test
    public void shouldSplitAddressWithoutNumber() {
        String[] result = PostAddressUtil.splitStreetAddressToStreetAndNumber("3:e Gatan Utan Nummer");
        assertEquals(result[0], "3:e Gatan Utan Nummer");
        assertEquals(result[1], "");
    }

    @Test
    public void shouldSplitAddressWithNumberRange() {
        String[] result = PostAddressUtil.splitStreetAddressToStreetAndNumber("Gatan 12-405");
        assertEquals(result[0], "Gatan");
        assertEquals(result[1], "12-405");
    }

    @Test
    public void shouldSplitAddressWithStairs() {
        String[] result = PostAddressUtil.splitStreetAddressToStreetAndNumber("Gatan 12, 3tr");
        assertEquals(result[0], "Gatan");
        assertEquals(result[1], "12, 3tr");
    }

    @Test
    public void shouldSplitAddressOnGroundFloor() {
        String[] result = PostAddressUtil.splitStreetAddressToStreetAndNumber("Gatan 12 nb");
        assertEquals(result[0], "Gatan");
        assertEquals(result[1], "12 nb");
    }

}
