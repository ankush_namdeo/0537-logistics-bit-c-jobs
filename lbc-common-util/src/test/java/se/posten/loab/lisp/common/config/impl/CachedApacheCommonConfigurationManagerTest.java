package se.posten.loab.lisp.common.config.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.Properties;

import org.apache.commons.configuration.Configuration;
import org.junit.Test;

import se.posten.loab.lisp.common.config.ConfigurationManager;
import se.posten.loab.lisp.common.config.ConfigurationParameterNotFoundException;
import se.posten.loab.lisp.common.config.InvalidConfigurationParameterValueException;

public class CachedApacheCommonConfigurationManagerTest {

    private static final String CONFIG_FILE_NAME = "/cachedApacheCommonConfigurationManager-configuration.xml";
    private static final String OVERRIDDEN_STRING = "new value";
    private static final String SWEDISH_STRING = "ABCÅÄÖ";
    private static final String SIMPLE_STRING = "simple string";
    private static final String SHORT_LIST_STRING_1 = "A";
    private static final String LONG_LIST_STRING_1 = "A";
    private static final String LONG_LIST_STRING_2 = "B";
    private static final String LONG_LIST_STRING_3 = "C";
    private static final String LONG_LIST_STRING_4 = "D";
    private static final String LONG_LIST_STRING_5 = "E";

    private void assertCorrectConfiguration(ConfigurationManager config) {
        assertTrue(config.getBoolean("test.boolean.true"));
        assertFalse(config.getBoolean("test.boolean.false"));
        assertTrue(config.getInt("test.integer.zero") == 0);
        assertTrue(config.getInt("test.integer.one") == 1);
        assertEquals(SIMPLE_STRING, config.getString("test.string.simple"));
        assertEquals(SWEDISH_STRING, config.getString("test.string.swedish"));
        assertEquals(OVERRIDDEN_STRING, config.getString("test.string.override"));

        List<String> list = config.getList("test.list.short");
        assertTrue(list.size() == 1);
        assertEquals(SHORT_LIST_STRING_1, list.get(0));

        list = config.getList("test.list.long");
        assertTrue(list.size() == 5);
        assertEquals(LONG_LIST_STRING_1, list.get(0));
        assertEquals(LONG_LIST_STRING_2, list.get(1));
        assertEquals(LONG_LIST_STRING_3, list.get(2));
        assertEquals(LONG_LIST_STRING_4, list.get(3));
        assertEquals(LONG_LIST_STRING_5, list.get(4));
    }

    private void assertCorrectProperties(ConfigurationManager config) {
        Properties props = config.getAllProperties();
        assertEquals("true", props.get("test.boolean.true"));
        assertEquals("no", props.get("test.boolean.false"));
        assertEquals("0", props.get("test.integer.zero"));
        assertEquals("1", props.get("test.integer.one"));
        assertEquals(SIMPLE_STRING, props.get("test.string.simple"));
        assertEquals(SWEDISH_STRING, props.get("test.string.swedish"));
        assertEquals(OVERRIDDEN_STRING, props.get("test.string.override"));
        assertEquals(SHORT_LIST_STRING_1, props.get("test.list.short"));
        assertEquals(LONG_LIST_STRING_1 + "," + LONG_LIST_STRING_2 + "," + LONG_LIST_STRING_3 + ","
                + LONG_LIST_STRING_4 + "," + LONG_LIST_STRING_5, props.get("test.list.long"));
    }

    @SuppressWarnings("unchecked")
    private void assertCorrectApacheConfiguration(Configuration config) {
        assertTrue(config.getBoolean("test.boolean.true"));
        assertFalse(config.getBoolean("test.boolean.false"));
        assertTrue(config.getInt("test.integer.zero") == 0);
        assertTrue(config.getInt("test.integer.one") == 1);
        assertEquals(SIMPLE_STRING, config.getString("test.string.simple"));
        assertEquals(SWEDISH_STRING, config.getString("test.string.swedish"));
        assertEquals(OVERRIDDEN_STRING, config.getString("test.string.override"));

        List<String> list = config.getList("test.list.short");
        assertTrue(list.size() == 1);
        assertEquals(SHORT_LIST_STRING_1, list.get(0));

        list = config.getList("test.list.long");
        assertTrue(list.size() == 5);
        assertEquals(SHORT_LIST_STRING_1, list.get(0));
        assertEquals(LONG_LIST_STRING_2, list.get(1));
        assertEquals(LONG_LIST_STRING_3, list.get(2));
        assertEquals(LONG_LIST_STRING_4, list.get(3));
        assertEquals(LONG_LIST_STRING_5, list.get(4));
    }

    private class MyConfigurationManager extends CachedApacheCommonConfigurationManager implements ConfigurationManager {
        @Override
        protected String getConfigFileName() {
            return CONFIG_FILE_NAME;
        }
    }

    @Test
    public void shouldReadCorrectConfigForSimpleConstructor() {
        CachedApacheCommonConfigurationManager config = new CachedApacheCommonConfigurationManager(CONFIG_FILE_NAME);
        assertCorrectConfiguration(config);
    }

    @Test
    public void shouldReadCorrectConfigForConstructorWithZeroCacheTime() {
        CachedApacheCommonConfigurationManager config = new CachedApacheCommonConfigurationManager(CONFIG_FILE_NAME, 0);
        assertCorrectConfiguration(config);
    }

    @Test
    public void shouldReadCorrectConfigForConstructorWithLongCacheTime() {
        CachedApacheCommonConfigurationManager config = new CachedApacheCommonConfigurationManager(CONFIG_FILE_NAME,
                24 * 60 * 60 * 1000);
        assertCorrectConfiguration(config);
    }

    @Test
    public void shouldReadCorrectConfigForDefaultConstructorWithInit() {
        CachedApacheCommonConfigurationManager config = new CachedApacheCommonConfigurationManager();
        config.init(CONFIG_FILE_NAME);
        assertCorrectConfiguration(config);
    }

    @Test
    public void shouldReadCorrectConfigForDefaultConstructorWithInitAndZeroCacheTime() {
        CachedApacheCommonConfigurationManager config = new CachedApacheCommonConfigurationManager();
        config.init(CONFIG_FILE_NAME, 0);
        assertCorrectConfiguration(config);
    }

    @Test
    public void shouldReadCorrectConfigForDefaultConstructorWithInitAndLongCacheTime() {
        CachedApacheCommonConfigurationManager config = new CachedApacheCommonConfigurationManager();
        config.init(CONFIG_FILE_NAME, 24 * 60 * 60 * 1000);
        assertCorrectConfiguration(config);
    }

    @Test
    public void shouldReadCorrectConfigWhenOverridingGetConfigFileName() {
        MyConfigurationManager config = new MyConfigurationManager();
        assertCorrectConfiguration(config);
    }

    @Test
    public void shouldReturnCorrectProperties() {
        MyConfigurationManager config = new MyConfigurationManager();
        assertCorrectProperties(config);
    }

    @Test
    public void shouldThrowExceptionIfNotInitialized() {
        CachedApacheCommonConfigurationManager config = new CachedApacheCommonConfigurationManager();
        try {
            config.getInt("test.integer.zero");
        } catch (IllegalStateException e) {
            assertTrue(e.getMessage().indexOf("initialized") > 0);
            return;
        }
        fail();
    }

    @Test
    public void shouldThrowExceptionIfAlreadyInitializedAndCallingInit1() {
        CachedApacheCommonConfigurationManager config = new CachedApacheCommonConfigurationManager(CONFIG_FILE_NAME);
        try {
            config.init("dummy");
        } catch (IllegalStateException e) {
            assertTrue(e.getMessage().indexOf("initialized") > 0);
            return;
        }
        fail();
    }

    @Test
    public void shouldThrowExceptionIfAlreadyInitializedAndCallingInit2() {
        CachedApacheCommonConfigurationManager config = new CachedApacheCommonConfigurationManager(CONFIG_FILE_NAME);
        try {
            config.init("dummy", 0);
        } catch (IllegalStateException e) {
            assertTrue(e.getMessage().indexOf("initialized") > 0);
            return;
        }
        fail();
    }

    @Test
    public void shouldReturnCorrectApacheConfiguration() {
        MyConfigurationManager configMgr = new MyConfigurationManager();
        Configuration config = configMgr.getConfiguration();
        assertCorrectApacheConfiguration(config);
    }

    @Test
    public void shouldSupportRefresh() {
        MyConfigurationManager config = new MyConfigurationManager();
        assertCorrectConfiguration(config);
        assertCorrectProperties(config);
        // This is quite a bad test. Should test with changed input file.
        config.refresh();
        assertCorrectConfiguration(config);
        assertCorrectProperties(config);
    }

    @Test
    public void shouldReturnInfoAboutItself() {
        CachedApacheCommonConfigurationManager config = new CachedApacheCommonConfigurationManager(CONFIG_FILE_NAME, 10);
        Properties props = config.getInfoAboutConfigurationManager();
        assertEquals("se.posten.loab.lisp.common.config.impl.CachedApacheCommonConfigurationManager", props
                .get("className"));
        assertEquals(CONFIG_FILE_NAME, props.get("configFile"));
        assertEquals("10ms", props.get("cacheTtl"));
        assertEquals("11", props.get("numberOfConfigurationParameters"));
    }

    @Test
    public void shouldReturnNullForMissingStringParameter() {
        MyConfigurationManager config = new MyConfigurationManager();
        assertNull(config.getString("dummy"));
    }

    @Test(expected = ConfigurationParameterNotFoundException.class)
    public void shouldThrowExceptionForMissingBooleanParameter() {
        MyConfigurationManager config = new MyConfigurationManager();
        config.getBoolean("dummy");
        fail();
    }

    @Test(expected = ConfigurationParameterNotFoundException.class)
    public void shouldThrowExceptionForMissingIntegerParameter() {
        MyConfigurationManager config = new MyConfigurationManager();
        config.getInt("dummy");
        fail();
    }

    @Test
    public void shouldreturnEmptyListForMissingListParameter() {
        MyConfigurationManager config = new MyConfigurationManager();
        List<String> list = config.getList("dummy");
        assertTrue(list.size() == 0);
    }

    @Test(expected = InvalidConfigurationParameterValueException.class)
    public void shouldThrowExceptionForInvalidBooleanParameter() {
        MyConfigurationManager config = new MyConfigurationManager();
        config.getBoolean("test.boolean.invalid");
        fail();
    }

    @Test(expected = InvalidConfigurationParameterValueException.class)
    public void shouldThrowExceptionForInvalidIntegerParameter() {
        MyConfigurationManager config = new MyConfigurationManager();
        config.getInt("test.integer.invalid");
        fail();
    }

}
