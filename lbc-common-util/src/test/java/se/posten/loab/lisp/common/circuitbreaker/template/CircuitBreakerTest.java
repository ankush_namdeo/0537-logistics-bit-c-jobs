package se.posten.loab.lisp.common.circuitbreaker.template;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.management.ManagementFactory;
import java.util.Set;
import java.util.concurrent.Callable;

import javax.management.JMX;
import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.junit.Test;

import se.posten.loab.lisp.common.circuitbreaker.AbstractCircuitBreakerTest;
import se.posten.loab.lisp.common.circuitbreaker.jmx.CircuitBreakerMBean;
import se.posten.loab.lisp.common.circuitbreaker.test.IStockService;
import se.posten.loab.lisp.common.circuitbreaker.test.StockService;

public class CircuitBreakerTest extends AbstractCircuitBreakerTest {

    private final CircuitBreaker templateBreaker;
    private final IStockService delegate;

    public CircuitBreakerTest() {
        delegate = new StockService();
        templateBreaker = new CircuitBreaker();
        stocks = new StockServiceDelegator(delegate, templateBreaker);
        stocksBreaker = templateBreaker;
    }

    @Test
    public void typical_simple_usage() throws Exception {
        int quote = templateBreaker.execute(new Callable<Integer>() {
            
            public Integer call() throws Exception {
                return delegate.getQuote("JAVA");
            }
        });

        assertEquals(5, quote);
    }

    @Test
    public void enable_and_use_jmx() throws Exception {
        stocksBreaker.setEnableJmx(true);
        stocks.getQuote("JAVA");

        MBeanServer server = ManagementFactory.getPlatformMBeanServer();
        String query = "com.tzavellas.circuitbreaker:type=CircuitBreaker,target=*,code=*";
        Set<ObjectName> names = server.queryNames(new ObjectName(query), null);
        assertTrue(names.size() >= 1);
        ObjectName name = names.iterator().next();
        CircuitBreakerMBean mbean = JMX.newMBeanProxy(ManagementFactory.getPlatformMBeanServer(), name,
                CircuitBreakerMBean.class);
        assertTrue(mbean.getCalls() >= 1);

        mbean.open();
        assertTrue(stocksBreaker.getCircuitInfo().isOpen());

        stocksBreaker.setEnableJmx(false);
    }

    private final class StockServiceDelegator implements IStockService {
        private final IStockService delegate;
        private final CircuitBreaker breaker;

        private StockServiceDelegator(IStockService delegate, CircuitBreaker breaker) {
            this.delegate = delegate;
            this.breaker = breaker;
        }

      
        public int getQuote(final String ticker) {
            return breaker.execute(new Callable<Integer>() {
              
                public Integer call() {
                    return delegate.getQuote(ticker);
                }
            });
        }

      
        public int faultyGetQuote(final String ticker) {
            return breaker.execute(new Callable<Integer>() {
               
                public Integer call() {
                    return delegate.faultyGetQuote(ticker);
                }
            });
        }
    }

}
