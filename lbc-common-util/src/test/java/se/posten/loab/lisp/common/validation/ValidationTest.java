package se.posten.loab.lisp.common.validation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import se.posten.loab.lisp.common.validation.ella.EllaValidator;
import se.posten.loab.lisp.common.validation.impl.AsciiCharactersAndDigitsValidator;
import se.posten.loab.lisp.common.validation.impl.DigitsValidator;
import se.posten.loab.lisp.common.validation.impl.EmailValidator;
import se.posten.loab.lisp.common.validation.impl.ISO31661Alpha2CountryCodeValidator;
import se.posten.loab.lisp.common.validation.impl.ISO88591NoControlCharsValidator;
import se.posten.loab.lisp.common.validation.impl.LengthValidator;
import se.posten.loab.lisp.common.validation.impl.LocaleValidator;
import se.posten.loab.lisp.common.validation.impl.NordicTextValidator;
import se.posten.loab.lisp.common.validation.impl.PostcodeValidator;
import se.posten.loab.lisp.common.validation.impl.SAPCustomerNumberValidator;

public class ValidationTest {

    @Test
    public void testCharactersAndDigitsValidator() throws Exception {
        AsciiCharactersAndDigitsValidator validator = new AsciiCharactersAndDigitsValidator();
        assertFalse(validator.validate("sgdsg+rd0"));
        assertFalse(validator.validate(null));
        assertFalse(validator.validate("dgaåäöÅÄÖ4365fSS"));
        assertTrue(validator.validate(""));
        assertTrue(validator.validate("dgaKJF4365fSS"));
    }

    @Test
    public void testDigitsValidator() throws Exception {
        DigitsValidator validator = new DigitsValidator();
        assertFalse(validator.validate("dga4365fSS"));
        assertFalse(validator.validate("sgdsg+rd0"));
        assertFalse(validator.validate(null));
        assertTrue(validator.validate(""));
        assertTrue(validator.validate("3242"));
    }

    @Test
    public void testLengthValidator() throws Exception {
        LengthValidator validator = new LengthValidator(5, 10);
        assertFalse(validator.validate("1234"));
        assertFalse(validator.validate("12345678901"));
        assertFalse(validator.validate(null));
        assertTrue(validator.validate("12345"));
        assertTrue(validator.validate("123456789Å"));
    }

    @Test
    public void testNordicTextValidator() throws Exception {
        NordicTextValidator validator = new NordicTextValidator();
        assertFalse(validator.validate("sgdsg+rd0"));
        assertFalse(validator.validate(null));
        assertTrue(validator.validate("dgaåäöÅÄÖ4365fSS"));
        assertTrue(validator.validate(""));
        assertTrue(validator.validate("kjh,f:d//_.*666 KJGFhhf3"));
        assertTrue(validator.validate("dgaKJF4365fSS"));
    }

    @Test
    public void testRemoveDpdCheck() throws Exception {
        assertEquals(14, EllaValidator.removeDpdChecksum("123456789012345").length());
        assertEquals(14, EllaValidator.removeDpdChecksum("12345678901234").length());
    }

    @Test
    public void testEmailValidator() throws Exception {
        EmailValidator validator = new EmailValidator();
        assertFalse(validator.validate("@test.com"));
        assertFalse(validator.validate(null));
        assertFalse(validator.validate(""));
        assertFalse(validator.validate("user@subdomain"));
        assertFalse(validator.validate("junk"));
        assertFalse(validator.validate("dgaåäöÅÄÖ4365fSS"));
        assertFalse(validator.validate("user@subdomain.c"));
        assertTrue(validator.validate("user@subdomain.com"));
        assertTrue(validator.validate("a@b.nu"));
    }

    @Test
    public void testISO88591NoControlCharsValidator() throws Exception {
        ISO88591NoControlCharsValidator validator = new ISO88591NoControlCharsValidator();
        assertFalse(validator.validate("sgdsg\n"));
        assertFalse(validator.validate("sgdsg\t"));
        assertFalse(validator.validate("aaa\raaa"));
        assertFalse(validator.validate("sgdsg\u0100"));
        assertFalse(validator.validate("sgdsg\u0000"));
        assertFalse(validator.validate(null));
        assertTrue(validator.validate("dgaåäöÅÄÖ4365fSS&<>~ÿ "));
        assertTrue(validator.validate("dgaåäö\u00A0"));
        assertTrue(validator.validate(""));
        assertTrue(validator.validate("kjh,f:d//_.*666 KJGFhhf3"));
        assertTrue(validator.validate("dgaKJF4365fSS"));
    }

    @Test
    public void testLocaleValidator() throws Exception {
        LocaleValidator validator = new LocaleValidator();
        assertFalse(validator.validate("Swedish"));
        assertFalse(validator.validate("US"));
        assertFalse(validator.validate("SE"));
        assertFalse(validator.validate("SV"));
        assertFalse(validator.validate("svv_SE"));
        assertFalse(validator.validate("sv_SEE"));
        assertFalse(validator.validate(null));
        assertFalse(validator.validate(""));
        assertTrue(validator.validate("en"));
        assertTrue(validator.validate("sv"));
        assertTrue(validator.validate("sv_SE"));
        assertTrue(validator.validate("sv_FI"));
        assertTrue(validator.validate("en_UK"));
        assertTrue(validator.validate("en_US"));
    }

    @Test
    public void testPostcodeValidatorSE() throws Exception {
        PostcodeValidator validator = new PostcodeValidator("SE");
        assertFalse(validator.validate("ABCDE"));
        assertFalse(validator.validate(""));
        assertFalse(validator.validate(null));
        assertFalse(validator.validate("1234"));
        assertFalse(validator.validate("123456"));
        assertFalse(validator.validate("123 45"));
        assertFalse(validator.validate(" 12345 "));
        assertTrue(validator.validate("12323"));
        assertTrue(validator.validate("99999"));
        assertTrue(validator.validate("00000"));
    }

    @Test
    public void testPostcodeValidatorFI() throws Exception {
        PostcodeValidator validator = new PostcodeValidator("FI");
        assertFalse(validator.validate("ABCDE"));
        assertFalse(validator.validate(""));
        assertFalse(validator.validate(null));
        assertFalse(validator.validate("1234"));
        assertFalse(validator.validate("123456"));
        assertFalse(validator.validate("123 45"));
        assertFalse(validator.validate(" 12345 "));
        assertTrue(validator.validate("12323"));
        assertTrue(validator.validate("99999"));
        assertTrue(validator.validate("00000"));
    }

    @Test
    public void testPostcodeValidatorDK() throws Exception {
        PostcodeValidator validator = new PostcodeValidator("DK");
        assertFalse(validator.validate("ABCDE"));
        assertFalse(validator.validate(""));
        assertFalse(validator.validate(null));
        assertFalse(validator.validate("12345"));
        assertFalse(validator.validate("123456"));
        assertFalse(validator.validate("123 45"));
        assertFalse(validator.validate(" 12345 "));
        assertTrue(validator.validate("1232"));
        assertTrue(validator.validate("9999"));
        assertTrue(validator.validate("0000"));
    }

    @Test
    public void testPostcodeValidatorNO() throws Exception {
        PostcodeValidator validator = new PostcodeValidator("NO");
        assertFalse(validator.validate("ABCDE"));
        assertFalse(validator.validate(""));
        assertFalse(validator.validate(null));
        assertFalse(validator.validate("12345"));
        assertFalse(validator.validate("123456"));
        assertFalse(validator.validate("123 45"));
        assertFalse(validator.validate(" 12345 "));
        assertTrue(validator.validate("1232"));
        assertTrue(validator.validate("9999"));
        assertTrue(validator.validate("0000"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPostcodeValidatorIllegalCountry() throws Exception {
        @SuppressWarnings("unused")
        PostcodeValidator validator = new PostcodeValidator("ZF");
    }

    @Test
    public void testSAPCustomerNumberValidator() throws Exception {
        SAPCustomerNumberValidator validator = new SAPCustomerNumberValidator();
        assertFalse(validator.validate("ABCDEFGH"));
        assertFalse(validator.validate(""));
        assertFalse(validator.validate(null));
        assertFalse(validator.validate("1234567"));
        assertFalse(validator.validate("12345678901"));
        assertFalse(validator.validate("123 45"));
        assertTrue(validator.validate("12345678"));
        assertTrue(validator.validate("123456789"));
        assertTrue(validator.validate("1234567890"));
        assertTrue(validator.validate("0012345678"));
    }

    @Test
    public void testISO31661Alpha2CountryCodeValidator() throws Exception {
        ISO31661Alpha2CountryCodeValidator validator = new ISO31661Alpha2CountryCodeValidator();
        assertFalse(validator.validate("SWE"));
        assertFalse(validator.validate(""));
        assertFalse(validator.validate(null));
        assertFalse(validator.validate(" SE "));
        assertFalse(validator.validate("ZG"));
        assertTrue(validator.validate("SE"));
        assertTrue(validator.validate("DK"));
        assertTrue(validator.validate("NO"));
        assertTrue(validator.validate("FI"));
    }

}
