package se.posten.loab.lisp.common.validation.hibernate;

import org.junit.Assert;
import org.junit.Test;

public class NoWildcardValidatorTest {

    @NoWildcardValidator(message = "x", wildcardChar = '%')
    public String dummy;
    @NoWildcardValidator(message = "y", wildcardChar = '*')
    public String dummy2;

    @Test
    public void testValidInput() throws Exception {
        NoWildcardValidatorImpl validator = new NoWildcardValidatorImpl();

        NoWildcardValidator annotation = this.getClass().getField("dummy").getAnnotation(NoWildcardValidator.class);
        validator.initialize(annotation);

        Assert.assertTrue(validator.isValid("abc", null));
        Assert.assertTrue(validator.isValid("a", null));
        Assert.assertTrue(validator.isValid("adjhaksjdhajksdhkjahskdjha sdjkah ksdjh aksdh aks", null));
        Assert.assertTrue(validator.isValid("", null));

        annotation = this.getClass().getField("dummy2").getAnnotation(NoWildcardValidator.class);
        validator.initialize(annotation);

        Assert.assertTrue(validator.isValid("abc", null));
        Assert.assertTrue(validator.isValid("a", null));
        Assert.assertTrue(validator.isValid("adjhaksjdhajksdhkjahskdjha sdjkah ksdjh aksdh aks", null));
        Assert.assertTrue(validator.isValid("", null));
    }

    @Test
    public void testInValidInput() throws Exception {
        NoWildcardValidatorImpl validator = new NoWildcardValidatorImpl();
        NoWildcardValidator annotation = this.getClass().getField("dummy").getAnnotation(NoWildcardValidator.class);
        validator.initialize(annotation);
        Assert.assertFalse(validator.isValid("%", null));
        Assert.assertFalse(validator.isValid("asdasdasd%", null));
        Assert.assertFalse(validator.isValid("%asdasdasd", null));
        Assert.assertFalse(validator.isValid("%%%%bc%", null));
    }

//    @Test
//    public void validateThis() throws Exception {
//        ClassValidator<NoWildcardValidatorTest> classValidator = new ClassValidator<NoWildcardValidatorTest>(
//                NoWildcardValidatorTest.class);
//
//        dummy = "a%";
//        InvalidValue[] values = classValidator.getInvalidValues(this);
//        Assert.assertEquals(1, values.length);
//        Assert.assertEquals("dummy x", values[0].toString());
//
//        dummy = "";
//        dummy2 = "***";
//        values = classValidator.getInvalidValues(this);
//        Assert.assertEquals(1, values.length);
//        Assert.assertEquals("dummy2 y", values[0].toString());
//
//    }

}
