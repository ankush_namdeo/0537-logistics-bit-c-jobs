package se.posten.loab.lisp.common.circuitbreaker.test;

public interface IStockService {

	int getQuote(String ticker);

	int faultyGetQuote(String ticker);
}
