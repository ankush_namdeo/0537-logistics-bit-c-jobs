package se.posten.loab.lisp.common.circuitbreaker.test;


public class StockService implements IStockService {
	
	public int getQuote(String ticker) {
		 return 5;
	 }
	 
	public int faultyGetQuote(String ticker) {
		 throw new ArithmeticException();
	 }
}
