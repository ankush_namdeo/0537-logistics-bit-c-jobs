package se.posten.loab.lisp.common.lpf;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;
import java.util.List;

import org.eclipse.xtext.util.StringInputStream;
import org.junit.Test;

import se.posten.loab.lisp.common.lpf.filter.DelimiterFilter;
import se.posten.loab.lisp.common.lpf.filter.FieldDef;
import se.posten.loab.lisp.common.lpf.filter.FieldValue;
import se.posten.loab.lisp.common.lpf.filter.GroupFilter;


public class CsvParserTest {

    private enum Field { C1,C2 };


    private final String data = "D11,D12\n" +
    		              "D21,D22\n" +
    		              "D31,D32";

    final GroupFilter fields = new DelimiterFilter(",",
            new FieldDef(Field.C1),
            new FieldDef(Field.C2)
    );


    @Test
    public void testParseWithoutHeaderColumns() throws Exception {
        InputStream is = new StringInputStream(data);
        CsvParser parser = new CsvParser();
        parser.setColumnHeader(false);
        parser.setDelimeter(",");
        parser.setNoRowsToSkip(0);
        parser.setFields(fields);
        List<FieldValue> res = parser.parse(is);
        assertEquals(3,res.size());

        assertEquals(res.get(0).getString(Field.C1), "D11");
        assertEquals(res.get(0).getString(Field.C2), "D12");

        assertEquals(res.get(1).getString(Field.C1), "D21");
        assertEquals(res.get(1).getString(Field.C2), "D22");

        assertEquals(res.get(2).getString(Field.C1), "D31");
        assertEquals(res.get(2).getString(Field.C2), "D32");


    }
}
