package se.posten.loab.lisp.common.circuitbreaker.test;

import java.util.Date;

public interface ITimeService {
	
	Date EXPECTED = new Date(); 

	Date networkTime();

	Date faultyNetworkTime();
}
