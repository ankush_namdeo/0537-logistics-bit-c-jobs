package se.posten.loab.lisp.common.validation.hibernate;

import org.junit.Assert;
import org.junit.Test;

public class WildcardValidatorTest {

    @WildcardValidator(message = "x", minAllowed = 3, wildcardChar = '%')
    public String dummy;
    @WildcardValidator(message = "y", minAllowed = 1, wildcardChar = '*')
    public String dummy2;

    @Test
    public void testValidInput() throws Exception {
        WildcardValidatorImpl validator = new WildcardValidatorImpl();

        WildcardValidator annotation = this.getClass().getField("dummy").getAnnotation(WildcardValidator.class);
        validator.initialize(annotation);

        Assert.assertTrue(validator.isValid("abc", null));
        Assert.assertTrue(validator.isValid("abc%", null));
        Assert.assertTrue(validator.isValid("%%%%abc%", null));

        Assert.assertTrue(validator.isValid("a", null));
        Assert.assertTrue(validator.isValid("adjhaksjdhajksdhkjahskdjha sdjkah ksdjh aksdh aks", null));

        annotation = this.getClass().getField("dummy2").getAnnotation(WildcardValidator.class);
        validator.initialize(annotation);

        Assert.assertTrue(validator.isValid("", null));
        Assert.assertTrue(validator.isValid("1*", null));
        Assert.assertTrue(validator.isValid("asdasdas********", null));

        Assert.assertTrue(validator.isValid("a", null));
    }

    @Test
    public void testInValidInput() throws Exception {
        WildcardValidatorImpl validator = new WildcardValidatorImpl();
        WildcardValidator annotation = this.getClass().getField("dummy").getAnnotation(WildcardValidator.class);
        validator.initialize(annotation);
        Assert.assertFalse(validator.isValid("%", null));
        Assert.assertFalse(validator.isValid("%%%%%", null));
        Assert.assertFalse(validator.isValid("%%%%bc%", null));
    }

//    @Test
//    public void validateThis() throws Exception {
//        ClassValidator<WildcardValidatorTest> classValidator = new ClassValidator<WildcardValidatorTest>(
//                WildcardValidatorTest.class);
//
//        dummy = "a%";
//        dummy2 = "a%asdadasd";
//        InvalidValue[] values = classValidator.getInvalidValues(this);
//        Assert.assertEquals(1, values.length);
//        Assert.assertEquals("dummy x", values[0].toString());
//
//        dummy = "asd%";
//        dummy2 = "***";
//        values = classValidator.getInvalidValues(this);
//        Assert.assertEquals(1, values.length);
//        Assert.assertEquals("dummy2 y", values[0].toString());
//
//    }
}
