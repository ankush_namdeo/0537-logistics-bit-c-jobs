package se.posten.loab.lisp.common.validation.impl;

import se.posten.loab.lisp.common.validation.Validator;

/**
 * Validates that string contains only ISO-8859-1 characters that are not control characters.
 * 
 * Note that new line and tab validates to false.
 */
public class ISO88591NoControlCharsValidator implements Validator<String> {

    public boolean validate(String target) {
        if (target == null) {
            return false;
        }
        for (int i = 0; i < target.length(); i++) {
            int val = target.codePointAt(i);
            if (val < 32 || val > 255 || (val > 126 && val < 160)) {
                return false;
            }
        }
        return true;
    }
}
