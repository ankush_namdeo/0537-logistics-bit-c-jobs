package se.posten.loab.lisp.common.config;

/**
 * <p>
 * This is an extension of ConfigurationManager that can be used for deploying a cached implementation of the
 * ConfigurationManager as MBean in JBoss.
 * </p>
 * <p>
 * Example:
 * 
 * <pre>
 * &#064;Service(name = &quot;configurationManager&quot;)
 * &#064;Management(CachedConfigurationManagerMBean.class)
 * public class MyConfigurationManagerBean extends CachedApacheCommonConfigurationManager {
 *    ...
 * }
 * </pre>
 * 
 * For full example, see se.posten.loab.lisp.booking.service.core.serviceimpl.ConfigurationManager.
 * 
 * @author stca708
 */
public interface CachedConfigurationManagerMBean extends ConfigurationManagerMBean {
    /**
     * Refreshes the cache and reloads all configuration parameters from its source.
     */
    public void refresh();
}
