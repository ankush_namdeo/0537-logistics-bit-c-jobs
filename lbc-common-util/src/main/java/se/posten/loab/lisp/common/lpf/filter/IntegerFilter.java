package se.posten.loab.lisp.common.lpf.filter;

import org.apache.commons.lang.StringUtils;


/**
 * Filter to convert a string to an Integer
 *
 * @author Christer Wikman, DevCode
 */
public class IntegerFilter implements Filter {

	private boolean mandatory = false;

	public IntegerFilter() {

	}

	public IntegerFilter(boolean mandatory) {
		this.mandatory = mandatory;
	}

	public boolean filter(String line, Enum<?> field, FieldValue storage) {
		String s = storage.getString(field);
		if (StringUtils.isEmpty(s) && !mandatory) {
			storage.put(field, null);
			return true;
		}
		try {
			Integer v = new Integer(s);
			storage.put(field, v);
		} catch (Exception e) {
			String msg = String.format("Enum %s : Failed to convert %s to Integer due to: %s", field, s, e);
			throw new IllegalStateException(msg);
		}
		return true;
	}
}
