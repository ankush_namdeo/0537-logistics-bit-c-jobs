package se.posten.loab.lisp.common.email;

/**
 * This exception is thrown when an email could not be sent by a {@link EmailSender}.
 * 
 * @author stca708
 */

public class SendEmailException extends Exception {
    private static final long serialVersionUID = 1L;

    public SendEmailException() {
        super();
    }

    public SendEmailException(String message, Throwable cause) {
        super(message, cause);
    }

    public SendEmailException(String message) {
        super(message);
    }

    public SendEmailException(Throwable cause) {
        super(cause);
    }
}
