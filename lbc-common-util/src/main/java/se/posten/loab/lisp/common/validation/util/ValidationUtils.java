package se.posten.loab.lisp.common.validation.util;

import java.util.List;

import se.posten.loab.lisp.common.validation.Validator;
import se.posten.loab.lisp.common.validation.ella.EllaValidator;


public class ValidationUtils {

	public static <T> boolean validate(List<Validator<T>> validators, T target) {
    	for(Validator<T> validator : validators) {
    		if(!validator.validate(target))
    			return false;
    	}
    	return true;
    }
	
	public static boolean isDPDItem(String itemId) {
		if(itemId == null || itemId.trim().length() == 0) {
			throw new IllegalArgumentException("itemId cannot be null or empty");
		}
		
		return EllaValidator.isDPDKolli(itemId);
	}
//	public static ItemIdType getItemIdType(String itemId) {
//		if(itemId == null || itemId.trim().length() == 0)
//			throw new IllegalArgumentException("itemId cannot be null or empty");
//		
//		if(EllaValidator.isPostenKolli(itemId)) {
//			return ItemIdType.POSTEN_SE;
//		}
//
//		if(EllaValidator.isDPDKolli(itemId)) {
//			return ItemIdType.DPD;
//		}
//
//		if(EllaValidator.isEANKolli(itemId)) {
//			return ItemIdType.EAN;
//		}
//
//		if(EllaValidator.isSISKolli(itemId)) {
//			return ItemIdType.SIS;
//		}
//
//		if(EllaValidator.isUPUKolli(itemId)) {
//			return ItemIdType.UPU;
//		}
//
//		return ItemIdType.UNKNOWN;
//	}

}
