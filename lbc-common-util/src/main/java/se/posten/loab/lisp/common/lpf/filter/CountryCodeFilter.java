package se.posten.loab.lisp.common.lpf.filter;

import se.posten.loab.lisp.common.util.CountryCode;

/**
 * Converts any 2-alpha, 3-alpha, or 3-digit country code to a country code
 *
 * @author Christer Wikman, DevCode
 *
 */
public class CountryCodeFilter implements Filter {
    private boolean required = false;

    public CountryCodeFilter(boolean required) {
        this.required = required;
    }

    public boolean filter(String line, Enum<?> field, FieldValue storage) {
        String code = storage.getString(field);
        CountryCode c = CountryCode.getInstance(code);
        storage.put(field, c);
        if (null == c) {
            String msg = String.format("Enum %s: Country code %s is unknown", field, code);
            if (required) {
                throw new IllegalStateException(msg);
            }
        }
        return true;
    }

}
