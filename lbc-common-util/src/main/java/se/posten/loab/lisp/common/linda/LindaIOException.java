package se.posten.loab.lisp.common.linda;

import java.io.File;
import java.io.IOException;

import org.sculptor.framework.errorhandling.ApplicationException;

public class LindaIOException extends ApplicationException {

    private static final long serialVersionUID = -3383891898423753588L;

    private LindaIOException(LogCodes code, String... parameters) {
        super(code.name(), code.getMsg());
        setMessageParameters(parameters);
    }

    private LindaIOException(LogCodes code, Throwable cause, String... parameters) {
        super(code.name(), code.getMsg(), cause);
        setMessageParameters(parameters);
    }

    public static final LindaIOException failedToCreateDirectory(File dir) {
        return new LindaIOException(LogCodes.LIN_003, dir.getAbsolutePath());
    }

    public static final LindaIOException failedToMoveFile(File file, File toDir, IOException cause) {
        return new LindaIOException(LogCodes.LIN_004, cause, file.getName(), toDir.getAbsolutePath());
    }

    public static final LindaIOException failedToCreateDLQMessageDir(File dir) {
        return new LindaIOException(LogCodes.LIN_200, dir.getAbsolutePath());
    }

    public static final LindaIOException failedToCreateDLQMessageFile(IOException cause) {
        return new LindaIOException(LogCodes.LIN_202, cause);
    }

    public static final LindaIOException failedToWriteDLQMessageFile(IOException cause) {
        return new LindaIOException(LogCodes.LIN_201, cause);
    }
}
