package se.posten.loab.lisp.common.errorhandling;

/**
 * Log codes for general failures.
 * 
 */
public enum CommonLogCodes {

    /**
     * ValidationException
     */
    COM_001,

    /**
     * UnexpectedRuntimeException
     */
    COM_002,

    /**
     * DatabaseAccessException
     */
    COM_003,
    /**
     * DatabaseAccessException but first jms deliver attempt, will be
     * redelivered
     */
    COM_004,
    /**
     * OptimisticLockingException
     */
    COM_005,
    /**
     * OutOfMemoryError
     */
    COM_006,
    /**
     * Fatal error
     */
    COM_007,

    /**
     * InvalidMessageException, Failed validation of JMS messages
     */
    COM_008,
    /**
     * MessageException, JMS messaging problems
     */
    COM_009,
    /**
     * ValidationException from Consumer (jms context)
     */
    COM_010,
    /**
     * OptimisticLockingException from Consumer (jms context) second or more
     * attempt, i.e. redelivered
     */
    COM_011

}
