package se.posten.loab.lisp.common.util.asynchronous;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;

import se.posten.loab.lisp.common.util.DataProvider;

/**
 * 
 * @author ANNI300
 *
 * @param <SERVICE>
 * @param <DATA>
 * @param <RESULT>
 */
public class AsynchInvoker<SERVICE, DATA, RESULT> {

        private static final Logger LOGGER = Logger.getLogger(AsynchInvoker.class);

        private Map<Future<RESULT>, DATA> map = new HashMap<Future<RESULT>, DATA>();
        private final SERVICE service;
        private final FutureCallBack<SERVICE, DATA, RESULT> callback;
        private final Set<Future<RESULT>> workers;
        private boolean aborted = false;
        private boolean started = false;

        public AsynchInvoker() {
            this.workers = new HashSet<Future<RESULT>>();
            this.service = null;
            this.callback = null;
        }
        
        public AsynchInvoker(SERVICE service, FutureCallBack<SERVICE, DATA, RESULT> callback) {
            this.service = service;
            this.callback = callback;
            this.workers = new HashSet<Future<RESULT>>();
        }

        public void run(DataProvider<DATA> dataProvider, int numThreads) {
            verifyNotStarted();
            try {
                while ((!(this.aborted)) && (dataProvider.hasNext())) {
                    if (this.workers.size() < numThreads) {
                        DATA batch = dataProvider.getNext();
                        Future<RESULT> result = invokeBatch(batch);
                        this.workers.add(result);
                        map.put(result, batch);
                    }   
                    handleDoneWorkerOrSleep();
                }

                while (!(this.workers.isEmpty())) {
                    handleDoneWorkerOrSleep();
                }
            } catch (InterruptedException e) {
                LOGGER.error("Asynchronous invoker was interrupted", e);
                Thread.currentThread().interrupt();
                throw new IllegalStateException("Sleep was interrupted for asynch invoker", e);
            }
        }

        private void verifyNotStarted() {
            if (this.started) {
                throw new IllegalStateException("Asynch invoker already started");
            }
            this.started = true;
        }

        private void handleDoneWorkerOrSleep() throws InterruptedException {
            Future<RESULT> doneWorker = findDoneWorker(this.workers);
            if (doneWorker != null) {
                this.workers.remove(doneWorker);
                DATA batch = map.remove(doneWorker);
                try {
                    this.callback.futureDone(doneWorker);
                } catch (BatchTryAgainException e) {
                    if(batch != null) {
                        this.workers.add(invokeBatch(batch));
                    } else {
                        LOGGER.warn("Asynchronous invoker could not handle BatchTryAgainException, batch == null. " + e.getMessage());
                    }
                } catch (AbortJobException e) {
                    this.aborted = true;
                }
            }
        }

        private Future<RESULT> invokeBatch(DATA batch) {
            return this.callback.invoke(this.service, batch);
        }

        private Future<RESULT> findDoneWorker(Set<Future<RESULT>> workers) {
            for (Future<RESULT> exec : workers) {
                if (exec.isDone()) {
                    return exec;
                }
            }
            return null;
        }
}
