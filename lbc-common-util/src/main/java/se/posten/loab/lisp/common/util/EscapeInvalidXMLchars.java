package se.posten.loab.lisp.common.util;

import java.nio.CharBuffer;
import java.util.HashSet;

/** Utility class to escape invalid XML characters, like CTRL-A, CTRL-A etc.
 *
 * @author Christer Wikman, DevCode
 *
 */
public class EscapeInvalidXMLchars {

    private static final HashSet<Character> illegalChars;

    static {
        final String escapeString = "\u0000\u0001\u0002\u0003\u0004\u0005"
                + "\u0006\u0007\u0008\u000B\u000C\u000E\u000F\u0010\u0011\u0012"
                + "\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001A\u001B\u001C"
                + "\u001D\u001E\u001F\uFFFE\uFFFF";

        illegalChars = new HashSet<Character>();
        for (int i = 0; i < escapeString.length(); i++) {
            illegalChars.add(escapeString.charAt(i));
        }
    }

    public static boolean isIllegal(char c) {
        return illegalChars.contains(c);
    }

    /**
     * Substitutes all illegal characters in the given string by the value of
     * If no illegal characters were found, no copy is made and the given string is returned.
     *
     * @param string String to escape
     * @return An escaped string
     */
    public static String escape(String string, char substitute) {
        char[] copy = null;
        boolean copied = false;
        for (int i = 0; i < string.length(); i++) {
            if (isIllegal(string.charAt(i))) {
                if (!copied) {
                    copy = string.toCharArray();
                    copied = true;
                }
                copy[i] = substitute;
            }
        }
        return copied ? new String(copy) : string;
    }

    public static void escape(CharBuffer cbuf, char substitute) {
        for (int i=0;i<cbuf.length();i++) {
            char c = cbuf.get(i);
            if (EscapeInvalidXMLchars.isIllegal(c)) {
                cbuf.put(i,substitute);
            }
        }
    }

    public static void escape(char[] cbuf, char substitute) {
        for (int i=0;i<cbuf.length;i++) {
            char c = cbuf[i];
            if (EscapeInvalidXMLchars.isIllegal(c)) {
                cbuf[i] = substitute;
            }
        }
    }

}
