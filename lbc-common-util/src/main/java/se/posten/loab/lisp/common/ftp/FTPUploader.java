package se.posten.loab.lisp.common.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;

import se.posten.loab.lisp.linda.export.ftp.FTPException;

public class FTPUploader {

    private static String TEMP_FILE_NAME_PREFIX = "_";

    public enum UploadType {
        ASCII, BINARY
    }

    private static final Logger log = Logger.getLogger(FTPUploader.class);
    private FTPClient ftpClient;
    private boolean uploadWithTemporaryName = true;

    /**
     * Creates a new FTPUploader and directly tries to connect to the FTP
     * server. Will throw {@link FTPException} if connection fails
     * 
     * @param serverIp
     * @param port
     * @param user
     * @param password
     * @param remoteDir
     * @throws FTPException
     */
    public FTPUploader(String serverIp, int port, String user, String password, String remoteDir) throws FTPException {
        connectToFtp(serverIp, port, user, password, remoteDir);
    }

    private void connectToFtp(String serverIp, int port, String user, String password, String remoteDir)
            throws FTPException {
        ftpClient = new FTPClient();
        try {
            ftpClient.connect(serverIp, port);
            if (!ftpClient.login(user, password)) {
                throw FTPException.invalidLogin(user, serverIp, port);
            }
            log.debug("Connected to FTP server: " + serverIp);
            log.debug(ftpClient.getReplyString());

            if (!ftpClient.changeWorkingDirectory(remoteDir)) {
                throw FTPException.couldNotSwitchToRemoteDirectory(remoteDir);
            }

        } catch (SocketException e) {
            throw FTPException.couldNotConnectToServer(serverIp, port, e);
        } catch (IOException e) {
            throw FTPException.couldNotCommunicateWithFTPServer(serverIp, port, e);
        }
    }

    /**
     * Upload a file to the FTP server in ASCII mode. The remote file will be
     * named as the original file
     * 
     * @throws FTPException
     *             on remote / upload errors
     */
    public void uploadFile(File f) throws FTPException {
        uploadFile(f, f.getName(), UploadType.ASCII);
    }

    /**
     * Upload a file to the FTP server in binary mode. The remote file will be
     * named as the original file
     * 
     * @throws FTPException
     *             on remote / upload errors
     */
    public void uploadFileBinary(File f) throws FTPException {
        uploadFile(f, f.getName(), UploadType.BINARY);
    }

    /**
     * Upload a file to the FTP server with the given upload type
     * 
     * @param newName
     *            the new name of the remote file
     * @throws FTPException
     */
    public void uploadFile(File f, String newName, UploadType uploadType) throws FTPException {
        if (ftpClient == null) {
            throw new IllegalStateException("FTP Client is not connected");
        }
        InputStream input = null;
        try {

            input = new FileInputStream(f);
            if (uploadType == UploadType.ASCII) {
                ftpClient.setFileType(FTP.ASCII_FILE_TYPE);
            } else {
                ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            }
            if (uploadWithTemporaryName) {
                String tempFileName = TEMP_FILE_NAME_PREFIX + newName;
                if (!ftpClient.storeFile(tempFileName, input)) {
                    throw FTPException.couldNotUploadFile();
                }
                if (!ftpClient.rename(tempFileName, newName)) {
                    throw FTPException.couldNotRenameRemoteFile(tempFileName, newName);
                }
            } else {
                if (!ftpClient.storeFile(newName, input)) {
                    throw FTPException.couldNotUploadFile();
                }
            }

        } catch (IOException e) {
            throw FTPException.couldNotUploadFile(e);
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (IOException e) {
                log.warn("Failed to close input stream", e);
            }
        }
    }

    /**
     * Must be called when ftp server connection should be closed
     * 
     * @throws IOException
     */
    public void disconnect() throws IOException {
        ftpClient.disconnect();
    }

    /**
     * Changes how uploads are done. If the attribute is true, all files will be
     * uploaded with a temporary name and then renamed to the original name.
     * Default setting is true
     */
    public void setUploadWithTemporaryName(boolean uploadWithTemporaryName) {
        this.uploadWithTemporaryName = uploadWithTemporaryName;
    }
}
