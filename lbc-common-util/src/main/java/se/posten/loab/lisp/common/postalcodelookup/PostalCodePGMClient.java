package se.posten.loab.lisp.common.postalcodelookup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import se.posten.loab.lisp.common.util.CountryCode;

import com.sun.jersey.api.client.Client;

/**
 * Utility for fetching postal codes from PGM via a REST-call. All postal codes
 * for per nordic country is cached for one hour, which makes it possible to use
 * the service extensively. The cache is updated in the background. URLs to PGM
 * is configured in the a property file called pgm.properties. The file must be
 * on the classpath.
 * 
 * @author Christer Wikman, DevCode
 * 
 */
public class PostalCodePGMClient {
    private static final String CACHE_POSTAL_CODES = "postalCodes";

    private static Log log = LogFactory.getLog(PostalCodePGMClient.class);

    private static String CONFIG_FILE = "pgm.properties";
    private static String POSTAL_CODES_URL = "postalCodesUrl";
    private static String POSTAL_CODES_COUNTRIES = "postalCodesCountryies";

    private static PropertiesConfiguration config = null;

    static {
        initConfigFile();
    }

    static void initConfigFile() {
        try {
            config = new PropertiesConfiguration(CONFIG_FILE);
            config.setReloadingStrategy(new FileChangedReloadingStrategy());
        } catch (Exception e) {
            log.error("Cannot find config file: " + CONFIG_FILE);
        }
    }

    private Cache getCache() {
        CacheManager mgr = CacheManager.getInstance();

        Cache cache = null;
        try {
            cache = mgr.getCache(CACHE_POSTAL_CODES);
        } catch (Exception ignore) {
        }

        if (null == cache) {
            cache = new Cache(CACHE_POSTAL_CODES, 500, false, false, 3600, 3600);
            mgr.addCache(cache);
        }
        return cache;

    }

    private Map<String, PostalCode> toMap(List<PostalCode> postalCodes) {
        Map<String, PostalCode> res = new HashMap<String, PostalCode>(postalCodes.size());
        for (PostalCode code : postalCodes) {
            res.put(code.getCode(), code);
        }
        return res;
    }

    private void updateCache(final CountryCode cc) {
        updateCache(cc, 0);
    }

    private void updateCache(final CountryCode cc, long waitInSec) {
        ExecutorService executor = Executors.newFixedThreadPool(2);
        Future<?> future = executor.submit(new Runnable() {
            @Override
            public void run() {
                log.info("Started to update cache key: " + cc);
                // Refresh existing (or create new empty element) so only one
                // reqest to fetchPostalCodes are executed
                try {
                    Element e = getCache().get(cc);
                    if (null == e) {
                        e = new Element(cc, new HashMap<String, PostalCode>());
                    }
                    Element refreshElement = new Element(e.getKey(), e.getValue());
                    getCache().put(refreshElement);
                    List<PostalCode> postalCodes = fetchPostalCodes(cc);
                    Map<String, PostalCode> postalCodeMap = toMap(postalCodes);
                    Element newElement = new Element(cc, postalCodeMap);
                    getCache().put(newElement);
                    log.info("Done update cache key: " + cc);
                } catch (Exception e) {
                    log.error("Failed to update cache key: " + cc);
                    try {
                        getCache().remove(cc);
                        // Create an element to avoid looping of updateCache
                        // when communication fails
                        Element refreshElement = new Element(cc, new HashMap<String, PostalCode>());
                        refreshElement.setTimeToLive(60);
                        getCache().put(refreshElement);
                    } catch (Exception e1) {
                        log.error("Failed to remove cache key: " + cc, e1);
                    }
                }
            }
        });
        if (waitInSec > 0) {
            try {
                future.get(waitInSec, TimeUnit.SECONDS);
            } catch (Exception ignore) {
            }
        }

        // shutdown executor otherwize we get a thread leak
        executor.shutdown();
    }

    public PostalCodePGMClient() {

    }

    public PostalCode getPostalCode(CountryCode cc, String postalCode) {
        if (StringUtils.isEmpty(postalCode) || !StringUtils.isNumeric(postalCode)) {
            return null;
        }
        Cache cache = getCache();
        Element e = cache.getQuiet(cc);
        if (null == e) {
            updateCache(cc, 30);
            e = cache.get(cc);
        }
        if (null == e) {
            return null;
        }
        if (e.isExpired()) {
            log.info("Expired");
            updateCache(cc);
        }

        @SuppressWarnings("unchecked")
        Map<String, PostalCode> res = (Map<String, PostalCode>) e.getObjectValue();
        return null != res ? res.get(postalCode) : null;
    }

    private boolean isCountryCodeEnabled(CountryCode cc) {
        String[] enabledCodes = config.getStringArray(POSTAL_CODES_COUNTRIES);
        if (null == enabledCodes || null == cc) {
            return false;
        }
        for (String code : enabledCodes) {
            if (cc.equals(CountryCode.getInstance(code))) {
                return true;
            }
        }
        return false;
    }

    private List<PostalCode> fetchPostalCodes(CountryCode cc) {
        List<PostalCode> postalCodes = new ArrayList<PostalCode>();
        if (null == config) {
            log.error("Cannot get postal codes. Missing config file: " + CONFIG_FILE);
            return postalCodes;
        }
        if (!isCountryCodeEnabled(cc)) {
            return postalCodes;
        }
        String url = null;
        String response = null;
        try {
            url = getUrl(cc);
            Client client = Client.create();
            response = client.resource(url).get(String.class);

            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonPostalCodes = jsonObject.getJSONArray("rows");

            int length = jsonPostalCodes.length();
            for (int i = 0; i < length; i++) {
                postalCodes.add(convertJSONToEntity(jsonPostalCodes.getJSONObject(i)));
            }
        } catch (JSONException e) {
            log.error("Invalid JSON response from: " + url + "\n" + response, e);
        }
        return postalCodes;
    }

    protected PostalCode convertJSONToEntity(JSONObject jsonPostalCode) throws JSONException {
        String code = jsonPostalCode.getString("postalcode");
        String city = jsonPostalCode.getString("postalcity");
        CountryCode cc = CountryCode.getInstance(jsonPostalCode.getString("country"));
        String box = jsonPostalCode.getString("box");
        PostalCode postalCode = new PostalCode(code, city, cc, "true".equalsIgnoreCase(box));
        return postalCode;
    }

    private String getUrl(CountryCode country) {
        String propUrl = config.getString(POSTAL_CODES_URL);
        if (null == propUrl) {
            throw new IllegalStateException("Property " + POSTAL_CODES_URL + " does not exist in file " + CONFIG_FILE);
        }
        String url = StringUtils.lowerCase(String.format(propUrl, country.getTwoAlphaCode()));
        return url;
    }

}
