package se.posten.loab.lisp.common.log;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sculptor.framework.context.ServiceContext;
import org.sculptor.framework.context.ServiceContextFactory;
import org.sculptor.framework.context.ServiceContextStore;

/**
 * Logs a debug message before and after method invocation if debug logging is enabled for intercepted class.
 * 
 * If service context is missing, a new service context is created before logging and it is then removed after method
 * invocation and logging.
 */
public class ServiceContextAndDebugLoggingInterceptor {

    private static final String DEFAULT_APP_ID = "system";

    @AroundInvoke
    public Object invoke(InvocationContext context) throws Exception {
        ServiceContext serviceContext = ServiceContextStore.get();
        String methodSign = null;
        Log log = LogFactory.getLog(context.getTarget().getClass());
        if (serviceContext == null) {
            ServiceContextStore.set(ServiceContextFactory.createServiceContext(DEFAULT_APP_ID));
        }
        try {
            if (!log.isDebugEnabled()) {
                return context.proceed();
            }
            methodSign = getMethodSignature(context);
            log.debug(new LogMessage(">>> Entering " + methodSign + ".", true));
            Object resObj = context.proceed();
            log.debug(new LogMessage("<<< Leaving " + methodSign + "."));
            return resObj;
        } catch (Exception e) {
            log.debug(new LogMessage("<<< Exception thrown from " + methodSign + "."));
            throw e;
        } finally {
            if (serviceContext == null) {
                // Since this interceptor created the serviceContext, we also remove it.
                ServiceContextStore.set(null);
            }
        }
    }

    private String getMethodSignature(InvocationContext context) {
        String methodSign = context.getMethod().getName() + "(";
        int i = 0;
        for (Object o : context.getParameters()) {
            if (i++ > 0) {
                methodSign += ", ";
            }
            methodSign += o.getClass().getSimpleName();
        }
        methodSign += ")";
        return methodSign;
    }

}
