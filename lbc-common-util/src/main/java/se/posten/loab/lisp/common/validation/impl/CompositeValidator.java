package se.posten.loab.lisp.common.validation.impl;

import java.util.List;

import se.posten.loab.lisp.common.validation.Validator;
import se.posten.loab.lisp.common.validation.util.ValidationUtils;

public class CompositeValidator implements Validator<String> {

	private List<Validator<String>> validators;
	
	public CompositeValidator(List<Validator<String>> validators) {
		this.validators = validators;
	}
	
	public boolean validate(String target) {
		return ValidationUtils.validate(validators, target);
	}
}
