package se.posten.loab.lisp.common.transform;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public final class SetTransformer {

    public static <T, E> Set<E> map(Collection<T> data, Transformer<T, E> transformer) {
        Set<E> transformed = new HashSet<E>();
        for (T t : data) {
            transformed.add(transformer.transform(t));
        }
        return transformed;
    }

    public static <T, E> Set<E> mapSkipNulls(Collection<T> data, Transformer<T, E> transformer) {
        Set<E> transformed = new HashSet<E>();
        for (T element : data) {
            E transformedElement = transformer.transform(element);
            if (transformedElement != null) {
                transformed.add(transformedElement);
            }
        }
        return transformed;
    }
    
    public static boolean nullOrEmptyOronlyContainsNullValues(Collection<?> ids) {
    	if(ids == null || ids.isEmpty()) {
    		return true;
    	}
    	
    	if(ids.contains(null)) {
    		ids.remove(null);
    	}
    	
    	if(ids.isEmpty()) {
    		return true;
    	}
    	
    	return false;
    }

}
