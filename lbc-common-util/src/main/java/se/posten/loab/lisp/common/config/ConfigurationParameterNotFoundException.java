package se.posten.loab.lisp.common.config;

/**
 * Runtime exception thrown when a given configuration parameter could not be found.
 * 
 * @author stca708
 */
public class ConfigurationParameterNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ConfigurationParameterNotFoundException(Exception e) {
        super(e);
    }
}
