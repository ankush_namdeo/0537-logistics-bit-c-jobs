package se.posten.loab.lisp.common.util.asynchronous;

public class BatchTryAgainException extends Exception {
    private static final long serialVersionUID = 1L;

    public BatchTryAgainException() {
    }

    public BatchTryAgainException(Throwable cause) {
        super(cause);
    }
}
