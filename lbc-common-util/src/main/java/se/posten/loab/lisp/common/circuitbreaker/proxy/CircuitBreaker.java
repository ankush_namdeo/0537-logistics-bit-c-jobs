package se.posten.loab.lisp.common.circuitbreaker.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import se.posten.loab.lisp.common.circuitbreaker.support.ProxyBasedCircuitBreakerAspectSupport;

/**
 * Implements the Circuit Breaker stability design pattern using dynamic proxy.
 * 
 * @see ProxyBasedCircuitBreakerAspectSupport
 * @author Patrik Nordwall
 * 
 */
public class CircuitBreaker extends ProxyBasedCircuitBreakerAspectSupport {

    public CircuitBreaker() {
    }

    @SuppressWarnings("unchecked")
    public <T> T createDynamicProxy(T delegate) {
        Class<?>[] interfaces = delegate.getClass().getInterfaces();
        ClassLoader loader = delegate.getClass().getClassLoader();
        InvocationHandler handler = new Handler(delegate);
        T proxy = (T) Proxy.newProxyInstance(loader, interfaces, handler);
        return proxy;
    }

    @Override
    protected Object getTarget(Object invocation) {
        InvocationParameters params = (InvocationParameters) invocation;
        return params.delegate;
    }

    @Override
    protected Object proceed(Object invocation) throws Throwable {
        try {
            InvocationParameters params = (InvocationParameters) invocation;
            Object result = params.method.invoke(params.delegate, params.args);
            return result;
        } catch (InvocationTargetException e) {
            throw e.getTargetException();
        }
    }

    protected boolean bypassBreaker(InvocationParameters invocation) {
        if (invocation.method.getName().equals("toString") && invocation.args.length == 0) {
            return true;
        }
        return false;
    }

    private class Handler implements InvocationHandler {
        private final Object delegate;

        Handler(Object delegate) {
            this.delegate = delegate;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            InvocationParameters invocation = new InvocationParameters(delegate, method, args);
            if (bypassBreaker(invocation)) {
                return proceed(invocation);
            }
            Object result = doExecute(invocation);
            return result;
        }
    }

    private static class InvocationParameters {
        final Object delegate;
        final Method method;
        final Object[] args;

        InvocationParameters(Object delegate, Method method, Object[] args) {
            this.delegate = delegate;
            this.method = method;
            this.args = args;
        }

    }

}
