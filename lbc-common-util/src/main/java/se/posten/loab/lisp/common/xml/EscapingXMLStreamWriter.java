package se.posten.loab.lisp.common.xml;
import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import se.posten.loab.lisp.common.util.EscapeInvalidXMLchars;

/**
 * Delegating {@link XMLStreamWriter} that filters out UTF-8 characters that
 * are illegal in XML.
 *
 * @author Christer Wikman, DevCode
 */
public class EscapingXMLStreamWriter implements XMLStreamWriter {

    public static final char substitute = ' '; // Correct substitute is '\uFFFD', but if the DB is not using UTF-8..
    private final XMLStreamWriter writer;

    public EscapingXMLStreamWriter(XMLStreamWriter writer) {
        this.writer = writer;
    }

    /**
     * Substitutes all illegal characters in the given string by the value of
     * {@link EscapingXMLStreamWriter#substitute}. If no illegal characters
     * were found, no copy is made and the given string is returned.
     *
     * @param string
     * @return
     */
    private String escape(String string) {
        return EscapeInvalidXMLchars.escape(string, substitute);
    }

    public void writeStartElement(String s) throws XMLStreamException {
        writer.writeStartElement(s);
    }

    public void writeStartElement(String s, String s1) throws XMLStreamException {
        writer.writeStartElement(s, s1);
    }

    public void writeStartElement(String s, String s1, String s2)
        throws XMLStreamException {
        writer.writeStartElement(s, s1, s2);
    }

    public void writeEmptyElement(String s, String s1) throws XMLStreamException {
        writer.writeEmptyElement(s, s1);
    }

    public void writeEmptyElement(String s, String s1, String s2)
        throws XMLStreamException {
        writer.writeEmptyElement(s, s1, s2);
    }

    public void writeEmptyElement(String s) throws XMLStreamException {
        writer.writeEmptyElement(s);
    }

    public void writeEndElement() throws XMLStreamException {
        writer.writeEndElement();
    }

    public void writeEndDocument() throws XMLStreamException {
        writer.writeEndDocument();
    }

    public void close() throws XMLStreamException {
        writer.close();
    }

    public void flush() throws XMLStreamException {
        writer.flush();
    }

    public void writeAttribute(String localName, String value) throws XMLStreamException {
        writer.writeAttribute(localName, escape(value));
    }

    public void writeAttribute(String prefix, String namespaceUri, String localName, String value)
        throws XMLStreamException {
        writer.writeAttribute(prefix, namespaceUri, localName, escape(value));
    }

    public void writeAttribute(String namespaceUri, String localName, String value)
        throws XMLStreamException {
        writer.writeAttribute(namespaceUri, localName, escape(value));
    }

    public void writeNamespace(String s, String s1) throws XMLStreamException {
        writer.writeNamespace(s, s1);
    }

    public void writeDefaultNamespace(String s) throws XMLStreamException {
        writer.writeDefaultNamespace(s);
    }

    public void writeComment(String s) throws XMLStreamException {
        writer.writeComment(s);
    }

    public void writeProcessingInstruction(String s) throws XMLStreamException {
        writer.writeProcessingInstruction(s);
    }

    public void writeProcessingInstruction(String s, String s1)
        throws XMLStreamException {
        writer.writeProcessingInstruction(s, s1);
    }

    public void writeCData(String s) throws XMLStreamException {
        writer.writeCData(escape(s));
    }

    public void writeDTD(String s) throws XMLStreamException {
        writer.writeDTD(s);
    }

    public void writeEntityRef(String s) throws XMLStreamException {
        writer.writeEntityRef(s);
    }

    public void writeStartDocument() throws XMLStreamException {
        writer.writeStartDocument();
    }

    public void writeStartDocument(String s) throws XMLStreamException {
        writer.writeStartDocument(s);
    }

    public void writeStartDocument(String s, String s1)
        throws XMLStreamException {
        writer.writeStartDocument(s, s1);
    }

    public void writeCharacters(String s) throws XMLStreamException {
        writer.writeCharacters(escape(s));
    }

    public void writeCharacters(char[] chars, int start, int len)
        throws XMLStreamException {
        writer.writeCharacters(escape(new String(chars, start, len)));
    }

    public String getPrefix(String s) throws XMLStreamException {
        return writer.getPrefix(s);
    }

    public void setPrefix(String s, String s1) throws XMLStreamException {
        writer.setPrefix(s, s1);
    }

    public void setDefaultNamespace(String s) throws XMLStreamException {
        writer.setDefaultNamespace(s);
    }

    public void setNamespaceContext(NamespaceContext namespaceContext)
        throws XMLStreamException {
        writer.setNamespaceContext(namespaceContext);
    }

    public NamespaceContext getNamespaceContext() {
        return writer.getNamespaceContext();
    }

    public Object getProperty(String s) throws IllegalArgumentException {
        return writer.getProperty(s);
    }
}
