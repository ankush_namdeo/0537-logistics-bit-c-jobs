package se.posten.loab.lisp.common.validation.impl;

import se.posten.loab.lisp.common.validation.Validator;

public class PostcodeValidator implements Validator<String> {

    private static final Validator<String> patternValidator_FI_SE = new PatternValidator("^[0-9]{5}$");
    private static final Validator<String> patternValidator_DK_NO = new PatternValidator("^[0-9]{4}$");
    private Validator<String> patternValidator = null;

    public PostcodeValidator(String countryCode) {
        if (countryCode.equals("FI") || countryCode.equals("SE")) {
            patternValidator = patternValidator_FI_SE;
        } else if (countryCode.equals("DK") || countryCode.equals("NO")) {
            patternValidator = patternValidator_DK_NO;
        } else {
            throw new IllegalArgumentException("CountryCode not supported by validator.");
        }
    }

    public boolean validate(String target) {
        return patternValidator.validate(target);
    }
}
