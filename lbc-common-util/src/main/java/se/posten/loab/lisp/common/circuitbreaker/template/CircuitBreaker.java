package se.posten.loab.lisp.common.circuitbreaker.template;

import java.util.concurrent.Callable;

import se.posten.loab.lisp.common.circuitbreaker.support.ProxyBasedCircuitBreakerAspectSupport;

/**
 * Implements the Circuit Breaker stability design pattern using template
 * method.
 * 
 * @see ProxyBasedCircuitBreakerAspectSupport
 * @author Patrik Nordwall
 */
public class CircuitBreaker extends ProxyBasedCircuitBreakerAspectSupport {

    public CircuitBreaker() {
    }

    @SuppressWarnings("unchecked")
    public <T> T execute(Callable<T> action) {
        try {
            return (T) doExecute(action);
        } catch (RuntimeException e) {
            throw e;
        } catch (Error e) {
            throw e;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected Object getTarget(Object action) {
        return action;
    }

    @Override
    protected Object proceed(Object invocation) throws Throwable {
        Callable<?> action = (Callable<?>) invocation;
        return action.call();
    }

}
