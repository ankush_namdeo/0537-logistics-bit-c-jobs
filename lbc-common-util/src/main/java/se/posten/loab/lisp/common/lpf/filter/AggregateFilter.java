package se.posten.loab.lisp.common.lpf.filter;


/** Aggregates a group of group filters
 * 
 * @author Christer Wikman, DevCode
 *
 */
public class AggregateFilter implements GroupFilter {

    private GroupFilter[] filters = null;

    public AggregateFilter(GroupFilter... filters) {
        this.filters = filters;
    }


    public boolean filter(String line, FieldValue storage) {
        for (GroupFilter f : filters) {
            if (!f.filter(line, storage)) {
                return false;
            }
        }
        return true;
    }



}
