package se.posten.loab.lisp.common.filter;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionUtil {

    public static <T> Collection<T> filter(Collection<T> target, Predicate<T> predicate) {
        Collection<T> result = new ArrayList<T>();
        for (T element : target) {
            if (predicate.apply(element)) {
                result.add(element);
            }
        }
        return result;
    }

    public static <T> Collection<T> filter(Collection<T> target, Predicate<T>[] predicates) {
        Collection<T> result = new ArrayList<T>();
        for (T element : target) {
            if (matchesAll(element, predicates)) {
                result.add(element);
            }
        }
        return result;
    }

    private static <T> boolean matchesAll(T element, Predicate<T>... predicates) {
        for (Predicate<T> predicate : predicates) {
            if (!predicate.apply(element)) {
                return false;
            }
        }
        return true;
    }
}
