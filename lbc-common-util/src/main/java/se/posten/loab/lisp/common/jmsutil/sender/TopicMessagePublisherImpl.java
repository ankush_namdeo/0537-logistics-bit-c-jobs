package se.posten.loab.lisp.common.jmsutil.sender;

import java.util.Enumeration;
import java.util.Properties;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;

import org.apache.log4j.Logger;

public class TopicMessagePublisherImpl implements TopicMessagePublisher {

    private static final Logger LOGGER = Logger.getLogger(TopicMessagePublisherImpl.class);

    private final Connection connection;
    private Properties messageProperties = new Properties();

    public TopicMessagePublisherImpl(TopicConnectionFactory topicConnectionFactory) throws JMSException {
        connection = topicConnectionFactory.createConnection();
    }

    public TopicMessagePublisherImpl(TopicConnectionFactory topicConnectionFactory, String messageProperty, String messagePropertyValue) throws JMSException {
        connection = topicConnectionFactory.createConnection();
        messageProperties.put(messageProperty, messagePropertyValue);        
    }
    
    public TopicMessagePublisherImpl(TopicConnectionFactory topicConnectionFactory, Properties messageProperties) throws JMSException {
        connection = topicConnectionFactory.createConnection();
        this.messageProperties = messageProperties;
    }

    @Override
    public void publishToTopic(final Topic topic, final String xmlMessage, final String correlationId) {
        TopicPublisher publisher = null;
        TopicSession session = null;
        TextMessage msg = null;
        try {
            session = (TopicSession) connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            publisher = session.createPublisher(topic);
            msg = session.createTextMessage(xmlMessage);
            if (correlationId != null) {
                msg.setJMSCorrelationID(correlationId);
            }

            Enumeration<?> enumeration = messageProperties.propertyNames();
            while (enumeration.hasMoreElements()) {
                String key = (String) enumeration.nextElement();
                msg.setStringProperty(key.toString(), messageProperties.getProperty(key));
            }
            connection.start();            
            publisher.setDeliveryMode(DeliveryMode.PERSISTENT);
            publisher.publish(msg);

        } catch (JMSException e) {
            throw new RuntimeException(e);
        } finally {
            close(session, publisher);
        }
    }
    
    @Override
    public void closeConnection() {
        if(connection != null) {
            try {
                connection.close();
            } catch (JMSException e) {
            }
        }
    }
    
    private void close(TopicSession session, TopicPublisher publisher) {
        if (session != null) {
            try {
                session.close();
            } catch (Exception ignore) {
            }
        }
        if (publisher != null) {
            try {
                publisher.close();
            } catch (Exception ignore) {
            }
        }
    }
}
