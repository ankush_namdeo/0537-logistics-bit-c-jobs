package se.posten.loab.lisp.common.lpf.filter;


/** A required field filter. Acts as a required field filter that
 * throws an Exception if a value that must be present isn't.
 *
 * @author Christer Wikman, DevCode
 */
public class RequiredFilter implements Filter {
    private final boolean required;

    public RequiredFilter() {
        required = true;
    }

    /** Creates new required filter
     * @param required False if the filter should act as a not required filter
     */

    public RequiredFilter( boolean required ) {
        this.required = required;
    }

    public boolean filter(String line, Enum<?> field, FieldValue storage) {
        Object val = storage.get( field );
        if( required && (val == null || "".equals(val.toString()))) {
            throw new IllegalArgumentException("Field " + field +" is required");
        }
        return true;
    }


}
