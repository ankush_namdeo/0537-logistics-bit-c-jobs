package se.posten.loab.lisp.common.validation.hibernate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NoWildcardValidatorImpl implements ConstraintValidator<NoWildcardValidator, String> {

    NoWildcardValidator annotation;
   
    public void initialize(NoWildcardValidator noWildcardValidator) {
        this.annotation = noWildcardValidator;
    }

	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
            return true;
        }
        return value.indexOf(annotation.wildcardChar()) < 0;
	}
}