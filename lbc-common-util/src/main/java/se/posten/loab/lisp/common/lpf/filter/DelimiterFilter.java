package se.posten.loab.lisp.common.lpf.filter;


/**
 * A filter to grab a field from the line specifying a delimiter of the field.
 *
 * @author Christer Wikman, DevCode
 */
public class DelimiterFilter implements GroupFilter {

    private final String delimeter;
    private final IHeaderDef headerDef;
    private final FieldDef[] fieldDefs;


    /** Interface for getting header index */
    public interface IHeaderDef {
        /** Calculate header index. If header does not exists then -1 is returned */
        int getIndex(String header);
    }

    /**
     * Creates new DelimeterFilter
     */
    public DelimiterFilter(String delimeter, IHeaderDef headerDef, FieldDef... fieldDefs ) {
        this.delimeter = delimeter;
        this.headerDef = headerDef;
        this.fieldDefs = fieldDefs;
    }

    /**
     * Creates new DelimeterFilter
     */
    public DelimiterFilter(String delimeter, FieldDef... fieldDefs ) {
        this(delimeter, null, fieldDefs);
    }

    public boolean filter(String line, FieldValue storage) {
        String[] items = line.split(delimeter);
        for (FieldDef fieldDef : fieldDefs) {
            Enum<?> name = fieldDef.getName();

            int index = null != headerDef ? headerDef.getIndex(name.name()) : name.ordinal();
            if (index >= 0 && index < items.length) {
                storage.put(name, items[index]);
                if (!fieldDef.filter(line, storage)) {
                    return false;
                }
            }
        }
        return true;
    }

}
