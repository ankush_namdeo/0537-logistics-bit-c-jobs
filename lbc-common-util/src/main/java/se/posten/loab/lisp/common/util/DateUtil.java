package se.posten.loab.lisp.common.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

/**
 * For testing purpose it is important that we create current date/time
 * instances using the utility methods in this class.
 * <p>
 * It is possible to define current date/time with system property
 * lisp.systemDateTime in format yyyy-MM-ddTHH:mm:ss. E.g.
 * -Dlisp.systemDateTime=2009-05-17T12:00:00
 * 
 */
public class DateUtil {

    private static DateTime nowForTest;
    private static LocalDate todayForTest;

    static {
        try {
            // for testing purpose you can define current date
            String fixedDateTime = System.getProperty("lisp.systemDateTime");
            if (fixedDateTime != null) {
                setNowForTest(new DateTime(fixedDateTime));
            }
        } catch (Throwable e) {
            Log log = LogFactory.getLog(DateUtil.class);
            log.error("Couldn't set systemDateTime: " + e.getMessage());
        }
    }

    private DateUtil() {
    }

    public static LocalDate today() {
        if (todayForTest != null) {
            return todayForTest;
        }
        return new LocalDate();
    }

    public static DateTime now() {
        if (nowForTest != null) {
            return nowForTest;
        }
        return new DateTime();
    }

    public static LocalDate toLocalDate(XMLGregorianCalendar xmlDate) {
        if (xmlDate == null) {
            return null;
        }
        return new LocalDate(xmlDate.getYear(), xmlDate.getMonth(), xmlDate.getDay());
    }

    public static DateTime toDateTime(XMLGregorianCalendar xmlDateTime) {
        if (xmlDateTime == null) {
            return null;
        }
        int millis = xmlDateTime.getMillisecond() == DatatypeConstants.FIELD_UNDEFINED ? 0 : xmlDateTime
                .getMillisecond();
        return new DateTime(xmlDateTime.getYear(), xmlDateTime.getMonth(), xmlDateTime.getDay(), xmlDateTime.getHour(),
                xmlDateTime.getMinute(), xmlDateTime.getSecond(), millis);
    }

    public static DateTime toDateTimeNoMillis(XMLGregorianCalendar xmlDateTime) {
        if (xmlDateTime == null) {
            return null;
        }
        int millis = 0;
        return new DateTime(xmlDateTime.getYear(), xmlDateTime.getMonth(), xmlDateTime.getDay(), xmlDateTime.getHour(),
                xmlDateTime.getMinute(), xmlDateTime.getSecond(), millis);
    }

    public static XMLGregorianCalendar toXmlDate(LocalDate localDate) {
        if (localDate == null) {
            return null;
        }
        try {
            DatatypeFactory factory = DatatypeFactory.newInstance();
            XMLGregorianCalendar result = factory.newXMLGregorianCalendarDate(localDate.getYear(), localDate
                    .getMonthOfYear(), localDate.getDayOfMonth(), DatatypeConstants.FIELD_UNDEFINED);
            return result;
        } catch (DatatypeConfigurationException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static XMLGregorianCalendar toXmlDateTime(DateTime dateTime) {
        if (dateTime == null) {
            return null;
        }
        try {
            DatatypeFactory factory = DatatypeFactory.newInstance();
            XMLGregorianCalendar result = factory.newXMLGregorianCalendar(dateTime.getYear(),
                    dateTime.getMonthOfYear(), dateTime.getDayOfMonth(), dateTime.getHourOfDay(), dateTime
                            .getMinuteOfHour(), dateTime.getSecondOfMinute(), dateTime.getMillisOfSecond(),
                    DatatypeConstants.FIELD_UNDEFINED);
            return result;
        } catch (DatatypeConfigurationException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static XMLGregorianCalendar toXmlDate(Date localDate) {
        if (localDate == null) {
            return null;
        }
        try {
            Calendar cal = new GregorianCalendar();
            cal.setTime(localDate);
            DatatypeFactory factory = DatatypeFactory.newInstance();
            XMLGregorianCalendar result = factory.newXMLGregorianCalendarDate(cal.get(Calendar.YEAR), cal
                    .get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH), DatatypeConstants.FIELD_UNDEFINED);
            return result;
        } catch (DatatypeConfigurationException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static XMLGregorianCalendar toXmlDateTime(Date dateTime) {
        if (dateTime == null) {
            return null;
        }
        try {
            Calendar cal = new GregorianCalendar();
            cal.setTime(dateTime);
            DatatypeFactory factory = DatatypeFactory.newInstance();

            XMLGregorianCalendar result = factory.newXMLGregorianCalendar(cal.get(Calendar.YEAR), cal
                    .get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.HOUR_OF_DAY), cal
                    .get(Calendar.MINUTE), cal.get(Calendar.SECOND), cal.get(Calendar.MILLISECOND),
                    DatatypeConstants.FIELD_UNDEFINED);

            return result;
        } catch (DatatypeConfigurationException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static XMLGregorianCalendar toXmlTime(Date dateTime) {
        if (dateTime == null) {
            return null;
        }
        try {
            Calendar cal = new GregorianCalendar();
            cal.setTime(dateTime);
            DatatypeFactory factory = DatatypeFactory.newInstance();

            XMLGregorianCalendar result = factory.newXMLGregorianCalendar(DatatypeConstants.FIELD_UNDEFINED,
                    DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED,
                    cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND), cal
                            .get(Calendar.MILLISECOND), DatatypeConstants.FIELD_UNDEFINED);

            return result;
        } catch (DatatypeConfigurationException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static Date normalizeTime(Date dateTime) {
        if (dateTime == null) {
            return null;
        }
        Calendar cal = new GregorianCalendar();
        cal.setTime(dateTime);
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    public static void setNowForTest(DateTime fixedNow) {
        nowForTest = fixedNow;
        if (fixedNow == null) {
            todayForTest = null;
        } else {
            todayForTest = fixedNow.toLocalDate();
        }
    }

    public static boolean isNowForTestSet() {
        return (nowForTest != null);
    }

}
