package se.posten.loab.lisp.common.util;

public interface DataProvider<E> {

    boolean hasNext();

    E getNext();

}