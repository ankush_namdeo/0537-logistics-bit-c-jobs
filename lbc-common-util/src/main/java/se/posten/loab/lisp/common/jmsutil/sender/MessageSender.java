/*
 * Based on package org.fornax.cartridges.sculptor.framework.consumer.
 * 
 * Copyright 2009 The Fornax Project Team, including the original
 * author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.posten.loab.lisp.common.jmsutil.sender;

import java.util.Properties;

import javax.jms.Destination;

/**
 * Interface for sending JMS messages.
 * 
 * This is an extension of the org.fornax.cartridges.sculptor.framework.consumer.MessageSender interface and it adds
 * methods for setting JMS String properties and for sending a BytesMessage.
 * 
 */
public interface MessageSender extends org.sculptor.framework.consumer.MessageSender {

    /**
     * Send a JMS String message.
     * 
     * @param destination
     *            The JMS Destination
     * @param message
     *            The String message to send.
     * @param jmsStringProperties
     *            All properties in this argument will be set as String Properties in the sent message.
     * @param correlationId
     *            An optional correlationId. Set to null if not used.
     */
    void sendMessage(Destination destination, String message, Properties jmsStringProperties, String correlationId);

    /**
     * Send a JMS bytes message (binary data)
     * 
     * @param destination
     *            The JMS Destination
     * @param message
     *            The binary data to send.
     * @param jmsStringProperties
     *            All properties in this argument will be set as String Properties in the sent message.
     * @param correlationId
     *            An optional correlationId. Set to null if not used.
     */
    void sendBytesMessage(Destination destination, byte[] message, Properties jmsStringProperties, String correlationId);
}