package se.posten.loab.lisp.common.lpf.filter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/** Parses a textual timestamp to a java.sql.Timestamp
 * 
 * @author Christer Wikman, DevCode
 */
public class TimestampParserFilter implements Filter {
    SimpleDateFormat dateformat;
    int start;
    int len;

    /**
     * @param format Date format according to format in SimpleDateFormat
     */
    public TimestampParserFilter(String format) {
        dateformat = new SimpleDateFormat( format );
    }

    public boolean filter(String line, Enum<?> field, FieldValue storage) throws IllegalArgumentException {
        String org="";
        try {
            org = storage.get( field ).toString();
            Date dst = dateformat.parse( org );
            storage.put( field, new java.sql.Timestamp( dst.getTime() ) );
            return true;
        } catch ( ParseException e ) {
            throw new IllegalArgumentException("Kunde inte tyda datum \"" + org + "\" i f�ltet: " + field );
        }
    }
}
