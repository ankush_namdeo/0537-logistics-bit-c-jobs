package se.posten.loab.lisp.common.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Collecting of time measurements and calculation of statistics. A.k.a.
 * TidMannen.
 * 
 */
public class TimeMeasurement {

    private static Log log = LogFactory.getLog(TimeMeasurement.class);
    private static final String NL = System.getProperty("line.separator", "\n");
    public static final long HOUR_IN_MILLIS = 1000 * 60 * 60;
    public static final long MINUTE_IN_MILLIS = 1000 * 60;
    public static final long SECOND_IN_MILLIS = 1000;
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public String module;

    private long first = -1L;
    private long last = -1L;
    private HashMap<String, Stats> matningar;

    public TimeMeasurement(String module) {
        this.module = module;
        matningar = new HashMap<String, Stats>();
    }

    public synchronized void reset() {
        matningar.clear();
        first = -1L;
        last = -1L;
    }

    /**
     * Add a measurement, with current time as stop time. Use System.nanoTime()
     * for the timestamps.
     * 
     * @param start
     *            in nanoSeconds
     */
    public synchronized void add(String key, long start) {
        add(key, start, System.nanoTime());
    }

    /**
     * Add a measurement. Use System.nanoTime() for the timestamps
     * 
     * @param start
     *            in nanoSeconds
     * @param stopp
     *            in nanoSeconds
     */
    public synchronized void add(String key, long start, long stop) {

        if (start == 0) {
            log.warn("ignoring record with start 0");
            return;
        }

        if (first < 0)
            first = System.currentTimeMillis();
        last = System.currentTimeMillis();

        Stats curr = matningar.get(key);
        if (curr == null) {
            curr = new Stats(0L, 0);
            matningar.put(key, curr);
        }
        curr.anrop++;

        // at least 1 ns
        if (start == stop)
            curr.tid += 1L;
        else
            curr.tid += (stop - start);
    }

    /*
     * Write measurement statistics to log.
     */
    public void log() {
        log.info(this);
    }

    /**
     * Current measurement statistics as String.
     */
    @Override
    public synchronized String toString() {
        List<String> lista = new ArrayList<String>();
        lista.addAll(matningar.keySet());
        Collections.sort(lista);
        StringBuilder builder = new StringBuilder();
        builder.append(NL);
        builder.append("***** TimeMeasurement statistics for module '").append(module).append("' *****").append(NL);
        if (first > 0 && last > 0) {
            builder.append("duration of test: ");
            builder.append((last - first) / 1000).append(" s. [");
            builder.append(format(new Date(first))).append(" => ").append(format(new Date(last)));
            builder.append("]");
        }
        builder.append(NL);
        for (String nyckel : lista) {
            Stats stats = matningar.get(nyckel);
            builder.append(nyckel);
            builder.append(": ");
            builder.append(toHumanFormat(stats.tid / 1000000));
            builder.append(" [");
            builder.append(stats.anrop);
            builder.append(" st ");
            builder.append("x ");
            builder.append((Math.round((((double) (stats.tid / 1000000)) / ((double) stats.anrop)) * 100.0)) / 100.0);
            builder.append(" ms.");
            builder.append("]");

            double duration = ((last - first)) / 1000.0;
            double rate = (stats.anrop / duration);
            rate = Math.round(rate * 100.0) / 100.0;
            if (rate < 0.0001) {
                rate = 0.0;
            }
            builder.append(" (rate:  ").append(rate).append(")");

            builder.append(NL);
        }
        builder.append("***** /TimeMeasurement statistics *****");
        builder.append(NL);
        return builder.toString();
    }

    private String toHumanFormat(long ms) {
        StringBuilder b = new StringBuilder();
        boolean startat = false;
        if (ms >= HOUR_IN_MILLIS) {
            b.append(ms / HOUR_IN_MILLIS);
            b.append(" h. ");
            ms = ms % HOUR_IN_MILLIS;
            startat = true;
        }

        if (ms >= MINUTE_IN_MILLIS) {
            b.append(ms / MINUTE_IN_MILLIS);
            b.append(" m. ");
            ms = ms % MINUTE_IN_MILLIS;
            startat = true;
        } else if (startat)
            b.append("0 m. ");

        if (ms >= SECOND_IN_MILLIS) {
            b.append(ms / SECOND_IN_MILLIS);
            b.append(" s. ");
            ms = ms % SECOND_IN_MILLIS;
            startat = true;
        } else if (startat)
            b.append("0 s. ");

        b.append(ms);
        b.append(" ms.");

        return b.toString();
    }

    private String format(Date date) {
        if (date == null)
            return null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_TIME_FORMAT);
        return simpleDateFormat.format(date);
    }

    private class Stats {
        private long tid;
        private int anrop;

        public Stats(long tid, int anrop) {
            this.tid = tid;
            this.anrop = anrop;
        }
    }

}
