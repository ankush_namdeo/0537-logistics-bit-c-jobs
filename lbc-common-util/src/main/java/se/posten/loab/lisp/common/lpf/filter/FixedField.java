package se.posten.loab.lisp.common.lpf.filter;
/** A filter to grab a field from the line specifying the start of the field and its length.
 * 
 * @author Christer Wikman, DevCode
 */
public class FixedField implements Filter {
    int start;
    int len;

    /** Creates new FixedField
     * @param start The start of the field
     * @param len The length of the field
     */
    public FixedField( int start, int len ) {
        this.start = start;
        this.len = len;
    }

    public boolean filter(String line, Enum<?> field, FieldValue storage) throws IllegalArgumentException {
        storage.put( field, line.substring( start, start+len ) );
        return true;
    }

}
