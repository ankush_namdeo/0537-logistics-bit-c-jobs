package se.posten.loab.lisp.common.statistics;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

public class StatLogInterceptor {

    @AroundInvoke
    public Object invoke(InvocationContext context) throws Exception {
        if (!StatLog.isLoggingEnabled()) {
            return context.proceed();
        }
        String targetName = context.getTarget().getClass().getSimpleName();
        String methodName = context.getMethod().getName();
        StringBuilder message = new StringBuilder();
        message.append("Statistics for ").append(targetName).append(".").append(methodName);
        String messageStr = message.toString();
        StatLog.logStart(methodName, messageStr);
        long startTime = System.currentTimeMillis();
        try {
            return context.proceed();
        } finally {
            long duration = System.currentTimeMillis() - startTime;
            messageStr = new StringBuilder("(Time: ").append(duration).append(" ms) ").append(messageStr).toString();
            StatLog.logEnd(methodName, messageStr);
        }
    }
}
