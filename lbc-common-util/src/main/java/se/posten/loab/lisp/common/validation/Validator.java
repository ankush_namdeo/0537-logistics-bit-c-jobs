package se.posten.loab.lisp.common.validation;

public interface Validator<T> {
	public boolean validate(T target);
}
