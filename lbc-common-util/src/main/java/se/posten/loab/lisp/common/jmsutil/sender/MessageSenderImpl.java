/*
 * Based on package org.fornax.cartridges.sculptor.framework.consumer.
 * 
 * Copyright 2009 The Fornax Project Team, including the original
 * author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.posten.loab.lisp.common.jmsutil.sender;

import java.util.Properties;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 * <p>
 * Implementation of MessageSender. The connection, including its lifecycle is provided by the client.
 * </p>
 * <p>
 * This is based on a copy of the org.fornax.cartridges.sculptor.framework.consumer.MessageSenderImpl class.
 * </p>
 */
public class MessageSenderImpl implements MessageSender {

    private final Connection connection;
    private int deliveryMode = DeliveryMode.PERSISTENT;
    private int acknowledgeMode = Session.AUTO_ACKNOWLEDGE;

    /**
     * Constructs a new MessageSenderImpl with deliveryMode = DeliveryMode.PERSISTENT and acknowledgeMode =
     * Session.AUTO_ACKNOWLEDGE.
     * 
     * @param connection
     *            The connection to use for sending the JMS message.
     */
    public MessageSenderImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void sendMessage(Destination destination, Message message) {
        Session session = null;
        MessageProducer sender = null;
        try {
            session = connection.createSession(false, acknowledgeMode);
            sender = session.createProducer(destination);
            sender.setDeliveryMode(deliveryMode);
            sender.send(message);
        } catch (JMSException e) {
            throw new RuntimeException(e);
        } finally {
            close(session, sender);
        }
    }

    @Override
    public void sendMessage(Destination destination, String message, String correlationId) {
        sendMessage(destination, message, null, correlationId);
    }

    @Override
    public void sendMessage(Destination destination, String message, Properties jmsStringProperties,
            String correlationId) {
        Session session = null;
        MessageProducer sender = null;
        try {
            session = connection.createSession(false, acknowledgeMode);
            TextMessage textMessage = session.createTextMessage(message);
            sender = setDataAndSendMessage(session, destination, textMessage, jmsStringProperties, correlationId);
        } catch (JMSException e) {
            throw new RuntimeException(e);
        } finally {
            close(session, sender);
        }
    }

    @Override
    public void sendBytesMessage(Destination destination, byte[] message, Properties jmsStringProperties,
            String correlationId) {
        Session session = null;
        MessageProducer sender = null;
        try {
            session = connection.createSession(false, acknowledgeMode);
            BytesMessage bytesMessage = session.createBytesMessage();
            bytesMessage.writeBytes(message);
            sender = setDataAndSendMessage(session, destination, bytesMessage, jmsStringProperties, correlationId);
        } catch (JMSException e) {
            throw new RuntimeException(e);
        } finally {
            close(session, sender);
        }
    }

    private MessageProducer setDataAndSendMessage(Session session, Destination destination, Message message,
            Properties jmsStringProperties, String correlationId) throws JMSException {
        MessageProducer sender;
        sender = session.createProducer(destination);
        extractStringProperties(message, jmsStringProperties);
        if (correlationId != null) {
            message.setJMSCorrelationID(correlationId);
        }
        sender.setDeliveryMode(deliveryMode);
        sender.send(message);
        return sender;
    }

    private void extractStringProperties(Message message, Properties jmsStringProperties) throws JMSException {
        if (jmsStringProperties != null) {
            for (String key : jmsStringProperties.stringPropertyNames()) {
                message.setStringProperty(key, jmsStringProperties.getProperty(key));
            }
        }
    }

    private void close(Session session, MessageProducer sender) {
        if (sender != null) {
            try {
                sender.close();
            } catch (Exception ignore) {
            }
        }
        if (session != null) {
            try {
                session.close();
            } catch (Exception ignore) {
            }
        }
    }

    public int getDeliveryMode() {
        return deliveryMode;
    }

    public void setDeliveryMode(int deliveryMode) {
        this.deliveryMode = deliveryMode;
    }

    public int getAcknowledgeMode() {
        return acknowledgeMode;
    }

    public void setAcknowledgeMode(int acknowledgeMode) {
        this.acknowledgeMode = acknowledgeMode;
    }

}
