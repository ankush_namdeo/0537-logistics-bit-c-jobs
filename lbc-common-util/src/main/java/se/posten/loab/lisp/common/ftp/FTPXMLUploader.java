package se.posten.loab.lisp.common.ftp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import se.posten.loab.lisp.linda.export.ftp.FTPException;

/**
 * The purpose of the FTPXMLUploader class is to marshal xml objects to files,
 * which will be zipped and uploaded to an ftp server
 * 
 * @author ansk719
 * 
 */
public class FTPXMLUploader extends FTPUploader {

    private static final Logger log = Logger.getLogger(FTPXMLUploader.class);
    private static final Charset CHARSET = Charset.forName("UTF-8");
    private Marshaller marshaller;

    public FTPXMLUploader(String serverIp, int port, String user, String password, String remoteDir,
            Class<?>... xmlClasses) throws FTPException {
        super(serverIp, port, user, password, remoteDir);
        createXMLMarshaller(xmlClasses);
    }

    public FTPXMLUploader(String serverIp, int port, String user, String password, String remoteDir,
            String schemaLocation, Class<?>... xmlClasses) throws FTPException {
        super(serverIp, port, user, password, remoteDir);
        createXMLMarshaller(xmlClasses);
        createSchema(schemaLocation);
    }

    private void createXMLMarshaller(Class<?>... xmlClasses) throws FTPException {
        try {
            JAXBContext jc = JAXBContext.newInstance(xmlClasses);
            marshaller = jc.createMarshaller();
            try {
                marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            } catch (PropertyException e) {
                log.warn("Could not set formatted output property: " + e.getMessage());
            }
        } catch (JAXBException e) {
            throw FTPException.couldNotCreateMarshaller(e);
        }
    }

    private void createSchema(String schemaLocation) {
        URL schemaResource = getClass().getResource(schemaLocation);
        if (schemaResource == null) {
            log.warn("Could not find schema for XML validation: " + schemaLocation);
        } else {
            SchemaFactory sf = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
            // setting this due to usage of ##any
            // sf.setFeature("http://apache.org/xml/features/validation/schema-full-checking",
            // false);
            try {
                Schema schema = sf.newSchema(schemaResource);
                marshaller.setSchema(schema);
            } catch (SAXException e) {
                log.warn("Could not set create schema for XML validation: ", e);
            }
        }
    }

    /**
     * Marshal the given xml object to a zipped xml file which will be uploaded
     * to the ftp server
     * 
     * @param xmlInstance
     *            the object to marshal and upload
     * @param fileName
     *            the name of the file to create
     * @param tmpDir
     *            the directory where temporary files are kept
     */
    public void uploadXML(Object xmlInstance, String fileName, File tmpDir) throws FTPException {
        File f = new File(tmpDir, fileName + ".zip");
        if (!tmpDir.exists() && !tmpDir.mkdirs()) {
            throw FTPException.couldNotCreateTempDirectory(tmpDir);
        }
        try {
            if (!f.exists() && !f.createNewFile()) {
                throw FTPException.couldNotCreateTempFile(tmpDir);
            }
            ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(f, false));
            zipOut.putNextEntry(new ZipEntry(fileName));
            OutputStreamWriter w = new OutputStreamWriter(zipOut, CHARSET);

            marshaller.marshal(xmlInstance, w);
            if (log.isTraceEnabled()) {
                StringWriter sw = new StringWriter();
                marshaller.marshal(xmlInstance, sw);
                log.trace(sw.toString());
            }
            w.close();
        } catch (IOException e) {
            throw FTPException.couldNotCreateTempFile(tmpDir, e);
        } catch (JAXBException e) {
            throw FTPException.couldNotMarshalXML(e);
        }

        uploadFileBinary(f);
    }

}
