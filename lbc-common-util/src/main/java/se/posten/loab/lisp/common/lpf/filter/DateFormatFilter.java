package se.posten.loab.lisp.common.lpf.filter;

import java.text.SimpleDateFormat;


/**
 * Converts a java.util.Date to a textual pattern. See SimpleDateFormat for pattern details
 *
 * @see SimpleDateFormat
 * @author Christer Wikman, DevCode
 */
public class DateFormatFilter implements Filter {

	private final SimpleDateFormat dateformat;
	private final String pattern;

	public DateFormatFilter(String pattern) {
		this(pattern, false);
	}

	public DateFormatFilter(String pattern, boolean lenient) {
		this.pattern = pattern;
		this.dateformat = new SimpleDateFormat(pattern);
		dateformat.setLenient(lenient);
	}

	public boolean filter(String line, Enum<?> field, FieldValue storage) {
		Object org = null;
		try {
			org = storage.get(field);
			if (org != null) {
				String dst = dateformat.format(org);
				storage.put(field, dst);
			}
			return true;
		} catch (Exception e) {
			if (!dateformat.isLenient()) {
				String msg = String.format("Enum %s : Failed to convert %s using pattern %s due to: %s",
					field, org, pattern, e);
				throw new IllegalStateException(msg);
			}
			storage.put(field, null);
			return true;
		}
	}
}
