package se.posten.loab.lisp.common.validation.impl;

import se.posten.loab.lisp.common.validation.Validator;

public class NordicTextValidator implements Validator<String> {

    // TODO: other non-ascii characters in nordic languages
    private static final Validator<String> patternValidator = new PatternValidator("[\\wåäöÅÄÖæÆøØ\\s\\-,/\\.;:\\*]*");

    public boolean validate(String target) {
        return patternValidator.validate(target);
    }
}
