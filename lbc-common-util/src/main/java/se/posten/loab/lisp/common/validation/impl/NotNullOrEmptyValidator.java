package se.posten.loab.lisp.common.validation.impl;

import se.posten.loab.lisp.common.validation.Validator;

public class NotNullOrEmptyValidator implements Validator<String> {

	public boolean validate(String target) {
		return target != null && target.trim().length() > 0;
	}
}
