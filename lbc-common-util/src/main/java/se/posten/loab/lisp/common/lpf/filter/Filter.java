/*
 * Copyright 2004 DevCode, Inc. All rights reserved.
 * DEVCODE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package se.posten.loab.lisp.common.lpf.filter;

/** Parser filter interface.
 * Implement this interface if you want to extend the LineParserFramework
 * 
 * @author Christer Wikman, DevCode
 */
public interface Filter {
    
    /** Filter operation.
     * This is where all work is done.
     * @param line Contains the current line
     * @param field Contains the name of the field we are working on
     * @param storage Contains field storage 
     * @return True if this filter doesn't terminate the filter pipe
     */    
    boolean filter( String line, Enum<?> field, FieldValue storage );
}

