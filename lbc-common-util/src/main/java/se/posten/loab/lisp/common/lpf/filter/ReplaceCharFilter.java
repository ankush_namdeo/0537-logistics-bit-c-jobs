package se.posten.loab.lisp.common.lpf.filter;

public class ReplaceCharFilter implements Filter {
    private final char myOldChar;
    private final char myNewChar;

    public ReplaceCharFilter(char oldChar, char newChar) {
        myOldChar = oldChar;
        myNewChar = newChar;
    }

    /**
     * @param line
     * @param field
     * @param storage
     * @throws IllegalArgumentException
     * @return  */
    public boolean filter( String line, Enum<?> field, FieldValue storage) throws IllegalArgumentException {
        String org = "";
        org = storage.get( field ).toString();
        String lineStr = org;
        try     {
            lineStr = lineStr.replace(myOldChar, myNewChar);
            storage.put( field, lineStr );
            return true;
        } catch ( Exception e ) {
            throw new IllegalArgumentException("Cannot replace " + myOldChar + " with " + myNewChar + " i str�ngen: " + lineStr );
        }
    }

}