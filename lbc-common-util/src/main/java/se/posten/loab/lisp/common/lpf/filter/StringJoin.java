package se.posten.loab.lisp.common.lpf.filter;

import org.apache.commons.lang.StringUtils;

/** Filter that joins two string field value and stores the result in the filtered field's value.
 *
 * @author Christer Wikman, DevCode
 *
 */
public class StringJoin implements Filter {

    private Enum<?> joinField = null;

    public StringJoin(Enum<?> joinField) {
        this.joinField = joinField;
    }

    public boolean filter(String line, Enum<?> field, FieldValue storage) {
        String s1 = storage.getAsString(field);
        String s2 = storage.getAsString(joinField);
        storage.put(joinField, null);
        storage.put(field, StringUtils.join(new String[] {s1,s2}));
        return false;

    }



}
