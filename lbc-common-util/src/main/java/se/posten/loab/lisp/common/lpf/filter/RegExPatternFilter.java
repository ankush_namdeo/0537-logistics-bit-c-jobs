package se.posten.loab.lisp.common.lpf.filter;

import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;


/** A regex pattern field filter for strings. Checks if a field complies to a specific
 * string pattern. The field compliance might be required or not. If the field is
 * not required then the field's data is nulled if it does not comply to the pattern.
 *
 * @see java.util.regex.Pattern
 * @author Christer Wikman, DevCode
 */
public class RegExPatternFilter implements Filter {

    private final boolean required;
    private final boolean nullInvalid;
    private final String regex;

    /** Creates a new regex filter
     *
     * @param regex Regex pattern
     * @param required Required or not
     * @param nullInvalid Set value to null if value does not match regex
     */
    public RegExPatternFilter(String regex, boolean required, boolean nullInvalid) {
        this.required = required;
        this.regex = regex;
        this.nullInvalid = nullInvalid;
    }

    public boolean filter(String line, Enum<?> field, FieldValue storage) {
        Object val = storage.get( field );

        String str = null != val ? val.toString() : null;
        if( StringUtils.isEmpty(str)) {
            if (required) {
                throw new IllegalArgumentException("Field " + field +" is required");
            }
            storage.put(field, null);
            return true;
        }
        boolean matches = Pattern.matches(regex, str);
        if (!nullInvalid && !matches) {
            String msg = String.format("Field %s: Value %s does not match %s", field, str, regex);
            throw new IllegalArgumentException(msg);
        }
        if (nullInvalid && !matches) {
            storage.put(field, null);
        }
        return true;
    }


}
