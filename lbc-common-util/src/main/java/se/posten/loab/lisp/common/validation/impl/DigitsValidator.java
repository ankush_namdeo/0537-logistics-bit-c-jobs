package se.posten.loab.lisp.common.validation.impl;

import se.posten.loab.lisp.common.validation.Validator;

public class DigitsValidator implements Validator<String> {

    private static final Validator<String> patternValidator = new PatternValidator("\\d*");

    public boolean validate(String target) {
        return patternValidator.validate(target);
    }
}
