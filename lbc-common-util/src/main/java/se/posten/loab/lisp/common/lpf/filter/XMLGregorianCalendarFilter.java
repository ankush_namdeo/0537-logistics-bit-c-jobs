package se.posten.loab.lisp.common.lpf.filter;

import java.util.Date;

import javax.xml.datatype.XMLGregorianCalendar;

import org.joda.time.DateTime;

import se.posten.loab.lisp.common.util.DateUtil;

/** Converts a date represented as a string pattern to a XMLGregorianCalendar
 *
 * @see DateFormatFilter
 * @see XMLGregorianCalendar
 * @author Christer Wikman, DevCode
 *
 */
public class XMLGregorianCalendarFilter extends DateParserFilter {

	public XMLGregorianCalendarFilter(String pattern, boolean lenient) {
		super(pattern, lenient);
	}

	@Override
	public boolean filter(String line, Enum<?> field, FieldValue storage) {
		boolean res = super.filter(line, field, storage);
		Date date = null;
		try {
			date = (Date) storage.get(field);
			if (null == date) {
				return res;
			}
			DateTime dateTime = new DateTime(date.getTime());
			XMLGregorianCalendar xmlDate = DateUtil.toXmlDateTime(dateTime);
			storage.put(field, xmlDate);
		} catch (Exception e) {
			String msg = String.format("Enum %s : Failed to convert %s to XMLGregorianCalendar",
					field, date);
				throw new IllegalStateException(msg,e);
		}
		return res;
	}

}
