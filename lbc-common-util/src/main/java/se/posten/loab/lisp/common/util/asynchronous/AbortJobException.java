package se.posten.loab.lisp.common.util.asynchronous;

public class AbortJobException extends Exception {
    private static final long serialVersionUID = 1L;

    public AbortJobException() {
    }

    public AbortJobException(Throwable cause) {
        super(cause);
    }
}
