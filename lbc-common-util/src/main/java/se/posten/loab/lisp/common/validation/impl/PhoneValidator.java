package se.posten.loab.lisp.common.validation.impl;

import se.posten.loab.lisp.common.validation.Validator;

public class PhoneValidator implements Validator<String> {

    private static final Validator<String> patternValidator = new PatternValidator("(\\+)?[0-9\\-/\\s]+");

    public boolean validate(String target) {
        return patternValidator.validate(target);
    }
}
