/*
 * FixedField.java
 *
 * Created on den 26 april 2001, 20:59
 */

package se.posten.loab.lisp.common.lpf.filter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;


/** Filter to convert a textual field to a integer java.lang.Integer field
 * 
 * @author Christer Wikman, DevCode
 */
public class NumberFormat implements Filter {

    DecimalFormat format;

    public NumberFormat(String pattern, Locale locale ) {
        format = new DecimalFormat( pattern, new DecimalFormatSymbols( locale ) );
    }
    
    public boolean filter(String line, Enum<?> field, FieldValue storage) throws IllegalArgumentException {
        Object org = "";
        try {
            org = storage.get( field );
            storage.put( field, format.format( org ) );
            return true;
        } catch ( NumberFormatException e ) {
            throw new IllegalArgumentException("Kunde inte tyda nummer \"" + org + "\" i f�ltet: " + field );
        }
    }    
}
