package se.posten.loab.lisp.common.linda;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public final class FileArchiver {

    /**
     * Move a file to a new directory. The destination directory will be created
     * if it does not exist.
     * 
     * @param fileToMove
     *            the file to move to the destination directory
     * @param destDir
     *            the destination directory
     * @throws LindaIOException
     *             if the destination directory could not be created or if the
     *             moving of the file failed
     */
    public static void moveToDirectory(File fileToMove, File destDir) throws LindaIOException {
        move(fileToMove, destDir, fileToMove.getName());
    }

    /**
     * Same as {@link #move(File, File)} but with a string representation of the
     * destination directory
     * 
     * @see #move(File, File)
     */
    public static void moveToDirectory(File fileToMove, String destDirPath) throws LindaIOException {
        move(fileToMove, new File(destDirPath), fileToMove.getName());
    }

    /**
     * Move a file to a new destination and a new file name.
     * 
     * @param fileToMove
     *            the file to move to the destination directory
     * @param destDir
     *            the destination directory
     * @param newFileName
     * @throws LindaIOException
     *             if the destination directory could not be created or if the
     *             moving of the file failed
     */
    public static void move(File fileToMove, File destDir, String newFileName) throws LindaIOException {
        if (!destDir.exists() && !destDir.mkdirs()) {
            throw LindaIOException.failedToCreateDirectory(destDir);
        }
        try {
            File newFile = new File(destDir, newFileName);
            FileUtils.moveFile(fileToMove, newFile);
        } catch (IOException e) {
            throw LindaIOException.failedToMoveFile(fileToMove, destDir, e);
        }
    }

}
