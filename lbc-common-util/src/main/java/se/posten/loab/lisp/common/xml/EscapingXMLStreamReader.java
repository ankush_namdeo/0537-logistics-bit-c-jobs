package se.posten.loab.lisp.common.xml;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.stream.Location;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import se.posten.loab.lisp.common.util.EscapeInvalidXMLchars;

/**
 * Delegating {@link XMLStreamReader} that filters out UTF-8 characters that
 * are illegal in XML.
 *
 * @author Christer Wikman, DevCode
 */
public class EscapingXMLStreamReader implements XMLStreamReader {

    private final XMLStreamReader reader;
    public static final char substitute = ' '; // Correct substitute is '\uFFFD', but if the DB is not using UTF-8..

    private String escape(String string) {
        return EscapeInvalidXMLchars.escape(string, substitute);
    }

    private char[] escape(char[] cbuf) {
        EscapeInvalidXMLchars.escape(cbuf, substitute);
        return cbuf;
    }

    public EscapingXMLStreamReader(XMLStreamReader reader) {
        this.reader = reader;
    }

    public void close() throws XMLStreamException {
        reader.close();
    }

    public int getAttributeCount() {
        return reader.getAttributeCount();
    }

    public String getAttributeLocalName(int index) {
        return reader.getAttributeLocalName(index);
    }

    public QName getAttributeName(int index) {
        return reader.getAttributeName(index);
    }

    public String getAttributeNamespace(int index) {
        return reader.getAttributeNamespace(index);
    }

    public String getAttributePrefix(int index) {
        return reader.getAttributePrefix(index);
    }

    public String getAttributeType(int index) {
        return reader.getAttributeType(index);
    }

    public String getAttributeValue(int index) {
        return escape(reader.getAttributeValue(index));
    }

    public String getAttributeValue(String namespaceURI, String localName) {
        return escape(reader.getAttributeValue(namespaceURI, localName));
    }

    public String getCharacterEncodingScheme() {
        return reader.getCharacterEncodingScheme();
    }

    public String getElementText() throws XMLStreamException {
        return escape(reader.getElementText());
    }

    public String getEncoding() {
        return reader.getEncoding();
    }

    public int getEventType() {
        return reader.getEventType();
    }

    public String getLocalName() {
        return reader.getLocalName();
    }

    public Location getLocation() {
        return reader.getLocation();
    }

    public QName getName() {
        return reader.getName();
    }

    public NamespaceContext getNamespaceContext() {
        return reader.getNamespaceContext();
    }

    public int getNamespaceCount() {
        return reader.getNamespaceCount();
    }

    public String getNamespacePrefix(int index) {
        return reader.getNamespacePrefix(index);
    }

    public String getNamespaceURI() {
        return reader.getNamespaceURI();
    }

    public String getNamespaceURI(int index) {
        return reader.getNamespaceURI(index);
    }

    public String getNamespaceURI(String prefix) {
        return reader.getNamespaceURI(prefix);
    }

    public String getPIData() {
        return reader.getPIData();
    }

    public String getPITarget() {
        return reader.getPITarget();
    }

    public String getPrefix() {
        return reader.getPrefix();
    }

    public Object getProperty(String name) throws IllegalArgumentException {
        return reader.getProperty(name);
    }

    public String getText() {
        return escape(reader.getText());
    }

    public char[] getTextCharacters() {
        return escape(reader.getTextCharacters());
    }

    public int getTextCharacters(int sourceStart, char[] target, int targetStart, int length) throws XMLStreamException {
        int res = reader.getTextCharacters(sourceStart, target, targetStart, length);
        escape(target);
        return res;
    }

    public int getTextLength() {
        return reader.getTextLength();
    }

    public int getTextStart() {
        return reader.getTextStart();
    }

    public String getVersion() {
        return reader.getVersion();
    }

    public boolean hasName() {
        return reader.hasName();
    }

    public boolean hasNext() throws XMLStreamException {
        return reader.hasNext();
    }

    public boolean hasText() {
        return reader.hasText();
    }

    public boolean isAttributeSpecified(int index) {
        return reader.isAttributeSpecified(index);
    }

    public boolean isCharacters() {
        return reader.isCharacters();
    }

    public boolean isEndElement() {
        return reader.isEndElement();
    }

    public boolean isStandalone() {
        return reader.isStandalone();
    }

    public boolean isStartElement() {
        return reader.isStartElement();
    }

    public boolean isWhiteSpace() {
        return reader.isWhiteSpace();
    }

    public int next() throws XMLStreamException {
        return reader.next();
    }

    public int nextTag() throws XMLStreamException {
        return reader.nextTag();
    }

    public void require(int type, String namespaceURI, String localName) throws XMLStreamException {
        reader.require(type, namespaceURI, localName);
    }

    public boolean standaloneSet() {
        return reader.standaloneSet();
    }



}
