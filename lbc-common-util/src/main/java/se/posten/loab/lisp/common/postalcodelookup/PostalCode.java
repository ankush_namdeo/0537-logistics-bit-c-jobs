package se.posten.loab.lisp.common.postalcodelookup;

import se.posten.loab.lisp.common.util.CountryCode;

public class PostalCode {

	private final String code;
	private final String city;
	private final CountryCode countryCode;
	private final boolean poBoxCode;
	
	public PostalCode(String code, String city, CountryCode countryCode, boolean postalOfficeBox) {
	    super();
	    this.code = code;
	    this.city = city;
	    this.countryCode = countryCode;
	    this.poBoxCode = postalOfficeBox;
    }

	public String getCode() {
    	return code;
    }

	public String getCity() {
    	return city;
    }

	public CountryCode getCountryCode() {
    	return countryCode;
    }

	public boolean isPoBoxCode() {
    	return poBoxCode;
    }
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("%s-%s %s", null != countryCode ? countryCode.getTwoAlphaCode() : "??", code, city));
		if (poBoxCode) { sb.append(" (PO Box code)"); }
		return sb.toString();		
	}

	
	
	
}
