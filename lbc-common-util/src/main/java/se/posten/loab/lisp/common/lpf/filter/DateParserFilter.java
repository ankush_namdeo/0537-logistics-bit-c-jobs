package se.posten.loab.lisp.common.lpf.filter;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Parses a textual date to a java.util.Date
 * 
 * @author Christer Wikman, DevCode
 */
public class DateParserFilter implements Filter {

	private SimpleDateFormat dateformat;
	private String pattern;
	
	public DateParserFilter(String pattern) {
		this(pattern, false);
	}

	public DateParserFilter(String pattern, boolean lenient) {
		this.pattern = pattern;
		dateformat = new SimpleDateFormat(pattern);
		dateformat.setLenient(lenient);
	}

	public boolean filter(String line, Enum<?> field, FieldValue storage) {
		String s = null;
		try {
			s = storage.get(field).toString();
			Date dst = dateformat.parse(s);
			storage.put(field, dst);
			return true;
		} catch (Exception e) {
			if (!dateformat.isLenient()) {
				String msg = String.format("Enum %s : Failed to parse %s using date pattern %s, due to %s",
					field, s, pattern, e);			
				throw new IllegalStateException(msg);
			}
			storage.put(field, null);
			return true;
		}
	}
}
