package se.posten.loab.lisp.common.lpf.filter;

/** Compares if a field conforms to a specific value
 * 
 * @author Christer Wikman, DevCOde
 */
public class StaticField implements Filter {
    
	private String value = null;
    private boolean required = false;
    
    public StaticField(String value, boolean required) {
    	this.value = value;
    }

    public StaticField(Enum<?> value, boolean required) {
    	this(value.name(), required);
    }

    public boolean filter(String line, Enum<?> field, FieldValue storage) {
        Object o = storage.get(field);
        if (!value.equals(o)) {
        	if (!required) {
        		return false;
        	}
        	String msg = String.format("Enum %s must be %s, but is %s in line %s", field, value, o, line);
        	throw new IllegalStateException(msg);
        }
        return true;
    }
    
}
