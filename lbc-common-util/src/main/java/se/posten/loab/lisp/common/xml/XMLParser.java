package se.posten.loab.lisp.common.xml;

import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

public class XMLParser {

    private boolean schemaValidation = true;

    private final Schema schema;
    private final JAXBContext jaxbContext;
    
    public XMLParser(JAXBContext jaxbContext, Schema schema) {
    	this.schema = schema;
    	this.jaxbContext = jaxbContext;
    }
    

    public Schema getSchema() {
    	return schema;
	}        
    
    public JAXBContext getJAXBContext() {
    	return jaxbContext;
    }
    
    
    public static Schema createSchema(String schemaLocation) {
        URL schemaResource = XMLParser.class.getResource(schemaLocation);
        if (schemaResource == null) {
            throw new RuntimeException("Didn't find schema: " + schemaLocation);
        }

        try {
	        SchemaFactory sf = SchemaFactory.newInstance(javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI);
	        Schema schema = sf.newSchema(schemaResource);
	        return schema;
        } catch (Exception e) {
        	throw new IllegalStateException("Invalid schema: "+schemaLocation+" due to "+e.getMessage(), e);
        }
    }

    public static JAXBContext createJAXBContext(String packageName) {
    	try {
	        JAXBContext jc = JAXBContext.newInstance(packageName);
	        return jc;
    	} catch (Exception e) {
    		throw new IllegalStateException("Failed to create JAXB context due to "+e.getMessage(), e);
    	}        
    }
    
    
    public void setSchemaValidation(boolean schemaValidation) {
        this.schemaValidation = schemaValidation;
    }

    public Object parse(String xmlMessage) {
        StringReader reader = new StringReader(xmlMessage);
        return parse(reader);
    }

    public Object parse(Reader reader) {
        try {
        	Unmarshaller unmarshaller = getJAXBContext().createUnmarshaller();
            if (schemaValidation) {
                unmarshaller.setSchema(getSchema());
            }
            XMLStreamReader xmlEventReader = XMLInputFactory.newInstance().createXMLStreamReader(reader);
            EscapingXMLStreamReader xmlReader = new EscapingXMLStreamReader(xmlEventReader);

            return unmarshaller.unmarshal(xmlReader);
        } catch (JAXBException e) {
            String message = (e.getCause() != null) ? e.getCause().getMessage() : e.getMessage();
            throw new IllegalArgumentException(message, e);
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage(), e);
        }
    }

    public String format(Object xmlObjects, boolean prettyFormatted) {
        StringWriter writer = new StringWriter();
        format(xmlObjects, writer, prettyFormatted);
        return writer.getBuffer().toString();
    }

    public String format(Object xmlObjects) {
        return format(xmlObjects,false);
    }

    public void format(Object xmlObjects, Writer writer, boolean prettyFormattaed) {
        try {
            JAXBContext jc = getJAXBContext();
            Marshaller marshaller = jc.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, prettyFormattaed);
            // marshaller.setProperty("com.sun.xml.bind.characterEscapeHandler", new EscapeInvalidXMLCharHandler());
            if (schemaValidation) {                
                marshaller.setSchema(getSchema());
            }
            XMLStreamWriter xmlStreamWriter = XMLOutputFactory.newInstance().createXMLStreamWriter(writer);
            EscapingXMLStreamWriter xmlWriter = new EscapingXMLStreamWriter(xmlStreamWriter);
            marshaller.marshal(xmlObjects, xmlWriter);
        } catch (Exception e) {
            throw new IllegalArgumentException(e.toString(), e);
        }
    }

    public void format(Object xmlObjects, Writer writer) {
        format(xmlObjects, writer, false);
    }

  
}

