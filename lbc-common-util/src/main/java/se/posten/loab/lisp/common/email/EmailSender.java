package se.posten.loab.lisp.common.email;

import se.posten.loab.lisp.mail.xml.MailMessage;

/**
 * Simple interface for sending an email.
 * 
 * @author stca708
 */
public interface EmailSender {
    /**
     * Sends an email based on a MailMessage (XML) object.
     * 
     * @param mailMessage
     *            The email to send. This is an instance of the MailMessage JAXB class that primarly is used by the NOS
     *            email-service.
     * @throws SendEmailException
     *             If something goes wrong.
     */
    public void sendEmail(MailMessage mailMessage) throws SendEmailException;

}