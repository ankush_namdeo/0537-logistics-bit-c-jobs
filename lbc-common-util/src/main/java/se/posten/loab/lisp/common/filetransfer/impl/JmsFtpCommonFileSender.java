package se.posten.loab.lisp.common.filetransfer.impl;

import java.util.Properties;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import se.posten.loab.lisp.common.filetransfer.FileSender;
import se.posten.loab.lisp.common.filetransfer.SendFileException;
import se.posten.loab.lisp.common.jmsutil.sender.MessageSender;
import se.posten.loab.lisp.common.jmsutil.sender.MessageSenderImpl;

/**
 * An implementation of FileSender that uses JMS to send files. It assumes that the consumer of the message is the LISP
 * FTP Common component that forwards the files to its configured FTP destination.
 * 
 * @author stca708
 */
public class JmsFtpCommonFileSender implements FileSender {

    private final ConnectionFactory connectionFactory;
    private final Destination destination;
    private static final String FTP_COMMON_BINARY_MESSAGE_KEY = "BinaryMessage";
    private static final String FTP_COMMON_FILE_NAME_KEY = "CamelFileName";
    private final Log logger = LogFactory.getLog(getClass());

    public JmsFtpCommonFileSender(ConnectionFactory connectionFactory, Destination destination) {
        this.connectionFactory = connectionFactory;
        this.destination = destination;
    }

    @Override
    public void sendFile(String fileName, byte[] fileContent) throws SendFileException {
        Connection connection = null;
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            MessageSender sender = new MessageSenderImpl(connection);
            Properties jmsStringProperties = new Properties();
            jmsStringProperties.setProperty(FTP_COMMON_FILE_NAME_KEY, fileName);
            jmsStringProperties.setProperty(FTP_COMMON_BINARY_MESSAGE_KEY, "true");
            sender.sendBytesMessage(destination, fileContent, jmsStringProperties, null);
        } catch (Throwable t) {
            throw new SendFileException("Could not put file on queue. Msg: " + t.getMessage(), t);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException e) {
                    logger.error("Could not close connection after sending file to queue. Msg:" + e.getMessage(), e);
                }
            }
        }
    }
}
