package se.posten.loab.lisp.common.util.asynchronous;

import java.util.concurrent.Future;

public abstract interface FutureCallBack<T, E, R> {
    
    /**
     * This must call an EJB method annotated with @Asynchronous
     * 
     * @param paramT
     * @param paramE
     * @return
     */
    public abstract Future<R> invoke(T paramT, E paramE);

    /**
     * Handles the result. 
     * The future is guaranteed to be ready without any wait.
     * 
     * @param paramFuture
     * @throws InterruptedException
     * @throws BatchTryAgainException
     * @throws AbortJobException
     */
    public abstract void futureDone(Future<R> paramFuture) throws InterruptedException, BatchTryAgainException, AbortJobException;
}
