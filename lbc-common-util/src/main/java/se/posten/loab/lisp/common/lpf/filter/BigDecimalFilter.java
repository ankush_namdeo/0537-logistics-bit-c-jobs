package se.posten.loab.lisp.common.lpf.filter;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;


/** Filter that converts a string to a big decimal
 * 
 * @author Christer Wikman, DevCode
 *
 */
public class BigDecimalFilter implements Filter {

    private boolean mandatory = false;

    public BigDecimalFilter() {
        this(false);
    }

    public BigDecimalFilter(boolean mandatory) {
        this.mandatory = mandatory;
    }


    public boolean filter(String line, Enum<?> field, FieldValue storage) {
        String s = storage.getString(field);
        if (StringUtils.isEmpty(s) && !mandatory) {
            storage.put(field, null);
            return true;
        }
        try {
            BigDecimal v = new BigDecimal(s);
            storage.put(field, v);
        } catch (Exception e) {
            String msg = String.format("Enum %s : Failed to convert %s to BigDecimal due to: %s", field, s, e);
            throw new IllegalStateException(msg);
        }
        return true;
    }

}
