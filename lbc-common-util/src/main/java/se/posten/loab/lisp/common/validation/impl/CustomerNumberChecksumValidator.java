package se.posten.loab.lisp.common.validation.impl;

import se.posten.loab.lisp.common.validation.Validator;
import se.posten.loab.lisp.common.validation.ella.EllaValidator;

public class CustomerNumberChecksumValidator implements Validator<String> {

	public boolean validate(String target) {
		return EllaValidator.checkCustomerNumber(target);
	}
}
