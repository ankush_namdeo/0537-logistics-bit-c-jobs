package se.posten.loab.lisp.common.filetransfer;

/**
 * This exception is thrown when a file could not be sent by a {@link FileSender}.
 * 
 * @author stca708
 */

public class SendFileException extends Exception {
    private static final long serialVersionUID = 1L;

    public SendFileException() {
        super();
    }

    public SendFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public SendFileException(String message) {
        super(message);
    }

    public SendFileException(Throwable cause) {
        super(cause);
    }
}
