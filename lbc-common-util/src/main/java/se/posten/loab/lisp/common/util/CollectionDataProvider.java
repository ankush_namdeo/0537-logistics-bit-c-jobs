package se.posten.loab.lisp.common.util;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Queue;

import se.posten.loab.lisp.common.util.DataProvider;

public class CollectionDataProvider<T> implements DataProvider<T> {
    Queue<T> queue;

    public CollectionDataProvider(Collection<T> elements) {
        this.queue = new ArrayDeque<T>(elements);
    }

    public T getNext() {
        return this.queue.poll();
    }

    public boolean hasNext() {
        return (!(this.queue.isEmpty()));
    }
}
