package se.posten.loab.lisp.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utilities for parsing of (postal) addresses.
 */
public class PostAddressUtil {

    private static final int MIN_LENGTH_STREET = 2;
    private static Pattern streetNumberPattern = Pattern.compile("\\s\\d");

    /**
     * Splits a street address into street name and number.
     * 
     * The parsing is very basic, and could be improved. Refer to test class for examples.
     * 
     * @param address
     *            The full street address.
     * @return An array with street name at index 0, and number at index 1.
     */
    public static String[] splitStreetAddressToStreetAndNumber(String address) {
        String[] response = { "", "" };

        Matcher m = streetNumberPattern.matcher(address);
        if (m.find()) {
            response[0] = address.substring(0, m.start()).trim();
            response[1] = address.substring(m.start()).trim();
        }

        if (response[0].length() < MIN_LENGTH_STREET) {
            response[0] = address;
        }

        return response;
    }

}
