package se.posten.loab.lisp.common.validation.impl;

import se.posten.loab.lisp.common.util.CountryCode;
import se.posten.loab.lisp.common.validation.Validator;

/**
 * Validates a 2 letter country code according to ISO-3166-2 alpha-2 standard. Only upper case letters are allowed. Null
 * or empty String will fail validation.
 * 
 * <p>
 * See <a href="http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2"> ISO 3166-1 alpha-2</a> for more information, including
 * a table of country codes.
 * </p>
 * 
 * @see se.posten.loab.lisp.common.util.CountryCode
 * 
 * @author stca708
 */

public class ISO31661Alpha2CountryCodeValidator implements Validator<String> {

    public boolean validate(String target) {
        return (CountryCode.getInstanceFromIsoTwoAlpha(target) != null && target.toUpperCase().equals(target));
    }
}
