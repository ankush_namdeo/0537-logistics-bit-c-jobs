package se.posten.loab.lisp.common.circuitbreaker.jmx;

import java.util.Date;

import se.posten.loab.lisp.common.circuitbreaker.support.CircuitBreakerAspectSupport;
import se.posten.loab.lisp.common.circuitbreaker.util.Duration;


public interface CircuitBreakerMBean {

    /**
     * Lazy assignment of the circuit breaker instance
     */
    void setBreaker(CircuitBreakerAspectSupport breaker);

    /**
     * Test whether the circuit is open. When the circuit is open, any calls made through the circuit will not be
     * propagated and an OpenCircuitException will be thrown.
     */
    boolean isOpen();

    /**
     * Open the circuit, causing any subsequent calls made through the circuit to throw an OpenCircuitException.
     */
    void open();

    /**
     * Test whether the circuit is closed. The circuit may at the same time be HalfOpen. When the circuit is closed,
     * calls are propagated. If the circuit is closed and not HalfOpen, the circuit is in "normal operation".
     */
    boolean isClosed();

    /**
     * Close the circuit, allowing any method call to propagate to their
     * recipients.
     */
    void close();

    /**
     * Test whether the circuit is half open. This normally means that the circle currently is closed so that a new
     * attempt can be made. The outcome of this attempt will decide if the circuit will close or open again.
     * 
     * When HalfOpen, the circuit is also Closed.
     */
    boolean isHalfOpen();

    /**
     * Get the Date the circuit was opened.
     * 
     * @return if the circuit is open return the open timestamp else null
     */
    Date getOpenTimestamp();

    /**
     * The number of calls being made through the circuit.
     */
    int getCalls();

    /**
     * The total number of failures.
     */
    int getFailures();

    /**
     * The number of failures since the circuit was closed. Will never exceed MaxFailures.
     */
    int getCurrentFailures();

    /**
     * The number of times the circuit has been opened.
     */
    int getTimesOpened();

    /**
     * The number of failures after the circuit opens.
     */
    int getMaxFailures();

    /**
     * Set the number of failures after the circuit opens.
     */
    void setMaxFailures(int n);

    /**
     * The timeout after which the circuit closes.
     */
    String getTimeout();

    /**
     * Set the timeout after which the circuit closes.
     * 
     * @param timeout
     *            a String formated Duration
     * 
     * @see Duration
     */
    void setTimeout(String timeout);

    /**
     * Get the duration after which the number of failures tracked by the circuit breaker gets reset. A reset means that
     * currentFailures are set to 0 and that the timer that counts against this currentFailuresDuration is set to 0.
     * 
     * The next failure will set currentFailures to 1 and start the timer against currentFailuresDuration.
     * 
     * The circuit will open if current state is closed, and currentFailures reaches maxFailures during the time
     * specified by this attribute.
     */
    String getCurrentFailuresDuration();

    /**
     * Specify the duration after which the number of failures tracked by the circuit breaker gets reset. See also the
     * get method.
     * 
     * @param duration
     *            a string representing a {@code Duration} object.
     * 
     * @see Duration
     */
    void setCurrentFailuresDuration(String duration);

    /**
     * Get the duration after which a method execution is considered a failure.
     */
    String getMaxMethodDuration();

    /**
     * Set the duration after which a method execution is considered a failure.
     * 
     * @param d
     *            a String formated Duration or <code>null</code> if you want to
     *            disable the tracking of execution time.
     * 
     * @see Duration
     */
    void setMaxMethodDuration(String duration);
}
