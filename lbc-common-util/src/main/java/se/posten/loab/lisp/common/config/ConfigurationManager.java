package se.posten.loab.lisp.common.config;

import java.util.List;
import java.util.Properties;

/**
 * Interface for retrieving configuration parameters.
 * 
 * @see se.posten.loab.lisp.common.config.impl.CachedApacheCommonConfigurationManager
 * 
 * @author stca708
 */
public interface ConfigurationManager {
    /**
     * Returns the string value for the configuration parameter given by the key.
     * 
     * @param key
     *            The name of the configuration parameter.
     * @return The string value if found. Otherwise null.
     */
    public String getString(String key);

    /**
     * Returns the integer value for the configuration parameter given by the key.
     * 
     * @throws ConfigurationParameterNotFoundException
     *             If configuration parameter could not be found.
     * @throws InvalidConfigurationParameterValueException
     *             If configuration parameter value is not an integer.
     * 
     * @param key
     *            The name of the configuration parameter.
     * @return The integer value.
     */
    public int getInt(String key);

    /**
     * Returns the boolean value for the configuration parameter given by the key.
     * 
     * @throws ConfigurationParameterNotFoundException
     *             If configuration parameter could not be found.
     * @throws InvalidConfigurationParameterValueException
     *             If configuration parameter value is not a boolean.
     * 
     * @param key
     *            The name of the configuration parameter.
     * @return The boolean value.
     */
    public boolean getBoolean(String key);

    /**
     * Returns the value as a list of Strings the configuration parameter given by the key.
     * 
     * @param key
     *            The name of the configuration parameter.
     * @return The value as a List of Strings if found. If no configuration parameter is found for the given key, an
     *         empty List is returned.
     */
    public List<String> getList(String key);

    /**
     * Returns all configuration parameters, currently managed by the ConfigurationManager, as Properties.
     * 
     * @return All configuration parameters, currently managed by the ConfigurationManager, as Properties.
     */
    public Properties getAllProperties();
}
