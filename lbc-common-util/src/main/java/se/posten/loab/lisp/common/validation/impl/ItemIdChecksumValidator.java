package se.posten.loab.lisp.common.validation.impl;

import se.posten.loab.lisp.common.validation.Validator;
import se.posten.loab.lisp.common.validation.ella.EllaValidator;

public class ItemIdChecksumValidator implements Validator<String> {
	
	public boolean validate(String itemId) {
		return EllaValidator.checkKolliId(itemId);
	}
}
