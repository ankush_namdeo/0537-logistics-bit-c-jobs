package se.posten.loab.lisp.common.validation.ella;

import se.posten.loab.lisp.common.validation.util.ValidationUtils;

public class EllaValidator {

    private static final int POSTEN_KOLLIID_LANGD = 13;
    private static final int SIS_KOLLIID_LANGD = 10;
    private static final int EAN_KOLLIID_LANGD = 20;
    private static final int DPD_KOLLIID_LANGD = 15;
    private static final int DPD_KOLLIID_NOCHECKSUM_LANGD = 14;
    private static final int OLDDPD_KOLLIID_LANGD = 12;
    private static final int DPD_LUBECK_DEPA = 908;
    private static final int DPD_START_DEPA = 300;
    private static final int DPD_SLUT_DEPA = 317;
    private static final int DPD_DEPA_LENGT = 4;

    // TODO: ???
    private static boolean OLD_DPD_OK = false;

    /**
     * Kontrollerar checksumman för ett kolli.
     * 
     * @param p_strKolliId
     *            EAN-kolliid
     * @return <code>true</code> om checksumman är korrekt, annars <code>false</code>.
     */
    public static boolean checkKolliId(String p_strKolliId) {

        // om dpd kolli med längd 14 - ingen checksiffra
        if (p_strKolliId.length() == DPD_KOLLIID_NOCHECKSUM_LANGD)
            return true;

        CheckSum check = CheckSum.getInstance();
        return check.isValid(p_strKolliId, CheckSum.PARCEL_ID);
    }

    public static boolean checkCustomerNumber(String customerNumber) {
        return CheckSum.getInstance().isValid(customerNumber, CheckSum.CUSTOMER_ID);
    }

    public static String removeDpdChecksum(String itemId) {
        if (itemId != null && itemId.length() == DPD_KOLLIID_LANGD) {
            return itemId.substring(0, DPD_KOLLIID_LANGD - 1);
        }
        return itemId;
    }

    /**
     * Kontrollera om ett kolli är ett UPU-kolli.
     * 
     * @param p_strKolliId
     *            kolliid
     * @return <code>true</code> om det är ett UPU-kolli, annars <code>false</code>.
     */
    public static boolean isUPUKolli(String p_strKolliId) {
        // Kontrollera prefix
        if (!p_strKolliId.startsWith("JJ")) {
            return false;
        }

        return true;
    }

    public static boolean isKandKolliIDTyp(final String p_kollinummer) {
        return (isDPDKolli(p_kollinummer) || isSISKolli(p_kollinummer) || isUPUKolli(p_kollinummer)
                || isEANKolli(p_kollinummer) || isPostenKolli(p_kollinummer));
    }

    /**
     * Kontrollera om ett kolliID tillhär Postens egen kolliserie. Checksiffran kontrolleras inte. Använd checkKolliId
     * för cheksifferkontroll.
     * 
     * @param p_strKolliId
     *            kolliid
     * @return <code>true</code> om kolliID tillhär Postens egen kolliserie, annars <code>false</code>.
     */
    public static boolean isPostenKolli(String p_strKolliId) {
        // Kontrollera om kolliid är 13 tecken långt
        if (p_strKolliId.length() != POSTEN_KOLLIID_LANGD) {
            return false;
        }

        return true;
    }

    /**
     * Kontrollera om ett kolli är ett SIS-kolli.
     * 
     * @param p_strKolliId
     *            kolliid
     * @return <code>true</code> om det är ett SIS-kolli, annars <code>false</code>.
     */
    public static boolean isSISKolli(String p_strKolliId) {
        // Kontrollera om kolliid är 10 tecken långt
        if (p_strKolliId.length() != SIS_KOLLIID_LANGD) {
            return false;
        }

        // kontrollera om kolliid är numeriskt
        if (isNumerisk(p_strKolliId) == false) {
            return false;
        }

        return true;
    }

    /**
     * Kontrollera om ett kolli är ett EAN-kolli. Checksiffran kontrolleras inte. Använd checkKolliId för
     * cheksifferkontroll.
     * 
     * @param p_strKolliId
     *            kolliid
     * @return <code>true</code> om det är ett EAN-kolli, annars <code>false</code>.
     */
    public static boolean isEANKolli(String p_strKolliId) {
        // Kontrollera om kolliid är 20 tecken långt
        if (p_strKolliId.length() != EAN_KOLLIID_LANGD) {
            return false;
        }

        // Kontrollera prefix
        if (!p_strKolliId.startsWith("00")) {
            return false;
        }

        return true;
    }

    /**
     * Kontrollera om ett kolli ser ut som ett DPD-kolli. Jag använder mig inte av motsvarande metod som finns i ELLALib
     * eftersom den även kollar checksiffran.
     * 
     * @param p_strKolliId
     *            DPD-kolliid
     * @return <code>true</code> om det är ett DPD-kolli, annars <code>false</code>.
     */
    public static boolean isDPDKolli(final String p_strKolliId) {
        if (OLD_DPD_OK && isOldDPDKolli(p_strKolliId)) {
            return true;
        }
        if (p_strKolliId.length() != DPD_KOLLIID_LANGD && p_strKolliId.length() != DPD_KOLLIID_NOCHECKSUM_LANGD) {
            return false;
        }

        // Stämmer checksiffran?
        return checkKolliId(p_strKolliId);
    }

    /**
     * Kontrollera om ett kolli ser ut som ett gamamlt DPD-kolli.
     * 
     * @param p_strKolliId
     *            DPD-kolliid
     * @return <code>true</code> om det är ett gammalt DPD-kolli, annars <code>false</code>.
     */
    private static boolean isOldDPDKolli(String p_strKolliId) {
        // Kontrollera om kolliid är 12 tecken långt
        if (p_strKolliId.length() != OLDDPD_KOLLIID_LANGD) {
            return false;
        }

        // är kolliid numeriskt?
        if (!isNumerisk(p_strKolliId)) {
            return false;
        }

        return true;
    }

    /**
     * Kontrollera om samtliga tecken är 0-9, ' ' är ej tillåten.
     * 
     * @return true om samtliga tecken är 0-9, ' ' är ej tillåten.
     */
    public static boolean isNumerisk(String s) {
        if (s == null)
            throw new IllegalArgumentException("isNumerisk() arg = null");
        int len = s.length();
        for (int i = 0; i < len; i++)
            if (!Character.isDigit(s.charAt(i)))
                return false;
        return true;
    }

    /**
     * Kontrollera om ett kolli är ett svenskt dpd-kolli. Checksiffran kontrolleras inte. Använd checkKolliId för
     * cheksifferkontroll.
     * 
     * @param p_strKolliId
     *            DPD-kolliid
     * @return <code>true</code> om det är ett svenskt dpd-kolli, annars <code>false</code>.
     */
    public static boolean isSwedishDPDKolli(String p_strKolliId) {

        // är det ett generellt dpd-kolli
        if (!isDPDKolli(p_strKolliId)) {
            return false;
        }

        String depa = getDepa(p_strKolliId);
        if (isNumerisk(depa)) {
            int iDepa = Integer.parseInt(depa);
            if (iDepa == DPD_LUBECK_DEPA) { // Lübeck ligger i Sverige
                return true;
            }
            if (iDepa < DPD_START_DEPA || iDepa > DPD_SLUT_DEPA) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

    /**
     * Kontrollera om ett kolli är ett svenskt kolli, dvs har en svensk nummerserie. Checksiffran kontrolleras inte.
     * Använd checkKolliId för cheksifferkontroll.
     * 
     * @param p_strKolliId
     *            kolliid
     * @return <code>true</code> om det är ett svenskt kolli, annars <code>false</code>.
     */
    public static boolean isSwedishKolli(String p_strKolliId) {

        // Kontrollera om kolliid är 13 tecken långt
        if (p_strKolliId.length() != POSTEN_KOLLIID_LANGD) {
            return false;
        }

        // Slutar kollit på SE?
        if (!p_strKolliId.endsWith("SE")) {
            return false;
        }
        // är kolliid numeriskt förutom suffix och checksiffra?
        if (!isNumerisk(p_strKolliId.substring(0, 10))) {
            return false;
        }

        return true;
    }

    /**
     * Hämtar ut depån från ett kolliid.
     * 
     * @param p_strKolliId
     *            kolliid
     * @return <code>depå</code>
     */
    public static String getDepa(String p_strKolliId) {
        return p_strKolliId.substring(0, DPD_DEPA_LENGT);
    }

    /**
     * Polish identifier by removing unwanted characters like checksum characters for DPD
     * 
     * @param identifier
     *            Identifier, can be null
     * @return Polished identifier
     */
    public static String polishIdentifier(String identifier) {
        if (null == identifier) {
            return identifier;
        }
        if (ValidationUtils.isDPDItem(identifier)) {
            return EllaValidator.removeDpdChecksum(identifier);
        }
        return identifier;
    }

}
