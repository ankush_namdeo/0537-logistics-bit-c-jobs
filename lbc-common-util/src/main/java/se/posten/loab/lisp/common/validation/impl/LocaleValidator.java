package se.posten.loab.lisp.common.validation.impl;

import org.apache.commons.lang.LocaleUtils;

import se.posten.loab.lisp.common.validation.Validator;

public class LocaleValidator implements Validator<String> {
    public boolean validate(String target) {
        if (target == null || target.isEmpty()) {
            return false;
        }
        try {
            LocaleUtils.toLocale(target);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }
}
