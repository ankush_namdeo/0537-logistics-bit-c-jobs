package se.posten.loab.lisp.common.jmsutil.sender;

import javax.jms.Topic;

public interface TopicMessagePublisher {

    void publishToTopic(Topic topic, String xmlMessage, String correlationId);

    void closeConnection();
}
