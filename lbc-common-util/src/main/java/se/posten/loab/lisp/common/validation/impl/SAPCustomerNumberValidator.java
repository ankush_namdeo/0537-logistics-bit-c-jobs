package se.posten.loab.lisp.common.validation.impl;

import se.posten.loab.lisp.common.validation.Validator;

/**
 * FIXME: This class is not complete. It validates any numeric field with 8-10 chars.
 * 
 * It has not been confirmed that this is a correct SAP Customer number format.
 * 
 */

public class SAPCustomerNumberValidator implements Validator<String> {

    private static final Validator<String> patternValidator = new PatternValidator("^[0-9]{8,10}$");

    public boolean validate(String target) {
        return patternValidator.validate(target);
    }
}
