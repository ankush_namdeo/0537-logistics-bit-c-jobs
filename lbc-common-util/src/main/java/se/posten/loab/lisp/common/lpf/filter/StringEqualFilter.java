package se.posten.loab.lisp.common.lpf.filter;

public class StringEqualFilter implements Filter {

    private String myName = null;
    private final char myEndChar;
    private int myStartIndex = -1;
    private int myEndIndex = -1;

    public StringEqualFilter(String name, char endChar) {
        myName = name;
        myEndChar = endChar;
    }

    /**
     * @param line
     * @param field
     * @param storage
     * @throws IllegalArgumentException
     * @return  */
    public boolean filter( String line, Enum<?> field, FieldValue storage) throws IllegalArgumentException {
        String lineStr = line.toString();
        try
        {
            myStartIndex = lineStr.indexOf(myName) + myName.length();
            myEndIndex = lineStr.indexOf(myEndChar, myStartIndex);

            lineStr = lineStr.substring(myStartIndex, myEndIndex);
            storage.put( field, lineStr );
            return true;
        } catch ( Exception e ) {
            throw new IllegalArgumentException("Cannot parse \"" + lineStr + "\" field: " + field );
        }
    }

}
