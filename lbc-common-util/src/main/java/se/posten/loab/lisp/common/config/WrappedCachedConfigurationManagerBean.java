package se.posten.loab.lisp.common.config;

import java.util.List;
import java.util.Properties;

/**
 * <p>
 * A simple wrapping class that can be used to contain a configuration manager that implements
 * CachedConfigurationManagerMBean.
 * </p>
 * <p>
 * Example:
 * 
 * <pre>
 * &#064;Service(name = &quot;configurationService&quot;)
 * &#064;Management(CachedConfigurationManagerMBean.class)
 * public class MyConfigurationServiceBean extends WrappedCachedConfigurationManagerBean {
 * 
 *     private static final long CONFIGURATION_MANAGER_CACHE_TIME_TO_LIVE_MS = 60 * 60 * 1000; //1h
 * 
 *     private String configFileName = &quot;/my-service-configuration.xml&quot;;
 * 
 *     // If running in JBoss as MBean 
 *     public void start() throws Exception {
 *         init();
 *     }
 * 
 *     // For unit test.
 *     &#064;PostConstruct
 *     public void postConstruct() {
 *         init();
 *     }
 * 
 *     // Initialize 
 *     private void init() {
 *         setCachedConfigurationManagerMBean(new CachedApacheCommonConfigurationManager(configFileName,
 *             CONFIGURATION_MANAGER_CACHE_TIME_TO_LIVE_MS));
 *         refresh();
 *     }
 * }
 * 
 * </pre>
 * 
 * For full usage example, see se.posten.loab.lisp.productionpointservice.core.serviceimpl.ConfigurationService.
 * </p>
 * 
 * @author stca708
 * 
 */
public class WrappedCachedConfigurationManagerBean implements CachedConfigurationManagerMBean {

    private CachedConfigurationManagerMBean configurationManager;

    /**
     * Sets the ConfigurationManager.
     * 
     * @param configurationManager
     */
    public void setCachedConfigurationManagerMBean(CachedConfigurationManagerMBean configurationManager) {
        this.configurationManager = configurationManager;
    }

    /**
     * Gets the wrapped ConfigurationManager.
     */
    public ConfigurationManager getConfigurationManager() {
        return configurationManager;
    }

    @Override
    public Properties getInfoAboutConfigurationManager() {
        return configurationManager.getInfoAboutConfigurationManager();
    }

    @Override
    public Properties getAllProperties() {
        return configurationManager.getAllProperties();
    }

    @Override
    public boolean getBoolean(String key) {
        return configurationManager.getBoolean(key);
    }


    @Override
    public int getInt(String key) {
        return configurationManager.getInt(key);
    }


    @Override
    public List<String> getList(String key) {
        return configurationManager.getList(key);
    }


    @Override
    public String getString(String key) {
        return configurationManager.getString(key);
    }
    
    @Override
    public void refresh() {
        configurationManager.refresh();
    }

    @Override
    public void start() throws Exception {
        // Do nothing. Override if needed.
    }

}