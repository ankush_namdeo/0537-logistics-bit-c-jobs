package se.posten.loab.lisp.common.linda;

import java.util.HashMap;

public final class TerminalMapping {

    private static HashMap<String, String> terminalMappings = new HashMap<String, String>();

    static {
        terminalMappings.put("01", "951");
        terminalMappings.put("02", "901");
        terminalMappings.put("03", "175");
        terminalMappings.put("06", "126");
        terminalMappings.put("07", "701");
        terminalMappings.put("08", "551");
        terminalMappings.put("09", "421");
        terminalMappings.put("11", "201");
        terminalMappings.put("22", "851");
        terminalMappings.put("23", "831");
        terminalMappings.put("951", "01");
        terminalMappings.put("901", "02");
        terminalMappings.put("175", "03");
        terminalMappings.put("126", "06");
        terminalMappings.put("701", "07");
        terminalMappings.put("551", "08");
        terminalMappings.put("421", "09");
        terminalMappings.put("201", "11");
        terminalMappings.put("851", "22");
        terminalMappings.put("831", "23");

    }

    public final static String getCspTerminalId(String itemLocationId) {
        return terminalMappings.get(itemLocationId);
    }

    public final static String getItemLocationId(String cspTerminalId) {
        return terminalMappings.get(cspTerminalId);
    }

}
