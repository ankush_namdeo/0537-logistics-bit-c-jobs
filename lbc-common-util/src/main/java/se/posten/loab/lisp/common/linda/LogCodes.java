package se.posten.loab.lisp.common.linda;

import java.text.MessageFormat;

import org.sculptor.framework.errorhandling.LogMessage;

public enum LogCodes {

    LIN_001("Failed to create temporary file {0}"),

    LIN_002("Failed to create temporary directory {0}"),

    LIN_003("Directory {0} does not exist and could not be created"),

    LIN_004("Failed to move file {0} to directory {1}"),

    LIN_100("Failed to upload file to FTP server"),

    LIN_101("Invalid user ({0}) or password when connecting to ftp server @{1}:{2}"),

    LIN_102("Could not connect to FTP server @{0}:{1}"),

    LIN_103("Could not communicate with FTP server @{0}:{1}"),

    LIN_104("Failed to switch ftp remote directory to {0}"),

    LIN_105("Failed to rename remote file {0} to {1}"),

    LIN_106("Failed to rename file {0} to {1}"),

    LIN_150("Failed to create XML marshaller"),

    LIN_151("Failed to marshal xml"),

    LIN_200("Failed to create directory for dead letter queue message storing: {0}"),

    LIN_201("Failed to write message to dead letter queue message file"),

    LIN_202("Failed to create file for dead letter queue message store"),

    LIN_203("JMS delivery failed (redelivered={0}). Detail message: {1}");

    private String msg;

    private LogCodes(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public LogMessage toLogMessage(Object... parameters) {
        try {
            String formattedMessage = MessageFormat.format(msg, parameters);
            return new LogMessage(name(), formattedMessage);
        } catch (IllegalArgumentException e) {
            return new LogMessage(name(), msg);
        }
    }

}
