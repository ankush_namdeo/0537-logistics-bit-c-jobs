/*
 * FixedField.java
 *
 * Created on den 26 april 2001, 20:59
 */

package se.posten.loab.lisp.common.lpf.filter;


/** Filter to convert a text true / false field to a boolean field
 *
 * @author Christer Wikman, DevCode
 */
public class BooleanFilter implements Filter {

    private String keyword = null;
    private boolean keywordIsTrue = false;
    private boolean ignoreCase = false;

    public BooleanFilter( String keyword, boolean keyWordIsTrue, boolean ignoreCase  ) {
        this.keyword = keyword;
        this.keywordIsTrue = keyWordIsTrue;
        this.ignoreCase = ignoreCase;
    }

    public BooleanFilter( String key, boolean representTrueOrFalse ) {
        this(key, representTrueOrFalse, false);
    }

    public BooleanFilter( String key ) {
        this(key, true, false);
    }

    public boolean filter(String line, Enum<?> field, FieldValue storage) throws IllegalArgumentException {

        try {
            String org = storage.get( field ).toString();
            boolean dst = null != org &&
                (ignoreCase ? keyword.equalsIgnoreCase(org)
                            : keyword.equals(org));
            dst = keywordIsTrue ? dst : !dst;
            storage.put( field, dst );
            return true;
        } catch ( Exception e ) {
            throw new IllegalStateException("Unknown error",e );
        }
    }
}
