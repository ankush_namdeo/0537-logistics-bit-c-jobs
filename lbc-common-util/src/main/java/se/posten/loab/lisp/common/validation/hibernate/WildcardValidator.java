package se.posten.loab.lisp.common.validation.hibernate;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * The wildcard validator can be used in GUI input objects to provide validation
 * that at least a certain number of characters are written when specifying a
 * wildcard search
 * <p>
 * Message bundle example: <br>
 * <code>
 * # Validation errors
 * lisp.validation.wildcard = Minst {minAllowed} tecken m\u00E5ste anges vid s\u00F6kning med jokertecken
 * </code>
 * 
 * @author ansk719 (Andreas Skoog)
 * 
 */
@Documented
@Target(value = { ElementType.METHOD, ElementType.FIELD })
@Retention(value = RetentionPolicy.RUNTIME)
@Constraint(validatedBy = WildcardValidatorImpl.class)

public @interface WildcardValidator {

    public Class<?>[] groups() default {};
    
    public Class<? extends Payload>[] payload() default {};
    
    /** The character that is used as wildcard */
    public char wildcardChar() default '%';

    /**
     * The minimum allowed characters when using a wildcard (except the wildcard
     * itself)
     */
    public int minAllowed() default 3;

    /** The internationalized error message to display upon validation errors */
    public java.lang.String message() default "{lisp.validation.wildcard}";

}
