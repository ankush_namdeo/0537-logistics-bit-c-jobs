package se.posten.loab.lisp.common.config;

/**
 * Runtime exception thrown when a value of a given configuration parameter could not be converted to the expected type.
 * 
 * @author stca708
 */
public class InvalidConfigurationParameterValueException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public InvalidConfigurationParameterValueException(Exception e) {
        super(e);
    }
}
