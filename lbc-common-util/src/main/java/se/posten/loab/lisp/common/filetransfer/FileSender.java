package se.posten.loab.lisp.common.filetransfer;

/**
 * Simple interface for sending files to an arbitrary destination defined by the implementation of the interface.
 * 
 * @author stca708
 */
public interface FileSender {
    /**
     * Sends a file.
     * 
     * @param fileName
     *            The name of the file.
     * @param fileContent
     *            The file content as binary data.
     * @throws SendFileException
     *             If something goes wrong.
     */
    public void sendFile(String fileName, byte[] fileContent) throws SendFileException;

}