package se.posten.loab.lisp.common.lpf.filter;


/** Selects which filter to use
 * 
 * @author Christer Wikman, DevCode
 *
 */
public class FieldDefSelectorFiltor implements GroupFilter {

    private GroupFilter selector = null;
    private GroupFilter groupFilter = null;

    public FieldDefSelectorFiltor(GroupFilter selector, GroupFilter filter) {
        this.selector = selector;
        this.groupFilter = filter;
    }

    public boolean filter(String line, FieldValue storage) {
        boolean selected = false;
        try {
            selected = selector.filter(line, storage);
        } catch (Exception ignore) {}
        if (selected) {
            groupFilter.filter(line, storage);
            return false;
        } else {
            return true;
        }
    }


}
