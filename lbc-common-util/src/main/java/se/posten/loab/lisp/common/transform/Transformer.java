package se.posten.loab.lisp.common.transform;

public interface Transformer<T, E> {

    E transform(T item);
}
