package se.posten.loab.lisp.common.validation;

/**
 * Generated interface for the Service ValidationService.
 */
public interface TrackingInformationValidator {
    public boolean validateItemId(String itemId);

    public boolean validateShipmentId(String shipmentId);

    public boolean validateCustomerNumber(String itemId);

    public boolean validateReference(String reference);

    public boolean validatePhoneNumber(String phone);

    boolean validateNotificationCode(String notificationCode);
}
