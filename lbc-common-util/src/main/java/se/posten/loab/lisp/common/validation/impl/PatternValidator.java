package se.posten.loab.lisp.common.validation.impl;

import java.util.regex.Pattern;

import se.posten.loab.lisp.common.validation.Validator;

public class PatternValidator implements Validator<String> {

	private Pattern pattern;
	
	public PatternValidator(String pattern) {
		this.pattern = Pattern.compile(pattern);
	}
	
	public boolean validate(String target) {
		if(target == null) return false;
		return pattern.matcher(target).matches();
	}
}
