package se.posten.loab.lisp.common.validation.ella;

import java.util.Hashtable;

public class CheckSum {
    // Typer av giltiga id
    public static final int PARCEL_ID = 1;
    public static final int CUSTOMER_ID = 2;
    public static final int SOCIAL_SECURITY_NUMBER = 3;
    public static final int OCR_REFERENCE_NUMBER = 4;

    // Längder
    private static final int CUSTOMER_ID_LEN = 10;
    private static final int OLD_DPD_PARCEL_ID_LEN = 12;
    private static final int DPD_PARCEL_ID_LEN = 15;
    private static final int EAN_PARCEL_ID_LEN = 20;
    private static final int PARCEL_ID_LEN = 13;
    private static final int SOCIAL_SECURITY_NUMBER_LEN = 10;

    // Tabell med olika id
    private static Hashtable<String, Integer> m_htType = null;

    // Referens till den enda instansen av klassen
    private static CheckSum m_cmInstance = null;

    /*
     * Default-konstruktor (privat)
     * 
     * @author Stefan Schwartz, Posten IT
     * 
     * @version 1.0
     */
    private CheckSum() {
    }

    /**
     * Instantierar (och initierar) den enda instansen av klassen.
     * 
     * @return Flagga som anger om klassen kunde instansieras.
     * @author Stefan Schwartz, Posten IT
     * @version 1.0
     */
    public static synchronized boolean init() {
        boolean isInstantiated = false;

        if (m_cmInstance != null) {
            // Klassen är redan instantierad
            isInstantiated = true;
        } else {
            try {
                // Skapa den enda instansen av CheckSum
                m_cmInstance = new CheckSum();

                // Skapa tabell med giltiga typer av id
                m_htType = new Hashtable<String, Integer>();

                m_htType.put(new String("customer_id"), new Integer(CUSTOMER_ID));
                m_htType.put(new String("kundnummer"), new Integer(CUSTOMER_ID));
                m_htType.put(new String("kundnr"), new Integer(CUSTOMER_ID));

                m_htType.put(new String("social_security_number"), new Integer(SOCIAL_SECURITY_NUMBER));
                m_htType.put(new String("personnummer"), new Integer(SOCIAL_SECURITY_NUMBER));
                m_htType.put(new String("personnr"), new Integer(SOCIAL_SECURITY_NUMBER));
                m_htType.put(new String("persnr"), new Integer(SOCIAL_SECURITY_NUMBER));

                m_htType.put(new String("parcel_id"), new Integer(PARCEL_ID));
                m_htType.put(new String("kolliid"), new Integer(PARCEL_ID));

                m_htType.put(new String("ocrReferensnummer"), new Integer(OCR_REFERENCE_NUMBER));
                m_htType.put(new String("ocr_reference_number"), new Integer(OCR_REFERENCE_NUMBER));

                // Ange att klassen instantierades (och initierades) utan fel
                isInstantiated = true;
            } catch (Exception e) {
                // Nollställ referenser
                m_cmInstance = null;
                m_htType = null;

                // Kunde inte skapa en instans av klassen
                isInstantiated = false;
            }
        }

        return isInstantiated;
    }

    /**
     * Hämta den enda instansen av klassen.
     * 
     * @return Den enda instansen av klassen CheckSum.
     * @author Stefan Schwartz, Posten IT
     * @version 1.0
     */
    public static CheckSum instance() {
        return getInstance();
    }

    /**
     * Hämta den enda instansen av klassen.
     * 
     * @return Den enda instansen av klassen CheckSum.
     * @author Stefan Schwartz, Posten IT
     * @version 1.0
     */
    public static CheckSum getInstance() {
        if (m_cmInstance == null) {
            synchronized (CheckSum.class) {
                if (!CheckSum.init()) {
                    return null;
                }
            }
        }

        return m_cmInstance;
    }

    /**
     * Beräkna kontrollsiffra enligt vägd modulus 10
     * 
     * @param p_strId
     *            Id
     * @return Kontrollsiffra
     * @exception NullPointerException
     *                om inparameter saknas.
     * @author Stefan Schwartz, Posten IT
     * @version 1.0
     */
    private int getModulo10CheckSum(String p_strId) {
        int iCheckSum; // Kontrollsiffra
        int iIndex = 0; // Index för vikt
        int iProduct;
        int iSum = 0;
        int iWeights[] = { 2, 1 }; // Vikter

        for (int i = p_strId.length() - 1; i >= 0; i--) {
            // Beräkna produkten...
            int iNumber = p_strId.charAt(i) - '0';
            iProduct = iNumber * iWeights[iIndex];

            // ...och ta fram delsumman (10 = 1+0, 11 = 1+1, osv.)
            iSum = iSum + (iProduct / 10) + (iProduct % 10);

            // Ta fram nästa vikt
            iIndex++;
            iIndex = iIndex % iWeights.length;
        }

        iCheckSum = iSum % 10;

        if (iCheckSum != 0) {
            iCheckSum = 10 - iCheckSum;
        }

        // Returnera kontrollsiffran
        return iCheckSum;
    }

    /**
     * Beräkna kontrollsiffra enligt vägd modulus 11
     * 
     * @param p_strId
     *            Id
     * @return Kontrollsiffra
     * @exception NullPointerException
     *                om inparameter saknas.
     * @author Stefan Schwartz, Posten IT
     * @version 1.0
     */
    private int getModulo11CheckSum(String p_strId) {
        int iCheckSum; // Kontrollsiffra
        int iIndex = 0; // Index för vikt
        int iSum = 0;
        int iWeights[] = { 7, 9, 5, 3, 2, 4, 6, 8 }; // Vikter

        for (int i = p_strId.length() - 1; i >= 0; i--) {
            // Beräkna delsumman
            int iNumber = p_strId.charAt(i) - '0';
            iSum = iSum + (iNumber * iWeights[iIndex]);

            // Ta fram nästa vikt
            iIndex++;
            iIndex = iIndex % iWeights.length;
        }

        iCheckSum = iSum % 11;

        if (iCheckSum == 0) {
            iCheckSum = 5;
        } else if (iCheckSum == 1) {
            iCheckSum = 0;
        } else {
            iCheckSum = 11 - iCheckSum;
        }

        // Returnera kontrollsiffra
        return iCheckSum;
    }

    /**
     * Beräkna kontrollsiffra för streckkod (EAN)
     * 
     * @param p_strEAN
     *            EAN-nummer
     * @exception NullPointerException
     *                om inparameter saknas.
     * @author Stefan Schwartz, Posten IT
     * @version 1.0
     */
    private int getEANCheckSum(String p_strEAN) {
        int iCheckSum = 0;
        int iIndex = 0;
        int iSum = 0;
        int iWeights[] = { 3, 1 };

        for (int i = 0; i < p_strEAN.length(); i++) {
            // Beräkna delsumma
            int iNumber = p_strEAN.charAt(i) - '0';
            iSum = iSum + iNumber * iWeights[iIndex];

            // Ta fram nästa vikt
            iIndex++;
            iIndex = iIndex % iWeights.length;
        }

        iCheckSum = iSum % 10;

        if (iCheckSum != 0) {
            iCheckSum = 10 - iCheckSum;
        }

        // Returnera kontrollsiffra
        return iCheckSum;
    }

    /**
     * Beräkna kontrollsiffra för DPD-kollin
     * 
     * @param p_strKolliId
     *            DPD-kolliid
     * @exception NullPointerException
     *                om inparameter saknas.
     * @author Stefan Schwartz, Posten IT
     * @version 1.0
     */
    public int getOldDPDCheckSum(String p_strKolliId) {
        int iCheckSum = 0;
        int iIndex = 0;
        int iProduct;
        int iSum = 0;
        int iWeights[] = { 3, 1 }; // Observera att vikterna är annorlunda
        // jämfört med getModulo10CheckSum().

        for (int i = 0; i < p_strKolliId.length(); i++) {
            // Beräkna produkten...
            int iNumber = p_strKolliId.charAt(i) - '0';
            iProduct = iNumber * iWeights[iIndex];

            // ...och ta fram delsumman
            iSum = iSum + iProduct;

            // Ta fram nästa vikt
            iIndex++;
            iIndex = iIndex % iWeights.length;
        }

        iCheckSum = iSum % 10;

        if (iCheckSum != 0) {
            iCheckSum = 10 - iCheckSum;
        }

        // Returnera kontrollsiffra
        return iCheckSum;
    }

    /**
     * Calculate the ISO/IEC 7064 check digit with the defined modulo value. Input: Pointer to the data buffer Return:
     * ISO check digit value
     */
    public char getDPDCheckSum(String p_strId) {
        int ISO7064_MODULO = 36;

        /* tabell för att konvertera ascii-tecken till ISO/IEC 7064 värde */
        int ascii2isoval[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 0, 0, 0,
                0, 0, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33,
                34, 35, 0, 0, 0, 0, 0, 0, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28,
                29, 30, 31, 32, 33, 34, 35, 0, 0, 0, 0, 0 };

        /* tabell för att konvertera ISO/IEC 7064 värde till ascii-tecken */
        char isoval2ascii[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
                'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '*' };

        int modulo;
        int checkDigit;
        int i;

        for (checkDigit = modulo = ISO7064_MODULO, i = 0; i < p_strId.length(); i++) {
            checkDigit += ascii2isoval[(p_strId.charAt(i))];

            if (checkDigit > modulo)
                checkDigit -= modulo;

            checkDigit *= 2;

            if (checkDigit > modulo)
                checkDigit -= modulo + 1;

        }

        checkDigit = modulo + 1 - checkDigit;

        if (checkDigit == modulo)
            checkDigit = 0;

        return (isoval2ascii[checkDigit]);
    }

    /**
     * Kontrollera om samtliga tecken i en sträng är siffror.
     * <p>
     * Metoden <code>isNumeric</code> kontrollerar om samtliga tecken i en sträng är siffror.
     * </p>
     * 
     * @param p_strArg
     *            Sträng som skall kontrolleras
     * @return Flagga som anger om strängen endast innehäller siffror.
     * @exception IllegalArgumentException
     *                om null skickas som argument till metoden.
     * @author Stefan Schwartz, Posten IT
     * @version 1.0
     */
    private boolean isNumeric(String p_strArg) {
        if (p_strArg == null) {
            throw new IllegalArgumentException("Felaktigt inargument");
        }

        int iLenght = p_strArg.length();
        for (int i = 0; i < iLenght; i++) {
            if (!Character.isDigit(p_strArg.charAt(i))) {
                return false;
            }
        }

        return true;
    }

    /**
     * Kontrollera id
     * 
     * @param p_strId
     *            Id
     * @param p_strType
     *            Typ av id
     * @return Flagga som anger om det är ett giltigt id.
     * @author Stefan Schwartz, Posten IT
     * @version 1.0
     */
    public boolean check(String p_strId, String p_strType) {
        return isValid(p_strId, p_strType);
    }

    /**
     * Kontrollera id
     * 
     * @param p_strId
     *            Id
     * @param p_iType
     *            Typ av id
     * @return Flagga som anger om det är ett giltigt id.
     * @author Stefan Schwartz, Posten IT
     * @version 1.0
     */
    public boolean check(String p_strId, int p_iType) {
        return isValid(p_strId, p_iType);
    }

    /**
     * Kontrollera checksumma för id
     * 
     * @param p_strId
     *            Id
     * @param p_strType
     *            Typ av id
     * @return Flagga som anger om det är ett giltigt id.
     * @exception IllegalArgumentException
     *                om det id som anges är null.
     * @author Stefan Schwartz, Posten IT
     * @version 1.0
     */
    public boolean isValid(String p_strId, String p_strType) {
        if (p_strId == null) {
            throw new IllegalArgumentException("Felaktigt id: null");
        }

        String strKey = p_strType.toLowerCase(); // Typ av id
        boolean bValid = false; // Flagga som anger om det är ett giltigt id
        int iType = -1; // Typ av id

        if (m_htType.containsKey(strKey)) {
            iType = m_htType.get(strKey).intValue();
        }

        // Kontrollera om det är ett giltigt id
        bValid = isValid(p_strId, iType);

        return bValid;
    }

    /**
     * Kontrollera checksumma för id
     * 
     * @param p_strId
     *            Id
     * @param p_iType
     *            Typ av id
     * @return Flagga som anger om det är ett giltigt id.
     * @exception IllegalArgumentException
     *                om en det id som skickas till metoden är null eller om en felaktig typ anges.
     * @author Stefan Schwartz, Posten IT
     * @version 1.0
     */
    public boolean isValid(String p_strId, int p_iType) {
        if (p_strId == null) {
            throw new IllegalArgumentException("Felaktigt id: null");
        }

        String strId = p_strId.trim(); // Id
        boolean bValid = false; // Flagga får giltigt id

        // Kontrollera att rätt typ har angivits för id:t
        if (!m_htType.containsValue(new Integer(p_iType))) {
            throw new IllegalArgumentException("Felaktig typ: " + p_iType);
        }

        /*
         * Anropa rätt metod för kontroll av id
         * 
         * Lägg till nya rader här vartefter nya kontrollmetoder tillkommer!
         */
        switch (p_iType) {
        case CUSTOMER_ID:
            bValid = checkCustomerId(strId);
            break;
        case PARCEL_ID:
            bValid = checkParcelId(strId);
            break;
        case SOCIAL_SECURITY_NUMBER:
            bValid = checkSocialSecurityNumber(strId);
            break;
        case OCR_REFERENCE_NUMBER:
            bValid = checkOcrReferenceNumber(strId);
            break;
        }

        return bValid;
    }

    /**
     * Kontrollera kundnummer
     * 
     * @param p_strId
     *            Kundnummer
     * @return Flagga som anger om kundnumret är korrekt
     * @author Stefan Schwartz, Posten IT
     * @version 1.0
     */
    private boolean checkCustomerId(String p_strId) {
        boolean isValid = false; // Flagga som anger om kundnumret är korrekt
        int iCheckSum; // Kontrollsiffra

        // Kontrollera längden på kundnumret
        // Mäste vara 10 tecken - alltid!
        if (p_strId.length() < CUSTOMER_ID_LEN) {
            return false;
        }

        // Beräkna och kontrollera kontrollsiffran
        iCheckSum = p_strId.charAt(CUSTOMER_ID_LEN - 1) - '0';

        if (iCheckSum == getModulo10CheckSum(p_strId.substring(0, CUSTOMER_ID_LEN - 1))) {
            isValid = true;
        }

        return isValid;
    }

    /**
     * Kontrollera kolliid
     * 
     * @param p_strId
     *            Kolliid
     * @return Flagga som anger om det är ett korrekt kolliid
     * @author Stefan Schwartz, Posten IT
     * @version 1.0
     */
    private boolean checkParcelId(String p_strId) {
        int iStart; // Startposition i kolliid:t
        int iCheckSum; // Kontrollsiffra
        String strKolliid; // Kolliid
        boolean isValid = false; // Flagga som anger om kolliid:t är korrekt

        // Ta fram längden på kolliid:t
        int iLength = p_strId.length();

        // Hantera inte UPU
        if (!p_strId.startsWith("JJ")) {
            switch (iLength) {
            case OLD_DPD_PARCEL_ID_LEN: {
                // Ta fram kontrollsiffra
                iCheckSum = p_strId.charAt(OLD_DPD_PARCEL_ID_LEN - 1) - '0';

                // ...och kolliid:t
                strKolliid = p_strId.substring(0, OLD_DPD_PARCEL_ID_LEN - 1);

                // Beräkna och kontrollera kontrollsiffran
                if (isNumeric(strKolliid)) {
                    if (iCheckSum == getOldDPDCheckSum(strKolliid)) {
                        isValid = true;
                    }
                }
            }
                break;

            case PARCEL_ID_LEN: {
                // Ta fram kontrollsiffra
                iCheckSum = p_strId.charAt(PARCEL_ID_LEN - 3) - '0';

                // Börjar kolliid med ett tecken?
                // Ange rätt startposition
                if (Character.isDigit(p_strId.charAt(1))) {
                    iStart = 0;
                } else {
                    iStart = 2;
                }

                // Beräkna och kontrollera kontrollsiffran
                strKolliid = p_strId.substring(iStart, PARCEL_ID_LEN - 3);
                if (isNumeric(strKolliid)) {
                    if (iCheckSum == getModulo11CheckSum(strKolliid)) {
                        isValid = true;
                    }
                }
            }
                break;

            case EAN_PARCEL_ID_LEN: {
                iCheckSum = p_strId.charAt(EAN_PARCEL_ID_LEN - 1) - '0';
                strKolliid = p_strId.substring(0, EAN_PARCEL_ID_LEN - 1);

                // Beräkna och kontrollera kontrollsiffran
                if (isNumeric(strKolliid)) {
                    if (iCheckSum == getEANCheckSum(strKolliid)) {
                        isValid = true;
                    }
                }
            }
                break;

            default:
                break;
            }
        }
        if (iLength == DPD_PARCEL_ID_LEN) {
            // Ta fram kontrolltecken
            char checkTecken = p_strId.charAt(DPD_PARCEL_ID_LEN - 1);

            // ...och kolliid:t
            strKolliid = p_strId.substring(0, DPD_PARCEL_ID_LEN - 1);

            // Beräkna och kontrollera kontrollsiffran
            if (checkTecken == getDPDCheckSum(strKolliid)) {
                isValid = true;
            }
        }

        return isValid;
    }

    /**
     * Kontrollera personnummer
     * <p>
     * Metoden <code>checkSocialSecurityNumber</code> kontrollerar om ett personnummer är korrekt angivet.
     * Kontrollsiffran beräknas på ett personnummer med utseendet YYMMDD-XXXX. Observera att metoden även arbetar med
     * personnummer på formatet YYYYMMDD-XXXX.
     * </p>
     * 
     * @param p_strId
     *            Personnummer
     * @return Flagga som anger om personnumret är korrekt.
     * @author Stefan Schwartz, Posten IT
     * @version 1.0
     */
    private boolean checkSocialSecurityNumber(String p_strId) {
        String strId; // Personnummer
        boolean isValid = false; // Flagga som anger om personnumret är giltigt
        int iCheckSum; // Kontrollsiffra
        int iStart = -1; // Startposition i strängen för beräkning av personnummer

        // Sätt rätt startposition i strängen
        int iLength = p_strId.length();
        if (iLength == SOCIAL_SECURITY_NUMBER_LEN) {
            iStart = 0;
        } else if (iLength == (SOCIAL_SECURITY_NUMBER_LEN + 2)) {
            iStart = 2;
        }

        // Beräkna (och kontrollera) kontrollsiffran
        if (iStart != -1) {
            strId = p_strId.substring(iStart, iLength - 1);
            iCheckSum = p_strId.charAt(iLength - 1) - '0';

            if (isNumeric(strId)) {
                if (iCheckSum == getModulo10CheckSum(strId)) {
                    isValid = true;
                }
            }
        }

        return isValid;
    }

    /**
     * Kontrollera ocrreferens
     * <p>
     * Metoden <code>checkOcrReferenceNumber</code> kontrollerar om en ocrreferens är korrekt angivet.
     * </p>
     * 
     * @param p_strId
     *            ocrreferens
     * @return Flagga som anger om referensen är korrekt.
     * @author Håkan Eriksson, Posten IT
     * @version 1.0
     */
    private boolean checkOcrReferenceNumber(String p_strId) {
        int iCheckSum = p_strId.charAt(p_strId.length() - 1) - '0';
        String ocrReferens = p_strId.substring(0, p_strId.length() - 1);
        boolean isValid = false;

        // Beräkna och kontrollera kontrollsiffran
        if (isNumeric(ocrReferens) && iCheckSum == getModulo10CheckSum(ocrReferens)) {
            isValid = true;
        }
        return isValid;
    }

    /**
     * main() - för testning!
     * 
     * @author Stefan Schwartz, Posten IT
     * @version 1.0
     */
    public static void main(String[] args) {
        CheckSum checksum = CheckSum.getInstance();

        // Lägg till kod här...!

        // DPD-testkollin
        System.out.println(checksum.check("301890000461", CheckSum.PARCEL_ID));
        System.out.println(checksum.check("301890000454", CheckSum.PARCEL_ID));
        System.out.println(checksum.check("301890000508", CheckSum.PARCEL_ID));
        System.out.println(checksum.check("301890000515", CheckSum.PARCEL_ID));
    }

}
