package se.posten.loab.lisp.linda.export.ftp;

import java.io.File;
import java.io.IOException;
import java.net.SocketException;

import javax.xml.bind.JAXBException;

import org.sculptor.framework.errorhandling.ApplicationException;

import se.posten.loab.lisp.common.linda.LogCodes;

public class FTPException extends ApplicationException {

    private static final long serialVersionUID = -7231457664719075111L;

    private FTPException(LogCodes code, String... parameters) {
        super(code.name(), code.getMsg());
        setMessageParameters(parameters);
    }

    private FTPException(LogCodes errorCode, Throwable cause, String... parameters) {
        super(errorCode.name(), errorCode.getMsg(), cause);
        setMessageParameters(parameters);
    }

    public static final FTPException couldNotCreateTempFile(File f) {
        return new FTPException(LogCodes.LIN_001, f.getAbsolutePath());
    }

    public static final FTPException couldNotCreateTempDirectory(File dir) {
        return new FTPException(LogCodes.LIN_002, dir.getAbsolutePath());
    }

    public static final FTPException couldNotCreateTempFile(File f, IOException e) {
        return new FTPException(LogCodes.LIN_001, e, f.getAbsolutePath());
    }

    public static final FTPException couldNotUploadFile(Exception e) {
        return new FTPException(LogCodes.LIN_100, e);
    }

    public static final FTPException couldNotUploadFile() {
        return new FTPException(LogCodes.LIN_100);
    }

    public static final FTPException couldNotRenameRemoteFile(String from, String to) {
        return new FTPException(LogCodes.LIN_105, from, to);
    }

    public static final FTPException invalidLogin(String user, String serverIp, int port) {
        return new FTPException(LogCodes.LIN_101, user, serverIp, String.valueOf(port));
    }

    public static final FTPException couldNotConnectToServer(String serverIp, int port, SocketException e) {
        return new FTPException(LogCodes.LIN_102, e, serverIp, String.valueOf(port));
    }

    public static final FTPException couldNotCommunicateWithFTPServer(String serverIp, int port, IOException e) {
        return new FTPException(LogCodes.LIN_103, e, serverIp, String.valueOf(port));
    }

    public static final FTPException couldNotSwitchToRemoteDirectory(String remoteDir) {
        return new FTPException(LogCodes.LIN_104, remoteDir);
    }

    public static final FTPException couldNotCreateMarshaller(JAXBException e) {
        return new FTPException(LogCodes.LIN_150, e);
    }

    public static final FTPException couldNotMarshalXML(JAXBException e) {
        return new FTPException(LogCodes.LIN_151, e);
    }

}
