package se.posten.loab.lisp.common.validation.impl;

import se.posten.loab.lisp.common.validation.Validator;

public class LengthValidator implements Validator<String> {

	private int min;
	private int max;
	
	public LengthValidator(int min, int max) {
		this.min = min;
		this.max = max;
	}
	
	public boolean validate(String target) {
		if(target == null) return false;
		return target.length() >= min && target.length() <= max;
	}	
}
