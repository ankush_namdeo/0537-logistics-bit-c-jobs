/*
 * FixedField.java
 *
 * Created on den 26 april 2001, 20:59
 */

package se.posten.loab.lisp.common.lpf.filter;
import java.text.DecimalFormat;
import java.text.ParseException;


/** Filter that converts a string to a Number
 *
 * @author Christer Wikman, DevCode
 */
public class NumberFilter implements Filter {

    private final DecimalFormat format;

    /** Create a new filter a decimal format pattern
     *
     * @param pattern Pattern, e.g.
     * @see DecimalFormat
     */
    public NumberFilter( String pattern  ) {
        format = new DecimalFormat( pattern );
    }

    public boolean filter(String line, Enum<?> field, FieldValue storage) throws IllegalArgumentException {
        String s=null;
        try {
            s = storage.get( field ).toString();
            Number dst = format.parse(s);
            storage.put( field, dst );
            return true;
        } catch ( ParseException e ) {
            String msg = String.format("Enum %s : Failed to convert %s to Number due to: %s", field, s, e);
            throw new IllegalArgumentException(msg);
        }
    }
}
