package se.posten.loab.lisp.common.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Represents a country code. Country codes are identified by their ISO 3166-1 codes.
 * See the <a href="http://en.wikipedia.org/wiki/ISO_3166-1_alpha-3">
 * ISO 3166-1 alpha-3</a> for more information, including a table of
 * country codes.
 * <p>
 * The class is designed so that there's never more than one
 * <code>CountryCode</code> instance for any given country. Therefore, there's
 * no public constructor. You obtain a <code>CountryCode</code> instance using
 * any of the <code>getInstanceXxx</code> methods.
 *
 * @author Christer Wikman, DevCode
 *
 */
public class CountryCode implements Serializable  {
    private static final Log log = LogFactory.getLog(CountryCode.class);

    private static final long serialVersionUID = -5383638901325946996L;
    private static final String FILE = "country_codes.txt";

    private String name = null;
    private String twoAlpha = null;
    private String threeAlpha = null;
    private String threeDigit = null;


    private CountryCode() {
    }

    /** Returns a 2-alpha code representation of the country code. */
    public String getTwoAlphaCode() {
        return twoAlpha;
    }

    /** Returns a 3-alpha code representation of the country code. */
    public String getThreeAlphaCode() {
        return threeAlpha;
    }

    /** Returns a 3-digit code representation of the country code. */
    public String getThreeDigitCode() {
        return threeDigit;
    }

    /** Returns a "name" representation of the country code. */
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return threeAlpha + " - " + name;
    }

    private CountryCode(String name, String twoAlpha, String threeAlpha, String threeDigit) {
        this.name = name;
        this.twoAlpha = twoAlpha;
        this.threeAlpha = threeAlpha;
        this.threeDigit = threeDigit;
    }

    private static Map<String, CountryCode> twoAlpaIsoToCountry = new HashMap<String, CountryCode>();
    private static Map<String, CountryCode> threeAlphaIsoToCountry = new HashMap<String, CountryCode>();
    private static Map<String, CountryCode> threeDigitIsoToCountry = new HashMap<String, CountryCode>();

    static {
    	initCountryCode();
    }

    /** Predefined country code for Sweden. */
    public static CountryCode SWE = getInstance("SWE");
    /** Predefined country code for Finland. */
    public static CountryCode FIN = getInstance("FIN");
    /** Predefined country code for Denmark. */
    public static CountryCode DNK = getInstance("DNK");
    /** Predefined country code for Norway. */
    public static CountryCode NOR = getInstance("NOR");

    private static void initCountryCode() {
    	BufferedReader is = null;
    	try {
	    	is = new BufferedReader(new InputStreamReader(CountryCode.class.getResourceAsStream(FILE)));
	    	String data = null;
	    	while ((data = is.readLine()) != null) {
	    		try {
	    		    CountryCode c = new CountryCode();
	    		    c.name = StringUtils.trim(data.substring(0,48));
		    		c.twoAlpha = data.substring(48, 50);
		    		if (!StringUtils.isAsciiPrintable(c.twoAlpha)) {
		    		    throw new IllegalArgumentException("Invalid 2-alpha: "+c.twoAlpha);
		    		}
		    		c.threeAlpha = data.substring(56,59);
		    		if (!StringUtils.isAsciiPrintable(c.threeAlpha)) {
		    		    throw new IllegalArgumentException("Invalid 3-alpha: "+c.threeAlpha);
		    		}
		    		c.threeDigit = data.substring(64,67);
		    		if (!StringUtils.isNumeric(c.threeDigit)) {
		    		    throw new IllegalArgumentException("Invalid 3-digit: "+c.threeDigit);
		    		}
		    		twoAlpaIsoToCountry.put(c.twoAlpha, c);
		    		threeAlphaIsoToCountry.put(c.threeAlpha, c);
		    		threeDigitIsoToCountry.put(c.threeDigit, c);
	    		} catch (Exception e) {
	    			log.error("-initCountryCode(): Failed to parse "+data+" due to: "+e.getMessage());
	    		}
	    	}
    	} catch (Exception e) {
    		log.error("-initCountryCode: Faile to read "+FILE,e);
    	} finally {
    		try {if (null != is) is.close();} catch (Exception ignore) {}
    	}
    }

    /**
     * Returns the <code>CountryCode</code> instance for the given ISO 2 alpha code
     *
     * @param isoTwoAlphaCode the ISO 3166-1 code of the country code
     * @return the <code>CountryCode</code> instance for the given code or null
     * if not found.
     */
    public static CountryCode getInstanceFromIsoTwoAlpha(String isoTwoAlphaCode) {
        if (null == isoTwoAlphaCode) {
            return null;
        }
        CountryCode country = twoAlpaIsoToCountry.get(isoTwoAlphaCode.toUpperCase());
        return country;
    }

    /**
     * Returns the <code>CountryCode</code> instance for the given ISO 3 digit code
     *
     * @param isoThreeDigitCode the ISO 3166-1 code of the country code
     * @return the <code>CountryCode</code> instance for the given code or null
     * if not found.
     */
    public static CountryCode getInstanceFromIsoThreeDigit(String isoThreeDigitCode) {
        if (null == isoThreeDigitCode) {
            return null;
        }
        CountryCode country = threeDigitIsoToCountry.get(isoThreeDigitCode);
        return country;
    }

    /**
     * Returns the <code>CountryCode</code> instance for the given ISO 3 alpha code
     *
     * @param isoThreeDigitCode the ISO 3166-1 code of the country code
     * @return the <code>CountryCode</code> instance for the given code or null
     * if not found.
     */
    public static CountryCode getInstanceFromIsoThreeAlpha(String isoThreeAlphaCode) {
        if (null == isoThreeAlphaCode) {
            return null;
        }
        CountryCode country = threeAlphaIsoToCountry.get(isoThreeAlphaCode);
        return country;
    }

    /**
     * Returns the <code>CountryCode</code> instance for any given
     * 2-alpha, 3-alpha or 3-digit ISO 3166-1 code.
     *
     * @param code any 2-alpha, 3-alpha, or 3-digit ISO 3166-1 code of the country code
     * @return the <code>CountryCode</code> instance for the given code or null
     * if not found.
     */
    public static CountryCode getInstance(String code) {
        if (null == code) {
            return null;
        }
        if (StringUtils.isNumeric(code)) {
            return getInstanceFromIsoThreeDigit(code);
        }
        if (StringUtils.length(code) == 3) {
            return getInstanceFromIsoThreeAlpha(code);
        }
        return getInstanceFromIsoTwoAlpha(code);
    }

    /**
     * Returns the 2-alpha code for any given 2-alpha, 3-alpha or 3-digit ISO 3166-1 code.
     *
     * @param code any 2-alpha, 3-alpha, or 3-digit ISO 3166-1 code of the country code
     * @return the 2-alpha code for the given code or null if not found.
     */
    public static String toTwoAlpha(String code) {
        CountryCode countryCode = CountryCode.getInstance(code);
        return null != countryCode ? countryCode.getTwoAlphaCode() : null;
    }

    /**
     * Returns the 3-alpha code for any given 2-alpha, 3-alpha or 3-digit ISO 3166-1 code.
     *
     * @param code any 2-alpha, 3-alpha, or 3-digit ISO 3166-1 code of the country code
     * @return the 3-alpha code for the given code or null if not found.
     */
    public static String toThreeAlpha(String code) {
        CountryCode countryCode = CountryCode.getInstance(code);
        return null != countryCode ? countryCode.getThreeAlphaCode() : null;
    }

    /**
     * Returns the 3-digit code for any given 2-alpha, 3-alpha or 3-digit ISO 3166-1 code.
     *
     * @param code any 2-alpha, 3-alpha, or 3-digit ISO 3166-1 code of the country code
     * @return the 3-digit code for the given code or null if not found.
     */
    public static String toThreeDigit(String code) {
        CountryCode countryCode = CountryCode.getInstance(code);
        return null != countryCode ? countryCode.getThreeDigitCode() : null;
    }

}
