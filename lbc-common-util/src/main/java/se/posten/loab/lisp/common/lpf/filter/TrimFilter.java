package se.posten.loab.lisp.common.lpf.filter;
/** A string trim filter.
 * This filter trims blank spaces in each end of the targeted field.
 * 
 * @author Christer Wikman, DevCode
 */
public class TrimFilter implements Filter {

    /** Creates new TrimFilter */
    public TrimFilter() {
    }

    /**
     * @param line
     * @param field
     * @param storage
     * @throws IllegalArgumentException
     * @return  */
    public boolean filter(String line, Enum<?> field, FieldValue storage) throws IllegalArgumentException {
        String val = (String)storage.get( field );
        if( val != null ) {
            val = val.trim();
            storage.put(field, val );
        }
        return true;
    }
}
