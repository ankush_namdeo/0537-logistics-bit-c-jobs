package se.posten.loab.lisp.common.lpf;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import se.posten.loab.lisp.common.lpf.filter.DelimiterFilter;
import se.posten.loab.lisp.common.lpf.filter.FieldValue;
import se.posten.loab.lisp.common.lpf.filter.GroupFilter;

/**
 * Generic CSV parser.
 *
 * @author Christer Wikman, DevCode
 *
 */
public class CsvParser {
    private static Log log = LogFactory.getLog(CsvParser.class.getClass());

    private int noRowsToSkip = 0;
    private String encoding = null;
    private String delimeter = ";";
    private String fileStartTag = null;
    private String fileEndTag = null;
    private Map<String,List<String>> filter;
    private final DynamicHeader fieldDefs = new DynamicHeader();
    private final StringBuilder errorMsg = new StringBuilder();
    private final List<FieldValue> records = new ArrayList<FieldValue>();
    private GroupFilter fields = null;
    private boolean columnHeader = false;

    public DynamicHeader getDynamicFieldDef() {
        return fieldDefs;
    }

    public int getNoRowsToSkip() {
        return noRowsToSkip;
    }

    public void setNoRowsToSkip(int noRowsToSkip) {
        this.noRowsToSkip = noRowsToSkip;
    }

    public void setFields(GroupFilter fields) {
        this.fields = fields;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public void setDelimeter(String delimeter) {
        this.delimeter = delimeter;
    }

    public String getDelimeter() {
        return delimeter;
    }

    public void setFileStartTag(String fileStartTag) {
        this.fileStartTag = fileStartTag;
    }

    public String getFileStartTag() {
        return fileStartTag;
    }

    public void setFileEndTag(String fileEndTag) {
        this.fileEndTag = fileEndTag;
    }

    public String getFileEndTag() {
        return fileEndTag;
    }

    public void setColumnHeader(boolean columnHeader) {
        this.columnHeader = columnHeader;
    }

    public boolean isColumnHeader() {
        return columnHeader;
    }

    protected static class DynamicHeader implements DelimiterFilter.IHeaderDef {

        private String[] headers = new String[0];

        public void setHeaders(String[] headers) {
            this.headers = headers;
        }

        public int getIndex(String header) {
            for (int i = 0; i < headers.length; i++) {
                if (headers[i].equals(header)) {
                    return i;
                }
            }
            return -1;
        }
    }

    protected String[] split(String s) {
        if (null == s) { return new String[0]; }
        return s.split(delimeter);
    }

    protected void assertStartsWith(String string, String prefix) {
        if (!string.startsWith(prefix)) {
            throw new IllegalStateException(String.format("Expecting %s and not %s", prefix, string));
        }
    }

    protected void parseFileHeader(LineNumberReader reader) throws IOException {
        String l = reader.readLine();
        if (null != fileStartTag) {
            assertStartsWith(l, fileStartTag);
        }
    }

    public List<FieldValue> parse(File file) throws IOException {
        InputStream is = null;
        try {
            is = new FileInputStream(file);
            return parse(is);
        }finally {
            if (null != is) { try { is.close(); } catch (Exception ignore) {} }
        }
    }

    public List<FieldValue> parse(InputStream is) throws IOException {
        LineNumberReader reader = null;
        if (null != encoding) {
            reader = new LineNumberReader(new InputStreamReader(is,encoding));
        } else {
            reader = new LineNumberReader(new InputStreamReader(is));
        }

        List<FieldValue> res = parse(reader);
        // Return null if there is no data
        if (null != res && res.isEmpty()) {
            res = null;
        }
        return res;
    }

    protected boolean filterRow(FieldValue row) {
        if (null == filter) { return false; }
        for (Entry<String,List<String>> f: filter.entrySet()) {
            String rowValue = row.getAsString(f.getKey());
            if (null == rowValue) {
                rowValue="null";
            }
            for (String val : f.getValue()) {
                if (val.equals(rowValue)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void setFilter(String allFilters) {
        if (null == allFilters) {
            this.filter = null;
            return;
        }
        try {
            this.filter = new HashMap<String,List<String>>();
            String[] filters = allFilters.split(",");
            for (String f : filters) {
                String[] val = f.split("=");
                if (val.length != 2) { continue; }
                String key = val[0].trim();
                List<String> values = filter.get(key);
                if (null == values) {
                    values = new ArrayList<String>();
                    filter.put(key,values);
                }
                values.add(val[1].trim());
            }
        } catch (Exception e) {
            String msg = "Invalid filter: "+allFilters;
            this.filter = null;
            throw new IllegalArgumentException(msg,e);
        }
    }

    protected boolean isEndTag(String l) {
        return null != l && null != getFileEndTag() && l.startsWith(getFileEndTag());
    }

    private boolean parseDataRows(LineNumberReader reader) throws IOException {
        String l = null;
        boolean res = true;
        int errCount = 0;
        while ((l = reader.readLine()) != null) {
            if (isEndTag(l)) {
                break;
            }
            try {
                FieldValue data = new FieldValue();
                fields.filter(l, data);
                if (!filterRow(data)) {
                    records.add(data);
                } else if (log.isDebugEnabled()) {
                    log.debug("-parseDataRows(): Filtering row "+reader.getLineNumber());
                }
            } catch (Exception e) {
                errorMsg.append(String.format("Failed to parse line: %s => %s, due to: %s\n", reader.getLineNumber(), l, e));
                errCount++;
                if (errCount > 1000) {
                    break;
                }
                res = false;
            }
        }
        if (!res) {
            throw new IllegalStateException(errorMsg.toString());
        }
        return res;
    }

    protected void skipRows(LineNumberReader reader) throws IOException {
        for (int i=0; i<noRowsToSkip; i++) {
            reader.readLine();
        }
    }

    protected List<FieldValue> parse(LineNumberReader reader) throws IOException {
        skipRows(reader);
        if (isColumnHeader()) {
            parseDefHeader(reader,fieldDefs);
        }
        if (!parseDataRows(reader)) {
            throw new IllegalStateException("Failed to parse following rows (might not be all):\n"+errorMsg.toString());
        }
        return records;
    }

    protected void parseDefHeader(LineNumberReader reader, DynamicHeader header) throws IOException {
        String l = reader.readLine();
        String[] defs = split(l);
        header.setHeaders(defs);
    }
}
