/*
 * Based on code from the following project:
 * 
 * Copyright 2007 The Fornax Project Team, including the original 
 * author or authors.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.posten.loab.lisp.common.log;

import org.sculptor.framework.context.ServiceContext;
import org.sculptor.framework.context.ServiceContextStore;
import org.sculptor.framework.errorhandling.ApplicationException;
import org.sculptor.framework.errorhandling.SystemException;

/**
 * This class is an extension of org.fornax.cartridges.sculptor.framework.errorhandling.LogMessage.
 * 
 * It adds capabilities to log info messages without error code or exception. It also makes it possible to log with only
 * session-id instead of dumping the full ServiceContext.
 * 
 * This class is intended to be used as parameter when logging. The purpose is to format (toString) the log message,
 * which consists of free text technical message, optional error code and data from ServiceContext. Logs belonging to
 * the same request can be tied together through session-id.
 * 
 * @author stca708
 */
public class LogMessage {
    private String errorCode;
    private final String message;
    private final ServiceContext serviceContext;
    private boolean logFullContext = true;

    /**
     * @param errorCode
     *            Well defined error code for the error type.
     * @param message
     *            Technical message. Used for debugging purpose, not intended for
     * @param serviceContext
     *            Contains user information etc.
     */
    public LogMessage(ServiceContext serviceContext, String errorCode, String message) {
        this.serviceContext = serviceContext;
        this.errorCode = errorCode;
        this.message = message;
    }

    /**
     * ServiceContext will be picked from org.fornax.cartridges.sculptor.framework.errorhandling.ServiceContextStore.
     * 
     * @see #LogMessage(ServiceContext, String, String)
     */
    public LogMessage(String errorCode, String message) {
        this(ServiceContextStore.get(), errorCode, message);
    }

    public LogMessage(ServiceContext serviceContext, SystemException e) {
        this(serviceContext, e.getErrorCode(), e.getMessage());
    }

    /**
     * ServiceContext will be picked from org.fornax.cartridges.sculptor.framework.errorhandling.ServiceContextStore.
     * 
     * @see #LogMessage(ServiceContext, SystemException)
     */
    public LogMessage(SystemException e) {
        this(ServiceContextStore.get(), e.getErrorCode(), e.getMessage());
    }

    public LogMessage(ServiceContext serviceContext, ApplicationException e) {
        this(serviceContext, e.getErrorCode(), e.getMessage());
    }

    /**
     * ServiceContext will be picked from org.fornax.cartridges.sculptor.framework.errorhandling.ServiceContextStore.
     * 
     * @see #LogMessage(ServiceContext, ApplicationException)
     */
    public LogMessage(ApplicationException e) {
        this(ServiceContextStore.get(), e.getErrorCode(), e.getMessage());
    }

    /**
     * Constructor for debug/info log message, when there is no error code or exception.
     * 
     * @param message
     *            Technical message. Used for debugging purpose.
     * @param serviceContext
     *            Contains user information etc.
     */
    public LogMessage(ServiceContext serviceContext, String message) {
        this(serviceContext, message, false);
    }

    /**
     * ServiceContext will be picked from org.fornax.cartridges.sculptor.framework.errorhandling.ServiceContextStore.
     * 
     * @see #LogMessage(ServiceContext, String)
     */
    public LogMessage(String message) {
        this(ServiceContextStore.get(), message);
    }

    /**
     * @param message
     *            Technical message. Used for debugging purpose.
     * @param serviceContext
     *            Contains user information etc.
     * @param logFullContext
     *            Set to true if all properties of the ServiceContext shall be logged.
     */
    public LogMessage(ServiceContext serviceContext, String message, boolean logFullContext) {
        this.serviceContext = serviceContext;
        this.message = message;
        this.logFullContext = logFullContext;
    }

    /**
     * ServiceContext will be picked from org.fornax.cartridges.sculptor.framework.errorhandling.ServiceContextStore.
     * 
     * @see #LogMessage(ServiceContext, String, boolean)
     */
    public LogMessage(String message, boolean logFullContext) {
        this(ServiceContextStore.get(), message, logFullContext);
    }

    public ServiceContext getServiceContext() {
        return serviceContext;
    }

    /**
     * Well defined error code for the error type.
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Technical message. Used for debugging purpose, not intended for end users.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Formats the log message.
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        if (errorCode != null) {
            sb.append("[").append(errorCode).append("] ");
        }
        if (serviceContext != null) {
            if (logFullContext) {
                sb.append(serviceContext).append(" ");
            } else {
                // Always log session-id so request/session can be tracked.
                sb.append("(session-id=").append(serviceContext.getSessionId()).append(")");
            }
        }
        sb.append(" : ").append(message);
        return sb.toString();
    }
}
