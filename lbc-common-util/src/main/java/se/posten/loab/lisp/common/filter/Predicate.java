package se.posten.loab.lisp.common.filter;

public interface Predicate<T> {
    boolean apply(T type);
}
