package se.posten.loab.lisp.common.config;

import java.util.Properties;

/**
 * This is an extension of ConfigurationManager that can be used for deploying the ConfigurationManager as MBean in
 * JBoss.
 * 
 * @author stca708
 */
public interface ConfigurationManagerMBean extends ConfigurationManager {

    /**
     * Lifecycle method called by JBoss if used as MBean Service.
     * 
     * @throws Exception
     */
    public void start() throws Exception;

    /**
     * Returns general info and current status about the ConfigurationManager. What is returned is dependent on the
     * implementation.
     * 
     * @return Properties with key-value pairs with info about the Configuration Manager.
     */
    public Properties getInfoAboutConfigurationManager();
}
