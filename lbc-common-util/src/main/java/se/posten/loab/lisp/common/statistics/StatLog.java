package se.posten.loab.lisp.common.statistics;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sculptor.framework.context.ServiceContext;
import org.sculptor.framework.context.ServiceContextStore;

import se.posten.loab.lisp.common.log.LogMessage;

public class StatLog {
    private static final Log log = LogFactory.getLog(StatLog.class);
    private static String host = System.getProperty("jboss.bind.address", "localhost");

    private static final String PHASE_START = "START";
    private static final String PHASE_END = "END";

    public static void log(String operation, String message) {
        log(operation, message, null);
    }

    public static void logStart(String operation, String message) {
        log(operation, message, PHASE_START);
    }

    public static void logEnd(String operation, String message) {
        log(operation, message, PHASE_END);
    }

    private static void log(String operation, String message, String phase) {
        if (!isLoggingEnabled()) {
            return;
        }

        String phase2 = (phase == null ? "" : phase);
        String code = "STAT_" + phase2;

        ServiceContext ctx = ServiceContextStore.get();
        if (ctx != null) {
            ctx.setProperty("host", host);
            ctx.setProperty("operation", operation);
        }

        LogMessage logMessage = new LogMessage(code, message + " " + phase2);
        log.info(logMessage.toString());
    }

    public static boolean isLoggingEnabled() {
        return log.isInfoEnabled();
    }

}
