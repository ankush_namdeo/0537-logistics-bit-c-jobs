package se.posten.loab.lisp.common.lpf.filter;

/**
 * @author Christer Wikman, DevCode
 */
public class SeparatedField implements Filter {
    private char separator = ' ';
    private int position = 0;

    /** Creates new FixedField
     * @param start The start of the field
     * @param len The length of the field
     */
    public SeparatedField(char separator, int position) {
        this.separator = separator;
        this.position = position;
    }

    public boolean filter(String line, Enum<?> field, FieldValue storage) throws IllegalArgumentException {

        int startchar = -1;
        int endchar = 0;

        String l = line.toString();
        for( int i = 0; i < position; i++ ) {
            startchar = l.indexOf( separator, startchar + 1 );
            if( startchar == -1 )
                throw new IllegalArgumentException("Position " + position + " doesn't exist!" );
        }

        endchar = l.indexOf( separator, startchar + 1 );
        if( endchar == -1 )
            endchar = l.length();


        storage.put( field, l.substring( startchar + 1, endchar ) );
        return true;
    }
}
