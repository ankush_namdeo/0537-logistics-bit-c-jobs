package se.posten.loab.lisp.common.cronjob;

import java.net.URL;
import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.DefaultConfigurationBuilder;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.StatefulJob;
import org.quartz.Trigger;

/**
 * Base class for quarts cron jobs. Cron expression is read from configuration
 * file define in /META-INF/cronJobConfig.xml Subclass implements the actual
 * work in doExecute method.
 * 
 * @deprecated use lisp-job instead
 * 
 */
@Deprecated
public abstract class TimerJob implements StatefulJob {
    public static final String CRON_EXPRESSION_PROPERTY_NAME = "cron.expression";

    private Log logger = LogFactory.getLog(getClass());

    public void execute(JobExecutionContext context) throws JobExecutionException {

        String jobName = jobName();
        if (!StringUtils.isEmpty(jobName)) {
            logger.info(jobName + " start");
        }

        try {
            long startTime = System.currentTimeMillis();

            doExecute();

            if (!StringUtils.isEmpty(jobName)) {
                logger.info(jobName() + " end. It took: " + (System.currentTimeMillis() - startTime) + " ms");
            }
        } finally {
            // Read properties from file
            String propertiesFile = propertiesFile();
            String cronExpression = getCronExpressionFromPropertiesFile(propertiesFile);
            if (!StringUtils.isEmpty(cronExpression)) {
                reschedule(context, cronExpression);
            }
        }

    }

    protected String jobName() {
        return getClass().getSimpleName();
    }

    protected String propertiesFile() {
        return "/META-INF/cronJobConfig.xml";
    }

    /**
     * Subclass implements the actual work
     * 
     */
    protected abstract void doExecute() throws JobExecutionException;

    protected boolean reschedule(JobExecutionContext jobExecutionContext, String cronExpression) {
        Scheduler scheduler = jobExecutionContext.getScheduler();
        JobDetail jobDetail = jobExecutionContext.getJobDetail();
        Trigger trigger = jobExecutionContext.getTrigger();
        CronTrigger cronTrigger = (CronTrigger) trigger;

        if (!cronExpression.equals(cronTrigger.getCronExpression())) {
            logger.info("****** Will change cron expression for " + jobName() + " : '" + cronExpression);

            logger.debug("Before change");
            listJobs(scheduler);

            try {
                cronTrigger.setCronExpression(cronExpression);

                Date fireTimeDate = jobExecutionContext.getFireTime();
                logger.debug("****** End time for trigger: '" + fireTimeDate + "'.");
                cronTrigger.setEndTime(fireTimeDate);

                String jobName = jobDetail.getName();
                String jobGroup = jobDetail.getGroup();
                scheduler.deleteJob(jobName, jobGroup);

                // Create new trigger
                logger.debug("****** Create new cron trigger with expression: '" + cronExpression + ".");
                cronTrigger = new CronTrigger(cronTrigger.getName(), cronTrigger.getGroup(), cronExpression);

                Date date = scheduler.scheduleJob(jobDetail, cronTrigger);
                logger.debug("****** Scheduled job (date = '" + date + "').");

                logger.debug("After change");
                listJobs(scheduler);
            } catch (ParseException e) {
                logger.warn("Wrong cron expression: '" + cronExpression + "'.");
                return false;
            } catch (SchedulerException e) {
                logger.warn("Couldn't schedule jobs with cron expression: '" + cronExpression + "'.");
                return false;
            }

        }
        return true;

    }

    protected String getCronExpressionFromPropertiesFile(String propertiesFile) {
        try {
            Properties properties = getPropertiesFromFile(propertiesFile);
            if (properties.getProperty(CRON_EXPRESSION_PROPERTY_NAME) != null
                    && properties.getProperty(CRON_EXPRESSION_PROPERTY_NAME).length() > 0) {
                return properties.getProperty(CRON_EXPRESSION_PROPERTY_NAME);
            }
        } catch (RuntimeException e) {
            logger.error("Problem reading properties from: " + propertiesFile, e);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    protected Properties getPropertiesFromFile(String resource) {
        if (logger.isDebugEnabled()) {
            logger.debug("Reading properties file: " + resource);
        }

        Configuration config = loadConfiguration(resource);

        Properties properties = new Properties();
        for (Iterator iter = config.getKeys(); iter.hasNext();) {
            String key = (String) iter.next();
            String value = config.getString(key);
            properties.setProperty(key, value);

            if (logger.isDebugEnabled()) {
                logger.debug("key = " + key + " value = " + value);
            }
        }

        return properties;
    }

    protected Configuration loadConfiguration(String resource) {
        try {
            URL configURL = getClass().getResource(resource);
            DefaultConfigurationBuilder builder = new DefaultConfigurationBuilder(configURL);
            // Ignore any configuration errors
            builder.clearErrorListeners();
            return builder.getConfiguration();
        } catch (ConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    protected void listJobs(Scheduler scheduler) {
        if (!logger.isDebugEnabled()) {
            return;
        }
        try {
            String[] jobGroupNames = scheduler.getJobGroupNames();
            for (String groupName : jobGroupNames) {

                String[] jobNames = scheduler.getJobNames(groupName);

                for (String jobName : jobNames) {
                    JobDetail jobDetail = scheduler.getJobDetail(jobName, groupName);

                    logger.debug("groupName = " + groupName + " jobName = " + jobName + " jobDetail = " + jobDetail);
                }
            }

            String[] triggerGroupNames = scheduler.getTriggerGroupNames();
            for (String triggerGroupName : triggerGroupNames) {
                String[] triggerNames = scheduler.getTriggerNames(triggerGroupName);

                for (String triggerName : triggerNames) {
                    Trigger trigger = scheduler.getTrigger(triggerName, triggerGroupName);

                    logger.debug("triggerGroupName = " + triggerGroupName + " triggerName = " + triggerName
                            + " trigger = " + trigger);
                }
            }

        } catch (SchedulerException e) {
            logger.error(e.getMessage(), e);
        }

    }

}
