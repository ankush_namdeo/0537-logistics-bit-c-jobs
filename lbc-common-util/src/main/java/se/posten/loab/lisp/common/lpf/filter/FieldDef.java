package se.posten.loab.lisp.common.lpf.filter;

public class FieldDef implements GroupFilter {

    private Enum<? extends Enum<?>> name = null;
    private Filter[] filters = null;

    public <ENUM extends Enum<?>> FieldDef(ENUM name, Filter... filters) {
        this.name = name;
        this.filters = filters;
    }

    public Enum<? extends Enum<?>> getName() {
        return name;
    }

    public boolean filter(String line, FieldValue storage) {
        for (Filter f : filters) {
            if (!f.filter(line,name,storage)) {
                return false;
            }
        }
        return true;
    }

}
