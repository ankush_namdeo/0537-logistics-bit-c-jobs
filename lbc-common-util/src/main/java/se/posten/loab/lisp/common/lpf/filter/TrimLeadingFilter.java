package se.posten.loab.lisp.common.lpf.filter;

import org.apache.commons.lang.StringUtils;

/** Trim leading characters. Filter that trims leading characters
 *
 * @see StringUtils.stripLeading
 *
 * @author Christer Wikman, DevCode
 */
public class TrimLeadingFilter implements Filter {

    private final String stripChars;
    /** Creates new TrimFilter */
    public TrimLeadingFilter(String stripChars) {
        this.stripChars = stripChars;
    }

    public boolean filter(String line, Enum<?> field, FieldValue storage) throws IllegalArgumentException {
        String val = (String)storage.get( field );
        if( val != null ) {
            val = StringUtils.stripStart(val, stripChars);
            storage.put(field, val );
        }
        return true;
    }
}
