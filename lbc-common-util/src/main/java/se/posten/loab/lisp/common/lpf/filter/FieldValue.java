package se.posten.loab.lisp.common.lpf.filter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

public class FieldValue {

    private final Map<Enum<?>,Object> enumToValue = new HashMap<Enum<?>,Object>();
    private final Map<String,Enum<?>> stringToEnum = new HashMap<String,Enum<?>>();

    public <ENUM extends Enum<?>> void put(ENUM name, Object value) {
        enumToValue.put(name, value);
        stringToEnum.put(name.name(), name);
    }

    public Object get(Enum<?> name) {
        return enumToValue.get(name);
    }

    public String getAsString(String name) {
        Enum<?> e = stringToEnum.get(name);
        if (null == e) { return null; }
        Object o = enumToValue.get(e);
        return null != o ? o.toString() : null;
    }

    @SuppressWarnings("unchecked")
    public <T> T get(Class<T> type, Enum<?> name) {
        Object v = get(name);
        try {
            type.cast(v);
            return (T) v;
        } catch (ClassCastException e) {
            String msg = String.format("Enum %s : Cannot cast '%s' : %s to %s", name, v, (null != v) ? v.getClass() : "null", type);
            throw new IllegalStateException(msg);
        }
    }

    public boolean isNull(Enum<?> name) {
        return null == get(name);
    }

    public boolean isEmpty(Enum<?> name) {
        return StringUtils.isEmpty(getString(name));
    }

    /** Checks if there is an enumeration with the same string value as the enumeration. */
    public boolean equals(Enum<?> name) {
        Object o = get(name);
        String v = (null != o) ? o.toString() : null;
        return name.name().equals(v);
    }

    /** Get boolean value.
     * @param name Field
     * @return Boolean value, null returns false
     */
    public boolean getBoolean(Enum<?> name) {
        Boolean res = get(Boolean.class,name);
        return null != res ? res : false;
    }

    public Date getDate(Enum<?> name) {
        return get(Date.class,name);
    }

    public String getString(Enum<?> name) {
        return StringUtils.trimToNull(get(String.class, name));
    }

    public String getAsString(Enum<?> name) {
        Object o = get(name);
        return StringUtils.trimToNull(null != o ? o.toString() : null);
    }

    public Integer getInt(Enum<?> name) {
        return get(Integer.class, name);
    }

    public BigDecimal getBigDecimal(Enum<?> name) {
        return get(BigDecimal.class, name);
    }

}
