package se.posten.loab.lisp.common.validation.impl;

import java.util.ArrayList;
import java.util.List;

import se.posten.loab.lisp.common.validation.TrackingInformationValidator;
import se.posten.loab.lisp.common.validation.Validator;

public class TrackingInformationValidatorImpl implements TrackingInformationValidator {

    // TODO: error message? one generic or one/several specific?
    // should validators return error message(s) and/or boolean?

    // TODO: these are not complete

    private static Validator<String> itemValidator = getItemValidator();
    private static Validator<String> shipmentValidator = getShipmentValidator();
    private static Validator<String> referenceValidator = getReferenceValidator();
    private static Validator<String> phoneValidator = getPhoneValidator();
    private static Validator<String> customerNumberValidator = getCustomerNumberValidator();
    private static Validator<String> notificationCodeValidator = getNotificationCodeValidator();

    public TrackingInformationValidatorImpl() {
    }

    public boolean validateItemId(String itemId) {
        return itemValidator.validate(itemId);
    }

    public boolean validateShipmentId(String shipmentId) {
        return shipmentValidator.validate(shipmentId);
    }

    public boolean validateCustomerNumber(String customerNumber) {
        return customerNumberValidator.validate(customerNumber);
    }

    public boolean validateReference(String reference) {
        return referenceValidator.validate(reference);
    }

    public boolean validatePhoneNumber(String phone) {
        return phoneValidator.validate(phone);
    }

    public boolean validateNotificationCode(String notificationCode) {
        return notificationCodeValidator.validate(notificationCode);
    }

    private static Validator<String> getReferenceValidator() {
        List<Validator<String>> referenceValidators = new ArrayList<Validator<String>>();
        referenceValidators.add(new NotNullOrEmptyValidator());
        referenceValidators.add(new LengthValidator(1, 35));
        referenceValidators.add(new NordicTextValidator());
        return new CompositeValidator(referenceValidators);
    }

    private static Validator<String> getShipmentValidator() {
        List<Validator<String>> shipmentValidators = new ArrayList<Validator<String>>();
        shipmentValidators.add(new NotNullOrEmptyValidator());
        shipmentValidators.add(new LengthValidator(9, 35));
        shipmentValidators.add(new AsciiCharactersAndDigitsValidator());
        return new CompositeValidator(shipmentValidators);
    }

    private static Validator<String> getItemValidator() {
        List<Validator<String>> itemValidators = new ArrayList<Validator<String>>();
        itemValidators.add(new NotNullOrEmptyValidator());
        itemValidators.add(new LengthValidator(9, 35));
        itemValidators.add(new AsciiCharactersAndDigitsValidator());
        itemValidators.add(new ItemIdKnownTypeValidator());
        itemValidators.add(new ItemIdChecksumValidator());
        return new CompositeValidator(itemValidators);
    }

    private static Validator<String> getCustomerNumberValidator() {
        List<Validator<String>> validators = new ArrayList<Validator<String>>();
        validators.add(new NotNullOrEmptyValidator());
        validators.add(new LengthValidator(6, 10));
        validators.add(new DigitsValidator());
        return new CompositeValidator(validators);
    }

    private static Validator<String> getPhoneValidator() {
        List<Validator<String>> validators = new ArrayList<Validator<String>>();
        validators.add(new NotNullOrEmptyValidator());
        validators.add(new LengthValidator(8, 20));
        validators.add(new PhoneValidator());
        return new CompositeValidator(validators);
    }

    private static Validator<String> getNotificationCodeValidator() {
        List<Validator<String>> validators = new ArrayList<Validator<String>>();
        validators.add(new NotNullOrEmptyValidator());
        validators.add(new LengthValidator(4, 4));
        validators.add(new DigitsValidator());
        return new CompositeValidator(validators);
    }

}
