package se.posten.loab.lisp.common.email.impl;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import se.posten.loab.lisp.common.email.EmailSender;
import se.posten.loab.lisp.common.email.SendEmailException;
import se.posten.loab.lisp.common.jmsutil.sender.MessageSender;
import se.posten.loab.lisp.common.jmsutil.sender.MessageSenderImpl;
import se.posten.loab.lisp.mail.xml.MailMessage;
import se.posten.loab.lisp.mail.xml.MailMessageParser;

/**
 * <p>
 * An implementation of EmailSender that uses JMS to send emails to the LISP NOS mail-service.
 * </p>
 * <p>
 * See package se.posten.loab.lisp.nos.mailservice. <//p>
 * 
 * @author stca708
 */
public class JmsNosEmailSender implements EmailSender {

    private final Log logger = LogFactory.getLog(getClass());
    private static final String ENCODING_UTF_8 = "UTF-8";
    private final ConnectionFactory connectionFactory;
    private final Destination destination;

    public JmsNosEmailSender(ConnectionFactory connectionFactory, Destination destination) {
        this.connectionFactory = connectionFactory;
        this.destination = destination;
    }

    @Override
    public void sendEmail(MailMessage mailMessage) throws SendEmailException {
        MailMessageParser parser = new MailMessageParser();
        parser.setSchemaValidation(false);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Writer writer = new OutputStreamWriter(byteArrayOutputStream, Charset.forName(ENCODING_UTF_8));
        parser.format(mailMessage, writer, true);
        Connection connection = null;
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            MessageSender sender = new MessageSenderImpl(connection);
            sender.sendBytesMessage(destination, byteArrayOutputStream.toByteArray(), null, null);
        } catch (Throwable t) {
            throw new SendEmailException("Could not put email on queue. Msg: " + t.getMessage(), t);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException e) {
                    logger.error("Could not close connection after sending email to queue. Msg:" + e.getMessage(), e);
                }
            }
        }
    }
}
