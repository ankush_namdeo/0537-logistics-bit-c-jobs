package se.posten.loab.lisp.common.util;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Queue;

public class BatchCollectionDataProvider<T> implements DataProvider<List<T>> {
    Queue<T> queue;
    private final int numElementsPerBatch;

    public BatchCollectionDataProvider(Collection<T> elements, int numElementsPerBatch) {
        this.queue = new ArrayDeque<T>(elements);
        this.numElementsPerBatch = numElementsPerBatch;
    }

    @Override
    public List<T> getNext() {
        List<T> data = new ArrayList<T>();
        for (int i = 0; i < numElementsPerBatch && !queue.isEmpty(); i++) {
            data.add(queue.poll());
        }
        return data;
    }

    @Override
    public boolean hasNext() {
        return !queue.isEmpty();
    }

}
