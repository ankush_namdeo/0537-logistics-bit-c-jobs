package se.posten.loab.lisp.common.ftp;

import java.io.File;
import java.io.IOException;

import org.apache.commons.vfs.FileObject;
import org.apache.commons.vfs.FileSystemException;
import org.apache.commons.vfs.FileSystemOptions;
import org.apache.commons.vfs.Selectors;
import org.apache.commons.vfs.impl.StandardFileSystemManager;
import org.apache.commons.vfs.provider.sftp.SftpFileSystemConfigBuilder;
import org.apache.log4j.Logger;

import se.posten.loab.lisp.linda.export.ftp.FTPException;

public class SFTPUploader {

	private static final Logger log = Logger.getLogger(SFTPUploader.class);
	
	private final StandardFileSystemManager sftpManager;
	private final FileSystemOptions fsOpts;
	
	private FileObject remoteFOFolder; 
	
    /**
     * Creates a new SFTPUploader and directly tries to connect to the SFTP
     * server. Will throw {@link FTPException} if connection fails
     * 
     * @param host
     * @param port
     * @param user
     * @param password
     * @param remoteDir
     * @throws IOException
     */
    public SFTPUploader(String host, int port, String user, String password, String remoteDir) throws FTPException {
    	
    	try {
			fsOpts = new FileSystemOptions();
			SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(fsOpts, "no");
			
			sftpManager = new StandardFileSystemManager();
			sftpManager.init();
			
			final String connectString = new StringBuilder()
				.append("sftp://")
				.append(user)
				.append(":")
				.append(password)
				.append("@")
				.append(host)
				.append(":")
				.append(port)
				.append(remoteDir)
				.toString();
			
			// Connect
			log.debug("Attempting SFTP connection using connectString " + connectString);
			remoteFOFolder = sftpManager.resolveFile(connectString, fsOpts);
    	}
    	catch (FileSystemException e) {
    		log.error("Failed SFTP connection", e);
    		throw FTPException.couldNotCommunicateWithFTPServer(host, port, e);
    	}
    }

    /**
     * Uploads the given file. If the file is a directory, all of its contents is also uploaded.
     * @param file
     * @throws IOException
     */
    public void upload(File file) throws FTPException {
    	FileObject newRemoteFile = null;
    	try {
    		if (log.isDebugEnabled())
    			log.debug("SFTP uploading file " + file.getAbsolutePath());
	    	
	    	newRemoteFile = sftpManager.resolveFile(remoteFOFolder, file.getName());

	    	FileObject localFile = sftpManager.resolveFile(file.getAbsolutePath());
	    	newRemoteFile.copyFrom(localFile, Selectors.SELECT_SELF_AND_CHILDREN);
    		if (log.isDebugEnabled())
    			log.debug("Success SFTP uploading file " + file.getAbsolutePath() + " to " + newRemoteFile.getName().getPath());

    	}
    	catch (FileSystemException e) {
    		log.error("Failed SFTP upload of file " + file.getAbsolutePath(), e);
    		throw FTPException.couldNotUploadFile(e);
    	}
    	finally {
    		if (newRemoteFile != null) {
    			try {
    				newRemoteFile.close();
    			}
    			catch (IOException e) {
    				log.error("Failed closing remote file " + newRemoteFile.getName().getFriendlyURI(), e);
    			}
    		}
    	}
    }
    
    public void close() {
    	sftpManager.close();
    }
}
