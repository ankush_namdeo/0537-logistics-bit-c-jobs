package se.posten.loab.lisp.common.validation.hibernate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class WildcardValidatorImpl implements ConstraintValidator<WildcardValidator, String> {

    WildcardValidator annotation;

    public void initialize(WildcardValidator wildcardValidator) {
        this.annotation = wildcardValidator;
    }

	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
            return true;
        }

        if (value.indexOf(annotation.wildcardChar()) >= 0) {
            String stripped = value.replace(Character.toString(annotation.wildcardChar()), "");
            return stripped.length() >= annotation.minAllowed();
        }
        return true;
	}
}
