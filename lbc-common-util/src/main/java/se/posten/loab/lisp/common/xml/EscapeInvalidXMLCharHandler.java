package se.posten.loab.lisp.common.xml;

import java.io.IOException;
import java.io.Writer;
import java.util.HashSet;

import com.sun.xml.bind.marshaller.CharacterEscapeHandler;

/**
 * Escapes XML invalid characters
 *
 * @author Christer Wikman, DevCode
 *
 */
public class EscapeInvalidXMLCharHandler implements CharacterEscapeHandler {

    public static final char substitute = '\uFFFD';
    private static final HashSet<Character> illegalChars;

    static {
        final String escapeString = "\u0000\u0001\u0002\u0003\u0004\u0005"
                + "\u0006\u0007\u0008\u000B\u000C\u000E\u000F\u0010\u0011\u0012"
                + "\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001A\u001B\u001C" + "\u001D\u001E\u001F\uFFFE\uFFFF";

        illegalChars = new HashSet<Character>();
        for (int i = 0; i < escapeString.length(); i++) {
            illegalChars.add(escapeString.charAt(i));
        }
    }

    private boolean isIllegal(char c) {
        return illegalChars.contains(c);
    }

    public void escape(char[] ch, int start, int length, boolean isAttVal, Writer out) throws IOException {
        int limit = start + length;
        for (int i = start; i < limit; i++) {
            char c = ch[i];
            if (isIllegal(c)) {
                if (i != start)
                    out.write(ch, start, i - start);
                start = i + 1;
                out.write(substitute);
                continue;
            }
            if (c == '&' || c == '<' || c == '>' || (c == '\"' && isAttVal)) {
                if (i != start)
                    out.write(ch, start, i - start);
                start = i + 1;
                switch (ch[i]) {
                case '&':
                    out.write("&amp;");
                    break;
                case '<':
                    out.write("&lt;");
                    break;
                case '>':
                    out.write("&gt;");
                    break;
                case '\"':
                    out.write("&quot;");
                    break;
                }
            }
        }
        if (start != limit)
            out.write(ch, start, limit - start);
    }
}