package se.posten.loab.lisp.common.validation.hibernate;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * The no wildcard validator can be used in GUI input components to assure that
 * no wildcard characters are specified by the user for fields that does not
 * support wildcards
 * <p>
 * Message bundle example: <br>
 * <code>
 * # Validation errors
 * lisp.validation.nowildcard = Jokertecken ({wildcardChar}) \u00E4r inte till\u00E5tna
 * </code>
 * 
 * @author ansk719 (Andreas Skoog)
 * 
 */
@Documented
@Target(value = { ElementType.METHOD, ElementType.FIELD })
@Retention(value = RetentionPolicy.RUNTIME)
@Constraint(validatedBy = NoWildcardValidatorImpl.class)
public @interface NoWildcardValidator {

    public Class<?>[] groups() default {};
    
    public Class<? extends Payload>[] payload() default {};
    
    /**
     * The character to search for. Validation fails if this character is
     * present in the text to validate
     */
    public char wildcardChar() default '%';

    /** The internationalized error message to display upon validation errors */
    public String message() default "{lisp.validation.nowildcard}";

}
