package se.posten.loab.lisp.common.config.impl;

import java.net.URL;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationConverter;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.ConversionException;
import org.apache.commons.configuration.DefaultConfigurationBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.joda.time.DateTime;

import se.posten.loab.lisp.common.config.CachedConfigurationManagerMBean;
import se.posten.loab.lisp.common.config.ConfigurationParameterNotFoundException;
import se.posten.loab.lisp.common.config.InvalidConfigurationParameterValueException;

/**
 * <p>
 * An implementation of ConfigurationManager.<br/>
 * </p>
 * <p>
 * This class can be used stand-alone as a POJO, but it can also, for example, be extended and used as a MBean in JBoss.
 * </p>
 * <p>
 * Example:
 * 
 * <pre>
 * &#064;Service(name = &quot;configurationManager&quot;)
 * &#064;Management(CachedConfigurationManagerMBean.class)
 * public class MyConfigurationManagerBean extends CachedApacheCommonConfigurationManager {
 * 
 *     private static final long CONFIGURATION_MANAGER_CACHE_TIME_TO_LIVE_MS = 24 * 60 * 60 * 1000;
 * 
 *     &#064;Override
 *     protected String getConfigFileName() {
 *         return &quot;/my-configuration.xml&quot;;
 *     }
 * 
 *     &#064;Override
 *     protected long getCacheTtl() {
 *         return CONFIGURATION_MANAGER_CACHE_TIME_TO_LIVE_MS;
 *     }
 * }
 * </pre>
 * 
 * Example of Apache configuration file:
 * 
 * <pre>
 * &lt;?xml version="1.0" encoding="ISO-8859-1" ?>
 * 
 * &lt;configuration>
 *     &lt;!-- After deploy time property overrides and additions -->
 *     &lt;properties fileName="myconfig.properties" optional="true"/>
 *     
 *     &lt;!-- Default values located within deployment -->
 *     &lt;properties fileName="myconfig-default.properties" optional="false"/>
 * &lt;/configuration>
 * </pre>
 * 
 * For full example usage, see <br/>
 * se.posten.loab.lisp.booking.service.core.serviceimpl.ConfigurationManager or<br/>
 * se.posten.loab.lisp.productionpointservice.core.serviceimpl.ConfigurationService
 * </p>
 * <p>
 * This implementation is thread safe.
 * </p>
 * 
 * @author stca708
 */
public class CachedApacheCommonConfigurationManager implements CachedConfigurationManagerMBean {
    /**
     * The cached configuration parameters will never expire.
     */
    public final static long CACHE_TTL_FOREVER = -1;

    private final Log logger = LogFactory.getLog(getClass());
    private final AtomicReference<Configuration> cachedConfiguration = new AtomicReference<Configuration>();
    private final AtomicLong lastRefresh = new AtomicLong();
    private String configFileName = null;
    private long cacheTtl = CACHE_TTL_FOREVER;

    /**
     * No arg constructor. Use together with {@link #init(String)} or {@link #init(String, long)}
     */
    public CachedApacheCommonConfigurationManager() {
    }

    /**
     * Constructor.
     * 
     * @param configFileName
     *            Name of Apache configuration file in classpath. Must start with "/".
     */
    public CachedApacheCommonConfigurationManager(String configFileName) {
        init(configFileName);
    }

    /**
     * Constructor.
     * 
     * @param configFileName
     *            Name of Apache configuration file in classpath. Must start with "/".
     * @param cacheTtl
     *            Number of milliseconds the cached configuration parameters will live in cache before being reloaded.
     */
    public CachedApacheCommonConfigurationManager(String configFileName, long cacheTtl) {
        init(configFileName, cacheTtl);
    }

    /**
     * Returns the name of the Apache configuration file.
     * 
     * This method is possible to override by subclasses.
     * 
     * @return The name of the Apache configuration file.
     */
    protected String getConfigFileName() {
        return configFileName;
    }

    /**
     * Returns the number of milliseconds the cached configuration parameters will live in cache before being reloaded.
     * 
     * This method is possible to override by subclasses.
     * 
     * @return The number of milliseconds the cached configuration parameters will live in cache before being reloaded.
     */
    protected long getCacheTtl() {
        return cacheTtl;
    }

    /**
     * Initializes the ConfigurationManager with an Apache configuration file. <br/>
     * <br/>
     * <b>This method is not thread-safe, and must be called only once</b>
     * 
     * @param configFileName
     *            Name of Apache configuration file in classpath. Must start with "/".
     */
    public void init(String configFileName) {
        if (this.configFileName != null) {
            throw new IllegalStateException("Already initialized.");
        }
        init(configFileName, -1);
    }

    /**
     * Initializes the ConfigurationManager with Apache configuration file and cache time-to-live. <br/>
     * <br/>
     * <b>This method is not thread-safe, and must be called only once</b>
     * 
     * @param configFileName
     *            Name of Apache configuration file in classpath. Must start with "/".
     * @param cacheTtl
     *            Number of milliseconds the cached configuration parameters will live in cache before being reloaded.
     */
    public void init(String configFileName, long cacheTtl) {
        if (this.configFileName != null) {
            throw new IllegalStateException("Already initialized.");
        }
        this.configFileName = configFileName;
        this.cacheTtl = cacheTtl;
    }

    /**
     * Returns all configuration parameters as an Apache Configuration object.
     * 
     * @return All configuration parameters as an Apache Configuration object.
     */
    public Configuration getConfiguration() {
        return getCachedConfiguration();
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.posten.loab.lisp.common.config.CachedConfigurationManagerMBean#refresh()
     */
    public void refresh() {
        Configuration newConfiguration = loadConfiguration();
        lastRefresh.set(System.currentTimeMillis());
        cachedConfiguration.set(newConfiguration);
    }

    public String getString(String key) {
        return getCachedConfiguration().getString(key);
    }

    public int getInt(String key) {
        int value = 0;
        try {
            value = getCachedConfiguration().getInt(key);
        } catch (RuntimeException e) {
            handleException(e);
        }
        return value;
    }

    private void handleException(RuntimeException e) {
        if (e instanceof NoSuchElementException) {
            throw new ConfigurationParameterNotFoundException(e);
        } else if (e instanceof ConversionException) {
            throw new InvalidConfigurationParameterValueException(e);
        }
        // If unknown, just rethrow.
        throw e;
    }

    public boolean getBoolean(String key) {
        boolean value = false;
        try {
            value = getCachedConfiguration().getBoolean(key);
        } catch (RuntimeException e) {
            handleException(e);
        }
        return value;
    }

    @SuppressWarnings("unchecked")
    public List<String> getList(String key) {
        return getCachedConfiguration().getList(key);
    }

    public Properties getAllProperties() {
        return ConfigurationConverter.getProperties(getConfiguration());
    }

    private void logAllPropertiesOnDebugLevel(Configuration config) {
        Properties props = ConfigurationConverter.getProperties(config);
        Set<String> keys = props.stringPropertyNames();
        for (String key : keys) {
            logger.debug("Property: " + key + " = " + props.getProperty(key));
        }
    }

    private Configuration getCachedConfiguration() {
        Configuration config = cachedConfiguration.get();
        if (config == null
                || (getCacheTtl() != CACHE_TTL_FOREVER && System.currentTimeMillis() > (lastRefresh.get() + getCacheTtl()))) {
            refresh();
            config = cachedConfiguration.get();
        }
        return config;
    }

    private Configuration loadConfiguration() {
        String configFileName = getConfigFileName();
        if (configFileName == null) {
            throw new IllegalStateException(
                    "CachedApacheCommonConfigurationManager has not been properly initialized (configFileName = null).");
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Loading configuration based on: " + configFileName);
        }
        Configuration config = null;
        try {
            URL configURL = getClass().getResource(configFileName);
            DefaultConfigurationBuilder builder = new DefaultConfigurationBuilder(configURL);
            // Ignore any configuration errors
            builder.clearErrorListeners();
            config = builder.getConfiguration();
            if (config == null) {
                throw new RuntimeException(
                        "Could not get configuration. Returned null configuration (configFileName = " + configFileName
                                + ").");
            }
            if (logger.isDebugEnabled()) {
                logAllPropertiesOnDebugLevel(config);
            }
        } catch (ConfigurationException e) {
            throw new RuntimeException("Configuration exception (configFileName = " + configFileName + ").", e);
        }
        return config;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.posten.loab.lisp.common.config.ConfigurationManagerMBean#getInfoAboutConfigurationManager()
     */
    public Properties getInfoAboutConfigurationManager() {
        Properties props = new Properties();
        props.setProperty("className", getClass().getName());
        props.setProperty("configFile", (getConfigFileName() != null ? getConfigFileName() : "null"));
        props.setProperty("cacheTtl", Long.toString(getCacheTtl()) + "ms");
        DateTime refreshDateTime = new DateTime(lastRefresh.get());
        props.setProperty("lastRefresh", refreshDateTime.toString());
        props.setProperty("numberOfConfigurationParameters", Integer.toString(getAllProperties().size()));
        return props;
    }

    /*
     * (non-Javadoc)
     * 
     * @see se.posten.loab.lisp.common.config.ConfigurationManagerMBean#start()
     */
    @Override
    public void start() throws Exception {
        // Do nothing.
    }

}
